<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<tiles:insertDefinition name="main">
	<tiles:putAttribute name="title">SIADE :: Sistema Integrado de Administracion de Edificios</tiles:putAttribute>

	<tiles:putAttribute name="css">
		<!-- Bootstrap 4.0-->
		<link rel="stylesheet"
			href="${pageContext.request.contextPath}ST/css/bootstrap-4.0.0/bootstrap.min.css">

		<!-- Bootstrap extend-->
		<link rel="stylesheet"
			href="${pageContext.request.contextPath}ST/css/loginSiade/bootstrap-extend.css">

		<!-- Theme style -->
		<link rel="stylesheet"
			href="${pageContext.request.contextPath}ST/css/loginSiade/master_style.css">

		<!-- Minimalelite Admin skins -->
		<link rel="stylesheet"
			href="${pageContext.request.contextPath}ST/css/loginSiade/skins/_all-skins.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</tiles:putAttribute>

	<tiles:putAttribute name="body">
		<body class="hold-transition login-page">
			<div class="login-box">
				<div class="login-logo">
					<a href="../../index.html"><b>SIADE </b>Admin</a>
				</div>
				<!-- /.login-logo -->
				<div class="login-box-body">
					<p class="login-box-msg">Logueate para iniciar tu Sesion</p>

					<form action="<c:url value="validarLoginV2"/>" method="post" class="form-element">
						<div class="form-group has-feedback">
							<input name="autenticacion.email" id="email" type="email" class="form-control" placeholder="Email">
							<span class="ion ion-email form-control-feedback"></span>
						</div>
						<div class="form-group has-feedback">
							<input name="autenticacion.clave" id="clave" type="password" class="form-control"
								placeholder="Password"> <span
								class="ion ion-locked form-control-feedback"></span>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="checkbox">
									<input type="checkbox" id="basic_checkbox_1"> <label
										for="basic_checkbox_1">Recordarme</label>
								</div>
							</div>
							<!-- /.col -->
							<div class="col-6">
								<div class="fog-pwd">
									<a href="javascript:void(0)"><i class="ion ion-locked"></i>
										Recuperar clave?</a><br>
								</div>
							</div>
							<!-- /.col -->
							<div class="col-12 text-center">
								<button type="submit" id="btnLogin"
									class="btn btn-info btn-block margin-top-10">INGRESAR</button>
							</div>
							<!-- /.col -->
						</div>
					</form>

					<div class="social-auth-links text-center">
						<p>- O -</p>
						<a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i
							class="fa fa-facebook"></i></a> <a href="#"
							class="btn btn-social-icon btn-circle btn-google"><i
							class="fa fa-google-plus"></i></a>
					</div>
					<!-- /.social-auth-links -->

					<div class="margin-top-30 text-center">
						<p>
							No tienes cuenta? <a href="<c:url value="crear-usuario"/>" class="text-info m-l-5">Registrate</a>
						</p>
					</div>

				</div>
				<!-- /.login-box-body -->
			</div>
			<!-- /.login-box -->
		</body>
	</tiles:putAttribute>

	<tiles:putAttribute name="script">
		<!-- jQuery 3 -->
		<script src="${pageContext.request.contextPath}ST/js/jquery-3.2.1/jquery.min.js"></script>

		<!-- popper -->
		<script src="${pageContext.request.contextPath}ST/js/popper-1.12.0/popper.min.js"></script>

		<!-- Bootstrap 4.0-->
		<script src="${pageContext.request.contextPath}ST/js/bootstrap-4.0.0/bootstrap.min.js"></script>
	</tiles:putAttribute>

	<tiles:putAttribute name="js">
		<script type="text/javascript">
			$(document).ready(function() {
				$("#bodyLogin").addClass('hold-transition');
				$("#bodyLogin").addClass('login-page');
					
				
				$("#submit1").hover(function() {
					$(this).animate({
						"opacity" : "1"
					}, "slow");
				});

				function validarEmail(focus) {
					$("#registro").val(
							$.trim($("#registro").val()));
					$("#registro").parent().removeClass(
							'errorTooltip');
					$("#registro").parent().tooltip('destroy');
					$("#registro").prev().prev().removeClass(
							'form-control-error');
					if ($("#registro").val() == '') {
						$("#registro").parent().addClass(
								'errorTooltip');
						$("#registro").parent().tooltip({
							trigger : 'hover',
							title : 'Ingresa un Email'
						});
						$("#registro").prev().prev().addClass(
								'form-control-error');
						if (focus) {
							$("#registro").focus();
						}
						return 0;
					} else {
						return 1;
					}
				}

				function validarClave(focus) {
					$("#clave").val($.trim($("#clave").val()));
					$("#clave").parent().removeClass(
							'errorTooltip');
					$("#clave").parent().tooltip('destroy');
					$("#clave").prev().prev().removeClass(
							'form-control-error');
					if ($("#clave").val() == '') {
						$("#clave").parent().addClass(
								'errorTooltip');
						$("#clave").parent().tooltip({
							trigger : 'hover',
							title : 'Ingresa una clave'
						});
						$("#clave").prev().prev().addClass(
								'form-control-error');
						if (focus) {
							$("#clave").focus();
						}
						return 0;
					} else {
						return 1;
					}
				}								
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>