<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Formulario de Suscripción Programada de Fondos Mutuos</tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/calculadora-mio-game.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">

</tiles:putAttribute>

<tiles:putAttribute name="conversioncodefacebok">
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	
	fbq('init', '254908304633078');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=254908304633078&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</tiles:putAttribute>

<tiles:putAttribute name="body">

<div style="background: url(${pageContext.request.contextPath}ST/img/calculadora-mio/fondo_cabecera.png) no-repeat;">
	<div class="row">
		<div class="col-sm-3 col-xs-5" >
			<img src="${pageContext.request.contextPath}ST/img/calculadora-mio/logo_mioNew.png" 
				class="img-responsive">
			</div>
			<div class="col-sm-9 col-xs-7" style="margin-top: 20px;padding-left:0px;">
				<span class="titulo-calculadora-mio-game">
					Conquista de forma programada eso que tanto quieres
				</span>
			</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="init-step1" id="barraProgreso" >
				<form novalidate="novalidate" class="form-horizontal tooltip-validation transfer-filter">
					<div class="wizard">
						<div data-title="Cálculo" data-multistep="true">
						</div>
						<div data-title="Solicitud" data-multistep="true">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>	
	
	<form id="formSuscripcionProgramada" name="formSuscripcionProgramada" action="<c:url value="suscripcion-correo"/>" method="post">
		<input type="hidden" name="tipo" id="tipo" value="0" />
		
		<input type="hidden" name="indClienteBBVACash" id="indClienteBBVACash" value="${clienteBbvaCash.idClienteBbvaCash}" />
		<input type="hidden" name="indAfiliacionBXI" id="indAfiliacionBXI" value="${clienteBbvaCash.indAfiliacionBXI}" />
		<input type="hidden" name="objetivo" id="objetivo" value="${calculadora.objetivo}" />
		<input type="hidden" name="tipoCalculo" id="tipoCalculo" value="${calculadora.tipo}" />
		<input type="hidden" name="divisa" id="divisa" value="${calculadora.divisa}" />
		<input type="hidden" name="monto" id="monto" value="${calculadora.monto}" />
		<input type="hidden" name="meses" id="meses" value="${calculadora.meses}" />
		<c:forEach var="listSuscripcionDetalles" items="${calculadora.listSuscripcionDetalles}">
			<input type="hidden" name="montoCuota" id="montoCuota" value="${listSuscripcionDetalles.montoCuota}" />
			<input type="hidden" name="periodicidad" id="periodicidad" value="${listSuscripcionDetalles.periodo}" />
			<input type="hidden" name="montoFuturo" id="montoFuturo" value="${listSuscripcionDetalles.montoFuturo}" />
		</c:forEach>

		
	<!-- Seccion de Cliente -->
		<div id="customer_section">
			<div class="row">
				<div class="col-sm-8"></div>
				<div id="divImgEscalera" class="col-sm-4 hidden-xs" style="position: absolute;right: 0;">
					<img src="${pageContext.request.contextPath}ST/img/calculadora-mio/hombre_escalera_frmcliente.png" class="img-responsive imagen_escalera_resize">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 titulo_principal txtRp_center" >
					Ahora que ya sabes como conseguirlo, puedes abrir un Fondo Mutuo con nosotros.<br>Déjanos tus datos para ayudarte a hacerlo:
				</div>
			</div>
			<div class="row">
				<div class="col-sm-8 margin_top_10">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="errorTooltip form-control txt-global" id="nombre" name="nombre" placeholder="Nombres" autocomplete="off" maxlength="50">
								<span class="help-block error-bbva"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="errorTooltip form-control txt-global" id="apellido" name="apellido" placeholder="Apellidos" autocomplete="off" maxlength="50">
								<span class="help-block error-bbva"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group select combo-responsive">
								<select class="errorTooltip form-control form-select combo-responsive" id="tipoDocumento" name="tipoDocumento" autocomplete="off">
									<c:forEach var="tipoDocumento" items="${tipoDocumentos}">
										<c:if test="${tipoDocumento.codigo==clienteBbvaCash.tipoDocumento}">
											<option value="${tipoDocumento.codigo}" selected>${tipoDocumento.nombre}</option>
										</c:if>
										<c:if test="${tipoDocumento.codigo!=clienteBbvaCash.tipoDocumento}">
											<option value="${tipoDocumento.codigo}">${tipoDocumento.nombre}</option>
										</c:if>
									</c:forEach> 
								</select>
								<span class="help-block error-bbva"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="errorTooltip form-control txt-global" id="numeroDocumento" name="numeroDocumento" placeholder="Número de Documento" autocomplete="off" maxlength="8" value="${clienteBbvaCash.numeroDocumento}">
								<span class="help-block error-bbva"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="errorTooltip form-control txt-global" id="correo" name="correo" placeholder="Correo Electrónico" autocomplete="off" maxlength="50">
								<span class="help-block error-bbva"></span>
							</div>
						</div>					
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="errorTooltip form-control txt-global" id="telefono" name="telefono" placeholder="Teléfono 1" autocomplete="off" maxlength="9">
								<span class="help-block error-bbva"></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" class="errorTooltip form-control txt-global" id="telefono2" name="telefono2" placeholder="Teléfono 2 (Opcional)" autocomplete="off" maxlength="9">
								<span class="help-block error-bbva"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group select combo-responsive" id="select_horario">
								<select class="errorTooltip form-control form-select" id="horario" name="horario">
								<option value="">Horario para Llamarte</option>
								<c:forEach var="horario" items="${horarios}">
									<option value="${horario.codigo}">${horario.nombre}</option>
								</c:forEach>
								</select>
								<span class="help-block error-bbva"></span>
							</div>
						</div>
					</div>							
				</div>
				<div class="col-sm-4 hidden-xs">
											
				</div>
			</div>
		</div>
	<!-- Seccion de aceptacion de terminos y envío del formulario -->
		<div id="common_section">
			<div class="row">
				<div class="col-sm-12 padding_left_form ">
					<div>
						<div class="checkbox" style="margin-bottom: 0px">
							<label style="font-size: 1.2em; padding-left: 0px;">
				            	<input type="checkbox" id="autorizacion_correo" name="autorizacion_correo" value="1">
				            	<span class="cr_ok_small"><i class="cr_ok_small-icon glyphicon glyphicon-ok-verde"></i></span>
				            	<span id="label_correo" class="listado_servicios" data-toggle="tooltip" data-placement="top" title="">Enviar mis resultados de MIO al correo indicado</span>
				          	</label>
				        </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 padding_left_form">
					<div class="form-group">
						<div class="checkbox" style="margin-top: 0px;margin-bottom: 0px">
							<label style="font-size: 1.2em; padding-left: 0px;">
				            	<input type="checkbox" id="autorizacion" name="autorizacion" value="1">
				            	<span class="cr_ok_small"><i class="cr_ok_small-icon glyphicon glyphicon-ok-verde"></i></span>
				            	<a href="#" class="" data-toggle="modal" data-target="#myModal"><span class="listado_servicios">He leído y autorizo el tratamiento de datos personales</span></a>
				          	</label>
				        </div>
					</div>
				</div>
			</div>
			<div class="row" style="display: none">
				<div class="col-sm-12">
					<div class="form-group">
						<label>
							<input type="text" class="errorTooltip form-control" id="opcional" name="opcional" placeholder="Opcional" autocomplete="off" maxlength="40">
						</label>
					</div>
				</div>
			</div>
			<div class="text_center">	
					<div class="text_center">
						<!-- <form action="<c:url value="mensaje-no-cliente"/>" method="post">  -->
							<button id="btnEnviarSuscripcion" name="btnEnviarSuscripcion" type="button" class="btn btn-verde">Enviar</button>
						<!-- </form>  -->
					</div>
				</div>
		</div>
		<div class="disclaimer-calculadora display_none" id="tabla_disclaimers">
			<div style="padding-bottom: 5px;">
				<span class="disclaimer-azul-calculadora">*El resultado de los cálculos es referencial.</span> La tasa de rentabilidad anual asumida para el cálculo, es la tasa equivalente al promedio de 
				las rentabilidades anuales de los últimos 5 años. En Soles, considera el promedio de las rentabilidades anuales del BBVA Cash Soles y en dólares el promedio de las rentabilidades anuales del 
				BBVA Cash Dólares, para el periodo seleccionado. La tasa de rentabilidad estimada no debe ser interpretada ni entendida como una promesa de rentabilidad constante en el tiempo.
			</div>
			<div style="padding-bottom: 5px;">
				Los fondos mutuos son administrados por BBVA Asset Management Continental SA SAF y comercializados por BBVA Continental.
				MIO Suscripción programada esta disponible para la suscripción de los fondos BBVA Cash Soles y BBVA Cash Dólares.
			</div>
			<div style="padding-bottom: 5px;">
				Consulta el detalle de rentabilidades anuales históricas de los fondos BBVA Cash Soles y BBVA Cash Dólares en nuestro sitio web y en las oficinas del BBVA Continental.
			</div>
			<div style="padding-bottom: 5px;">
				<span class="disclaimer-azul-calculadora">La rentabilidad o ganancia obtenida en el pasado</span> no garantiza que se repita en el futuro. Esta rentabilidad no incluye el efecto 
				de las comisiones de suscripción y rescate, ni el impuesto a la renta.
			</div>
		</div>
	 </form>  
</div>

<!-- Seccion de tratamiendo de datos personales -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">AUTORIZACIÓN DE RECOPILACIÓN Y TRATAMIENTO DE DATOS</h4>
		</div>
		<div class="modal-body">
        &#8226; La información que Usted nos proporciona sobre su nombre, apellido, nacionalidad, estado civil, documento de identidad, ocupación, estudios, domicilio, correo electrónico, teléfono, estado de salud, actividades que realiza, ingresos económicos, patrimonio, gastos, entre otros, así como la referida a los rasgos físicos y/o de conducta que lo identifican o lo hacen identificable como es su huella dactilar, su voz, etc. (datos biométricos), conforme a Ley es considerada como Datos Personales. Recuerde que es posible que esta información también pueda ser obtenida a través de otras personas, sociedades y/o instituciones (públicas o privadas, nacionales o extranjeras).<br>
		&#8226; Usted nos da su consentimiento libre, previo, expreso e informado para que sus Datos Personales sean tratados por el Banco, sus subsidiarias y empresas vinculadas, es decir, que puedan ser: recopilados, registrados,organizados, almacenados, conservados, elaborados, modificados, bloqueados, suprimidos, extraídos, consultados, utilizados, transferidos o procesados de cualquier otra forma prevista por Ley. Esta autorización es indefinida y se mantendrá inclusive después de terminada(s) la(s) operación(es) y/o el(los) Contrato(s) que Usted tenga o pueda tener con el Banco, sus subsidiarias y empresas vinculadas.<br>
		&#8226; Sus Datos Personales serán almacenados (guardados) en el Banco de Datos de Clientes del cual el Banco es titular o en cualquier otro que en el futuro podamos establecer. El Banco ha adoptado las medidas necesarias para mantener segura la información.<br>
		&#8226; Al dar esta autorización Usted permite que (i) evaluemos su comportamiento en el sistema bancario y su capacidad de pago, (ii) podamos decidir si se otorga el(los) producto(s) y/o servicio(s) que solicite, (iii) le ofrezcamos otros productos y/o servicios del Banco y/o de terceros vinculados o no (por ejemplo cuentas, préstamos, entre otros), lo que podremos hacer también a través de terceras personas (por ejemplo, en asociaciones o alianzas comerciales), (iv) le enviemos ofertas comerciales, publicidad e información en general de los productos y/o servicios del Banco y/o de terceros vinculados o no, (v) gestionemos el cobro de deudas, de ser el caso, (vi) usemos y/o transfiramos esta información (dentro o fuera del país) a terceros vinculados o no al Banco, nacionales o extranjeros, públicos o privados (por ejemplo: otros bancos, imprentas, empresas de mensajería, auditoría, entre otros).<br>
		&#8226; <b>IMPORTANTE:</b> Usted declara que se le ha informado que tiene derecho a no proporcionar la autorización para el tratamiento de sus Datos Personales y que si no la proporciona no podremos tratar sus Datos Personales en la forma explicada en la presente cláusula, lo que no impide su uso para la ejecución (desarrollo) y cumplimiento (ej: pago) del servicio de Suscripción Programada.<br>
		&#8226; Asimismo, Usted tiene los derechos de información, acceso rectificación, cancelación, oposición y tratamiento objetivo de Datos Personales. Para hacer uso de estos derechos deberá presentar una solicitud escrita en nuestras oficinas. Se podrán establecer otros canales para tramitar estas solicitudes, lo que será informado oportunamente por el Banco a través de su Página Web.<br>
		&#8226; <b>IMPORTANTE:</b> Si Usted no quiere autorizarnos o quiere revocar el tratamiento de sus Datos Personales, pídale al representante del Banco el formulario correspondiente al Servicio de Suscripción Programada, si la contratación no es presencial, Usted lo podrá descargar en la página web: <a href="https://www.bbvacontinental.pe/" target="_blank">www.bbvacontinental.pe</a><br/>
		</div>
	</div>
	</div>
</div>

</tiles:putAttribute>

<tiles:putAttribute name="script">

<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_calculadora.js"></script>
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_plugin.js"></script>

<script type="text/javascript">
		$(window).on("load", function() {
			history.pushState("regular", null, "suscripcion-programada-regular");
			llenarDataLayer(TAG_NO_CLIENTE,"");
		});
</script>

<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip();
    $(document).ready(function(){
    	$(document).ready(function(){
    		transicion_barra_progreso(2,500);
    	});
    	posicion = $("#formSuscripcionProgramada").offset().top + 10;
    	$("#divImgEscalera").css("top", posicion);
    	
    	$("#numeroDocumento").numeric({
    		maxPreDecimalPlaces : 8,
    		maxDigits           : 8,
    		allowMinus          : false,
    		allowThouSep        : false,
    		allowDecSep         : false
    	});
    	
    });
    
    $("input[type=radio][name=opcType]").change(function () {
    	var tipo=$("input[name=opcType]:checked").val();
    	$('#tipo').val(tipo);
    	$("#question_section").fadeOut(500,function(){
	    	$("#tabla_disclaimers").fadeIn("slow")
	    	$("#common_section").fadeIn("slow")
    	});
		if(tipo=='1'){			
			$("#btnEnviarSuscripcion").addClass('btn-verde');
			$("#btnEnviarSuscripcion").removeClass('btn-azul');
			$("#customer_section").fadeIn(2000,function(){
				_satellite.track("fondosMutuosEvento49");
	    	});
		}else{
			$("#btnEnviarSuscripcion").addClass('btn-azul');
			$("#btnEnviarSuscripcion").removeClass('btn-verde');
			$("#user_section").fadeIn(2000,function(){
				_satellite.track("fondosMutuosEvento49");
	    	});
		}
    });
    
    $("#btnEnviarSuscripcion").click(function( eventBtnEnviarSuscripcion ) {
		var enviar=1;
   		
   		if($('#tipo').val()=='0'){
    		enviar=enviar*nombre($('#nombre'));    		
    		enviar=enviar*apellido();
    		enviar=enviar*numeroDocumento();
			enviar=enviar*correo($("#correo"));
			enviar=enviar*telefono();
			enviar=enviar*telefono2();
			enviar=enviar*horario();
			//enviar=enviar*servicios();
   		}else{
   			enviar=enviar*nombre($('#nombre_user'));  
   			enviar=enviar*correo($("#correo_user"));
   		}
		if(enviar==0){
			event.preventDefault();
		}else{
			_satellite.track("SolicitudFondosMutuosEvent57");
			setTimeout(function() {document.formSuscripcionProgramada.submit();}, 650);
		}
    });
    
   	$("#nombre_user").alpha({disallow :'¿¡'});
   	$("#correo_user").blur(function(){correo($("#correo_user"));});
   	$("input[type=checkbox][name=servicios]").change(function () {
       	servicios();
       });
   	$("#nombre").blur(function(){nombre($('#nombre'));});
   	$("#nombre_user").blur(function(){nombre($('#nombre_user'));});
   	$("#apellido").blur(function(){apellido();});
   	$("#numeroDocumento").blur(function(){numeroDocumento();});
   	//$("#numeroDocumento").numeric({allowMinus:false});
   	$("#correo").blur(function(){correo($("#correo"));});
   	$("#telefono").numeric({allowMinus:false});
   	$("#telefono").blur(function(){telefono();});
   	$("#telefono2").numeric({allowMinus:false});
   	$("#telefono2").blur(function(){telefono2();});
   	$("#horario").blur(function(){horario();});
   	
   	$("#tipoDocumento").change(function(){
   		$("#numeroDocumento").val("");
   		$("#numeroDocumento").unbind();
   		//$("#numeroDocumento").blur(function(){numeroDocumento();});
   		if($("#tipoDocumento").val()=='L'){
   			$("#numeroDocumento").numeric({
   				maxPreDecimalPlaces : 8,
   				maxDigits           : 8,
   				allowMinus          : false,
   				allowThouSep        : false,
   				allowDecSep         : false
   			});
   		}else if($("#tipoDocumento").val()=='E' || $("#tipoDocumento").val()=='D'){
   			$("#numeroDocumento").alphanum({
   				allow: '/-',
   				maxLength: 12,
   				allowSpace: false,
   				disallow: '°¡¿´¨.'
   			});
   		}else if($("#tipoDocumento").val()=='P'){
   			$("#numeroDocumento").alphanum({
   				maxLength: 12,
   				allowSpace: false,
   				disallow: '°¡¿´¨.'
   			});
   		}
  	});
   	
   	$("#nombre").keypress(function (key) {
        
        if (
        		(key.charCode < 97 || key.charCode > 122) 
        		&& (key.charCode < 65 || key.charCode > 90)
                && (key.charCode != 241) //ñ
                 && (key.charCode != 209) //Ñ
                 && (key.charCode != 32) //espacio
                 && (key.charCode != 225) //á
                 && (key.charCode != 233) //é
                 && (key.charCode != 237) //í
                 && (key.charCode != 243) //ó
                 && (key.charCode != 250) //ú
                 && (key.charCode != 193) //Á
                 && (key.charCode != 201) //É
                 && (key.charCode != 205) //Í
                 && (key.charCode != 211) //Ó
                 && (key.charCode != 218) //Ú
        	) {
        	return false;
        }
    });
   	
   	
   	$("#apellido").keypress(function (key) {
        
        if (
        		(key.charCode < 97 || key.charCode > 122) 
        		&& (key.charCode < 65 || key.charCode > 90)
                && (key.charCode != 241) //ñ
                 && (key.charCode != 209) //Ñ
                 && (key.charCode != 32) //espacio
                 && (key.charCode != 225) //á
                 && (key.charCode != 233) //é
                 && (key.charCode != 237) //í
                 && (key.charCode != 243) //ó
                 && (key.charCode != 250) //ú
                 && (key.charCode != 193) //Á
                 && (key.charCode != 201) //É
                 && (key.charCode != 205) //Í
                 && (key.charCode != 211) //Ó
                 && (key.charCode != 218) //Ú
        	) {
        	return false;
        }
    });
   	
   	
   	function nombre(obj){
			obj.val($.trim(obj.val()));
  		if(obj.val()==''){
  			obj.tooltip({trigger : 'hover', title : 'Escribe tu nombre'});
  			obj.addClass('form-control-error');
				return 0;
			}else{
				obj.tooltip('destroy');
				obj.removeClass('form-control-error');
				return 1;
			}
  	}
	function apellido(){
		$("#apellido").val($.trim($("#apellido").val()));
		if($("#apellido").val()==''){
			$("#apellido").tooltip({trigger : 'hover', title : 'Escribe tu apellido'});
			$("#apellido").addClass('form-control-error');
			return 0;
		}else{
			$('#apellido').tooltip('destroy');
			$("#apellido").removeClass('form-control-error');
			return 1;
		}    			
	}
	function numeroDocumento(){
		if(
				$.trim($("#numeroDocumento").val())=='' || 
				($("#tipoDocumento").val()=='L' && $.trim($("#numeroDocumento").val()).length!=8) ||
				($("#tipoDocumento").val()=='E' && $.trim($("#numeroDocumento").val()).length>12) ||
				($("#tipoDocumento").val()=='D' && $.trim($("#numeroDocumento").val()).length>12) ||
				($("#tipoDocumento").val()=='P' && $.trim($("#numeroDocumento").val()).length>12)
			){
				$("#numeroDocumento").tooltip({trigger : 'hover', title : 'Escribe un número de documento válido'});
				$("#numeroDocumento").addClass('form-control-error');						
				return 0;
			}else{
				$('#numeroDocumento').tooltip('destroy');
				$("#numeroDocumento").removeClass('form-control-error');				
				return 1;
			}
	}
	function correo(obj){
 		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
 		if(obj.val()=='' || !regex.test(obj.val())){
			obj.tooltip({trigger : 'hover', title : 'Escribe un correo válido'});
			obj.addClass('form-control-error');
			return 0;
		}else{
			obj.tooltip('destroy');
			obj.removeClass('form-control-error');
			return 1;
		}
  	}
	function telefono(){
 		if($("#telefono").val().length<7 || $("#telefono").val().length>9){
			$("#telefono").tooltip({trigger : 'hover', title : 'Escribe un teléfono de contacto válido'});
			$("#telefono").addClass('form-control-error');
			return 0;
		}else{
			$('#telefono').tooltip('destroy');
			$("#telefono").removeClass('form-control-error');
			return 1;
		}
  	}
	function telefono2(){
		if($("#telefono2").val()==""){
			$('#telefono2').tooltip('destroy');
			$("#telefono2").removeClass('form-control-error');
			return 1;
		}
 		if($("#telefono2").val().length<7 || $("#telefono2").val().length>9){
			$("#telefono2").tooltip({trigger : 'hover', title : 'Escribe un teléfono de contacto válido'});
			$("#telefono2").addClass('form-control-error');
			return 0;
		}else{
			$('#telefono2').tooltip('destroy');
			$("#telefono2").removeClass('form-control-error');
			return 1;
		}
  	}
	function horario(){
		if($("#horario").val()==''){
			$("#horario").tooltip({trigger : 'hover', title : 'Selecciona un horario para llamarte'});
			$("#horario").addClass('form-combo-error');
			/*Borde de Select*/
			$("#select_horario").addClass('error-combo-borde');
			return 0;
		}else{
			$('#horario').tooltip('destroy');
			$("#horario").removeClass('form-combo-error');
			/*Borde de Select*/
			$("#select_horario").removeClass('error-combo-borde');
			return 1;
		}
  	}    
	function servicios(){
		var atLeastOneIsChecked = $('input[name="servicios"]:checked').length > 0;	
		if(atLeastOneIsChecked){
			$('#label_SUSCRIPCION').removeClass('colorError');
			$('#label_AFILIACION_EECC').removeClass('colorError');
			$("#titServicios").removeClass('colorError errorServicios');
			$('#titServicios').tooltip('destroy');
			return 1;
		}else{
			$('#label_SUSCRIPCION').addClass('colorError');
			$('#label_AFILIACION_EECC').addClass('colorError');
			$("#titServicios").addClass('colorError errorServicios');
			$("#titServicios").tooltip({trigger : 'hover', title : 'Selecciona los servicios con los que te gustaría contar'});
			return 0;
		}
	}  	
</script>

</tiles:putAttribute>
<tiles:putAttribute name="googletagrmkt">
<!-- Google Code para etiquetas de remarketing MCC -->
<!--------------------------------------------------
Es posible que las etiquetas de remarketing todavía no estén asociadas a la información de identificación personal o que estén en páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar la etiqueta, consulte http://google.com/ads/remarketingsetup.
--------------------------------------------------->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 977810892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/977810892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</tiles:putAttribute>
</tiles:insertDefinition>