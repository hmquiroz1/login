<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Calculadora MIO Game</tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/calculadora-mio-game.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">

<style>
</style>

</tiles:putAttribute>

<tiles:putAttribute name="conversioncodefacebok">
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	
	fbq('init', '254908304633078');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=254908304633078&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</tiles:putAttribute>
<tiles:putAttribute name="body">

<div class="row fondo-calculadora">
	<div class="col-sm-12">
		<div class="row">
			<div class="col-sm-3 col-xs-5" >
				<img src="${pageContext.request.contextPath}ST/img/calculadora-mio/logo_mioNew.png" class="img-responsive">
			</div>
			<div class="col-sm-9 col-xs-7" style="margin-top: 20px;padding-left:0px;">
				<span class="titulo-calculadora-mio-game">
					Conquista de forma programada eso que tanto quieres
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="init-step1" id="barraProgreso">
					<form novalidate="novalidate" class="form-horizontal tooltip-validation transfer-filter">
						<div class="wizard">
							<div data-title="Cálculo" data-multistep="true">
							</div>
							<div data-title="Solicitud" data-multistep="true">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-offset-9 col-sm-3 padding_titulo_principal">
				<div id="pdfCalculadora" class="padding_right_35 text_right hidden hidden-xs">
				<form action="<c:url value="calculadora-mio-pdf"/>" target="_blank">
					<button type="submit" class="btn btn-calculadora btn_pdf">Guardar en PDF</button>
				</form>			
				</div>
			</div>
		</div>
	</div>
</div>

<form id="formSuscripcionProgramada" name="formSuscripcionProgramada" action="<c:url value="suscripcion-correo"/>" method="post">	
	<input type="hidden" name="tipo" id="tipo" value="1"/>
	<input type="hidden" name="autorizacion_correo" id="autorizacion_correo" value="1"/>
	<input type="hidden" name="nombre" id="nombre" value="${clienteBbvaCash.nombres}" />
	<input type="hidden" name="tipoDocumento" id="tipoDocumento" value="${clienteBbvaCash.tipoDocumento}" />
	<input type="hidden" name="documento" id="documento" value="${clienteBbvaCash.numeroDocumento}" />
	
	<input type="hidden" name="indClienteBBVACash" id="indClienteBBVACash" value="${clienteBbvaCash.idClienteBbvaCash}" />
	<input type="hidden" name="indAfiliacionBXI" id="indAfiliacionBXI" value="${clienteBbvaCash.indAfiliacionBXI}" />
	<input type="hidden" name="objetivo" id="objetivo" value="${calculadora.objetivo}" />
	<input type="hidden" name="tipoCalculo" id="tipoCalculo" value="${calculadora.tipo}" />
	<input type="hidden" name="divisa" id="divisa" value="${calculadora.divisa}" />
	<input type="hidden" name="monto" id="monto" value="${calculadora.monto}" />
	<input type="hidden" name="meses" id="meses" value="${calculadora.meses}" />
	<c:forEach var="listSuscripcionDetalles" items="${calculadora.listSuscripcionDetalles}">
		<input type="hidden" name="montoCuota" id="montoCuota" value="${listSuscripcionDetalles.montoCuota}" />
		<input type="hidden" name="periodicidad" id="periodicidad" value="${listSuscripcionDetalles.periodo}" />
		<input type="hidden" name="montoFuturo" id="montoFuturo" value="${listSuscripcionDetalles.montoFuturo}" />
	</c:forEach>
	<input type="hidden" name="descFondo1" id="descFondo1" value="${clienteBbvaCash.descFondo}" />
	<input type="hidden" name="descFondo2" id="descFondo2" value="${clienteBbvaCash.descFondo2}" />
	
<div class="row margin_top_10 hidden-xs">
	<div class="col-sm-1"></div>
	<div class="col-sm-10" style="text-align: justify;">
		<div class="titulo-calculadora-combo" >
			<span style="font-size: 28px;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;">
				¡Bienvenido ${clienteBbvaCash.nombres}!,
			</span> 
			<span style="font-size: 28px;color: #4791c8;font-weight: normal;font-family: BBVA Web Book;">
				eres nuestro cliente de 
			</span>
			<c:set var="descFondo2" value="${clienteBbvaCash.descFondo2}"/>
			<c:set var="indAfiliacionBXI" value="${clienteBbvaCash.indAfiliacionBXI}"/> 
			
			<span style="font-size: 28px;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;">
				Fondos Mutuos ${clienteBbvaCash.descFondo}<c:if test="${empty descFondo2}">. </c:if>
			</span>
			
			<c:if test="${not empty descFondo2}">
			<span style="font-size: 28px;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;">
				y/o ${clienteBbvaCash.descFondo2}.
			</span><br>	
			</c:if>

			<span style="font-size: 28px;color: #4791c8;font-weight: normal;font-family: BBVA Web Book;">			
			<c:if test="${not empty indAfiliacionBXI}">
				Ahora que ya sabes cómo conseguir eso que tanto quieres, descubre cómo programar tus aportes:
			</c:if>
			<c:if test="${empty indAfiliacionBXI}">
				Actualmente no te encuentras afiliado a la banca por internet para que puedas programar tus aportes, hazlo aquí: 
			</c:if>
			</span>
		</div>
	</div>
	<div class="col-sm-1"></div>
</div>


<div class="row margin_top_10 hidden-sm hidden-md hidden-lg hidden-xl">
	<div class="col-sm-1"></div>
	<div class="col-sm-10" style="text-align: justify;">
		<div class="titulo-calculadora-combo" >
			<span style="font-size: 20px;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;">
				¡Bienvenido ${clienteBbvaCash.nombres}!,
			</span> 
			<span style="font-size: 20px;color: #4791c8;font-weight: normal;font-family: BBVA Web Book;">
				eres nuestro cliente de 
			</span>
			<c:set var="descFondo2" value="${clienteBbvaCash.descFondo2}"/>
			<c:set var="indAfiliacionBXI" value="${clienteBbvaCash.indAfiliacionBXI}"/> 
			
			<span style="font-size: 20px;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;">
				Fondos Mutuos ${clienteBbvaCash.descFondo}<c:if test="${empty descFondo2}">. </c:if>
			</span><br>
			
			<c:if test="${not empty descFondo2}">
			<span style="font-size: 20px;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;">
				y/o ${clienteBbvaCash.descFondo2}.
			</span><br>	
			</c:if>

			<span style="font-size: 20px;color: #4791c8;font-weight: normal;font-family: BBVA Web Book;">			
			<c:if test="${not empty indAfiliacionBXI}">
				Ahora que ya sabes cómo conseguir eso que tanto quieres, descubre cómo programar tus aportes:
			</c:if>
			<c:if test="${empty indAfiliacionBXI}">
				Actualmente no te encuentras afiliado a la banca por internet para que puedas programar tus aportes, hazlo aquí: 
			</c:if>
			</span>
		</div>
	</div>
	<div class="col-sm-1"></div>
</div>

<div id="common_section" class="row hidden-xs" style="margin-top:5%">
	<div class="col-sm-12">	
		<div class="text_center">
			<c:if test="${not empty indAfiliacionBXI}">
				<a href="https://www.bbvacontinental.pe/personas/inversiones/fondos-mutuos-suscripcion-programada/#contenido-desplegable-0" target="_blank">
					<span class="btn btn-verde" style="padding-left:40px;padding-right:40px">Aprende cómo hacerlo</span>
				</a>
			</c:if>
			<c:if test="${empty indAfiliacionBXI}">
				<a href="https://bancaporinternet.bbvacontinental.pe/bdpnux_pe_web/bdpnux_pe_web/inscripcion/index" target="_blank">
					<span class="btn btn-verde" style="padding-left:40px;padding-right:40px">Afíliate aquí</span>
				</a>
			</c:if>										 																			
		</div>
	</div>
</div>

<div id="common_section" class="row hidden-sm hidden-md hidden-lg hidden-xl" style="margin-top:5%">
	<div class="col-sm-12">	
		<div class="text_center">
			<c:if test="${not empty indAfiliacionBXI}">
				<a href="https://www.bbvacontinental.pe/personas/inversiones/fondos-mutuos-suscripcion-programada/#contenido-desplegable-0" target="_blank">
					<span class="btn btn-verde">Aprende cómo hacerlo</span>
				</a>
			</c:if>
			<c:if test="${empty indAfiliacionBXI}">
				<a href="https://bancaporinternet.bbvacontinental.pe/bdpnux_pe_web/bdpnux_pe_web/inscripcion/index" target="_blank">
					<span class="btn btn-verde">Afíliate aquí</span>
				</a>
			</c:if>										 																			
		</div>
	</div>
</div>

<div id="common_section" class="row" style="margin-top:5%;">
	<div class="col-sm-1"></div>
	<div class="col-sm-4">
		<div class="checkbox hidden-xs" style="text-align:right">
			<label style="font-size: 1.1em; padding-left: 0px;">				            	
				<span id="label_correo" class="listado_servicios" data-toggle="tooltip" data-placement="top" title="">Enviar mis resultados de MIO a:</span>
			</label>
		</div>
		
		<div class="checkbox hidden-sm hidden-md hidden-lg hidden-xl" style="text-align:left">
			<label style="font-size: 1.1em; padding-left: 0px;">				            	
				<span id="label_correo" class="listado_servicios" data-toggle="tooltip" data-placement="top" title="">Enviar mis resultados de MIO a:</span>
			</label>
		</div>
		
	</div>
	<div class="col-sm-4" id="correoResultado">
		<div class="form-group" style="width: 100%">
			<input type="text" class="errorTooltip form-control txt-global" id="correo_user" name="correo_user" placeholder="Correo" autocomplete="off" maxlength="80" style="width: 100%">
			<span class="help-block error-bbva"></span>
		</div>
	</div>	
	<div class="col-sm-1">	
		<div class="text_center">
			<button id="btnEnviar" type="button" class="btn btn-azul">Enviar</button>										 																			
		</div>
	</div>														
	<div class="col-sm-2"></div>
</div>

</form>

</tiles:putAttribute>

<tiles:putAttribute name="script">
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_calculadora.js"></script>
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_plugin.js"></script>

<script type="text/javascript">
		$(window).on("load", function() {
			if($("#indAfiliacionBXI").val()=="1"){
				history.pushState("afiliado", null, "suscripcion-programada-afiliado");	
			}
			else{
				history.pushState("no-afiliado", null, "suscripcion-programada-no-afiliado");				
			}
			llenarDataLayer(TAG_CLIENTE,"");
		});
</script>

<script type="text/javascript">

$(document).ready(function(){
	transicion_barra_progreso(2,500);
});

$("input[type=checkbox][name=autorizacion]").change(function () {
	var tipo=$("input[name=autorizacion]:checked").val();
	if(tipo=="1"){
		$("#correoResultado").css("display","block");
	}else if(tipo=="2"){
		$("#correoResultado").css("display","none");
	}else if(tipo==null){
		$("#correoResultado").css("display","none");
	}	
   });
   

$("#correo_user").blur(function(){correo($("#correo_user"));});
function correo(obj){
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(obj.val()=='' || !regex.test(obj.val())){
		obj.tooltip({trigger : 'hover', title : 'Escribe un correo válido'});
		obj.addClass('form-control-error');
		return 0;
	}else{
		obj.tooltip('destroy');
		obj.removeClass('form-control-error');
		return 1;
	}
}


$("#numeroDocumento").blur(function(){ver_documento();});

$("#btnIniciar").click(function(){
	if(ver_documento()!=0){
		$("#divInicio").removeClass("hidden");
		$("#divInicio").show();
		desbloquearCalculador();
		limpiarFormulario();
	}	
});

$("#btnEnviar").click(function( eventBtnEnviarSuscripcion ) {
	var enviar=1;
	enviar=enviar*correo($("#correo_user"));
	
	if(enviar==0){
		event.preventDefault();
	}else{
		_satellite.track("SolicitudFondosMutuosEvent57");
		setTimeout(function() {document.formSuscripcionProgramada.submit();}, 650);
	}
});

</script>
</tiles:putAttribute>
<tiles:putAttribute name="googletagrmkt">
<!-- Google Code para etiquetas de remarketing MCC -->
<!--------------------------------------------------
Es posible que las etiquetas de remarketing todavía no estén asociadas a la información de identificación personal o que estén en páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar la etiqueta, consulte http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 977810892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/977810892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</tiles:putAttribute>
</tiles:insertDefinition>