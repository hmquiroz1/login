<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Formulario de Fondos Mutuos</tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/suscripcion-programada.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">

</tiles:putAttribute>

<tiles:putAttribute name="body">

<div class="row">
	<div class="col-sm-10 titulo_nivel2 padding_titulo_principal padding_left_form" >
			Conquista de manera programada lo que tanto quieres
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="init-step1" id="barraProgreso">
			<form novalidate="novalidate" class="form-horizontal tooltip-validation transfer-filter">
				<div class="wizard">
					<div data-title="Cálculo" data-multistep="true">
					</div>
					<div data-title="Solicitud" data-multistep="true">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div  id="question_section">
<div class="row">
	<div class="col-sm-12 padding_left_form margin_bottom_10 margin_top_10 titulo_nivel3 text_center" >
			&iquest;Eres cliente de Fondos Mutuos BBVA Cash Soles y/o BBVA Cash Dólares?
	</div>
</div>
<div class="row">
	<form>
		<div class="col-sm-6 rightRD rightRD_center">
			<label class="radio-inline">
				<input type="radio" value="1" name="opcType">
				<span data-toggle="tooltip" data-placement="top" title="" class="font-blue">Sí</span>
			</label>
		</div>
		<div class="col-sm-6 leftRD leftRD_center">
			<label class="radio-inline font-blue">
				<input type="radio" value="0" name="opcType">
				<span data-toggle="tooltip" data-placement="top" title="" class="font-blue">No</span>
			</label>
		</div>
	</form>
</div>
</div>
<form id="formSuscripcionProgramada" name="formSuscripcionProgramada" action="<c:url value="suscripcion-mensaje"/>" method="post">
<input type="hidden" name="tipo" id="tipo" />
<div id="user_section" class="display_none">
<div class="row">
	<div class="col-sm-12 padding_left_form margin_bottom_10 margin_top_10 titulo_nivel3 txtRD_left_center" >
			${tituloObjetivo}, acércate a cualquier <a href="http://www.ubicanosbbvacontinental.pe" target="_blank" class="font-blue"><ins>oficina BBVA</ins></a> para abrir tu fondo mutuo.
	</div>		
</div>
<div class="seccion_resaltada">
	<div class="row">
		<div class="col-sm-12 txtRD_left_center">
			Para recibir más información sobre MÍO - Suscripción programada de Fondos Mutuos, déjanos tus datos:
		</div>
	</div>
	<div class="row margin_top_10">
		<div class="col-sm-1">
		</div>
		<div class="col-sm-10">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<input type="text" class="errorTooltip form-control" id="nombre_user" name="nombre_user" placeholder="Nombre" autocomplete="off" maxlength="50">
						<span class="help-block error-bbva"></span>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<input type="text" class="errorTooltip form-control" id="correo_user" name="correo_user" placeholder="Correo Electrónico" autocomplete="off" maxlength="80">
						<span class="help-block error-bbva"></span>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-1">
		</div>
	</div>
</div>	
</div>
<div id="customer_section" class="display_none">
<div class="row">
	<div class="col-sm-12 padding_left_form margin_bottom_10 margin_top_10 titulo_nivel3 txtRD_left_center" >
			Completa la siguiente información y te contactaremos
	</div>
</div>

<div class="row">
	<div class="col-sm-7 padding_left_form margin_top_10">
		<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<input type="text" class="errorTooltip form-control" id="nombre" name="nombre" placeholder="Nombre" autocomplete="off" maxlength="50">
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<input type="text" class="errorTooltip form-control" id="apellido" name="apellido" placeholder="Apellido" autocomplete="off" maxlength="50">
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		</div>
		<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<select class="errorTooltip form-control form-select" id="tipoDocumento" name="tipoDocumento" autocomplete="off">
				<c:forEach var="tipoDocumento" items="${tipoDocumentos}">
					<option value="${tipoDocumento.codigo}">${tipoDocumento.nombre}</option>
				</c:forEach> 
				</select>
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<input type="text" class="errorTooltip form-control" id="numeroDocumento" name="numeroDocumento" placeholder="Número de Documento" autocomplete="off" maxlength="8">
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		</div>
		<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<select class="errorTooltip form-control form-select" id="departamento" name="departamento" autocomplete="off">
				<option value="">Departamento</option>
				<c:forEach var="departamento" items="${departamentos}">
					<option value="${departamento.codigo}">${departamento.nombre}</option>
				</c:forEach>
				</select>
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<input type="text" class="errorTooltip form-control" id="correo" name="correo" placeholder="Correo Electrónico" autocomplete="off" maxlength="50">
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		</div>
		<div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<input type="text" class="errorTooltip form-control" id="telefono" name="telefono" placeholder="Teléfono de Contacto" autocomplete="off" maxlength="9">
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-group">
				<select class="errorTooltip form-control form-select" id="horario" name="horario">
				<option value="">Horario para Llamarte</option>
				<c:forEach var="horario" items="${horarios}">
					<option value="${horario.codigo}">${horario.nombre}</option>
				</c:forEach>
				</select>
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		</div>			
		<div class="row">
			<div class="col-sm-12">
				<div class="seccion_resaltada">
					<div class=" form-group">
						<span id="titServicios" data-toggle="tooltip" data-placement="top" title="">&iquest;Con qué servicios te gustaría contar?*</span> <br>
						<c:forEach var="servicio" items="${serviciosFFMM}">
							<div class="checkbox">
							<label id="label_${servicio.codigo}" class="padding_left_min opciones">
								<input type="checkbox" value="${servicio.valor}|${servicio.descripcion}" name="servicios" class="">  
								<span class="cr green"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
								<span class="font_normal" data-toggle="tooltip" data-placement="top" title="">${servicio.descripcion}</span>
							</label>
							</div>
						</c:forEach>
						<span  class="font_normal padding_left_min font_14" >
						* Sin costo adicional
						</span>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<div class="col-sm-5 hidden-xs">
	<img src="${pageContext.request.contextPath}ST/img/ffmm_pub.jpg" class="img-responsive">
	</div>
</div>
</div>
<div id="common_section" class="display_none">
	<div class="row">
	<div class="col-sm-12 padding_left_form ">
		<div class="form-group">
		<div class="checkbox">
          <label style="font-size: 1.2em; padding-left: 0px;">
            <input type="checkbox" id="autorizacion" name="autorizacion" value="1">
            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
            <a href="#" style="padding: 0px;text-align: left;white-space: inherit;" class="btn btn-link" data-toggle="modal" data-target="#myModal">He leído y autorizo el tratamiento de datos personales</a>
          </label>
        </div>
		</div>
	</div>
	</div>
	<div class="row" style="display: none">
	<div class="col-sm-12">
		<div class="form-group">
		<label>
			<input type="text" class="errorTooltip form-control" id="opcional" name="opcional" placeholder="Opcional" autocomplete="off" maxlength="40">
		</label>
		</div>
	</div>
	</div>
	<div class="row">
	<div class="col-sm-12 text_center">
		<button id="btnEnviarSuscripcion" name="btnEnviarSuscripcion" type="button" class="btn btn-primary">Enviar</button>
	</div>
	</div>	
</div>
</form>
<div class="row">
	<div class="col-sm-12">
		<div class="tabla_disclaimers display_none" id="tabla_disclaimers">
		<ul>
			<li>*El resultado de los cálculos es referencial. </b> La tasa de rentabilidad anual asumida para el cálculo, es la tasa equivalente al promedio de las rentabilidades anuales de los últimos 5 años. En Soles, considera el promedio de las rentabilidades anuales del BBVA Cash Soles y en dólares el promedio de las rentabilidades anuales del BBVA Cash Dólares, para el periodo seleccionado. La tasa de rentabilidad estimada no debe ser interpretada ni entendida como una promesa de rentabilidad constante en el tiempo</li>
			<li>Consulta el detalle de <b>rentabilidad anuales históricas</b> de los fondos BBVA Cash Soles y BBVA Dólares en <a target="_blank" href="https://www.bbvacontinental.pe/personas/ahorro-e-inversion/bbva-asset-management-continental-saf/bbva-cash-fmiv-y-bbva-cash-dolares-fmiv/#pane1">nuestro sitio web</a> y en las oficinas del BBVA Continental</li>
			<li>La <b>rentabilidad o ganancia obtenida en el pasado</b> no garantiza que se repita en el futuro. Esta rentabilidad no incluye el efecto de las comisiones de suscripción y rescate, ni el impuesto a la renta.</li>
		</ul>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">AUTORIZACIÓN DE RECOPILACIÓN Y TRATAMIENTO DE DATOS</h4>
		</div>
		<div class="modal-body">
        &#8226; La información que Usted nos proporciona sobre su nombre, apellido, nacionalidad, estado civil, documento de identidad, ocupación, estudios, domicilio, correo electrónico, teléfono, estado de salud, actividades que realiza, ingresos económicos, patrimonio, gastos, entre otros, así como la referida a los rasgos físicos y/o de conducta que lo identifican o lo hacen identificable como es su huella dactilar, su voz, etc. (datos biométricos), conforme a Ley es considerada como Datos Personales. Recuerde que es posible que esta información también pueda ser obtenida a través de otras personas, sociedades y/o instituciones (públicas o privadas, nacionales o extranjeras).<br>
		&#8226; Usted nos da su consentimiento libre, previo, expreso e informado para que sus Datos Personales sean tratados por el Banco, sus subsidiarias y empresas vinculadas, es decir, que puedan ser: recopilados, registrados,organizados, almacenados, conservados, elaborados, modificados, bloqueados, suprimidos, extraídos, consultados, utilizados, transferidos o procesados de cualquier otra forma prevista por Ley. Esta autorización es indefinida y se mantendrá inclusive después de terminada(s) la(s) operación(es) y/o el(los) Contrato(s) que Usted tenga o pueda tener con el Banco, sus subsidiarias y empresas vinculadas.<br>
		&#8226; Sus Datos Personales serán almacenados (guardados) en el Banco de Datos de Clientes del cual el Banco es titular o en cualquier otro que en el futuro podamos establecer. El Banco ha adoptado las medidas necesarias para mantener segura la información.<br>
		&#8226; Al dar esta autorización Usted permite que (i) evaluemos su comportamiento en el sistema bancario y su capacidad de pago, (ii) podamos decidir si se otorga el(los) producto(s) y/o servicio(s) que solicite, (iii) le ofrezcamos otros productos y/o servicios del Banco y/o de terceros vinculados o no (por ejemplo cuentas, préstamos, entre otros), lo que podremos hacer también a través de terceras personas (por ejemplo, en asociaciones o alianzas comerciales), (iv) le enviemos ofertas comerciales, publicidad e información en general de los productos y/o servicios del Banco y/o de terceros vinculados o no, (v) gestionemos el cobro de deudas, de ser el caso, (vi) usemos y/o transfiramos esta información (dentro o fuera del país) a terceros vinculados o no al Banco, nacionales o extranjeros, públicos o privados (por ejemplo: otros bancos, imprentas, empresas de mensajería, auditoría, entre otros).<br>
		&#8226; <b>IMPORTANTE:</b> Usted declara que se le ha informado que tiene derecho a no proporcionar la autorización para el tratamiento de sus Datos Personales y que si no la proporciona no podremos tratar sus Datos Personales en la forma explicada en la presente cláusula, lo que no impide su uso para la ejecución (desarrollo) y cumplimiento (ej: pago) del servicio de Suscripción Programada.<br>
		&#8226; Asimismo, Usted tiene los derechos de información, acceso rectificación, cancelación, oposición y tratamiento objetivo de Datos Personales. Para hacer uso de estos derechos deberá presentar una solicitud escrita en nuestras oficinas. Se podrán establecer otros canales para tramitar estas solicitudes, lo que será informado oportunamente por el Banco a través de su Página Web.<br>
		&#8226; <b>IMPORTANTE:</b> Si Usted no quiere autorizarnos o quiere revocar el tratamiento de sus Datos Personales, pídale al representante del Banco el formulario correspondiente al Servicio de Suscripción Programada, si la contratación no es presencial, Usted lo podrá descargar en la página web: <a href="https://www.bbvacontinental.pe/" target="_blank">www.bbvacontinental.pe</a><br/>
		</div>
	</div>
	</div>
</div>

</tiles:putAttribute>

<tiles:putAttribute name="script">

<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip();
    $(document).ready(function(){
    	$(document).ready(function(){
    		transicion_barra_progreso(2,500);
    	});
    });
    $("input[type=radio][name=opcType]").change(function () {
    	var tipo=$("input[name=opcType]:checked").val();
    	$('#tipo').val(tipo);
    	$("#question_section").fadeOut(500,function(){
	    	$("#tabla_disclaimers").fadeIn("slow")
	    	$("#common_section").fadeIn("slow")
    	});
		if(tipo=='1'){
			$("#customer_section").fadeIn(2000,function(){
				_satellite.track("fondosMutuosEvento49");
	    	});
		}else{
			$("#user_section").fadeIn(2000,function(){
				_satellite.track("fondosMutuosEvento49");
	    	});
		}
    });
    
    $("#btnEnviarSuscripcion").click(function( eventBtnEnviarSuscripcion ) {
		var enviar=1;
   		
   		if($('#tipo').val()=='1'){
    		enviar=enviar*nombre($('#nombre'));    		
    		enviar=enviar*apellido();
    		enviar=enviar*numeroDocumento();
			enviar=enviar*departamento();
			enviar=enviar*correo($("#correo"));
			enviar=enviar*telefono();
			enviar=enviar*horario();
			enviar=enviar*servicios();
   		}else{
   			enviar=enviar*nombre($('#nombre_user'));  
   			enviar=enviar*correo($("#correo_user"));
   		}
		if(enviar==0){
			event.preventDefault();
		}else{
			_satellite.track("SolicitudFondosMutuosEvent57");
			setTimeout(function() {document.formSuscripcionProgramada.submit();}, 650);
		}
    });
    
   	$("#nombre_user").alpha({disallow :'¿¡'});
   	$("#correo_user").blur(function(){correo($("#correo_user"));});
       $("input[type=checkbox][name=servicios]").change(function () {
       	servicios();
       });
   	$("#nombre").alpha({disallow :'¿¡'});
   	$("#apellido").alpha({disallow :'¿¡'});
   	$("#numeroDocumento").numeric({allowMinus:false});
   	$("#telefono").numeric({allowMinus:false});
   	$("#nombre").blur(function(){nombre($('#nombre'));});
   	$("#nombre_user").blur(function(){nombre($('#nombre_user'));});
   	$("#apellido").blur(function(){apellido();});
   	$("#numeroDocumento").blur(function(){numeroDocumento();});
   	$("#departamento").blur(function(){departamento();});
   	$("#correo").blur(function(){correo($("#correo"));});
   	$("#telefono").blur(function(){telefono();});
   	$("#horario").blur(function(){horario();});
   	$("#tipoDocumento").change(function(){
  	    	$("#numeroDocumento").unbind();
  	    	$("#numeroDocumento").blur(function(){numeroDocumento();});
      		if($("#tipoDocumento").val()=='L'){
      			$("#numeroDocumento").attr("maxlength", "8");
      	    	$("#numeroDocumento").numeric({allowMinus:false});
      		}else if($("#tipoDocumento").val()=='E' || $("#tipoDocumento").val()=='D'){
      			$("#numeroDocumento").attr("maxlength", "12");
      	    	$("#numeroDocumento").alphanum({allow :'/-', allowSpace :false});
      		}else if($("#tipoDocumento").val()=='P'){
      			$("#numeroDocumento").attr("maxlength", "12");
      	    	$("#numeroDocumento").alphanum({allowSpace :false});
      		}
      	});
  	
  		function nombre(obj){
  			obj.val($.trim(obj.val()));
      		if(obj.val()==''){
      			obj.tooltip({trigger : 'hover', title : 'Escribe tu nombre'});
      			obj.addClass('form-control-error');
  				return 0;
  			}else{
  				obj.tooltip('destroy');
  				obj.removeClass('form-control-error');
  				return 1;
  			}
      	}
  		function apellido(){
  			$("#apellido").val($.trim($("#apellido").val()));
  			if($("#apellido").val()==''){
  				$("#apellido").tooltip({trigger : 'hover', title : 'Escribe tu apellido'});
  				$("#apellido").addClass('form-control-error');
  				return 0;
  			}else{
  				$('#apellido').tooltip('destroy');
  				$("#apellido").removeClass('form-control-error');
  				return 1;
  			}    			
  		}
  		function numeroDocumento(){
  			if($("#numeroDocumento").val()=='' || 
  					($("#numeroDocumento").val().length!=8 && $("#tipoDocumento").val()=='L')){
  				$("#numeroDocumento").tooltip({trigger : 'hover', title : 'Escribe un número de documento válido'});
  				$("#numeroDocumento").addClass('form-control-error');
  				return 0;
  			}else{
  				$('#numeroDocumento').tooltip('destroy');
  				$("#numeroDocumento").removeClass('form-control-error');
  				return 1;
  			}
  		}
  		function departamento(){
      		if($("#departamento").val()==''){
  				$("#departamento").tooltip({trigger : 'hover', title : 'Selecciona un departamento'});
  				$("#departamento").addClass('form-control-error');
  				return 0;
  			}else{
  				$('#departamento').tooltip('destroy');
  				$("#departamento").removeClass('form-control-error');
  				return 1;
  			}
      	}
  		function correo(obj){
      		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      		if(obj.val()=='' || !regex.test(obj.val())){
  				obj.tooltip({trigger : 'hover', title : 'Escribe un correo válido'});
  				obj.addClass('form-control-error');
  				return 0;
  			}else{
  				obj.tooltip('destroy');
  				obj.removeClass('form-control-error');
  				return 1;
  			}
      	}
  		function telefono(){
      		if($("#telefono").val().length<7 || 
      				$("#telefono").val().length>9){
  				$("#telefono").tooltip({trigger : 'hover', title : 'Escribe un teléfono de contacto válido'});
  				$("#telefono").addClass('form-control-error');
  				return 0;
  			}else{
  				$('#telefono').tooltip('destroy');
  				$("#telefono").removeClass('form-control-error');
  				return 1;
  			}
      	}
  		function horario(){
  			if($("#horario").val()==''){
  				$("#horario").tooltip({trigger : 'hover', title : 'Selecciona un horario para llamarte'});
  				$("#horario").addClass('form-control-error');
  				return 0;
  			}else{
  				$('#horario').tooltip('destroy');
  				$("#horario").removeClass('form-control-error');
  				return 1;
  			}
      	}    
  		function servicios(){
  			var atLeastOneIsChecked = $('input[name="servicios"]:checked').length > 0;	
  			if(atLeastOneIsChecked){
  				$('#label_SUSCRIPCION').removeClass('colorError');
  				$('#label_AFILIACION_EECC').removeClass('colorError');
  				$("#titServicios").removeClass('colorError errorServicios');
  				$('#titServicios').tooltip('destroy');
  				return 1;
  			}else{
  				$('#label_SUSCRIPCION').addClass('colorError');
  				$('#label_AFILIACION_EECC').addClass('colorError');
  				$("#titServicios").addClass('colorError errorServicios');
  				$("#titServicios").tooltip({trigger : 'hover', title : 'Selecciona los servicios con los que te gustaría contar'});
  				return 0;
  			}
  		}
</script>

</tiles:putAttribute>
</tiles:insertDefinition>