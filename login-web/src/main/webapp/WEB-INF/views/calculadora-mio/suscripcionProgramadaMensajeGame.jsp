<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Suscripción Programada - Calculadora MIO </tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/calculadora-mio-game.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">
	
</tiles:putAttribute>

<tiles:putAttribute name="conversioncodefacebok">
	<!-- Facebook Conversion Code for Registros Mio -->
	<script>(function() {
	  var _fbq = window._fbq || (window._fbq = []);
	  if (!_fbq.loaded) {
	    var fbds = document.createElement('script');
	    fbds.async = true;
	    fbds.src = 'https://connect.facebook.net/en_US/fbds.js';
	    var s = document.getElementsByTagName('script')[0];
	    s.parentNode.insertBefore(fbds, s);
	    _fbq.loaded = true;
	  }
	})();
	window._fbq = window._fbq || [];
	window._fbq.push(['track', '6053652605068', {'value':'0.00','currency':'USD'}]);
	</script>
	<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6053652605068&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
	
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	
	fbq('init', '254908304633078');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=254908304633078&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</tiles:putAttribute>

<tiles:putAttribute name="conversioncodegoogle">
	<!-- Google Code for MIO Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 977810892;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "uIMoCN-hgGkQzOug0gM";
	var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/977810892/?label=uIMoCN-hgGkQzOug0gM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
</tiles:putAttribute>

<tiles:putAttribute name="body">

<div class="row">
<div class="col-sm-12">

<div style="background: url(${pageContext.request.contextPath}ST/img/calculadora-mio/fondo_cabecera.png) no-repeat;">
	<div class="row">
		<div class="col-sm-3 col-xs-5" >
			<img src="${pageContext.request.contextPath}ST/img/calculadora-mio/logo_mio.png" 
				class="img-responsive">
			</div>
			<div class="col-sm-9 col-xs-7" style="margin-top: 20px;padding-left:0px;">
				<span class="titulo-calculadora-mio-game">
					Conquista de forma programada eso que tanto quieres
				</span>
			</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<div class="init-step1" id="barraProgreso">
				<form novalidate="novalidate" class="form-horizontal tooltip-validation transfer-filter">
					<div class="wizard">
						<div data-title="Cálculo" data-multistep="true">
						</div>
						<div data-title="Solicitud" data-multistep="true">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="row" style="text-align: center;">
		<div class="col-sm-12">
			<h3 class="titulo-confirmacion-reg"><b>¡<c:out value="${nombreCompleto}" />, HEMOS RECIBIDO TU SOLICITUD!</b></h3>
		</div>
		<div class="col-sm-12" style="text-align: justify;">
			<c:choose>
			    <c:when test="${tipoCliente=='1'}">
			        <p class="texto-confirmacion-reg margin_top_20 visible-xs-block" style="text-align: justify;">En un máximo de 48 horas nos comunicaremos contigo para que puedas acceder a: </p>
			        <p class="texto-confirmacion-reg margin_top_20 text_center hidden-xs">En un máximo de 48 horas nos comunicaremos contigo para que puedas acceder a: </p>
			    </c:when>
			    <c:otherwise>
			        <p class="texto-confirmacion-reg margin_top_20 text_center">Recuerda que puedes acercarte a cualquier <a href="http://www.ubicanosbbvacontinental.pe" target="_blank" class="font-blue">oficina BBVA</a> para que podamos programar tus aportes.</p>
			    </c:otherwise>
			</c:choose>
		</div>
	</div>
	
	<div class="row" style="text-align: center;">
		<div class="col-sm-12">
			<c:choose>
			    <c:when test="${tipoCliente=='1'}">
					<c:forEach var="servicio" items="${serviciosElegidos}">
						<div class="row">
							<div class="col-sm-12">
								<span class="texto-confirmacion-reg">${servicio} </span>
							</div>
						</div>
					</c:forEach>
			    </c:when>
			    <c:otherwise>
			         <br>
			    </c:otherwise>
			</c:choose>
		</div>		
	</div>
	
	<div class="row" style="margin-top: 30px;">
		<div class="col-sm-12">
			<span class="texto_14">
				<b class="texto-confirmacion-como-funciona">Cómo funciona <span class="font-blue negrita">MÍO - Suscripción Programada</span></b>
			</span>
		</div>
	</div>
	
	<div class="row" 
		style="background: url(${pageContext.request.contextPath}ST/img/calculadora-mio/fondo_como_funciona.png) no-repeat left bottom;">
		<div class="col-sm-12">
			<div class="row">
				<div class="col-sm-6">
					<p class="texto-item-como-funciona item_resultado">Con esta autorización  se cargará de tu cuenta de ahorros el monto que tú decidas con la frecuencia que tú elijas de manera automática.</p>
				</div>
				<div class="col-sm-6">
					<p class="texto-item-como-funciona item_resultado">Para que se realice el cargo automático es indispensable tener el saldo necesario disponible.</p>
				</div>				
			</div>
			<div class="row">
				<div class="col-sm-6">
					<p class="texto-item-como-funciona item_resultado">Si en un periodo no existe el saldo disponible, no se acumalará para el siguiente aporte.</p>					
				</div>
				<div class="col-sm-6">
					<p class="texto-item-como-funciona item_resultado">Si la fecha de cargo cae feriado o fin de semana se realizará al día útil siguiente.</p>					
				</div>				
			</div>
			<div class="row">
				<div class="col-sm-6">
					<p class="texto-item-como-funciona item_resultado">Este servicio permanecerá activo mientras tú lo decidas.</p>
				</div>
				<div class="col-sm-6">								
				</div>				
			</div>
		</div>
		
		<div>
			<img src="${pageContext.request.contextPath}ST/img/calculadora-mio/pasto_hombre_confimacion.png" class="img-responsive">
		</div>
	</div>
</div>
	
 </div>
</div>

<div class="row" style="margin-top: 35px;">
	<div class="col-sm-12">
		<span class="icono-estrella"></span>
		<c:choose>
		    <c:when test="${tipoCliente=='1'}">
				<span class="texto-recomendacion"><b style="font-weight:400;">Recuerda que:</b></span>
				<p class="texto-item-como-funciona padding_left_40">
					Estos servicios se encuentran actualmente para <b style="color: #4791c8;">Fondos BBVA Cash Soles y BBVA Cash Dólares,</b> que invierten principalmente en depósitos a plazo y cuentas de ahorro.
				</p>
		    </c:when>
		    <c:otherwise>
				<span class="texto-recomendacion"><b style="font-weight:400;">Te recomendamos usar</b></span>
				<p class="texto-item-como-funciona padding_left_40">
					Los <b style="color: #4791c8;">Fondos BBVA Cash Soles y BBVA Cash Dólares,</b> que invierten principalmente en depósitos a plazo y cuentas de ahorro.
				</p>
		    </c:otherwise>
		</c:choose>
	</div>
</div>

<div class="row" style="margin-top: 35px; text-align: center;">
	<div class="col-sm-12">
		<button type="button" style="width: 204px; height: 40px;" class="btn btn-azul" id="btnFFMM">Ir a Fondos Mutuos</button>
	</div>
</div>

</tiles:putAttribute>

<tiles:putAttribute name="script">
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_calculadora.js"></script>
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_plugin.js"></script>
<script>
$(document).ready(function(){
	transicion_barra_progreso(2,500);
    $("#btnFFMM").click(function(event) {
    	var isInIFrame = (window.location != window.parent.location);
    	if(isInIFrame==true){
    		window.parent.location.href='https://www.bbvacontinental.pe/personas/ahorro-e-inversion/bbva-asset-management-continental-saf/';
    	}
    	else {
    		window.location.href='https://www.bbvacontinental.pe/personas/ahorro-e-inversion/bbva-asset-management-continental-saf/';
    	}
    });
}); 
</script>
</tiles:putAttribute>
<tiles:putAttribute name="googletagrmkt">
<!-- Google Code para etiquetas de remarketing MCC -->
<!--------------------------------------------------
Es posible que las etiquetas de remarketing todavía no estén asociadas a la información de identificación personal o que estén en páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar la etiqueta, consulte http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 977810892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/977810892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</tiles:putAttribute>
</tiles:insertDefinition>