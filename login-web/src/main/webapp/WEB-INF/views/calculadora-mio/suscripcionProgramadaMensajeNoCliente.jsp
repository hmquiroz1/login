<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Calculadora MIO Game</tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/calculadora-mio-game.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">

<style>

</style>

</tiles:putAttribute>

<tiles:putAttribute name="conversioncodefacebok">
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	
	fbq('init', '254908304633078');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=254908304633078&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</tiles:putAttribute>


<tiles:putAttribute name="body">

<div class="row fondo-calculadora">
	<div class="col-sm-12">
	<div class="row">
		<div class="col-sm-3 col-xs-5" >
			<img src="${pageContext.request.contextPath}ST/img/calculadora-mio/logo_mioNew.png" class="img-responsive">
		</div>
		<div class="col-sm-9 col-xs-7" style="margin-top: 20px;padding-left:0px;">
			<span class="titulo-calculadora-mio-game">
				Conquista de forma programada eso que tanto quieres
			</span>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="init-step1" id="barraProgreso">
				<form novalidate="novalidate" class="form-horizontal tooltip-validation transfer-filter">
					<div class="wizard">
						<div data-title="Cálculo" data-multistep="true">
						</div>
						<div data-title="Solicitud" data-multistep="true">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-offset-9 col-sm-3 padding_titulo_principal">
			<div id="pdfCalculadora" class="padding_right_35 text_right hidden hidden-xs">
			<form action="<c:url value="calculadora-mio-pdf"/>" target="_blank">
				<button type="submit" class="btn btn-calculadora btn_pdf">Guardar en PDF</button>
			</form>			
			</div>
		</div>
	</div>
	
	</div>	
	
</div>

<form id="formMensajeNoCliente" name="formMensajeNoCliente">
		
		<input type="hidden" name="indClienteBBVACash" id="indClienteBBVACash" value="${clienteBbvaCash.idClienteBbvaCash}" />
		<input type="hidden" name="indAfiliacionBXI" id="indAfiliacionBXI" value="${clienteBbvaCash.indAfiliacionBXI}" />
		<input type="hidden" name="objetivo" id="objetivo" value="${calculadora.objetivo}" />
		<input type="hidden" name="tipoCalculo" id="tipoCalculo" value="${calculadora.tipo}" />
		<input type="hidden" name="divisa" id="divisa" value="${calculadora.divisa}" />
		<input type="hidden" name="monto" id="monto" value="${calculadora.monto}" />
		<input type="hidden" name="meses" id="meses" value="${calculadora.meses}" />
		<c:forEach var="listSuscripcionDetalles" items="${calculadora.listSuscripcionDetalles}">
			<input type="hidden" name="montoCuota" id="montoCuota" value="${listSuscripcionDetalles.montoCuota}" />
			<input type="hidden" name="periodicidad" id="periodicidad" value="${listSuscripcionDetalles.periodo}" />
			<input type="hidden" name="montoFuturo" id="montoFuturo" value="${listSuscripcionDetalles.montoFuturo}" />
		</c:forEach>
		
	<div class="row" style="margin-top:5%">
			<div class="col-sm-12" style="text-align: center;">
				<div class="titulo-calculadora-combo" >
				<p style="font-size: 28px;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;">
					¡Gracias ${clienteBbvaCash.nombres}!
				</p> 
				<span style="font-size: 28px;color: #4791c8;font-weight: normal;font-family: BBVA Web Book;">
					Te llamaremos en el horario indicado.				  
				</span> 
				<p style="font-size: 28px;color: #4791c8;font-weight: normal;font-family: BBVA Web Book;">
					Puedes ver más información de nuestros fondos mutuos
					<span style="color: #0069b1;text-decoration: underline;">
						<a href="https://www.bbvacontinental.pe/personas/inversiones/bbva-asset-management-continental-saf/" target="_blank"> aquí</a>.
					</span>								  
				</p> 			
				</div>
			</div>
	</div>	
</form>	
</tiles:putAttribute>

<tiles:putAttribute name="script">
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_calculadora.js"></script>
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_plugin.js"></script>

<script type="text/javascript">
		$(window).on("load", function() {			
			llenarDataLayer(TAG_MSJ_NO_CLIENTE,"");
		});
</script>

<script type="text/javascript">

$(document).ready(function(){
	transicion_barra_progreso(2,500);
	$("#correoResultado").css("display","none");	
});

$("input[type=checkbox][name=autorizacion]").change(function () {
	var tipo=$("input[name=autorizacion]:checked").val();
	if(tipo=="1"){
		$("#correoResultado").css("display","block");
	}else if(tipo=="2"){
		$("#correoResultado").css("display","none");
	}else if(tipo==null){
		$("#correoResultado").css("display","none");
	}	
   });
   

$("#correo_user").blur(function(){correo($("#correo_user"));});
function correo(obj){
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(obj.val()=='' || !regex.test(obj.val())){
		obj.tooltip({trigger : 'hover', title : 'Escribe un correo válido'});
		obj.addClass('form-control-error');
		return 0;
	}else{
		obj.tooltip('destroy');
		obj.removeClass('form-control-error');
		return 1;
	}
}


$("#numeroDocumento").blur(function(){ver_documento();});

$("#btnIniciar").click(function(){
	if(ver_documento()!=0){
		$("#divInicio").removeClass("hidden");
		$("#divInicio").show();
		desbloquearCalculador();
		limpiarFormulario();
	}	
});

</script>
</tiles:putAttribute>
<tiles:putAttribute name="googletagrmkt">
<!-- Google Code para etiquetas de remarketing MCC -->
<!--------------------------------------------------
Es posible que las etiquetas de remarketing todavía no estén asociadas a la información de identificación personal o que estén en páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar la etiqueta, consulte http://google.com/ads/remarketingsetup.
--------------------------------------------------->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 977810892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/977810892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</tiles:putAttribute>
</tiles:insertDefinition>