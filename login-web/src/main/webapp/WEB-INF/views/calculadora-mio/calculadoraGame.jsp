<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Calculadora MIO Game</tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/calculadora-mio-game.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">

<style>

</style>

</tiles:putAttribute>

<tiles:putAttribute name="conversioncodefacebok">
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	
	fbq('init', '254908304633078');
	fbq('track', "PageView");</script>
	<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=254908304633078&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
</tiles:putAttribute>
<tiles:putAttribute name="body">

<div class="row fondo-calculadora">
	<div class="col-sm-12">
	<div class="row">
		<div class="col-sm-3 col-xs-5" style="margin-top:10px">
			<img src="${pageContext.request.contextPath}ST/img/calculadora-mio/logo_mioNew.png" class="img-responsive">
		</div>
		<div class="col-sm-9 col-xs-7" style="margin-top: 20px;padding-left:0px;">
			<span class="titulo-calculadora-mio-game">
				Conquista de forma programada eso que tanto quieres
			</span>
		</div>
	</div>

	<div class="row ">
		<div class="col-sm-12">
			<div class="init-step1" id="barraProgreso">
				<form novalidate="novalidate" class="form-horizontal tooltip-validation transfer-filter">
					<div class="wizard">
						<div data-title="Cálculo" data-multistep="true">
						</div>
						<div data-title="Solicitud" data-multistep="true">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-offset-9 col-sm-3 padding_titulo_principal">
			<div id="pdfCalculadora" class="padding_right_35 text_right hidden hidden-xs">
			<form action="<c:url value="calculadora-mio-pdf"/>" target="_blank">
				<button type="submit" class="btn btn-calculadora btn_pdf">Guardar en PDF</button>
			</form>			
			</div>
		</div>
	</div>
	<div class="row margin_top_20">		
		<div class="col-sm-5" style="text-align: right;">
			<div class="titulo-calculadora-combo">Mi meta es invertir para conseguir:</div>
		</div>
		<div class="col-sm-6" style="text-align: left;">
			<div class="form-group select combo-responsive" style="width: 100%">
				<select class="errorTooltip form-control form-select combo-responsive" id="opcionCalculadora" name="opcionCalculadora" autocomplete="off">
				<c:forEach var="opcCalculadora" items="${opciones}">
					<option value="${opcCalculadora.valor}">${opcCalculadora.descripcion}</option>
				</c:forEach> 
				</select>
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>
	<div class="row">
		<div class="col-sm-5" style="text-align: right;">
			<div class="titulo-calculadora-combo">Documento de Identidad:</div>
		</div>
		<div class="col-sm-3">
			<div class="form-group select combo-responsive" style="width: 100%">
				<select class="errorTooltip form-control form-select combo-responsive" id="tipoDocumento" name="tipoDocumento" autocomplete="off">
				<c:forEach var="tipoDocumento" items="${tipoDocumentos}">
					<option value="${tipoDocumento.codigo}">${tipoDocumento.nombre}</option>
				</c:forEach> 
				</select>
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		<div class="col-sm-3" style="text-align: left;">
			<div class="form-group" style="width: 100%">
				<input type="text" class="errorTooltip form-control txt-numero-documento" id="numeroDocumento" name="numeroDocumento" placeholder="Número de Documento" autocomplete="off" style="width: 100%">
				<span class="help-block error-bbva"></span>
			</div>
		</div>
		<div class="col-sm-1"></div>
	</div>	
	
	<div class="col-sm-12">	
		<div class="text_center margin_top_10 margin_bottom_10">	
			<button class="btn-iniciarPixel" type="button" id="btnIniciar"></button>			
		</div>		
	</div>
	<div id="divInicio" class="hidden">
	<div class="col-sm-12 margin_top_20">
		<div class="column-5">
			<div class="box-calculadora">
				<img class="box-calculadora-img" src="${pageContext.request.contextPath}ST/img/calculadora-mio/box_calculadora_prueba.png">
				<div class="titulo-box-calculadora">¿Cómo quieres calcular tu objetivo?</div>
				<div class="radio">
					<label class="tipo-calculadora">
						<input type="radio" id="tipo" name="tipo" value="1" checked="checked">
						<span class="cm2"><span class="cm2-icon"></span></span>
						Por el monto total que quiero alcanzar
					</label>
		        </div>
		        <div class="radio">
					<label class="tipo-calculadora">
						<input type="radio" id="tipo" name="tipo" value="2">
						<span class="cm2"><span class="cm2-icon"></span></span>
						Por el monto que puedo aportar periódicamente
					</label>
		        </div>
		        <hr style="border-top: 1px solid #ee9638;margin-bottom: 0px">
				<div id="divTituloAporte" class="titulo2-box-calculadora">¿Cuánto necesitarías para lograrlo?</div>
				
				<div class="aporte-calculadora">
					<div class="monto-calculadora">
						<div class="form-group">
							<input data-placement="top" type="text" style="width: 140px;" class="errorTooltip form-control txt-global monto-soles" id="monto" name="monto" placeholder="" autocomplete="off" maxlength="10">
						</div>
					</div>
					<div class="divisa-calculadora">
						<div class="radio">
							<label class="soles-calculadora">
								<input type="radio" id="divisaCalculadora" name="divisaCalculadora" value="SOLES" checked="checked">
								<span class="cm2"><span class="cm2-icon"></span></span>
								Soles
							</label>
				        </div>
					</div>
					<div class="divisa-calculadora">
						<div class="radio">
							<label class="dorales-calculadora">
								<input type="radio" id="divisaCalculadora" name="divisaCalculadora" value="DOLARES">
								<span class="cm2"><span class="cm2-icon"></span></span>
								Dólares
							</label>
				        </div>
					</div>
				</div>
				
				<div class="subtitulo-box-calculadora">De manera:</div>
				<div class="periodos-calculadora">
					<div class="periodo-calculadora">
						<div class="radio">
							<label>
								<input type="radio" id="periodo" name="periodo" value="12" checked="checked">
								<span class="cm2" style="margin-right:.2em"><span class="cm2-icon"></span></span>
								Mensual
							</label>
				        </div>
					</div>
					<div class="periodo-calculadora">
						<div class="radio">
							<label>
								<input type="radio" id="periodo" name="periodo" value="24">
								<span class="cm2" style="margin-right:.2em"><span class="cm2-icon"></span></span>
								Quincenal
							</label>
				        </div>
					</div>
					<div class="periodo-calculadora">
						<div class="radio">
							<label>
								<input type="radio" id="periodo" name="periodo" value="48">
								<span class="cm2" style="margin-right:.2em"><span class="cm2-icon"></span></span>
								Semanal
							</label>
				        </div>
					</div>
				</div>
				<hr style="border-top: 1px solid #ee9638;margin-top: 0px;margin-bottom: -5px">
				<div class="meses-calculadora">
					<div style="display: table-cell;color: #0069b1; font-size: 20px; font-weight: bold; padding-right: 20px;vertical-align: top;">
						¿En cuánto tiempo?
					</div>
					<div style="display: table-cell;padding-right: 20px;vertical-align: middle;">
						<input data-placement="top" type="number" pattern="\d*" style="width: 73px;padding-left: 20px;" class="errorTooltip form-control txt-global" id="meses" name="meses" placeholder="" autocomplete="off" />
					</div>
					<div style="display: table-cell; font-weight: bold;vertical-align: middle;">
						meses
					</div>
				</div>
				<div class="btn-calcular-box">
					<button id="btnCalcular" type="button" class="btn-calcularPixel"></button>
				</div>
			</div>
			
		</div>
		<div class="column-2 col-arrow-calculadora" align="center">
			<img src="${pageContext.request.contextPath}ST/img/calculadora-mio/flecha.png" class="arrow-calculadora">
		</div>
		<div class="column-5 box-resultado box-cuota">
			<div id="divResultado">
				<div style="color: #0069b1; font-size: 20px;text-align: center; font-weight: bold; padding-top: 150px; padding-bottom:150px">Descubre el resultado aquí</div>
			</div>
			<div class="col-sm-12 text_center" style="padding-top: 28px;padding-left: 0px; padding-right: 0px">
				<span id="spanDescubreResultado" class="display_none" style="font-family: BBVA Web Book; font-size: 15px; color:#429d11">
					Descubre cómo hacer realidad tu objetivo
				</span>
			</div>		
			
			<div class="col-sm-12 text_center" style="padding-top: 18px;">
				<form action="<c:url value="suscripcion-programada"/>" method="post">
					<button class="btn-continuarPixel" id="btnSuscribir"></button>
				</form>
			</div>
			
			
			<div id="divImagenGame" style="height: 87px;text-align: right;position: absolute;bottom: 0;right: 0;">
				<img style="margin-right: 10px;margin-top: -20px;" src="/forleaST/img/calculadora-mio/resultado_cofre_cerrado.png">
			</div>
			<div id="divVolverCalcular" style="width: 100%; height: 20px;text-align: center;position: absolute;bottom: 0;left: 0;">
				<span id="spanVolverCalcular" class="display_none" style="font-family: BBVA Web Book; font-size: 15px; color:#ffffff; text-decoration: underline; cursor: pointer; cursor: hand;">Volver a Calcular</span>
			</div>			
		</div>
	</div>
	
	
	<div class="col-sm-12 margin_top_20 hidden-xs">
		<div class="disclaimer-calculadora">
			<div style="padding-bottom: 5px;">
				<span class="disclaimer-azul-calculadora">(*) El resultado de los cálculos es referencial.</span> La tasa de rentabilidad anual asumida para el cálculo, es la tasa equivalente al promedio de 
				las rentabilidades anuales de los últimos 5 años. En Soles, considera el promedio de las rentabilidades anuales del BBVA Cash Soles y en dólares el promedio de las rentabilidades anuales del 
				BBVA Cash Dólares, para el periodo seleccionado. La tasa de rentabilidad estimada no debe ser interpretada ni entendida como una promesa de rentabilidad constante en el tiempo.
			</div>
			<div style="padding-bottom: 5px;">
				Los fondos mutuos son administrados por BBVA Asset Management Continental SA SAF y comercializados por BBVA Continental.
				MIO Suscripción programada esta disponible para la suscripción de los fondos BBVA Cash Soles y BBVA Cash Dólares.
			</div>
			<div style="padding-bottom: 5px;">
				Consulta el detalle de rentabilidades anuales históricas de los fondos BBVA Cash Soles y BBVA Cash Dólares en nuestro sitio web y en las oficinas del BBVA Continental.
			</div>
			<div style="padding-bottom: 5px;">
				<span class="disclaimer-azul-calculadora">La rentabilidad o ganancia obtenida en el pasado</span> no garantiza que se repita en el futuro. Esta rentabilidad no incluye el efecto 
				de las comisiones de suscripción y rescate, ni el impuesto a la renta.
			</div>
		</div>
	</div>
	
	<div class="col-sm-12 margin_top_20 hidden-sm hidden-md hidden-lg hidden-xl">
		<div class="disclaimer-calculadora">
			<div id="content2" style="height: 1.5em;overflow: hidden">
				<div style="padding-bottom: 5px;">
				<span class="disclaimer-azul-calculadora">(*) El resultado de los cálculos es referencial.</span> La tasa de rentabilidad anual asumida para el cálculo, es la tasa equivalente al promedio de 
				las rentabilidades anuales de los últimos 5 años. En Soles, considera el promedio de las rentabilidades anuales del BBVA Cash Soles y en dólares el promedio de las rentabilidades anuales del 
				BBVA Cash Dólares, para el periodo seleccionado. La tasa de rentabilidad estimada no debe ser interpretada ni entendida como una promesa de rentabilidad constante en el tiempo.
				</div>
				<div style="padding-bottom: 5px;">
					Los fondos mutuos son administrados por BBVA Asset Management Continental SA SAF y comercializados por BBVA Continental.
					MIO Suscripción programada esta disponible para la suscripción de los fondos BBVA Cash Soles y BBVA Cash Dólares.
				</div>
				<div style="padding-bottom: 5px;">
					Consulta el detalle de rentabilidades anuales históricas de los fondos BBVA Cash Soles y BBVA Cash Dólares en nuestro sitio web y en las oficinas del BBVA Continental.
				</div>
				<div style="padding-bottom: 5px;">
					<span class="disclaimer-azul-calculadora">La rentabilidad o ganancia obtenida en el pasado</span> no garantiza que se repita en el futuro. Esta rentabilidad no incluye el efecto 
					de las comisiones de suscripción y rescate, ni el impuesto a la renta.
				</div>
				
			</div>
			<div>
				<span  id="leermas2" style="text-decoration: underline;color:#9FA9B0;font-weight: bold;">Ver más</span>
				<span id="ocultar2"  style="text-decoration: underline;color:#9FA9B0;font-weight: bold;">Ver menos</span>
			</div>
									
		</div>
	</div>
</div>	
</div>
</div>

</tiles:putAttribute>

<tiles:putAttribute name="script">
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_calculadora.js"></script>
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_plugin.js"></script>

<script type="text/javascript">
	$(window).on("load", function() {
		//resetDatosIngreso();
		llenarDataLayer(TAG_MI_META_CALCULO,"");
	});
</script>

<script type="text/javascript">
function hidden_btns(){
	$("#pdfCalculadora").addClass("hidden");
	$("#btnSuscribir").addClass("display_none");
	$("#btnSuscribir1").addClass("display_none");
}
$(document).ready(function(){
	$(".subtitulo-box-calculadora").css("display","none");
	$(".periodos-calculadora").css("display","none");
 	$(".titulo2-box-calculadora").css("padding-top", "40px");
	$(".aporte-calculadora").css("padding-bottom", "42px");
	$("#numeroDocumento").numeric({
		maxPreDecimalPlaces : 8,
		maxDigits           : 8,
		allowMinus          : false,
		allowThouSep        : false,
		allowDecSep         : false
	});
	$("#monto").mask('10,000,000', {reverse: true, translation:  {'1': {pattern: /[1-9]/}}});
	$("#opcionCalculadora").focus();
	
	$("#ocultar2").addClass("hidden");	
	
	var reducedHeight2 = $('#content2').height();
	
	$('#leermas2').click(function() {		
	    $('#content2').css('height', 'auto');
	    var fullHeight = $('#content2').height();
	    $('#content2').height(reducedHeight2);
	    $('#content2').animate({height: fullHeight}, 500);	    
	    $("#leermas2").addClass("hidden");
	    $("#ocultar2").removeClass("hidden");
	});
	
	$('#ocultar2').click(function() {
		$('#content2').height(reducedHeight2);
		$('#content2').animate({height: reducedHeight2}, 500);
		$("#leermas2").removeClass("hidden");
		$("#ocultar2").addClass("hidden");
	});		
});


$("#opcionCalculadora").click(function(){
	llenarDataLayer(TAG_MI_META_EVENTO_CLICK,DESCRIPCION_MI_META);
});	

$("#tipoDocumento").click(function(){
	llenarDataLayer(TAG_MI_META_EVENTO_CLICK,DESCRIPCION_DOCUMENTO_IDENTIDAD);
});	

$("#numeroDocumento").click(function(){
	llenarDataLayer(TAG_MI_META_EVENTO_CLICK,DESCRIPCION_NUMERO_DOCUMENTO);
});



$("#monto, #meses").click(function () {
   	hidden_btns();
});
$("#numeroDocumento").blur(function(){numeroDocumento();});
$("#monto").blur(function(){ver_monto();});
$("#monto").keypress(function(e) {
	code = e.which || e.keyCode;
	if($.trim($("#monto").val()).length==0){
		if(code==48){
			e.preventDefault();			
		}	
	}
});
$("#meses").blur(function(){ver_meses();});
$("#meses").keyup(function(e){
	if(!isMobile()){
		return;
	}
	code = e.which || e.keyCode;
    // Allow: backspace, delete, tab, escape, enter
    if ($.inArray(code, [8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (code == 65 && (e.ctrlKey == true || e.metaKey == true)) || 
         // Allow: home, end, left, right, down, up
        (code >= 35 && code <= 40)) {
             // let it happen, don't do anything
             return;    	
    }
	
	if($.trim($("#meses").val()).length>3){
		$("#meses").val($("#meses").val().substring(0,3));
		e.preventDefault();
		return false;
	}
	
	if(!(/^[0-9]+$/).test(String.fromCharCode(code))){
		//si no es numero, puede ser 229
		//229 -> código teclado numérico android, para todos los numeros
		if(code!=229){
			$("#meses").val($("#meses").val().substring(0,$("#meses").val().length-1));
			e.preventDefault();
		}	
	}
});
$("#meses").keypress(function(e){
	if(isMobile()){
		return;
	}
	code = e.which || e.keyCode;

    // Allow: backspace, delete, tab, escape, enter
    if ($.inArray(code, [8, 9, 27, 13, 110, 190]) !== -1
       // Allow: Ctrl+A, Command+A
       || (code == 65 && (e.ctrlKey == true || e.metaKey == true))) 
        // Allow: home, end, left, right, down, up
        //    (code >= 35 && code <= 40)) 
    {
             // let it happen, don't do anything
             return;    	
    }
	
	if($.trim($("#meses").val()).length==3){
		e.preventDefault();
	}	
	
	if(!(/^[0-9]+$/).test(String.fromCharCode(code))){
		e.preventDefault();
	}
	
});
$("#tipoDocumento").change(function(){
	$("#numeroDocumento").val("");
	$("#numeroDocumento").unbind();
	//$("#numeroDocumento").blur(function(){numeroDocumento();});
	if($("#tipoDocumento").val()=='L'){
		$("#numeroDocumento").numeric({
			maxPreDecimalPlaces : 8,
			maxDigits           : 8,
			allowMinus          : false,
			allowThouSep        : false,
			allowDecSep         : false
		});
	}else if($("#tipoDocumento").val()=='E' || $("#tipoDocumento").val()=='D'){
		$("#numeroDocumento").alphanum({
			allow: '/-',
			maxLength: 12,
			allowSpace: false,
			disallow: '°¡¿´¨.'
		});
	}else if($("#tipoDocumento").val()=='P'){
		$("#numeroDocumento").alphanum({
			maxLength: 12,
			allowSpace: false,
			disallow: '°¡¿´¨.'
		});
	}
	
});

$("input[name='tipo']").click(function(){
	if($("input[name='tipo']:checked").val()=='1'){
		$(".subtitulo-box-calculadora").css("display","none");
		$(".periodos-calculadora").css("display","none");
		$(".titulo2-box-calculadora").css("padding-top", "40px");
		$(".aporte-calculadora").css("padding-bottom", "42px");
		$("#divTituloAporte").html('¿Cuánto necesitarías para lograrlo?');
	}else{
		$(".subtitulo-box-calculadora").css("display","");
		$(".periodos-calculadora").css("display","");
		$(".titulo2-box-calculadora").css("padding-top", "");
		$(".aporte-calculadora").css("padding-bottom", "");
		$("#divTituloAporte").html('¿Cuánto aportarías?');
	}
});
$("input[name='divisaCalculadora']").click(function(){
	if($("input[name='divisaCalculadora']:checked").val()=='SOLES'){
		$("#monto").removeClass("monto-dolares");
		$("#monto").addClass("monto-soles");
	}else{
		$("#monto").removeClass("monto-soles");
		$("#monto").addClass("monto-dolares");
	}
});



$("#btnIniciar").click(function(){	
	
	llenarDataLayer(TAG_MI_META_EVENTO_CLICK,DESCRIPCION_BOTON_INICIO);
	if(numeroDocumento()!=0){
		
		$.ajax({
			contentType : "application/json",
			url : '<c:url value="validar-cliente"/>',
			data : {
				tipoDocumento: $("#tipoDocumento").val(),
				numeroDocumento: $("#numeroDocumento").val(),
				objetivo: $("#opcionCalculadora option:selected").text(),				
			},
			timeout : 3000,
			success : function(clienteBbvaCash) {
				indInicio = "C";
				indClienteBBVACash = clienteBbvaCash.idClienteBbvaCash;
				indAfiliacionBXI   = clienteBbvaCash.indAfiliacionBXI;
				objetivo		   = $("#opcionCalculadora option:selected").text();				
				llenarDataLayer(TAG_MI_META_CALCULO,"");				
				$("#divInicio").removeClass("hidden");
				$("#divInicio").show();
				desbloquearCalculador();
				limpiarFormulario();	    
			    
				$("#numeroDocumento").prop("disabled", true);
				$("#btnIniciar").prop("disabled", true);
				
				var posicion = $(".box-calculadora").offset().top - 10;
				transicion(posicion);
			},
			error : function(e) {
	
			},
			done : function(e) {
				
			}
		});						
	}	
});

$("#btnCalcular").click(function(){
	var enviar=1;
	enviar=enviar*ver_monto();
	enviar=enviar*ver_meses();
	
	if(enviar==1){
		$.ajax({
			contentType : "application/json",
			url : '<c:url value="calculadora-mensaje"/>',
			data : {
				tipoDocumento: $("#tipoDocumento").val(),
				numeroDocumento: $("#numeroDocumento").val(),
				tipo: $("input[name='tipo']:checked").val(),
				monto: $('#monto').val(),
				divisa: $("input[name='divisaCalculadora']:checked").val(),
				periodo: $("input[name='periodo']:checked").val(),
				meses: $('#meses').val(),
				objetivo: $("#opcionCalculadora option:selected").text()
			},
			timeout : 3000,
			success : function(calculadora) {
				if($("#opcionCalculadora").val()=='opc_casa'){
					$("#divImagenGame").html('<img style="margin-right: 10px;margin-top: -90px;" src="${pageContext.request.contextPath}ST/img/calculadora-mio/resultado_opc_depa.png">');
				}
				if($("#opcionCalculadora").val()=='opc_carro'){
					$("#divImagenGame").html('<img style="margin-right: 10px;margin-top: -35px;" src="${pageContext.request.contextPath}ST/img/calculadora-mio/resultado_opc_auto.png">');
				}
				if($("#opcionCalculadora").val()=='opc_estudios'){
					$("#divImagenGame").html('<img style="margin-right: 10px;margin-top: -75px;" src="${pageContext.request.contextPath}ST/img/calculadora-mio/resultado_opc_estudios.png">');
				}
				if($("#opcionCalculadora").val()=='opc_vacaciones'){
					$("#divImagenGame").html('<img style="margin-right: 10px;margin-top: -55px;" src="${pageContext.request.contextPath}ST/img/calculadora-mio/resultado_opc_vacaciones.png">');
				}
				if($("#opcionCalculadora").val()=='opc_matrimonio'){
					$("#divImagenGame").html('<img style="margin-right: 10px;margin-top: -55px;" src="${pageContext.request.contextPath}ST/img/calculadora-mio/resultado_opc_matrimonio.png">');
				}
				if($("#opcionCalculadora").val()=='opc_otro'){
					$("#divImagenGame").html('<img style="margin-right: 10px;margin-top: -50px;" src="${pageContext.request.contextPath}ST/img/calculadora-mio/resultado_opc_otro.png">');
				}
				var resultado='';
				if ($("input[name='tipo']:checked").val()=="1"){
					resultado='<div style="text-align:center; color: #0069b1; font-size: 18px;font-weight: bold;padding-top: 20px;">El resultado de los cálculos es referencial (*)</div>';
					resultado=resultado+'<div style="text-align:center;color: #0069b1; font-size: 18px;padding-top: 18px;padding-bottom:18px">Para cumplir tu objetivo necesitarías:</div>';
					$.each(calculadora.cuotaPeriodo, function(key, value) {
						if(key=='semanal'){
							resultado=resultado+'<div style="display: table; width: 100%">';
							resultado=resultado+'<div style="display: table-cell;color: #0069b1; font-size: 16px;text-align:right;width:30%;padding-right:10px">Semanal</div>';
							resultado=resultado+'<div style="display: table-cell;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;text-align:center;width:52%">'+value+'</div>';
							resultado=resultado+'<div style="display: table-cell;color: #0069b1; font-size: 16px;font-weight: bold;text-align:left;width:18%;padding-left:10px">ó</div>';
							resultado=resultado+'</div>';
						}
						if(key=='quincenal'){
							resultado=resultado+'<hr style="margin-top: 1px;margin-bottom: 1px;">';
							resultado=resultado+'<div style="display: table; width: 100%;">';
							resultado=resultado+'<div style="display: table-cell;color: #0069b1; font-size: 16px;text-align:right;width:30%;padding-right:10px">Quincenal</div>';
							resultado=resultado+'<div style="display: table-cell;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;text-align:center;width:52%">'+value+'</div>';
							resultado=resultado+'<div style="display: table-cell;color: #0069b1; font-size: 16px;font-weight: bold;text-align:left;width:18%;padding-left:10px">ó</div>';
							resultado=resultado+'</div>';
						}
						if(key=='mensual'){
							resultado=resultado+'<hr style="margin-top: 1px;margin-bottom: 1px;">';
							resultado=resultado+'<div style="display: table; width: 100%;">';
							resultado=resultado+'<div style="display: table-cell;color: #0069b1; font-size: 16px;text-align:right;width:30%;padding-right:10px">Mensual</div>';
							resultado=resultado+'<div style="display: table-cell;color: #0069b1;font-weight: bold;font-family: BBVA Web Book;text-align:center;width:52%">'+value+'</div>';
							resultado=resultado+'<div style="display: table-cell;color: #0069b1; font-size: 16px;font-weight: bold;text-align:left;width:18%;padding-left:10px">&nbsp;</div>';
							resultado=resultado+'</div>';
						}
					});				
				}else{
					resultado='<div style="text-align: center;color: #0069b1; font-size: 18px;font-weight: bold;padding-top: 30px;">El resultado del cálculo es referencial (*)</div>';
					resultado=resultado+'<div style="text-align: center;color: #0069b1; font-size: 20px;padding-top: 30px;">Al finalizar tu inversión,</div>';
					resultado=resultado+'<div style="text-align: center;color: #0069b1; font-size: 20px;">tendrías un total de:</div>';
					resultado=resultado+'<div style="text-align: center;color: #0069b1; font-size: 36px;font-weight: bold;font-family: BBVA Web Book;padding-top: 20px;">'+calculadora.divisaSimbolo +" "+ calculadora.montoFuturo+'</div>';
					resultado=resultado+'<div style="text-align: center;color: #006ec1; font-size: 15px;font-weight: bold;">(incluyendo rentabilidad: '+calculadora.divisaSimbolo +" "+ calculadora.rentabilidad+')</div>';
				}
				bloquearCalculador();				
				$("#divResultado").html(resultado);
				$("#pdfCalculadora").removeClass("hidden");
				$("#spanVolverCalcular").removeClass("display_none");
				$("#spanDescubreResultado").removeClass("display_none");
				$("#btnSuscribir").removeClass("display_none");
				$("#btnSuscribir1").removeClass("display_none");
				var posicion = $(".box-resultado").offset().top - 10;
				transicion(posicion);
				_satellite.track("fondosMutuosSimuladorMensajeCalcular");
			},
			error : function(e) {
	
			},
			done : function(e) {
				
			}
		});
	}
});
$("#spanVolverCalcular").click(function(){	
	desbloquearCalculador();
	limpiarFormulario();
	var posicion = $(".box-calculadora").offset().top - 10
	transicion(posicion);
});
function bloquearCalculador(){
	$("#btnCalcular").attr("disabled","disabled");
	$("input[name=tipo][value=1]").attr("disabled","disabled");
	$("input[name=tipo][value=2]").attr("disabled","disabled");
	$("#monto").attr("disabled","disabled");
	$("input[name=divisaCalculadora][value=SOLES]").attr("disabled","disabled");
	$("input[name=divisaCalculadora][value=DOLARES]").attr("disabled","disabled");
	$("input[name=periodo][value=12]").attr("disabled","disabled");
	$("input[name=periodo][value=24]").attr("disabled","disabled");
	$("input[name=periodo][value=48]").attr("disabled","disabled");
	$("#meses").attr("disabled","disabled");
}
function desbloquearCalculador(){
	$("#btnCalcular").prop("disabled", false);
	$("input[name=tipo][value=1]").prop("disabled", false);
	$("input[name=tipo][value=2]").prop("disabled", false);
	$("#monto").prop("disabled", false);
	$("input[name=divisaCalculadora][value=SOLES]").prop("disabled", false);
	$("input[name=divisaCalculadora][value=DOLARES]").prop("disabled", false);
	$("input[name=periodo][value=12]").prop("disabled", false);
	$("input[name=periodo][value=24]").prop("disabled", false);
	$("input[name=periodo][value=48]").prop("disabled", false);
	$("#meses").prop("disabled", false);
}
function limpiarFormulario(){
	$("#btnCalcular").prop("disabled", false);
	$("input[name=tipo][value=1]").prop('checked', true);
	if($("input[name='tipo']:checked").val()=='1'){
		$(".subtitulo-box-calculadora").css("display","none");
		$(".periodos-calculadora").css("display","none");
		$(".titulo2-box-calculadora").css("padding-top", "40px");
		$(".aporte-calculadora").css("padding-bottom", "42px");
		$("#divTituloAporte").html('¿Cuánto necesitarías para lograrlo?');
	}
	$("input[name=periodo][value=12]").prop('checked', true);
	$("#monto").val("");
	$("#monto").removeClass("monto-dolares");
	$("#monto").addClass("monto-soles");
	$("input[name=divisaCalculadora][value=SOLES]").prop('checked', true);
	$("#meses").val("");
	$("#divResultado").html("<div style='color: #0069b1; font-size: 20px;text-align: center; font-weight: bold; padding-top: 150px; padding-bottom: 150px;'>Descubre el resultado aquí</div>");
	$("#divImagenGame").html('<img style="margin-right: 10px;margin-top: -20px;" src="/forleaST/img/calculadora-mio/resultado_cofre_cerrado.png">');
	$("#pdfCalculadora").addClass("hidden");
	$("#spanVolverCalcular").addClass("display_none");
	$("#spanDescubreResultado").addClass("display_none");
	$("#btnSuscribir").addClass("display_none");
	$("#btnSuscribir1").addClass("display_none");
}

function numeroDocumento(){
	if(
		$.trim($("#numeroDocumento").val())=='' || 
		($("#tipoDocumento").val()=='L' && $.trim($("#numeroDocumento").val()).length!=8) ||
		($("#tipoDocumento").val()=='E' && $.trim($("#numeroDocumento").val()).length>12) ||
		($("#tipoDocumento").val()=='D' && $.trim($("#numeroDocumento").val()).length>12) ||
		($("#tipoDocumento").val()=='P' && $.trim($("#numeroDocumento").val()).length>12)
	){
		$("#numeroDocumento").tooltip({trigger : 'hover', title : 'Escribe un número de documento válido'});
		$("#numeroDocumento").addClass('form-control-error');						
		return 0;
	}else{
		$('#numeroDocumento').tooltip('destroy');
		$("#numeroDocumento").removeClass('form-control-error');				
		return 1;
	}
}

function ver_monto(){
	var tmonto=$("#monto").val().replace(/,/g,"")*1.0;
	if(tmonto==0 || isNaN(tmonto)){
		$("#monto").tooltip({trigger : 'hover', title : 'Por favor, escribe un monto válido'});
		$("#monto").addClass('form-control-error');
		return 0;
	}else{
		$('#monto').tooltip('destroy');
		$("#monto").removeClass('form-control-error');
		return 1;
	}
}

function ver_meses(){
	if($("#meses").val()=='' || $("#meses").val()=='0'){
		$("#meses").tooltip({trigger : 'hover', title : 'Por favor, escribe un número de meses de 3 dígitos como máximo'});
		$("#meses").addClass('form-control-error');
		return 0;
	}else{
		if(!((/^[0-9]{1,3}$/).test($("#meses").val()))){
			$("#meses").tooltip({trigger : 'hover', title : 'Por favor, escribe un número de meses de 3 dígitos como máximo'});
			$("#meses").addClass('form-control-error');
		}else{
			$('#meses').tooltip('destroy');
			$("#meses").removeClass('form-control-error');
			return 1;			
		}
	}
}

function transicion(posicion){
	if(top == window){
		//scroll misma pagina
		$('html, body').animate({ scrollTop: posicion }, 500);
	}else{
		//scroll tridion
		top.postMessage("[scrollTridion]"+posicion, '*');
	}

	
}

function isMobile(){
	var device = navigator.userAgent

	if (device.match(/Iphone/i) || device.match(/iPad/i)|| device.match(/Ipod/i)|| device.match(/Android/i)|| device.match(/J2ME/i)|| device.match(/BlackBerry/i)|| device.match(/iPhone|iPad|iPod/i)|| device.match(/Opera Mini/i)|| device.match(/IEMobile/i)|| device.match(/Mobile/i)|| device.match(/Windows Phone/i)|| device.match(/windows mobile/i)|| device.match(/windows ce/i)|| device.match(/webOS/i)|| device.match(/palm/i)|| device.match(/bada/i)|| device.match(/series60/i)|| device.match(/nokia/i)|| device.match(/symbian/i)|| device.match(/HTC/i))
	 { 
		return true;
	}
	else
	{
		return false;
	}
}
</script>
</tiles:putAttribute>
<tiles:putAttribute name="googletagrmkt">
<!-- Google Code para etiquetas de remarketing MCC -->
<!--------------------------------------------------
Es posible que las etiquetas de remarketing todavía no estén asociadas a la información de identificación personal o que estén en páginas relacionadas con las categorías delicadas. Para obtener más información e instrucciones sobre cómo configurar la etiqueta, consulte http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 977810892;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/977810892/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</tiles:putAttribute>
</tiles:insertDefinition>