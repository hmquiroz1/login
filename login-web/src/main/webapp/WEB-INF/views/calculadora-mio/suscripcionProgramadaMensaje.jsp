<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Suscripción Programada</tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/suscripcion-programada.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">
	
</tiles:putAttribute>

<tiles:putAttribute name="body">

<div class="row">
	<div class="col-sm-10 titulo_nivel2 padding_titulo_principal padding_left_form" >
			Conquista de manera programada lo que tanto quieres
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="init-step2">
			<form novalidate="novalidate" class="form-horizontal tooltip-validation transfer-filter">
				<div class="wizard">
					<div data-title="Cálculo" data-multistep="true">
					</div>
					<div data-title="Solicitud" data-multistep="true">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div >

<div class="row">
	<div class="col-sm-12" align="center">
		<h3 class="font-blue"><b> <img src="${pageContext.request.contextPath}ST/img/icono-check.png">  ¡<c:out value="${nombreCompleto}" />, hemos recibido tu solicitud!</b></h3>
		<c:choose>
		    <c:when test="${tipoCliente=='1'}">
		        <p class="text-center">En un máximo de 48 horas nos comunicaremos contigo para que puedas acceder a: </p>
		    </c:when>
		    <c:otherwise>
		        <p class="centerRD_left">Recuerda que puedes acercarte a cualquier <a href="http://www.ubicanosbbvacontinental.pe" target="_blank" class="font-blue">oficina BBVA</a> para que podamos programar tus aportes de fondos mutuos inmediatamente</p>
		    </c:otherwise>
		</c:choose>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 text_center">
		<c:choose>
		    <c:when test="${tipoCliente=='1'}">
				<c:forEach var="servicio" items="${serviciosElegidos}">
					<div class="row">
						<div class="col-sm-12">
							<span class="font-seguro">${servicio} </span>
						</div>
					</div>
				</c:forEach>
		    </c:when>
		    <c:otherwise>
		         <br>
		    </c:otherwise>
		</c:choose>
		<br>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
	<div class="seccion_resaltada">
		<div class="row">
			<div class="col-sm-12 msj">
				<span class="icm_libre">
				</span>
				<p>
				<span class="texto_14 padding_left_40">
				<b>Cómo funciona <span class="texto_resaltado">MÍO - Suscripción Programada</span></b>
				</span>
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<p class="texto_12 item_resultado">Con esta autorización  se cargará de tu cuenta de ahorros el monto que tú decidas con la frecuencia que tú elijas de manera automática.</p>
			</div>
			<div class="col-sm-6">
				<p class="texto_12 item_resultado">Para que se realice el cargo automático es indispensable tener el saldo necesario disponible.</p>
			</div>				
		</div>
		<div class="row">
			<div class="col-sm-6">
				<p class="texto_12 item_resultado">Si en un periodo no existe el saldo disponible, no se acumalará para el siguiente aporte programado.</p>					
			</div>
			<div class="col-sm-6">
				<p class="texto_12 item_resultado">Si la fecha de cargo cae feriado o fin de semana se realizará al día útil siguiente.</p>					
			</div>				
		</div>
		<div class="row">
			<div class="col-sm-6">
				<p class="texto_12 item_resultado">Este servicio permanecerá activo mientras tú lo decidas.</p>
			</div>
			<div class="col-sm-6">
				
			</div>				
		</div>
	</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12">
		<div class="msj">
			<span class="icm_estrella">
			</span>
			<c:choose>
			    <c:when test="${tipoCliente=='1'}">
					<span class="texto_12 texto_resaltado padding_left_form">
					<b>Recuerda que:</b>
					</span>
					<p class="texto_12 padding_left_form">
					Estos servicios se encuentran actualmente para <b>Fondos BBVA Cash Soles y BBVA Cash Dólares,</b> que invierten principalmente en depósitos a plazo y cuentas de ahorro.
					</p>
			    </c:when>
			    <c:otherwise>
					<span class="texto_12 texto_resaltado padding_left_form">
					<b>Te recomendamos usar</b>
					</span>
					<p class="texto_12 padding_left_form">
					Los <b>Fondos BBVA Cash Soles y BBVA Cash Dólares,</b> que invierten principalmente en depósitos a plazo y cuentas de ahorro.
					</p>
			    </c:otherwise>
			</c:choose>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 margin_bottom_10" align="center">
		<div align="center">
			<button type="button" class="btn btn-primary" id="btnFFMM">Ver más sobre Fondos Mutuos</button>
		</div>
	</div>
</div>
</div>

</tiles:putAttribute>

<tiles:putAttribute name="script">
<script>
$(document).ready(function(){
    $("#btnFFMM").click(function(event) {
    	var isInIFrame = (window.location != window.parent.location);
    	if(isInIFrame==true){
    		window.parent.location.href='https://www.bbvacontinental.pe/personas/ahorro-e-inversion/bbva-asset-management-continental-saf/';
    	}
    	else {
    		window.location.href='https://www.bbvacontinental.pe/personas/ahorro-e-inversion/bbva-asset-management-continental-saf/';
    	}
    });
}); 
</script>
</tiles:putAttribute>
</tiles:insertDefinition>