<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Calculadora MIO Home</tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/calculadora-mio.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">

</tiles:putAttribute>

<tiles:putAttribute name="body">

<div class="row margin_bottom_10">
	<div class="col-sm-9 titulo_nivel2 padding_titulo_principal">
			Conquista de manera programada lo que tanto quieres
	</div>
	<div class="col-sm-3 padding_titulo_principal">
		<div id="pdfCalculadora" class="padding_right_35 text_right">
		<form action="<c:url value="calculadora-mio-pdf"/>" target="_blank">
			<button type="submit" class="btn btn-calculadora btn_pdf">Guardar en PDF</button>
		</form>			
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="init-step1">
			<form novalidate="novalidate" class="form-horizontal tooltip-validation transfer-filter">
				<div class="wizard">
					<div data-title="Cálculo" data-multistep="true">
					</div>
					<div data-title="Solicitud" data-multistep="true">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<form id="formCalculadora" action="<c:url value="calculadora-mensaje"/>" method="post">
<input type="hidden" name="seleccion" value="${seleccion}">
<div class="row">
	<div class="col-sm-12 margin_top_10">
		<p><b>¿Qué objetivo quieres lograr?</b></p>
	</div>
</div>
<div class="row">
	<div class="col-sm-12 padding_objetivos margin_bottom_10">
		<div class="row" id="div_opciones">
			<c:forEach var="opcCalculadora" items="${opciones}">
			<div class="col-sm-2 padding_right_0 divRD" >
				<label class="text_center" id="label_${opcCalculadora.valor}">
					<input type="radio" value="${opcCalculadora.descripcion}" name="opcionCalculadora" class="${opcCalculadora.valor}" id="opc_${opcCalculadora.orden}"> 
					<span class="opciones_calculadora" id="label_img_${opcCalculadora.valor}"><img src="${pageContext.request.contextPath}ST/img/${opcCalculadora.valor}.png"></span>
					<span class="font-calculadora" id="label_txt_${opcCalculadora.valor}" data-toggle="tooltip" data-placement="top" title="${opcCalculadora.descripcion}">${opcCalculadora.descripcion}</span>
				</label>
			</div>
			</c:forEach>
		</div>
	</div>
</div>
<div class="row margin_bottom_10 margin_top_10">
	<div class="col-sm-12">
		<p><b>Calcula la inversión que necesitas para lograrlo</b></p>	
	</div>
</div>
<div class="row margin_bottom_10 margin_top_10">	
	<div class="col-sm-2ths">
		<div class="formulario_input min_height_panel" id="formulario_input">
		<div class="row">
			<div class="col-sm-12">
				<p class="subtitulo">
				¿Cómo quieres calcular tu objetivo?
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 padding_opciones">
				<label class="normal_13" id="label_tipo_1">
					<input type="radio" value="1" name="tipo" class="" checked="checked" id="tipo_monto">  
					<span class="" data-toggle="tooltip" data-placement="top" title="">Por el monto que necesitarás para cumplirlo</span>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 padding_opciones">
				<label class="normal_13" id="label_tipo_2">
					<input type="radio" value="2" name="tipo" class="" id="tipo_cuota">  
					<span class="" data-toggle="tooltip" data-placement="top" title="">Por el monto que aportarías periódicamente</span>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<p class="subtitulo" id="label_monto">
				¿Cuánto necesitarías para lograrlo?
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 padding_opciones padding_right_0">
				<div class="form-group">
					<input data-placement="top" type="text" class="errorTooltip form-control input_monto" id="monto" name="monto" placeholder="" autocomplete="off" maxlength="11">
					<span class="help-block error-bbva" style="display:none;"></span>
				</div>
			</div>
			<div class="col-xs-3 padding_top_10 padding_right_0">
				<label>
					<input type="radio" value="SOLES" name="divisaCalculadora" class="" checked="checked" id="divisaSoles">  
					<span class="normal_13" data-toggle="tooltip" data-placement="top" title="Soles">Soles</span>
				</label>						
			</div>
			<div class="col-xs-3 padding_top_10 padding_left_0 padding_right_0">
				<label>
					<input type="radio" value="DOLARES" name="divisaCalculadora" class="" id="divisaDolares">  
					<span class="normal_13" data-toggle="tooltip" data-placement="top" title="Dólares">Dólares</span>
				</label>						
			</div>
		</div>
		<div class="row margin_top_10 display_none" id="seccion_periodo">
			<div class="col-sm-3 padding_opciones padding_right_0">
				<p class="normal_13 padding_right_0">De manera:
				</p>
			</div>
			<div class="col-sm-9 padding_left_5 "> 
				<div class="row fondo-periodo">
					<div class="col-xs-12 padding_right_0">
						<div class="col-xs-4 padding_left_0">
							<label id="label_12">
								<input type="radio" value="12" name="periodo" class="" checked="checked" id="per_mensual">  
								<span class="normal_13" data-toggle="tooltip" data-placement="top" title="">Mensual</span>
							</label>
						</div>
						<div class="col-xs-4 padding_left_0">
							<label id="label_24">
								<input type="radio" value="24" name="periodo" class="" id="per_quincenal">  
								<span class="normal_13" data-toggle="tooltip" data-placement="top" title="" >Quincenal</span>
							</label>
						</div>
						<div class="col-xs-4 padding_left_0">
							<label id="label_48">
								<input type="radio" value="48" name="periodo" class="" id="per_semanal">  
								<span class="normal_13" data-toggle="tooltip" data-placement="top" title="">Semanal</span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<p class="subtitulo" id="label_meses">
				¿En cuánto tiempo?
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6 padding_opciones padding_right_0">
				<div class="form-group">
					<input data-placement="top" type="text" class="errorTooltip form-control text_center" id="meses" name="meses" placeholder="" autocomplete="off" maxlength="3">
					<span class="help-block error-bbva" style="display:none;"></span>
					</div>
				</div>
				<div class="col-xs-6"><p class="normal_13 padding_top_10">meses</p>
				</div>
			</div>
			<div class="row margin_top_10 margin_bottom_10 padding_bottom_10">
				<div class="col-sm-4">
				</div>
				<div class="col-sm-4">
					<p class="contenido_centrado">
					<button type="submit" class="btn btn-primary btn-secondary">Calcular</button>
					</p>
				</div>
				<div class="col-sm-4">
				</div>
			</div>
			</div>
		</div>
		<div class="col-sm-arrow img-responsive">
		</div>
		<div class="col-sm-2ths">
			<div class="row">
				<div class="col-sm-12">
				<div class="formulario_output min_height_panel" id="formulario_output">
					<div class="text_center subtitulo alinear_output">
					Descubre el resultado aquí *
					</div>
				</div>
				</div>
			</div>		
		</div>
	</div>
	</form>
	<div class="row display_none" id="seccion_error">
		<div class="col-sm-12">
			<div class="msj_advertencia_ajax">
				<span class="ico_error"><img alt="ico-error-bbvanet.png" 
src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjREMDExMTc0M0VGMDExRTJBN0YwRjI0RENEN0NEMjJDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjREMDExMTc1M0VGMDExRTJBN0YwRjI0RENEN0NEMjJDIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NEQwMTExNzIzRUYwMTFFMkE3RjBGMjREQ0Q3Q0QyMkMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NEQwMTExNzMzRUYwMTFFMkE3RjBGMjREQ0Q3Q0QyMkMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6SOH/TAAAC70lEQVR42ryXTUgVURTH5+XLQEoqgwhcVDvXldCHZYpERWkR0ZeE1EKJwspa5KLaVItsI5GbgiiiKLCn70nZNxlFYVCbFm1CKktQDDML803/A/+Jy+XemTdP5x34cWfu57nnnnvmTMx1XefFvINOFrIQ3ADLQS/YBj6FnWSak50UgZNcfAQsAc1gbq4UkAV3gjdgFXgF9oCluVBgPmgCcXAavKc18sExtkeqQCmoBG9BN+se0xoVYFmUCsjujgAXnAHjrP8LzvK5nj4SiQJrQDl4CTq0tiR9YSNYEYUCsvtG8Ie7TWvtE7SKlAcytUIYBTbw2j0FXZY+KfAErANVU6mA7GYf+AUuBPQ9D35n6guZKrAFrKTXdwf0vQ/u0V9qpkKBOWAv+AnalPp85UqW8t2TNvav4/hJKbCb0a6Tu1OP5Tp4yLJIs0KKVts1GQVmge1glIvoMq6Vqtymz8j4mdkqIKYvAwmD56d5JR2W+rW8yzGrOU9oBSTWb+W9ThraY0R/VuNCO8vNtrX8FKij9h3cTTbSTt+p5FGEUkAG5HHxMUO7q5g9zXddxqiEzFMbRoFann2S52+S6WAxnxfx3SQJ3ogKU1yI+zjfDHAL/LD0kfpTYDazIr9+N/mRatCP02SBTdx9yuJ8nozwqj0Dl/lukyRvRDnn91WgiVFNJh22TCjt68EVcA5c4+Q2iw6zr4w76ncENfT8BL9qNikGLaBEqZOEdAf4bBnzHDwCa0G151u6BZpZXvTZvRchS7Q6SUgLfcYMgFauecJ0BNWc5A54HXC/+/lPoIoc2feAcWKFB8pH7P8RFCq7b/XxaHU3x8FX/qB8ZB4wFDBuiD5TxfV6PAUamc3KubzLMMp9YRoeVnoZXSU818f4azbKaFXG9Dpqkc32SDbt+UABGAR9Tm6kj8dREFecagFTr07m+VFJnMFIsux+T4H9dKJL4BDTqbwIFp9gciJX+IMEPU+BLjZeNdzvKOQbOCyp2z8BBgBQUK+WpMWT2QAAAABJRU5ErkJggg==" /></span>
				<span class="texto_error" id="texto_error"></span>
			</div>
		</div>
		<div class="col-sm-12">
			<div align="center" class="margin_top_10">
				<button type="button" class="btn btn-calculadora btn_final btn_ffmm" id="btnFFMM_error">Ir a fondos Mutuos</button>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div align="center" class="margin_top_10">
				<form id="formCalculadora" action="<c:url value="suscripcion-programada"/>" method="post">
					<button type="submit" class="btn btn-primary btn_solicitar" id="btnSuscribir">Solicitar</button>
				</form>
		</div>
		<div align="center" class="margin_top_10 display_none" id="pdfCalculadoraRD">
		<form action="<c:url value="calculadora-mio-pdf"/>" target="_blank">
			<button type="submit" class="btn btn-calculadora btn_pdf">Guardar en PDF</button>
		</form>		
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="tabla_disclaimers" id="tabla_disclaimers">
		<ul>
			<li><b class="font-blue">*El resultado de los cálculos es referencial. </b> La tasa de rentabilidad anual asumida para el cálculo, es la tasa equivalente al promedio de las rentabilidades anuales de los últimos 5 años. En Soles, considera el promedio de las rentabilidades anuales del BBVA Cash Soles y en dólares el promedio de las rentabilidades anuales del BBVA Cash Dólares, para el periodo seleccionado. La tasa de rentabilidad estimada no debe ser interpretada ni entendida como una promesa de rentabilidad constante en el tiempo</li>
			<li>Consulta el detalle de <b>rentabilidades anuales históricas</b> de los fondos BBVA Cash Soles y BBVA Dólares en <a target="_blank" href="https://www.bbvacontinental.pe/personas/ahorro-e-inversion/bbva-asset-management-continental-saf/bbva-cash-fmiv-y-bbva-cash-dolares-fmiv/#pane1">nuestro sitio web</a> y en las oficinas del BBVA Continental</li>
			<li>La <b>rentabilidad o ganancia obtenida en el pasado</b> no garantiza que se repita en el futuro. Esta rentabilidad no incluye el efecto de las comisiones de suscripción y rescate, ni el impuesto a la renta.</li>
		</ul>
		</div>
	</div>
</div>

<div id="seccion_resultado_1" class="hidden">
	<div class="vertical-align-wrap text_center subtitulo alinear_output_opc1 texto_output_opc1">
	Para cumplir tu objetivo necesitarías*:
	<div id="out_opciones" class="vertical-align middle" align=center></div>
	</div>
</div>
<div id="seccion_resultado_2" class="hidden" >
	<div class="text_center subtitulo alinear_output_opc2">
		<div class="vertical-align middle texto_output_opc1">
			Al finalizar tu inversión, <br>tendrías un total de*:<br>
			<span id="monto_futuro" class="normal_30"></span><br>
			<span class="normal_15">(incluyendo rentabilidad: <span id="rentabilidad" class="font-blue"></span>)</span>
		</div>
	</div>
</div>
<div id="seccion_resultado_defecto" class="hidden">
	<div class="text_center subtitulo alinear_output">
	Descubre el resultado aquí *
	</div>
</div>
<div id="pdf_output" class="display_none"></div>

</tiles:putAttribute>

<tiles:putAttribute name="script">
<script type="text/javascript">
function iniciar(){
	$("#formulario_output").html($("#seccion_resultado_defecto").html());
	hidden_btns();
}
function hidden_btns(){
	$("#pdfCalculadora").addClass("hidden");
	$("#pdfCalculadoraRD").addClass("hidden");
	$("#btnSuscribir").addClass("display_none");
	$("#seccion_error").addClass("display_none");
	$("#texto_error").html("");
}
   $(document).ready(function(){
   	$('[data-toggle="tooltip"]').tooltip();
   	$("#opc_1").prop('checked', true);
   $("input[type=radio][name=opcionCalculadora]").change(function () {
   	iniciar();
  		$("#monto").val("");
  		$("#meses").val("");
   });
$("#tipo_cuota, #tipo_monto").change(function () {
   	var tipo=$("input[name=tipo]:checked").val();
   	iniciar();
  		$("#monto").val("");
  		$("#meses").val("");
   	if (tipo=="2"){
   		$("#seccion_periodo").removeClass("display_none");	
   		$("#formulario_input").addClass("min_height_panel_cuotas");
   		$("#formulario_output").addClass("min_height_panel_cuotas");
   		$("#label_monto").html("¿Cuánto aportarías?");
   		$("#label_meses").html("¿Durante cuánto tiempo?");
   	}else{
   		$("#seccion_periodo").addClass("display_none");
   		$("#formulario_input").removeClass("min_height_panel_cuotas");
   		$("#formulario_output").removeClass("min_height_panel_cuotas");
   		$("#label_monto").html("¿Cuánto necesitarías para lograrlo?");
   		$("#label_meses").html("¿En cuánto tiempo?");
   	}
   });
   $("#btnFFMM_error").click(function(event) {
   	var isInIFrame = (window.location != window.parent.location);
   	var url='https://www.bbvacontinental.pe/personas/ahorro-e-inversion/bbva-asset-management-continental-saf/bbva-cash-fmiv-y-bbva-cash-dolares-fmiv/#pane1';
   	if(isInIFrame==true){
   		window.parent.location.href=url;
   	}
   	else {
   		window.location.href=url;
   	}
   });
   $("#monto, #meses").click(function () {
   	hidden_btns();
   });
   $("#per_semanal, #per_quincenal, #per_mensual").click(function () {
   	hidden_btns();
   });
   $("#divisaSoles, #divisaDolares").change(function () {
   	hidden_btns();
   	var divisa=$("input[name=divisaCalculadora]:checked").val();
	$("#monto").removeClass("dollar");
	$("#monto").removeClass("euro");
	$("#monto").removeClass("sol");
	if( divisa=='SOLES'){
		$("#monto").addClass("sol");
	}else if(divisa=='DOLARES'){
		$("#monto").addClass("dollar");
	}else if(divisa=='EUROS'){
		$("#monto").addClass("euro");
	}
});
$('#divisaSoles').trigger("change");
$('#tipo_monto').trigger("change");
   });   
$('#formCalculadora').submit(function(event) {
	event.preventDefault();	
	var enviar=1;
	enviar=enviar*ver_monto();
	enviar=enviar*ver_meses();
	
	if(enviar==1){
		var tipo = $("input[name='tipo']:checked").val();
		var monto = $('#monto').val();
		var divisa = $("input[name='divisaCalculadora']:checked").val();
		var periodo = $("input[name='periodo']:checked").val();
		var meses = $('#meses').val();
		var json = { "monto" : monto, "divisa" : divisa, "meses": meses};
		var objetivo = $("input[name='opcionCalculadora']:checked").val();
		
		var data = 'monto='	+ monto+ '&meses='+ meses+ '&divisa='+ divisa+ '&tipo='	+ tipo	+ '&periodo='+ periodo + '&objetivo='+ objetivo;
		calcular(data,tipo);
	}
	event.preventDefault();	
     });
function calcular(data,tipo) {
	$("#formulario_output").html($("#seccion_resultado_defecto").html());
	$("#out_opciones").html("");
	$("#pdf_output").html("");
	var html_output="";
	$.ajax({
		contentType : "application/json",
		url : $("#formCalculadora").attr( "action"),
		data : data,
		timeout : 3000,
		success : function(calculadora) {
			$("#seccion_resultado_defecto").addClass("display_none");
			if (tipo=="1"){
				var ifirst="0";
				$("#seccion_resultado_1").removeClass("display_none");
				$.each(calculadora.cuotaPeriodo, function(key, value) {
				    if(ifirst=="1"){
				    	$("#out_opciones").append('<br><span class="titulo_nivel2 font-blue" align=center>ó<br>');
				    	$("#out_opciones").append('<img src="${pageContext.request.contextPath}ST/img/separador.png" id=separador class=img-responsive /></span>');
				    }
				    $("#out_opciones").append("<span class='titulo_nivel2'> <b class='font-blue negrita_32'>"+ value + "</b> " + key + "</span>");
				    $("#pdf_output").append("|"+key+"-"+value);
				    ifirst="1";
				});
				$("#formulario_output").fadeOut(500,function(){$("#formulario_output").html($("#seccion_resultado_1").html());});
			}else{
				$("#monto_futuro").html(calculadora.divisaSimbolo +" "+ calculadora.montoFuturo);
				$("#rentabilidad").html(calculadora.divisaSimbolo +" "+ calculadora.rentabilidad);
				$("#pdf_output").append(calculadora.divisaSimbolo+"|"+calculadora.montoFuturo+"|" +calculadora.rentabilidad);
				$("#formulario_output").fadeOut(500,function(){$("#formulario_output").html($("#seccion_resultado_2").html());});
			}
			$('#formulario_output').fadeIn(1500);
			$("#pdfCalculadora").removeClass("hidden");
			$("#pdfCalculadoraRD").removeClass("hidden");
			$("#btnSuscribir").removeClass("display_none");
			_satellite.track("fondosMutuosSimuladorMensajeCalcular");
		},
		error : function(e) {
			$("#texto_error").html("En este momento no podemos calcular el resultado de tu inversión. Por favor, inténtelo más tarde.");
			$("#seccion_error").removeClass("display_none");
		},
		done : function(e) {
	    
		}
	});
}
$("#monto").mask('000,000,000,000,000', {reverse: true});
   $("#meses").numeric({allowMinus:false,allowDecSep:false,allowThouSep:false});
   $("#monto").blur(function(){hidden_btns();});
   $("#meses").blur(function(){hidden_btns();});
   $("#monto").keyup(function(){ver_monto();});
   $("#meses").keyup(function(){ver_meses();});
   
   function ver_monto(){
   	var tmonto=$("#monto").val().replace(/,/g,"")*1.0;
	if(tmonto==0 || isNaN(tmonto)){
		$("#monto").tooltip({trigger : 'hover', title : 'Alerta! Por favor, escribe un monto válido'});
		$("#monto").addClass('form-control-error');
		return 0;
	}else{
		$('#monto').tooltip('destroy');
		$("#monto").removeClass('form-control-error');
		return 1;
	}
   }
   function ver_meses(){
	if($("#meses").val()=='' || $("#meses").val()=='0'){
		$("#meses").tooltip({trigger : 'hover', title : 'Alerta! Por favor, escribe un número de meses válido'});
		$("#meses").addClass('form-control-error');
		return 0;
	}else{
		$('#meses').tooltip('destroy');
		$("#meses").removeClass('form-control-error');
		return 1;
	}
   }
 </script>
</tiles:putAttribute>
</tiles:insertDefinition>