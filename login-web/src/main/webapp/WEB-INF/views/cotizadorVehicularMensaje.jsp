<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Cotizador Vehicular</tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/cotizador-vehicular.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">

</tiles:putAttribute>

<tiles:putAttribute name="body">
<h3>Cotiza tu Seguro Vehicular y solicítalo en segundos aquí</b></h3>
<div class="row">
	<div class="col-sm-12">
		<div id="barraProgreso" class="init-step1">
			<form novalidate="novalidate" id="form-prest-ext" class="form-horizontal tooltip-validation transfer-filter">
				<div class="wizard wizard-prest">
					<div data-title="Cotiza" data-multistep="true"></div>
					<div data-title="Solicita" data-multistep="true"></div>					
				</div>
			</form>
		</div>
	</div>
</div>
	
<div class="well well-lg well-bbva well-sucess well-sucess-xs" style="margin-top: 10px;">
	<div class="row">
		<div class="col-sm-12" align="center">
			<h3 class="font-blue"><b><img src="${pageContext.request.contextPath}ST/img/icono-check.png"> ¡<c:out value="${nombreCompleto}" />, hemos recibido tu solicitud!</b></h3>
		</div>
		<div class="col-sm-12" style="text-align: justify;">
			<p align="center">Te hemos enviado un correo a  ${correo} con la constancia  y la cotización con nuestros planes de <span class="t_azul">Seguro Vehicular BBVA. </span> 
			En un máximo de 24 horas te llamaremos para explicarte cómo puedes adquirir el plan de tu preferencia. </p>
		</div>
	</div>
</div>
<div class="detail-sucess">
	<c:set var="estilo" value=""/>
	<c:set var="planBeneficios" value="${beneficios['planBeneficios']}"/>
	<div class="visible-xs">
		<div class="row">
			<div class="col-xs-12">
				<span class="t_azul t_20 bbva_web_book">${aviso}</span>
			</div>
		</div>
		<c:forEach var="plan" items="${beneficios['plans']}">
		<c:set var="bDescuento" value="0"/>
		<div class="row">
			<div class="col-xs-7 pd_right_0">
				<div class="div_seguro_${plan.codigo} div_seguro_xs"></div>
			</div>
			<div class="col-xs-5 pd_left_0">
				<c:set var="idPlan" value="${plan.codigo}"/>
				<c:set var="estilo" value=""/>
				<c:if test="${fn:length(cotizacionDetalle[idPlan].detalleMensual)>0}">
					<c:set var="estilo" value="er"/>
				</c:if>
				<div class=" div_seguro_monto_xs div_seguro_monto_${plan.codigo} div_seguro_monto_xs_${estilo} text-center">
					<div style="margin-top: 20%;">
					<c:choose> 
						<c:when test="${fn:length(cotizacionDetalle[idPlan].detalleMensual)>0}">
							<strong>${cotizacionDetalle[idPlan].detalleMensual}
							</strong>
						</c:when>
						<c:otherwise>
							<strong>
								<span>${cotizacionDetalle[idPlan].divisaSimbolo}</span>   
								<fmt:formatNumber type="number" pattern="###,##0.00" value="${cotizacionDetalle[idPlan].cuotaMensual}" /> <br> <span class="t_12">al mes</span>
							</strong>
						</c:otherwise>
					</c:choose>
					</div>
				</div>
			</div>
		</div>
		<c:if test="${fn:length(cotizacionDetalle[idPlan].detalleMensual)==0}">	
		<div class="hidden" id="beneficios_${plan.codigo}">
			<c:forEach var="beneficio" items="${beneficios['beneficios']}">
			<c:if test="${beneficio.tipo=='D' && bDescuento=='0'}">
				<div class="row t_16" style="padding-top:20px;padding-bottom:20px;"><div class="col-sm-12 t_resaltado">Descuentos</div></div>
				<c:set var="bDescuento" value="1"/>
			</c:if>
			<div class="row t_14 row_summary" >
				<div class="col-xs-8">
					<div style="display: table;width:100%;">
						<div style="width:18%;vertical-align: middle;display: table-cell;">
							<div class='numberCircle_small <c:if test="${beneficio.id <10}"> paddingNumberCircle_small </c:if>'>${beneficio.id}</div>
						</div>
						<div style="width: 72%;display: table-cell;padding-left: 5px;">
							<div class="font-blue mrgBeneficio">${beneficio.descripcion}</div>
						</div>
					</div>
				</div>
				<c:set var="key" value="${beneficio.id}_${plan.codigo}"/>
				<div class="col-xs-4 text-left">${planBeneficios[key].detalle}</div>				
			</div>
			</c:forEach>
		</div>
		<div class="row text-center">
			<div class="col-xs-12 lnkBeneficioPlanMobile" onclick="verBeneficios('${plan.codigo}')">
				<span class="t_resaltado" id="spnkVerMas_${plan.codigo}">Ver más sobre Coberturas y Descuentos</span> <span id="lnkArrow_${plan.codigo}" class="t_resaltado glyphicon glyphicon-triangle-bottom"></span>
			</div>
		</div>
		</c:if>
		<br>
		</c:forEach>
	</div>
	<div class="d_summary hidden-xs">
		<div class="row">
			<div class="col-sm-6">
				<div style="padding-top: 15%;">
					<span class="t_azul t_20 bbva_web_book"><b>${aviso}</b></span>
				</div>
			</div>
			
			<c:forEach var="plan" items="${beneficios['plans']}">
			<div class="col-sm-2">
				<div class="div_seguro_${plan.codigo} div_seguro"></div>
				<c:set var="idPlan" value="${plan.codigo}"/>
				<c:set var="estilo" value=""/>
				<c:if test="${fn:length(cotizacionDetalle[idPlan].detalleMensual)>0}">
					<c:set var="estilo" value="er"/>
				</c:if>
				<div class=" div_seguro_monto div_seguro_monto_${plan.codigo} div_seguro_monto_${estilo} text-center">
					<strong>
						<c:choose> 
							<c:when test="${fn:length(cotizacionDetalle[idPlan].detalleMensual)>0}">
								<strong>${cotizacionDetalle[idPlan].detalleMensual}
								</strong>
							</c:when>
							<c:otherwise>
								<strong>
									<span>${cotizacionDetalle[idPlan].divisaSimbolo}</span>   
									<fmt:formatNumber type="number" pattern="###,###.00" value="${cotizacionDetalle[idPlan].cuotaMensual}" /> <br> <span class="t_12">al mes</span>
								</strong>
							</c:otherwise>
						</c:choose>
					</strong>
				</div>
			</div>
			</c:forEach>
			
		</div>
		<div class="row">
			<div class="col-sm-12">
				<span class="t_resaltado t_16">Nuestros planes incluyen:</span>
			</div>
		</div>
		<c:set var="bDescuento" value="0"/>
		<c:set var="contBeneficio" value="0"/>
		<c:set var="styleBeneficio" value=""/>
		<div id="divBeneficios">
			<c:forEach var="beneficio" items="${beneficios['beneficios']}">
				<c:set var="contBeneficio" value="${contBeneficio + 1}" />
				<c:if test="${contBeneficio>6}" >
					<c:set var="styleBeneficio" value="hidden" />
				</c:if>
				<c:if test="${beneficio.tipo=='D' && bDescuento=='0'}">
					<div class="row t_16 ${styleBeneficio}" style="padding-top:20px;padding-bottom:20px;"><div class="col-sm-12 t_resaltado">Descuentos</div></div>
					<c:set var="bDescuento" value="1"/>
				</c:if>
			<div class="row t_14 row_summary ${styleBeneficio}">
				<div class="col-sm-6">
				<div style="width: 15%;display:inline-block;" class='numberCircle_small <c:if test="${beneficio.id <10}"> paddingNumberCircle_small </c:if>'>${beneficio.id}</div>
				<div style="width: 80%;display:inline-block;" class="font-blue">${beneficio.descripcion}</div>
				</div>
				<c:forEach var="plan" items="${beneficios['plans']}">
					<c:set var="key" value="${beneficio.id}_${plan.codigo}"/>
					<c:set var="idPlan" value="${plan.codigo}"/>
					<div class="col-sm-2 text-center">
					<c:if test="${fn:length(cotizacionDetalle[idPlan].detalleMensual)==0}">
						${planBeneficios[key].detalle}
					</c:if>
					</div>
				</c:forEach>		
			</div>
			</c:forEach>
			<div class="row text-center">
				<div class="col-sm-12 lnkBeneficioPlan" onclick="verMasBeneficios()" id="divVerMasBeneficios">
					<span class="t_resaltado">Ver más sobre Coberturas y Descuentos</span> <span id="lnkArrow_${plan.codigo}" class="t_resaltado glyphicon glyphicon-triangle-bottom"></span>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<div class="d_summary">
<div class="row">
<div class="col-sm-12 col-xs-12 well-sucess-attach-xs">
	<div class="span_pdf"><a href="https://www.bbvacontinental.pe/fbin/mult/coberturas-seguro-vehicular-bbva_tcm1105-656879.pdf" target="_blank">Descarga el detalle de todos los planes</a></div>
	<div class="span_pdf"><a href="https://www.bbvacontinental.pe/fbin/mult/deducibles-seguro-vehicular-bbva_tcm1105-656875.pdf" target="_blank">Descarga la tabla de los deducibles</a></div>
	<div class="span_arrows"><a href="https://www.bbvacontinental.pe/personas/seguros/autos/seguro-vehicular/#ficha-content-4" target="_blank">Términos y condiciones de los descuentos</a></div>
</div>
</div>
</div>
<br>
<div class="row">
	<div class="col-sm-12" align="center">
		<a href="<c:url value="cotizador-vehicular"/>" class="btn btn-primary">Finalizar</a>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="tabla_disclaimers" id="tabla_disclaimers">
		    <p class="disc_importante">&nbsp;
			</p>
			<p class="disc_importante"><b class="t_resaltado">(1)</b> Aplican condiciones/restricciones y exclusiones del certificado de seguros.
			</p>
			<p class="disc_importante"><b class="t_resaltado">(2)</b> HH.CC.DM.V. y Terrorismo = Huelga, Conmoción Civil, Daño Malicioso, Vandalismo y Terrorismo.
			</p>
			<p class="disc_importante"><b class="t_resaltado">(3)</b> El Sistema Speed te permite recibir atención inmediatamente en el lugar del siniestro para exonerarte de los trámites legales y policiales derivados del mismo, y/o entregarte la orden de reparación o reposición para tu vehículo.
			</p>
			<p class="disc_importante"><b class="t_resaltado">(4)</b> El descuento aplica en láminas de seguridad, cuero vinil automotriz y otros accesorios, así como precios especiales en la instalación de GPS en Protemax. 
			</p>
			<p class="disc_importante"><b class="t_resaltado">(5)</b> El descuento aplica a lavado de salón y cosmética automotriz. 
			</p>
			<p class="disc_importante"><b class="t_resaltado">IMPORTANTE:</b> La validez de esta cotización es de 30 días calendarios o hasta la fecha de término de la campaña vigente, lo que ocurra primero, Culminada dicha fecha se deberá realizar otra cotización. 
			</p>
		</div>
	</div>
</div>

</tiles:putAttribute>

<tiles:putAttribute name="script">
<script src="${pageContext.request.contextPath}ST/js/cotizador_seguro_vehicular_datalayer.js"></script>
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar.js"></script>
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_plugin.js"></script>
<script>
document.onkeydown = function(e){
tecla = (document.all) ? e.keyCode : e.which;
if (tecla = 116) {
	return false;
	}
}
</script>
<script type="text/javascript">
$(window).on("load", function() {
	//resetDatosIngreso();
	llenarDataLayerCotizadorSeguroVehicular(TAG_SOLICITA, "");
});
function verMasBeneficios(){
	$("#divBeneficios>div").removeClass("hidden");
	$("#divVerMasBeneficios").addClass("hidden");
}
$(document).ready(function(){
	$(document).ready(function(){
		transicion_barra_progreso(2,500);
	});
});
function verBeneficios(id){
	if ($("#lnkArrow_"+id).hasClass("glyphicon-triangle-bottom")){
		$('#beneficios_'+id).removeClass('hidden');
		$('#spnkVerMas_'+id).html("Ver menos sobre Coberturas y Descuentos");
	}else{
		$('#beneficios_'+id).addClass('hidden');
		$('#spnkVerMas_'+id).html("Ver más sobre Coberturas y Descuentos");
	}
	$('#lnkArrow_'+id).toggleClass('glyphicon-triangle-bottom glyphicon-triangle-top');
}
</script>
</tiles:putAttribute>
</tiles:insertDefinition>