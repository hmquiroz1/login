<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<tiles:insertDefinition name="mainMenuV2">
<tiles:putAttribute name="title">SIADE :: Sistema Integrado de Administracion de Edificios :: Intranet</tiles:putAttribute>
<tiles:putAttribute name="css">		
	
	<link href="${pageContext.request.contextPath}ST/css/mantenimiento/asistencia/mnt.asistencia.css" rel="stylesheet">
    
    <!-- fullCalendar -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}ST/assets/vendor_components/fullcalendar/dist/fullcalendar.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}ST/assets/vendor_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
	
</tiles:putAttribute>

<tiles:putAttribute name="body">
	  <!-- Content Wrapper. Contains page content -->
	  <div class="content-wrapper">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        ${menuItem.titulocontentheader}
	      </h1>
	      <ol class="breadcrumb">
	        <li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> ${menuItem.descopcionpadre} </a></li>
	        <li class="breadcrumb-item active">${menuItem.descopcionhijo}</li>
	      </ol>
	    </section>
	
	    <!-- Main content -->
	    <section class="content">
	      <div class="row">
	        <div class="col-lg-3 col-md-12">
	          <div class="box box-solid bg-dark">
	            <div class="box-header with-border">
	              <h4 class="box-title">Eventos Base</h4>
	            </div>
	            <div class="box-body">
	              <!-- the events -->
	              <div id="external-events" >
	                <div class="external-event text-success dot-outline" data-class="bg-success"><i class="fa fa-hand-o-right"></i>Solicito Permiso</div>
	                <div class="external-event text-warning dot-outline" data-class="bg-warning"><i class="fa fa-hand-o-right"></i>Enfermedad</div>
	                <div class="external-event text-info dot-outline" data-class="bg-info"><i class="fa fa-hand-o-right"></i>Medio Turno</div>
	                <div class="external-event text-primary dot-outline" data-class="bg-primary"><i class="fa fa-hand-o-right"></i>Asistencia Parcial</div>
	                <div class="external-event text-danger dot-outline" data-class="bg-danger"><i class="fa fa-hand-o-right"></i>Falta Completa</div>
	              </div>
	              <div class="event-fc-bt">
	                <!-- checkbox -->
					<div class="checkbox margin-top-20">
						<input id="drop-remove" type="checkbox">
						<label for="drop-remove">
							Borrar despues de arrastrar
						</label>
					</div>
	             	<a href="#" data-toggle="modal" data-target="#add-new-events" class="btn btn-lg btn-success btn-block margin-top-10">
						<i class="ti-plus"></i> Agregar Evento
					</a>
	              </div>
	            </div>
	            <!-- /.box-body -->
	          </div>
	          <!-- /. box -->
	        </div>
	        <!-- /.col -->
	        <div class="col-lg-9 col-md-12">
	          <div class="box box-dark">
	            <div class="box-body no-padding">
	              <!-- THE CALENDAR -->
	              <div id="calendar"></div>
	            </div>
	            <!-- /.box-body -->
	          </div>
	          <!-- /. box -->
	        </div>
	        <!-- /.col -->
	      </div>
	      <!-- BEGIN MODAL -->
			<div class="modal none-border" id="my-event">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"><strong>Add Event</strong></h4>
						</div>
						<div class="modal-body"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
							<button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
						</div>
					</div>
				</div>
			</div>
			<!-- Modal Add Category -->
			<div class="modal fade none-border" id="add-new-events">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"><strong>Add</strong> a category</h4>
						</div>
						<div class="modal-body">
							<form role="form">
								<div class="row">
									<div class="col-md-6">
										<label class="control-label">Category Name</label>
										<input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
									</div>
									<div class="col-md-6">
										<label class="control-label">Choose Category Color</label>
										<select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
											<option value="success">Success</option>
											<option value="warning">Warning</option>
											<option value="info">Info</option>
											<option value="primary">Primary</option>
											<option value="danger">Danger</option>
										</select>
									</div>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
							<button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<!-- END MODAL -->
			<!-- Modal  para Añadir Evento -->
			<div id="createEventModal" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title">Agregar Evento</h4>
						</div>
						<div class="modal-body">
							<div class="control-group">
								<label class="control-label" for="inputPatient">Titulo:</label>
								<div class="field desc">
									<input class="form-control" id="title" name="title"
										placeholder="Event" type="text" value="">
								</div>
							</div>

							<input type="hidden" id="startTime" /> <input type="hidden"
								id="endTime" />

							<div class="control-group">
								<label class="control-label">Color de Categoria:</label> 
								<select	class="form-control form-white"
									data-placeholder="Choose a color..." name="category-color" id="category-color">
									<option value="bg-success">Success</option>
									<option value="bg-danger">Danger</option>
									<option value="bg-info">Info</option>
									<option value="bg-primary">Primary</option>
									<option value="bg-warning">Warning</option>
									<option value="bg-inverse">Inverse</option>
								</select>
							</div>

							<div class="control-group" style="margin-top: 5px;">
								<label class="control-label" for="when">Fecha:</label>
								<div class="controls controls-row" id="when"></div>
							</div>

						</div>
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true">
								<i class="fa fa-ban"></i> Cancelar
							</button>
							<button type="submit" class="btn btn-success" id="submitButton">
								<i class="fa fa-check"></i> Guardar
							</button>
						</div>
					</div>

				</div>
			</div>

			<!-- Modal para Ver Evento -->
			<div id="calendarModal" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">×</button>
							<h4 class="modal-title">Detalle Evento</h4>
						</div>
						<div id="modalBody" class="modal-body">
<!-- 							<input class="form-control" id="modalTitle" name="modalTitle" type="text" value=""> -->
							<div class="control-group">
								<label class="control-label" for="inputPatient">Titulo:</label>
								<div class="field desc">
									<input class="form-control" id="modalTitle" name="modalTitle"
										type="text" value="">
								</div>
							</div>
<!-- 							<div id="modalWhen" style="margin-top: 5px;"></div> -->
							<div class="control-group" style="margin-top: 5px;">
								<label class="control-label" for="when">Fecha:</label>
								<div class="controls controls-row" id="modalWhen"></div>
							</div>
						</div>
						<input type="hidden" id="eventID" />
						<div class="modal-footer">
							<button class="btn" data-dismiss="modal" aria-hidden="true"><i class="fa fa-ban"></i> Cancel</button>
							<button type="submit" class="btn btn-danger" id="deleteButton"><i class="fa fa-trash"></i> Delete</button>
							<button type="submit" class="btn btn-success" id="guardarButton"><i class="fa fa-check"></i> Guardar</button>
						</div>
					</div>
				</div>
			</div>
			<!--Modal-->
	      <!-- /.row -->
	    </section>
	    <!-- /.content -->
	  </div>
	  <!-- /.content-wrapper -->

</tiles:putAttribute>

<tiles:putAttribute name="js">
	<!-- fullCalendar -->
	<script src="${pageContext.request.contextPath}ST/assets/vendor_components/moment/moment.js"></script>	
	<script src="${pageContext.request.contextPath}ST/assets/vendor_components/fullcalendar/dist/fullcalendar.min.js"></script>
	<script src="${pageContext.request.contextPath}ST/assets/vendor_components/fullcalendar/dist/locale/es.js"></script>
	
	<script src="${pageContext.request.contextPath}ST/js/mantenimiento/asistencia/mntAsistencia.js?version=${versionJavascript}"></script>		
</tiles:putAttribute>
    
</tiles:insertDefinition>