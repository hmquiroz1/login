<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<tiles:insertDefinition name="mainMenu">
<tiles:putAttribute name="title">SIADE :: Sistema Integrado de Administracion de Edificios :: Intranet</tiles:putAttribute>
<tiles:putAttribute name="css">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">	
    <link href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/select/1.2.5/css/select.dataTables.min.css" rel="stylesheet">
    <link href="https://editor.datatables.net/extensions/Editor/css/editor.dataTables.min.css" rel="stylesheet">
    <!-- Libreria CSS propia -->
    <link href="${pageContext.request.contextPath}ST/css/mantenimiento/uo/mnt.uo.css" rel="stylesheet">
<%--     <link href="${pageContext.request.contextPath}ST/css/datepicker/datepicker.css" rel="stylesheet"> --%>
    <link href="${pageContext.request.contextPath}ST/css/datepicker-jquery/datepicker.css" rel="stylesheet">
</tiles:putAttribute>

<tiles:putAttribute name="body">
		
		
	<!-- line modal -->
	<div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
				<h3 class="modal-title" id="lineModalLabel">Agregar Unidad Organizativa</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
					 	<label for="txtNombre">Nombre</label>
		                <input type="text" class="form-control" id="txtNombre" placeholder="Ingrese Nombre">
	                </div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
					 	<label for="txtDescripcion">Descripcion</label>
	                	<input type="text" class="form-control" id="txtDescripcion" placeholder="Descripcion">
	                </div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-2 col-xs-6">
							<p>Estado:</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="onoffswitch">							
					        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
					        <label class="onoffswitch-label" for="myonoffswitch">
					            <span class="onoffswitch-inner"></span>
					            <span class="onoffswitch-switch"></span>
					        </label>			       	
				        </div>
			        </div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-5">				
						<div class="form-group">
							<span class="glyphicon glyphicon-calendar" style="color: #3489c1;"></span>			
							<label for="dateField-periodicas-empieza-1">Fecha Inicio:</label>
							<fmt:formatDate pattern="dd/MM/yyyy" value="${fechaHoy}" var="fechaFormateada" />							
							<input type="text" id="fechaDesde" name="fechaDesde" class="form-control" data-toggle="datepicker" data-format="dd/mm/yy" placeholder="dd/mm/yyyy" value="${fechaFormateada}">							
						</div>					
					</div>	
					<div class="col-md-5">	
						<div class="form-group">	
							<span class="glyphicon glyphicon-calendar" style="color: #3489c1;"></span>		
							<label for="dateField-periodicas-finaliza-1">Fecha Fin:</label>
							<input type="text" id="fechaHasta" name="fechaHasta" class="form-control" data-toggle="datepicker" data-format="dd/mm/yy" placeholder="dd/mm/yyyy" value="${fechaFormateada}">						
						</div>
					</div>	
					 
				</div>	           
	
			</div>
			<div class="modal-footer">
				<div class="btn-group btn-group-justified" role="group" aria-label="group button">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Cerrar</button>
					</div>
					<div class="btn-group btn-delete hidden" role="group">
						<button type="button" id="delImage" class="btn btn-default btn-hover-red" data-dismiss="modal"  role="button">Delete</button>
					</div>
					<div class="btn-group" role="group">
						<button type="button" id="grabarUO" class="btn btn-default btn-hover-green" role="button">Guardar</button>
					</div>
				</div>
			</div>
		</div>
	  </div>
	</div>
	
	<!-- INicio Data Table -->
<!-- 	<div class="container">Fin Container -->
<!-- 		<div class="row"> -->
<!-- 			<div class="col-sm-12"> -->
			<br>
			 <div class="row">
				<div class="col-sm-12">
	        		<h4>Mantenimiento de Unidades Organizativas</h4>
	        	</div>
			</div>
	        <div class="row">
				<div class="col-sm-12">
					<div>
						<p data-placement="top" data-toggle="tooltip" title="New"><button id="newUO"class="btn btn-success btn-sm" data-title="New" data-toggle="modal" data-target="#squarespaceModal" ><span class="glyphicon glyphicon-plus">Añadir</span></button></p>
					</div>
				</div>
			</div>
	        <div class="table-responsive"> <!-- Inicio div enclosing the table -->
			<table id="tblUO" class="table table-striped table-bordered table-hover" style="width:100%">
		        <thead>
		            <tr>
		             	<th><input type="checkbox" id="checkall" /></th>
		                <th>ID</th>
		                <th>Descripcion</th>
		                <th>Nombre</th>
		                <th>Estado</th>
		                <th>Fec.Ini.Vig</th>
		                <th>Fec.Fin.Vig</th>
		                <th>Editar</th>
		                <th>Borrar</th>
		            </tr>
		        </thead>
		        <tbody>
		        	<c:forEach items="${listaUO}" var="uo" varStatus="i">
			        	<tr>
			            	<td><input type="checkbox" class="checkthis" /></td>
			                <td >${uo.iduo}</td>
			                <td>${uo.descripcion}</td>
			                <td>${uo.nombre}</td>
			                <td><c:if test="${uo.estado == 'A'}">ACTIVO</c:if><c:if test="${uo.estado == 'I'}">INACTIVO</c:if></td>
			                <td>${uo.fecinivig}</td>
			                <td>${uo.fecfinvig}</td>
			                <td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
		    				<td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>		                
			            </tr>
		        	</c:forEach>		            	            
		        </tbody>
		        <tfoot>
		            <tr>
		            	<th></th>
		                <th>ID</th>
		                <th>Descripcion</th>
		                <th>Nombre</th>
		                <th>Estado</th>
		                <th>Fec.Ini.Vig</th>
		                <th>Fec.Fin.Vig</th>
		                <th>Editar</th>
		                <th>Borrar</th>		                
		            </tr>
		        </tfoot>
		    </table>
		    
		    </div><!-- Fin Div Enclosing the table -->
<!-- 		</div>Fin Div col-sm-12 -->
		
<!-- 		</div>Fin Div row -->
<!-- 	</div>Fin Container -->
	<!-- Fin Data Table -->
	
	
</tiles:putAttribute>

<tiles:putAttribute name="scripts">
<script src="${pageContext.request.contextPath}ST/js/datatable/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}ST/js/datatable/dataTables.bootstrap4.min.js"></script>
<script src="${pageContext.request.contextPath}ST/js/mantenimiento/uo/mntUO.js?version=${versionJavascript}"></script>
<%-- <script src="${pageContext.request.contextPath}ST/js/datepicker/bootstrap-datepicker.js?version=${versionJavascript}"></script> --%>
<script src="${pageContext.request.contextPath}ST/js/datepicker-jquery/datepicker.js?version=${versionJavascript}"></script>
</tiles:putAttribute>

    
</tiles:insertDefinition>