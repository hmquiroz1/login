<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<div class="modal-dialog" id="consulta-edicion-uo">
	<div class="modal-content no-border deposito-plazo-fijo-width-popup-detalle-cancelacion">
		<div class="modal-box" >
			<div class="modal-header">
				<button type="button" class="close-icon" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="deposito-plazo-fijo-detalle-cancelacion">¿Qu&eacute; pasa si cancelo mi Dep&oacute;sito a Plazo Fijo antes del plazo elegido?</h4>
			</div>
			<br>
			<div class="modal-body">
				
				<div class="row">
					<div class="col-md-12">
						
						<div class="row">
							<div class="col-xs-12">
								<p class="text-left deposito-plazo-fijo-estilo-cancelacion-popup">
									<b class="deposito-plazo-fijo-estilo-cancelacion-popup-sub-titulo">
										Seg&uacute;n el plazo de tu Dep&oacute;sito a Plazo Fijo suceder&aacute; lo siguiente:
									</b>
								</p>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-6">
								<hr class="deposito-plazo-fijo-detalle-cancelacion deposito-plazo-fijo-margen-detalle-cancelacion">
								<p class="text-left"><b>PERIODO</b></p>
							</div>
							<div class="col-xs-6">
								<hr class="deposito-plazo-fijo-detalle-cancelacion deposito-plazo-fijo-margen-detalle-cancelacion">
								<p class="text-left"><b>GANANCIA</b></p>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-6">
								<hr class="deposito-plazo-fijo-detalle-cancelacion deposito-plazo-fijo-margen-detalle-cancelacion">
								<p class="text-left deposito-plazo-fijo-estilo-cancelacion-popup">Hasta 30 d&iacute;as</p>
							</div>
							<div class="col-xs-6">
								<hr class="deposito-plazo-fijo-detalle-cancelacion deposito-plazo-fijo-margen-detalle-cancelacion">
								<p class="text-justify deposito-plazo-fijo-estilo-cancelacion-popup">Si el dep&oacute;sito se cancela antes del plazo pactado y dentro de los 30 d&iacute;as de haberse efectuado el dep&oacute;sito, no se generar&aacute; pago de inter&eacute;s alguno.</p>
							</div>
						</div>
						
						<div class="row">
							<div class="col-xs-6">
								<hr class="deposito-plazo-fijo-detalle-cancelacion deposito-plazo-fijo-margen-detalle-cancelacion">
								<p class="text-left deposito-plazo-fijo-estilo-cancelacion-popup">31 d&iacute;as a m&aacute;s</p>
							</div>
							<div class="col-xs-6">
								<hr class="deposito-plazo-fijo-detalle-cancelacion deposito-plazo-fijo-margen-detalle-cancelacion">
								<p class="text-justify deposito-plazo-fijo-estilo-cancelacion-popup">A partir del d&iacute;a 31 y antes del final del plazo del dep&oacute;sito, se pagar&aacute; la tasa de inter&eacute;s que el Banco tenga establecida para dep&oacute;sitos a plazo de 30 d&iacute;as, en la fecha en que se produzca la terminaci&oacute;n.</p>
							</div>
						</div>
						
						

					</div>
				</div>		
			</div>
		</div>
	</div>
</div>