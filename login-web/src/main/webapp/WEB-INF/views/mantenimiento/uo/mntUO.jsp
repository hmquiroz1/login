<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<tiles:insertDefinition name="mainMenuV2">
	<tiles:putAttribute name="title">SIADE :: Sistema Integrado de Administracion de Edificios :: Intranet</tiles:putAttribute>
	<tiles:putAttribute name="css">
		<!-- DataTables libreria Principal -->
		<link
			href="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/DataTables-1.10.20/css/jquery.dataTables.min.css"
			rel="stylesheet">
		<!-- DataTables libreria - Extension Buttons (copy,print,pdf,csv) -->
		<link
			href="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/Buttons-1.6.1/css/buttons.dataTables.min.css"
			rel="stylesheet">
		<!-- DataTables libreria - Select integration rows to export (copy,print,pdf,csv) -->
		<link
			href="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/Select-1.3.1/css/select.dataTables.min.css"
			rel="stylesheet">
		
		<!-- Libreria CSS propia -->
		<link
			href="${pageContext.request.contextPath}ST/css/mantenimiento/uo/mnt.uo.css"
			rel="stylesheet">
		<%--     <link href="${pageContext.request.contextPath}ST/css/datepicker/datepicker.css" rel="stylesheet"> --%>
		<link
			href="${pageContext.request.contextPath}ST/css/datepicker-jquery/datepicker.css"
			rel="stylesheet">
		<!-- toast CSS -->
    	<link href="${pageContext.request.contextPath}ST/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css" rel="stylesheet">
	</tiles:putAttribute>

	<tiles:putAttribute name="body">

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>${menuItem.titulocontentheader}</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#"><i
							class="fa fa-dashboard"></i> ${menuItem.descopcionpadre} </a></li>
					<li class="breadcrumb-item active">${menuItem.descopcionhijo}</li>
				</ol>
			</section>

			<!-- Main content -->
			<section class="content">
				<!-- Agregar UO -->
				<div class="modal fade" id="agregarModal" tabindex="-1"
					role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">×</span><span class="sr-only">Close</span>
								</button>
								<h3 class="modal-title" id="lineModalLabel">Agregar Unidad
									Organizativa</h3>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-sm-12 ">
										<label for="txtNombre">Nombre</label> <input type="text"
											class="form-control" id="txtNombre"
											placeholder="Ingrese Nombre">
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-sm-12">
										<label for="txtDescripcion">Descripcion</label> <input
											type="text" class="form-control" id="txtDescripcion"
											placeholder="Descripcion">
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-sm-2 col-xs-4">
										<p>Estado:</p>
									</div>
									<div class="col-sm-10 col-xs-8">
										<input type="checkbox" name="chkEstadoNew" id="chkEstadoNew" class="filled-in chk-col-blue" checked />
										<label id="lblEstadoNew" for="chkEstadoNew">ACTIVO</label>
									</div>									
								</div>	
								<br>
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<span class="glyphicon glyphicon-calendar"
												style="color: #3489c1;"></span> <label
												for="dateField-periodicas-empieza-1">Fecha Inicio:</label>
											<fmt:formatDate pattern="dd/MM/yyyy" value="${fechaHoy}"
												var="fechaFormateada" />
											<input type="text" id="fechaDesde" name="fechaDesde"
												class="form-control" data-toggle="datepicker"
												data-format="dd/mm/yy" placeholder="dd/mm/yyyy"
												value="${fechaFormateada}">
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<span class="glyphicon glyphicon-calendar"
												style="color: #3489c1;"></span> <label
												for="dateField-periodicas-finaliza-1">Fecha Fin:</label> <input
												type="text" id="fechaHasta" name="fechaHasta"
												class="form-control" data-toggle="datepicker"
												data-format="dd/mm/yy" placeholder="dd/mm/yyyy"
												value="${fechaFormateada}">
										</div>
									</div>

								</div>

							</div>
							<div class="modal-footer">
								<div class="btn-group btn-group-justified" role="group"
									aria-label="group button">
									<div class="btn-group" role="group">
										<button type="button" class="btn btn-default"
											data-dismiss="modal" role="button">Cerrar</button>
									</div>									
									<div class="btn-group" role="group">
										<button type="button" id="grabarUO"
											class="btn btn-default btn-hover-green" role="button">Guardar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Editar UO -->
				<div class="modal fade" id="editarModal" tabindex="-1"
					role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">
									<span aria-hidden="true">×</span><span class="sr-only">Close</span>
								</button>
								<h3 class="modal-title" id="lineModalLabel">Editar Unidad
									Organizativa</h3>
							</div>
							<div class="modal-body">
								<div class="row">
									<div class="col-sm-12">
										<label for="txtNombre">Nombre</label> <input type="text"
											class="form-control" id="txtNombreEdit"
											placeholder="Ingrese Nombre">
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-sm-12">
										<label for="txtDescripcion">Descripcion</label> <input
											type="text" class="form-control" id="txtDescripcionEdit"
											placeholder="Descripcion">
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-sm-2 col-xs-4">
										<p>Estado:</p>
									</div>
									<div class="col-sm-10 col-xs-8">
										<input type="checkbox" id="chkEstadoEdit" class="filled-in chk-col-blue" checked />
										<label id="lblEstadoEdit" for="md_checkbox_26"></label>
									</div>
								</div>								
								<br>
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<span class="glyphicon glyphicon-calendar"
												style="color: #3489c1;"></span> <label
												for="dateField-periodicas-empieza-1">Fecha Inicio:</label>
											<fmt:formatDate pattern="dd/MM/yyyy" value="${fechaHoy}"
												var="fechaFormateada" />
											<input type="text" id="fechaDesdeEdit" name="fechaDesde"
												class="form-control" data-toggle="datepicker"
												data-format="dd/mm/yy" placeholder="dd/mm/yyyy"
												value="">
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<span class="glyphicon glyphicon-calendar"
												style="color: #3489c1;"></span> <label
												for="dateField-periodicas-finaliza-1">Fecha Fin:</label> <input
												type="text" id="fechaHastaEdit" name="fechaHasta"
												class="form-control" data-toggle="datepicker"
												data-format="dd/mm/yy" placeholder="dd/mm/yyyy"
												value="">
										</div>
									</div>

								</div>

							</div>
							<div class="modal-footer">
								<div class="btn-group btn-group-justified" role="group"
									aria-label="group button">
									<div class="btn-group" role="group">
										<button type="button" class="btn btn-default"
											data-dismiss="modal" role="button">Cerrar</button>
									</div>									
									<div class="btn-group" role="group">
										<button type="button" id="grabarUO"
											class="btn btn-default btn-hover-green" role="button">Guardar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- INicio Data Table -->				
				<br>
				<div class="row">
					<div class="col-sm-12">
						<h4>Mantenimiento de Unidades Organizativas</h4>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-xs-12">
						<div class="row">
							<div class="col-sm-2 col-xs-12">
								<p data-placement="top" data-toggle="tooltip" title=""
									data-original-title="New">
									<button id="newUO" class="btn bg-green btn-sm"
										data-title="New" data-toggle="modal"
										data-target="#agregarModal">
										<span class="glyphicon glyphicon-plus">Añadir</span>
									</button>
								</p>
							</div>	
							<div class="col-sm-2 col-xs-12">
								<p data-placement="top" data-toggle="tooltip" title=""
									data-original-title="New">
									<button id="test" class="btn bg-green btn-sm">
										<span class="glyphicon glyphicon-plus">Prueba Toastr</span>
									</button>
								</p>
							</div>						
						</div>
					</div>

				</div>
				<div class="table-responsive">
					<!-- Inicio div enclosing the table -->
					<table id="tblUO"
						class="table table-striped table-bordered table-hover"
						style="width: 100%">
						<thead>
							<tr>								
								<th>ID</th>
								<th>Descripcion</th>
								<th>Nombre</th>
								<th>Estado</th>
								<th>Fec.Ini.Vig</th>
								<th>Fec.Fin.Vig</th>
								<th>Editar</th>
								<th>Borrar</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${listaUO}" var="uo" varStatus="i">
								<tr>									
									<td>${uo.iduo}</td>
									<td>${uo.descripcion}</td>
									<td>${uo.nombre}</td>
									<c:if test="${uo.estado == 'A'}"><td class="highlightgreen">ACTIVO</td></c:if>
									<c:if test="${uo.estado == 'I'}"><td class="highlightred">INACTIVO</td></c:if>
									<td>${uo.fecinivig}</td>
									<td>${uo.fecfinvig}</td>
									<td><p data-placement="top" data-toggle="tooltip"
											title="Edit">
											<button class="btn btn-datatable-edit btn-xs" data-title="Edit" onclick="editarUO(${uo.iduo})">
												<span class="glyphicon glyphicon-pencil"></span>
											</button>
										</p></td>
									<td><p data-placement="top" data-toggle="tooltip"
											title="Delete">
											<button class="btn btn-datatable-delete btn-xs" data-title="Delete"
												data-toggle="modal" data-target="#eliminarModal" data-uoid="${uo.iduo}">
												<span class="glyphicon glyphicon-trash"></span>
											</button>
										</p></td>
								</tr>
							</c:forEach>
						</tbody>
						<tfoot>
							<tr>								
								<th>ID</th>
								<th>Descripcion</th>
								<th>Nombre</th>
								<th>Estado</th>
								<th>Fec.Ini.Vig</th>
								<th>Fec.Fin.Vig</th>
								<th>Editar</th>
								<th>Borrar</th>
							</tr>
						</tfoot>
					</table>

				</div>				
				<!-- Fin Data Table -->
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

	</tiles:putAttribute>

	<tiles:putAttribute name="js">
		<!-- DataTables libreria Principal -->
		<script
			src="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/DataTables-1.10.20/js/jquery.dataTables.min.js"></script>
		<script
			src="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>
		
		<!-- DataTables Extension - Select integration - export selected rows -->
		<script
			src="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/Buttons-1.6.1/js/dataTables.buttons.min.js"></script>			
		<script
			src="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/Buttons-1.6.1/js/buttons.flash.min.js"></script>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
		<script
			src="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/Buttons-1.6.1/js/buttons.html5.min.js"></script>
		<script
			src="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/Buttons-1.6.1/js/buttons.print.min.js"></script>
		<script
			src="${pageContext.request.contextPath}ST/assets/vendor_plugins/DataTables-1.10.20/Select-1.3.1/js/dataTables.select.min.js"></script>			

		<!-- Libreria Propia -->
		<script
			src="${pageContext.request.contextPath}ST/js/mantenimiento/uo/mntUO.js?version=${versionJavascript}"></script>
		<%-- <script src="${pageContext.request.contextPath}ST/js/datepicker/bootstrap-datepicker.js?version=${versionJavascript}"></script> --%>
		<script
			src="${pageContext.request.contextPath}ST/js/datepicker-jquery/datepicker.js?version=${versionJavascript}"></script>
	    
	    <!-- Toast Notifications -->
		<script src="${pageContext.request.contextPath}ST/assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.js"></script>
	    <script src="${pageContext.request.contextPath}ST/js/menuSiade/pages/toastr.js?version=${versionJavascript}"></script>
	    <script src="${pageContext.request.contextPath}ST/js/menuSiade/pages/notification.js"></script>
	</tiles:putAttribute>


</tiles:insertDefinition>