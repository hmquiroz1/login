<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<html lang="en">
<head>
	 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="${pageContext.request.contextPath}ST/img/loginSiade/favicon.ico">
    
	<title><tiles:insertAttribute name="title" /></title>
	<tiles:insertAttribute name="resources" ignore="true" />
	
	<tiles:insertAttribute name="css" />	
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
    
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
	<input type="hidden" id="codigoUO" value="${usuarioModel.iduo}" />
	<input type="hidden" id="codigoCC" value="${usuarioModel.idcc}" />
	
	<!-- INICIO MENU -->
	<div class="wrapper">

	  <header class="main-header">
	    <!-- Logo -->
	    <a href="index.html" class="logo">
	      <!-- mini logo -->
		  <b class="logo-mini">
			  <span class="light-logo"><img src="${pageContext.request.contextPath}ST/img/menuSiade/logo-light.png" alt="logo"></span>
			  <span class="dark-logo"><img src="${pageContext.request.contextPath}ST/img/menuSiade/logo-dark.png" alt="logo"></span>
		  </b>
	      <!-- logo-->
	      <span class="logo-lg">
			  <img src="${pageContext.request.contextPath}ST/img/menuSiade/logo-light-text.png" alt="logo" class="light-logo">
		  	  <img src="${pageContext.request.contextPath}ST/img/menuSiade/logo-dark-text.png" alt="logo" class="dark-logo">
		  </span>
	    </a>
	    <!-- Header Navbar -->
	    <nav class="navbar navbar-static-top">
	      <!-- Sidebar toggle button-->
	      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
	        <span class="sr-only">Toggle navigation</span>
	      </a>
	
	      <div class="navbar-custom-menu">
	        <ul class="nav navbar-nav">
			  
			  <li class="search-box">
	            <a class="nav-link hidden-sm-down" href="javascript:void(0)"><i class="mdi mdi-magnify"></i></a>
	            <form class="app-search" style="display: none;">
	                <input type="text" class="form-control" placeholder="Busqueda &amp; <Enter>"> <a class="srh-btn"><i class="ti-close"></i></a>
				</form>
	          </li>			
			  
	          <!-- Messages -->
	          <li class="dropdown messages-menu">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              <i class="mdi mdi-email"></i>
	            </a>
	            <ul class="dropdown-menu scale-up">
	              <li class="header">You have 5 messages</li>
	              <li>
	                <!-- inner menu: contains the actual data -->
	                <ul class="menu inner-content-div">
	                  <li><!-- start message -->
	                    <a href="#">
	                      <div class="pull-left">
	                        <img src="${pageContext.request.contextPath}ST/img/menuSiade/user2-160x160.jpg" class="rounded-circle" alt="User Image">
	                      </div>
	                      <div class="mail-contnet">
	                         <h4>
	                          Lorem Ipsum
	                          <small><i class="fa fa-clock-o"></i> 15 mins</small>
	                         </h4>
	                         <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
	                      </div>
	                    </a>
	                  </li>
	                  <!-- end message -->
	                  <li>
	                    <a href="#">
	                      <div class="pull-left">
	                        <img src="${pageContext.request.contextPath}ST/img/menuSiade/user3-128x128.jpg" class="rounded-circle" alt="User Image">
	                      </div>
	                      <div class="mail-contnet">
	                         <h4>
	                          Nullam tempor
	                          <small><i class="fa fa-clock-o"></i> 4 hours</small>
	                         </h4>
	                         <span>Curabitur facilisis erat quis metus congue viverra.</span>
	                      </div>
	                    </a>
	                  </li>
	                  <li>
	                    <a href="#">
	                      <div class="pull-left">
	                        <img src="${pageContext.request.contextPath}ST/img/menuSiade/user4-128x128.jpg" class="rounded-circle" alt="User Image">
	                      </div>
	                      <div class="mail-contnet">
	                         <h4>
	                          Proin venenatis
	                          <small><i class="fa fa-clock-o"></i> Today</small>
	                         </h4>
	                         <span>Vestibulum nec ligula nec quam sodales rutrum sed luctus.</span>
	                      </div>
	                    </a>
	                  </li>
	                  <li>
	                    <a href="#">
	                      <div class="pull-left">
	                        <img src="${pageContext.request.contextPath}ST/img/menuSiade/user3-128x128.jpg" class="rounded-circle" alt="User Image">
	                      </div>
	                      <div class="mail-contnet">
	                         <h4>
	                          Praesent suscipit
	                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
	                         </h4>
	                         <span>Curabitur quis risus aliquet, luctus arcu nec, venenatis neque.</span>
	                      </div>
	                    </a>
	                  </li>
	                  <li>
	                    <a href="#">
	                      <div class="pull-left">
	                        <img src="${pageContext.request.contextPath}ST/img/menuSiade/user4-128x128.jpg" class="rounded-circle" alt="User Image">
	                      </div>
	                      <div class="mail-contnet">
	                         <h4>
	                          Donec tempor
	                          <small><i class="fa fa-clock-o"></i> 2 days</small>
	                         </h4>
	                         <span>Praesent vitae tellus eget nibh lacinia pretium.</span>
	                      </div>
	                    </a>
	                  </li>
	                </ul>
	              </li>
	              <li class="footer"><a href="#">See all e-Mails</a></li>
	            </ul>
	          </li>
	          <!-- Notifications -->
	          <li class="dropdown notifications-menu">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              <i class="mdi mdi-bell"></i>
	            </a>
	            <ul class="dropdown-menu scale-up">
	              <li class="header">You have 7 notifications</li>
	              <li>
	                <!-- inner menu: contains the actual data -->
	                <ul class="menu inner-content-div">
	                  <li>
	                    <a href="#">
	                      <i class="fa fa-users text-aqua"></i> Curabitur id eros quis nunc suscipit blandit.
	                    </a>
	                  </li>
	                  <li>
	                    <a href="#">
	                      <i class="fa fa-warning text-yellow"></i> Duis malesuada justo eu sapien elementum, in semper diam posuere.
	                    </a>
	                  </li>
	                  <li>
	                    <a href="#">
	                      <i class="fa fa-users text-red"></i> Donec at nisi sit amet tortor commodo porttitor pretium a erat.
	                    </a>
	                  </li>
	                  <li>
	                    <a href="#">
	                      <i class="fa fa-shopping-cart text-green"></i> In gravida mauris et nisi
	                    </a>
	                  </li>
	                  <li>
	                    <a href="#">
	                      <i class="fa fa-user text-red"></i> Praesent eu lacus in libero dictum fermentum.
	                    </a>
	                  </li>
	                  <li>
	                    <a href="#">
	                      <i class="fa fa-user text-red"></i> Nunc fringilla lorem 
	                    </a>
	                  </li>
	                  <li>
	                    <a href="#">
	                      <i class="fa fa-user text-red"></i> Nullam euismod dolor ut quam interdum, at scelerisque ipsum imperdiet.
	                    </a>
	                  </li>
	                </ul>
	              </li>
	              <li class="footer"><a href="#">View all</a></li>
	            </ul>
	          </li>
	          <!-- Tasks-->
	          <li class="dropdown tasks-menu">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              <i class="mdi mdi-message"></i>
	            </a>
	            <ul class="dropdown-menu scale-up">
	              <li class="header">You have 6 tasks</li>
	              <li>
	                <!-- inner menu: contains the actual data -->
	                <ul class="menu inner-content-div">
	                  <li><!-- Task item -->
	                    <a href="#">
	                      <h3>
	                        Lorem ipsum dolor sit amet
	                        <small class="pull-right">30%</small>
	                      </h3>
	                      <div class="progress xs">
	                        <div class="progress-bar progress-bar-aqua" style="width: 30%" role="progressbar"
	                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                          <span class="sr-only">30% Complete</span>
	                        </div>
	                      </div>
	                    </a>
	                  </li>
	                  <!-- end task item -->
	                  <li><!-- Task item -->
	                    <a href="#">
	                      <h3>
	                        Vestibulum nec ligula
	                        <small class="pull-right">20%</small>
	                      </h3>
	                      <div class="progress xs">
	                        <div class="progress-bar progress-bar-danger" style="width: 20%" role="progressbar"
	                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                          <span class="sr-only">20% Complete</span>
	                        </div>
	                      </div>
	                    </a>
	                  </li>
	                  <!-- end task item -->
	                  <li><!-- Task item -->
	                    <a href="#">
	                      <h3>
	                        Donec id leo ut ipsum
	                        <small class="pull-right">70%</small>
	                      </h3>
	                      <div class="progress xs">
	                        <div class="progress-bar progress-bar-light-blue" style="width: 70%" role="progressbar"
	                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                          <span class="sr-only">70% Complete</span>
	                        </div>
	                      </div>
	                    </a>
	                  </li>
	                  <!-- end task item -->
	                  <li><!-- Task item -->
	                    <a href="#">
	                      <h3>
	                        Praesent vitae tellus
	                        <small class="pull-right">40%</small>
	                      </h3>
	                      <div class="progress xs">
	                        <div class="progress-bar progress-bar-yellow" style="width: 40%" role="progressbar"
	                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                          <span class="sr-only">40% Complete</span>
	                        </div>
	                      </div>
	                    </a>
	                  </li>
	                  <!-- end task item -->
	                  <li><!-- Task item -->
	                    <a href="#">
	                      <h3>
	                        Nam varius sapien
	                        <small class="pull-right">80%</small>
	                      </h3>
	                      <div class="progress xs">
	                        <div class="progress-bar progress-bar-red" style="width: 80%" role="progressbar"
	                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                          <span class="sr-only">80% Complete</span>
	                        </div>
	                      </div>
	                    </a>
	                  </li>
	                  <!-- end task item -->
	                  <li><!-- Task item -->
	                    <a href="#">
	                      <h3>
	                        Nunc fringilla
	                        <small class="pull-right">90%</small>
	                      </h3>
	                      <div class="progress xs">
	                        <div class="progress-bar progress-bar-primary" style="width: 90%" role="progressbar"
	                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
	                          <span class="sr-only">90% Complete</span>
	                        </div>
	                      </div>
	                    </a>
	                  </li>
	                  <!-- end task item -->
	                </ul>
	              </li>
	              <li class="footer">
	                <a href="#">View all tasks</a>
	              </li>
	            </ul>
	          </li>
			  <!-- User Account-->
	          <li class="dropdown user user-menu">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              <img src="${pageContext.request.contextPath}ST/img/menuSiade/user5-128x128.jpg" class="user-image rounded-circle" alt="User Image">
	            </a>
	            <ul class="dropdown-menu scale-up">
	              <!-- User image -->
	              <li class="user-header">
	                <img src="${pageContext.request.contextPath}ST/img/menuSiade/user5-128x128.jpg" class="float-left rounded-circle" alt="User Image">
	
	                <p>
	                  ${usuarioModel.nombre}
	                  <small class="mb-5">${usuarioModel.email}</small>
	                  <a href="#" class="btn btn-danger btn-sm btn-rounded">Ver Mi Perfil</a>
	                </p>
	              </li>
	              <!-- Menu Body -->
	              <li class="user-body">
	                <div class="row no-gutters">
	                  <div class="col-12 text-left">
	                    <a href="#"><i class="ion ion-person"></i> Mi Perfil</a>
	                  </div>
	                  <div class="col-12 text-left">
	                    <a href="#"><i class="ion ion-email-unread"></i> Mi Buzón</a>
	                  </div>
	                  <div class="col-12 text-left">
	                    <a href="#"><i class="ion ion-settings"></i> Configuracion Global</a>
	                  </div>
					<div role="separator" class="divider col-12"></div>
					  <div class="col-12 text-left">
	                    <a href="#"><i class="ti-settings"></i> Configuracion Cuenta</a>
	                  </div>
					<div role="separator" class="divider col-12"></div>
					  <div class="col-12 text-left">
	                    <a href="<%= request.getContextPath()%>${contextoSiade}/logoutV2"><i class="fa fa-power-off"></i> Salir</a>
	                  </div>				
	                </div>
	                <!-- /.row -->
	              </li>
	            </ul>
	          </li>
	          <!-- Control Sidebar Toggle Button -->
	          <c:choose>
		  		<c:when test="${usuarioModel.tipoperfil eq 'ADMIN'}">
		          <li>
		            <a href="#" data-toggle="control-sidebar"><i class="fa fa-cog fa-spin"></i></a>
		          </li>
		        </c:when>				
			  </c:choose>   
	        </ul>
	      </div>
	    </nav>
	  </header>
	  
	  <!-- Left side column. contains the logo and sidebar -->
	  <aside class="main-sidebar">
	    <!-- sidebar-->
	    <section class="sidebar">
	      
	      <!-- sidebar menu-->
	      <ul class="sidebar-menu" data-widget="tree">
	        <li class="user-profile treeview">
	          <a href="index.html">
				<img src="${pageContext.request.contextPath}ST/img/menuSiade/user5-128x128.jpg" alt="user">
	            <span>${usuarioModel.nombre}</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
			  <ul class="treeview-menu">
	            <li><a href="javascript:void()"><i class="fa fa-user mr-5"></i>Mi Perfil </a></li>
				<li><a href="javascript:void()"><i class="fa fa-money mr-5"></i>Mi Balance</a></li>
				<li><a href="javascript:void()"><i class="fa fa-envelope-open mr-5"></i>Mi Buzón</a></li>
				<li><a href="javascript:void()"><i class="fa fa-cog mr-5"></i>Configuracion Cuenta</a></li>
				<li><a href="<%= request.getContextPath()%>${contextoSiade}/logoutV2"><i class="fa fa-power-off mr-5"></i>Salir</a></li>
	          </ul>
	        </li>
			<!-- INI PRUEBA MENU DINAMICO -->
			
			<li class="header nav-small-cap">${usuarioModel.nomEdificio}</li>		
			<li class="active">
	          <a href="index.html">
	            <i class="fa fa-dashboard"></i> <span>Principal</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	        </li>
			<c:forEach items="${listaMenuOpcionesModel}" varStatus="i" var="menuOpcionesModel">
				<li class="treeview ${activeHome == menuOpcionesModel.idperfilopcion ? 'active menu-open' : ''}">				
					<a href="javascript:void(0)">
						<i class="fa ${menuOpcionesModel.icono}"></i>
						<span>${menuOpcionesModel.descopcion}</span>
						<c:if test="${menuOpcionesModel.indnew eq 'S'}"> <span style="color: green;margin-left: 5px;"><i class="fa fa-star"></i></span> </c:if>
						<span class="pull-right-container">
						  <i class="fa fa-angle-right pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
					  <c:forEach items="${menuOpcionesModel.hijos}" varStatus="j" var="menuOpcionesHijosModel">
							<li ${activeSubHome == menuOpcionesHijosModel.idperfilopcion ? 'class="active"' : ''}>
								<a href="<%= request.getContextPath()%>${menuOpcionesHijosModel.link}?idopcionpadre=${menuOpcionesModel.idperfilopcion}&idopcionhijo=${menuOpcionesHijosModel.idperfilopcion}&descopcionpadre=${menuOpcionesModel.descopcion}&descopcionhijo=${menuOpcionesHijosModel.descopcion}&iconopcionpadre=${menuOpcionesModel.icono}&titulocontentheader=${menuOpcionesHijosModel.tituloheader}">
									<i class="fa ${menuOpcionesHijosModel.icono}"></i>${menuOpcionesHijosModel.descopcion}								
								</a>
							</li>
					   </c:forEach>
					</ul>                        		
				</li>						
			</c:forEach>
			
			<!-- FIN PRUEBA MENU DINAMICO -->
	        <li class="header nav-small-cap">PERSONAL</li>
	        <li class="active">
	          <a href="index.html">
	            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-th"></i>
	            <span>App</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/app/app-chat.html"><i class="fa fa-circle-thin"></i>Chat app</a></li>
	            <li><a href="pages/app/app-contact.html"><i class="fa fa-circle-thin"></i>Contact / Employee</a></li>
	            <li><a href="pages/app/app-ticket.html"><i class="fa fa-circle-thin"></i>Support Ticket</a></li>
				<li><a href="pages/app/calendar.html"><i class="fa fa-circle-thin"></i>Calendar</a></li>
	            <li><a href="pages/app/profile.html"><i class="fa fa-circle-thin"></i>Profile</a></li>
	            <li><a href="pages/app/userlist-grid.html"><i class="fa fa-circle-thin"></i>Userlist Grid</a></li>
				<li><a href="pages/app/userlist.html"><i class="fa fa-circle-thin"></i>Userlist</a></li>
	          </ul>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-envelope"></i> <span>Mailbox</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/mailbox/mailbox.html"><i class="fa fa-circle-thin"></i>Inbox</a></li>
	            <li><a href="pages/mailbox/compose.html"><i class="fa fa-circle-thin"></i>Compose</a></li>
	            <li><a href="pages/mailbox/read-mail.html"><i class="fa fa-circle-thin"></i>Read</a></li>
	          </ul>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-laptop"></i>
	            <span>UI Elements</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/UI/badges.html"><i class="fa fa-circle-thin"></i>Badges</a></li>
	            <li><a href="pages/UI/border-utilities.html"><i class="fa fa-circle-thin"></i>Border</a></li>
	            <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-thin"></i>Buttons</a></li>			
	            <li><a href="pages/UI/bootstrap-switch.html"><i class="fa fa-circle-thin"></i>Bootstrap Switch</a></li>
	            <li><a href="pages/UI/cards.html"><i class="fa fa-circle-thin"></i>User Card</a></li>
	            <li><a href="pages/UI/color-utilities.html"><i class="fa fa-circle-thin"></i>Color</a></li>
	            <li><a href="pages/UI/date-paginator.html"><i class="fa fa-circle-thin"></i>Date Paginator</a></li>
	            <li><a href="pages/UI/dropdown.html"><i class="fa fa-circle-thin"></i>Dropdown</a></li>
	            <li><a href="pages/UI/dropdown-grid.html"><i class="fa fa-circle-thin"></i>Dropdown Grid</a></li>
	            <li><a href="pages/UI/general.html"><i class="fa fa-circle-thin"></i>General</a></li>
	            <li><a href="pages/UI/icons.html"><i class="fa fa-circle-thin"></i>Icons</a></li>
	            <li><a href="pages/UI/media-advanced.html"><i class="fa fa-circle-thin"></i>Advanced Medias</a></li>
				<li><a href="pages/UI/modals.html"><i class="fa fa-circle-thin"></i>Modals</a></li>
				<li><a href="pages/UI/nestable.html"><i class="fa fa-circle-thin"></i>Nestable</a></li>
	            <li><a href="pages/UI/notification.html"><i class="fa fa-circle-thin"></i>Notification</a></li>
	            <li><a href="pages/UI/portlet-draggable.html"><i class="fa fa-circle-thin"></i>Draggable Portlets</a></li>
	            <li><a href="pages/UI/ribbons.html"><i class="fa fa-circle-thin"></i>Ribbons</a></li>
	            <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-thin"></i>Sliders</a></li>
	            <li><a href="pages/UI/sweatalert.html"><i class="fa fa-circle-thin"></i>Sweet Alert</a></li>
	            <li><a href="pages/UI/tab.html"><i class="fa fa-circle-thin"></i>Tabs</a></li>
	            <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-thin"></i>Timeline</a></li>
	            <li><a href="pages/UI/timeline-horizontal.html"><i class="fa fa-circle-thin"></i>Horizontal Timeline</a></li>
	          </ul>
	        </li>
	        <li class="header nav-small-cap">FORMS, TABLE & LAYOUTS</li>
			<li class="treeview">
	          <a href="#">
	            <i class="fa fa-bars"></i>
	            <span>Widgets</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/widgets/blog.html"><i class="fa fa-circle-thin"></i>Blog</a></li>
	            <li><a href="pages/widgets/chart.html"><i class="fa fa-circle-thin"></i>Chart</a></li>
	            <li><a href="pages/widgets/list.html"><i class="fa fa-circle-thin"></i>List</a></li>
	            <li><a href="pages/widgets/social.html"><i class="fa fa-circle-thin"></i>Social</a></li>
	            <li><a href="pages/widgets/statistic.html"><i class="fa fa-circle-thin"></i>Statistic</a></li>
	            <li><a href="pages/widgets/weather.html"><i class="fa fa-circle-thin"></i>Weather</a></li>
	            <li><a href="pages/widgets/widgets.html"><i class="fa fa-circle-thin"></i>Widgets</a></li>
	          </ul>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-files-o"></i>
	            <span>Layout Options</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-thin"></i>Boxed</a></li>
	            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-thin"></i>Fixed</a></li>
	            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-thin"></i>Collapsed Sidebar</a></li>
	          </ul>
	        </li>		
			<li class="treeview">
	          <a href="#">
	            <i class="fa fa-square-o"></i>
	            <span>Box</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/box/advanced.html"><i class="fa fa-circle-thin"></i>Advanced</a></li>
	            <li><a href="pages/box/basic.html"><i class="fa fa-circle-thin"></i>Basic</a></li>
	            <li><a href="pages/box/color.html"><i class="fa fa-circle-thin"></i>Color</a></li>
				<li><a href="pages/box/group.html"><i class="fa fa-circle-thin"></i>Group</a></li>
	          </ul>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-pie-chart"></i>
	            <span>Charts</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-thin"></i>ChartJS</a></li>
	            <li><a href="pages/charts/flot.html"><i class="fa fa-circle-thin"></i>Flot</a></li>
	            <li><a href="pages/charts/inline.html"><i class="fa fa-circle-thin"></i>Inline charts</a></li>
	            <li><a href="pages/charts/morris.html"><i class="fa fa-circle-thin"></i>Morris</a></li>
	            <li><a href="pages/charts/peity.html"><i class="fa fa-circle-thin"></i>Peity</a></li>
	          </ul>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-edit"></i> <span>Forms</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-thin"></i>Advanced Elements</a></li>
	            <li><a href="pages/forms/code-editor.html"><i class="fa fa-circle-thin"></i>Code Editor</a></li>
	            <li><a href="pages/forms/editor-markdown.html"><i class="fa fa-circle-thin"></i>Markdown</a></li>
	            <li><a href="pages/forms/editors.html"><i class="fa fa-circle-thin"></i>Editors</a></li>
	            <li><a href="pages/forms/form-validation.html"><i class="fa fa-circle-thin"></i>Form Validation</a></li>
	            <li><a href="pages/forms/form-wizard.html"><i class="fa fa-circle-thin"></i>Form Wizard</a></li>
	            <li><a href="pages/forms/general.html"><i class="fa fa-circle-thin"></i>General Elements</a></li>
	            <li><a href="pages/forms/mask.html"><i class="fa fa-circle-thin"></i>Formatter</a></li>
	            <li><a href="pages/forms/xeditable.html"><i class="fa fa-circle-thin"></i>Xeditable Editor</a></li>
	          </ul>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-table"></i> <span>Tables</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/tables/simple.html"><i class="fa fa-circle-thin"></i>Simple tables</a></li>
	            <li><a href="pages/tables/data.html"><i class="fa fa-circle-thin"></i>Data tables</a></li>
	            <li><a href="pages/tables/editable-tables.html"><i class="fa fa-circle-thin"></i>Editable Tables</a></li>
	            <li><a href="pages/tables/table-color.html"><i class="fa fa-circle-thin"></i>Table Color</a></li>
	          </ul>
	        </li>
			<li>
	          <a href="pages/email/index.html">
	            <i class="fa fa-envelope-open-o"></i> <span>Emails</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	        </li>
			<li class="header nav-small-cap">EXTRA COMPONENTS</li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-map"></i> <span>Map</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/map/map-google.html"><i class="fa fa-circle-thin"></i>Google Map</a></li>
	            <li><a href="pages/map/map-vector.html"><i class="fa fa-circle-thin"></i>Vector Map</a></li>
	          </ul>
	        </li>
			<li class="treeview">
	          <a href="#">
	            <i class="fa fa-plug"></i> <span>Extension</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/extension/fullscreen.html"><i class="fa fa-circle-thin"></i>Fullscreen</a></li>
	          </ul>
	        </li>        
			<li class="treeview">
	          <a href="#">
	            <i class="fa fa-file"></i> <span>Sample Pages</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="pages/samplepage/blank.html"><i class="fa fa-circle-thin"></i>Blank</a></li>
	            <li><a href="pages/samplepage/coming-soon.html"><i class="fa fa-circle-thin"></i>Coming Soon</a></li>
	            <li><a href="pages/samplepage/custom-scroll.html"><i class="fa fa-circle-thin"></i>Custom Scrolls</a></li>
				<li><a href="pages/samplepage/faq.html"><i class="fa fa-circle-thin"></i>FAQ</a></li>
				<li><a href="pages/samplepage/gallery.html"><i class="fa fa-circle-thin"></i>Gallery</a></li>
				<li><a href="pages/samplepage/invoice.html"><i class="fa fa-circle-thin"></i>Invoice</a></li>
				<li><a href="pages/samplepage/lightbox.html"><i class="fa fa-circle-thin"></i>Lightbox Popup</a></li>
				<li><a href="pages/samplepage/pace.html"><i class="fa fa-circle-thin"></i>Pace</a></li>
				<li><a href="pages/samplepage/pricing.html"><i class="fa fa-circle-thin"></i>Pricing</a></li>
	            <li class="treeview">
	              <a href="#"><i class="fa fa-circle-thin"></i>Authentication 
	                <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	              </a>
	              <ul class="treeview-menu">
	                <li><a href="pages/samplepage/login.html"><i class="fa fa-circle"></i>Login</a></li>
	                <li><a href="pages/samplepage/register.html"><i class="fa fa-circle"></i>Register</a></li>
	                <li><a href="pages/samplepage/lockscreen.html"><i class="fa fa-circle"></i>Lockscreen</a></li>
	                <li><a href="pages/samplepage/user-pass.html"><i class="fa fa-circle"></i>Recover password</a></li>				  
	              </ul>
	            </li>            
				<li class="treeview">
	              <a href="#"><i class="fa fa-circle-thin"></i>Error Pages 
	                <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	              </a>
	              <ul class="treeview-menu">
	                <li><a href="pages/samplepage/404.html"><i class="fa fa-circle"></i>404</a></li>
	                <li><a href="pages/samplepage/500.html"><i class="fa fa-circle"></i>500</a></li>
	                <li><a href="pages/samplepage/maintenance.html"><i class="fa fa-circle"></i>Maintenance</a></li>		  
	              </ul>
	            </li> 
	          </ul>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-share"></i> <span>Multilevel</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="#">Level One</a></li>
	            <li class="treeview">
	              <a href="#">Level One
	                <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	              </a>
	              <ul class="treeview-menu">
	                <li><a href="#">Level Two</a></li>
	                <li class="treeview">
	                  <a href="#">Level Two
	                    <span class="pull-right-container">
	              <i class="fa fa-angle-right pull-right"></i>
	            </span>
	                  </a>
	                  <ul class="treeview-menu">
	                    <li><a href="#">Level Three</a></li>
	                    <li><a href="#">Level Three</a></li>
	                  </ul>
	                </li>
	              </ul>
	            </li>
	            <li><a href="#">Level One</a></li>
	          </ul>
	        </li>        
	        
	      </ul>
	    </section>
	  </aside>
	
	    
		<c:choose>
		  <c:when test="${paginaprincipal eq 'S'}">
		   <!-- Content Wrapper. Contains page content -->
	  	  <div class="content-wrapper">
			  <!-- Content Header (Page header) -->
			  <section class="content-header">	  
				  <h1>
					Dashboard
					<small>Pagina Principal</small>
				  </h1>
				  <ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="breadcrumb-item active">Dashboard</li>
				  </ol>
			  </section>
			  <!-- Main content -->
		      <section class="content">
			  <div class="row">
		        <div class="col-12 ">
				  <div class="row">
		
					<div class="col-sm-6 col-lg-3">
					  <div class="box pull-up">
						  <div class="box-body">
							<div class="flexbox mb-1">
							  <div>
								<p class="text-info font-size-26 font-weight-300 mb-0">1025</p>
								Products Sold
							  </div>
							  <div class="text-info font-size-40"><i class="mdi mdi-basket-fill"></i></div>
							</div>
							<div class="progress progress-xxs mt-10 mb-0">
							  <div class="progress-bar bg-info" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						  </div>
					  </div>
					</div>
		
		
					<div class="col-sm-6 col-lg-3">
					  <div class="box pull-up">
						  <div class="box-body">
							<div class="flexbox mb-1">
							  <div>
								<p class="text-warning font-size-26 font-weight-300 mb-0">$658</p>
								Net Profit
							  </div>
							  <div class="text-warning font-size-40"><i class="mdi mdi-chart-line"></i></div>
							</div>
							<div class="progress progress-xxs mt-10 mb-0">
							  <div class="progress-bar bg-warning" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						  </div>
					  </div>
					</div>
		
		
					<div class="col-sm-6 col-lg-3">
					  <div class="box pull-up">
						  <div class="box-body">
							<div class="flexbox mb-1">
							  <div>
								<p class="text-success font-size-26 font-weight-300 mb-0">452</p>
								New Customers
							  </div>
							  <div class="text-success font-size-40"><i class="mdi mdi-account-plus"></i></div>
							</div>
							<div class="progress progress-xxs mt-10 mb-0">
							  <div class="progress-bar bg-success" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						  </div>
					  </div>
					</div>
		
		
					<div class="col-sm-6 col-lg-3">
					  <div class="box pull-up">
						  <div class="box-body">
							<div class="flexbox mb-1">
							  <div>
								<p class="text-danger font-size-26 font-weight-300 mb-0">90.89 %</p>
								Customer Satisfaction
							  </div>
							  <div class="text-danger font-size-40"><i class="mdi mdi-heart"></i></div>
							</div>
							<div class="progress progress-xxs mt-10 mb-0">
							  <div class="progress-bar bg-danger" role="progressbar" style="width: 35%; height: 4px;" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
							</div>
						  </div>
					  </div>
					</div>
				  </div>
		        </div>
		        <!-- /.col -->		  
		      </div>
				
		      	 
		      <div class="row">
				  
				 <div class="col-xl-4 col-lg-6 col-12">	
		          <!-- PRODUCT LIST -->
					  <div class="box">
						<div class="box-header with-border">
						  <h3 class="box-title">Recently Products</h3>
		
						  <div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						  </div>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
						  <ul class="products-list product-list-in-box">
							<li class="item">
							  <div class="product-img">
								<img src="${pageContext.request.contextPath}ST/img/menuSiade/default-50x50.gif" alt="Product Image">
							  </div>
							  <div class="product-info">
								<a href="javascript:void(0)" class="product-title">iphone 7plus
								  <span class="label label-warning pull-right mt-5">$300</span></a>
								<span class="product-description">
									  12MP Wide-angle and telephoto cameras.
									</span>
							  </div>
							</li>
							<!-- /.item -->
							<li class="item">
							  <div class="product-img">
								<img src="${pageContext.request.contextPath}ST/img/menuSiade/default-50x50.gif" alt="Product Image">
							  </div>
							  <div class="product-info">
								<a href="javascript:void(0)" class="product-title">Apple Tv
								  <span class="label label-info pull-right mt-5">$400</span></a>
								<span class="product-description">
									  Library | For You | Browse | Radio
									</span>
							  </div>
							</li>
							<!-- /.item -->
							<li class="item">
							  <div class="product-img">
								<img src="${pageContext.request.contextPath}ST/img/menuSiade/default-50x50.gif" alt="Product Image">
							  </div>
							  <div class="product-info">
								<a href="javascript:void(0)" class="product-title">MacBook Air<span
									class="label label-danger pull-right mt-5">$450</span></a>
								<span class="product-description">
									  Make big things happen. All day long.
									</span>
							  </div>
							</li>
							<!-- /.item -->
							<li class="item">
							  <div class="product-img">
								<img src="${pageContext.request.contextPath}ST/img/menuSiade/default-50x50.gif" alt="Product Image">
							  </div>
							  <div class="product-info">
								<a href="javascript:void(0)" class="product-title">iPad Pro
								  <span class="label label-success pull-right mt-5">$289</span></a>
								<span class="product-description">
									  Anything you can do, you can do better.
									</span>
							  </div>
							</li>
							<!-- /.item -->
						  </ul>
						</div>
						<!-- /.box-body -->
						<div class="box-footer text-center">
						  <a href="javascript:void(0)" class="uppercase">View All Products</a>
						</div>
						<!-- /.box-footer -->
					  </div>           
				 </div>
		        <div class="col-xl-4 col-lg-6 col-12">
		          <!-- Widget: user widget style 3 -->
		          <div class="box box-widget widget-user-3">
		            <!-- Add the bg color to the header using any of the bg-* classes -->
		            <div class="widget-user-header bg-img text-white"  style="background: url('${pageContext.request.contextPath}ST/img/menuSiade/img1.jpg') center center;" data-overlay="5">
		              <div class="info-user">
						  <h3 class="widget-user-username">Michael Jorden</h3>
						  <h6 class="widget-user-desc">Developer</h6>
		              </div>
		              <div class="widget-user-image clearfix">
		                <img class="rounded-circle" src="${pageContext.request.contextPath}ST/img/menuSiade/user7-128x128.jpg" alt="User Avatar">
		              </div>
		              <!-- /.widget-user-image -->
		            </div>
		            <div class="box-footer no-padding">
		              <ul class="nav d-block nav-stacked">
		                <li class="nav-item"><a href="#" class="nav-link">Sales <span class="pull-right badge bg-blue">1310</span></a></li>
		                <li class="nav-item"><a href="#" class="nav-link">Projects <span class="pull-right badge bg-green">120</span></a></li>
		                <li class="nav-item"><a href="#" class="nav-link">Followers <span class="pull-right badge bg-yellow">8K</span></a></li>
		                <li class="nav-item"><a href="#" class="nav-link">Tasks <span class="pull-right badge bg-red">58</span></a></li>
		              </ul>
		            </div>
		          </div>
		          <!-- /.widget-user -->			
					
		          	<div class="flexbox flex-justified text-center bg-white mb-20">
		              <div class="no-shrink py-30">
		                <span class="ion ion-social-facebook font-size-50" style="color: #3b5998"></span>
		              </div>
		
		              <div class="py-25 bg-blue">
		                <div class="font-size-30">+250</div>
		                <span>Likes</span>
		              </div>
		            </div>
					
		        </div>
		        <!-- /.col -->  
				  
				         		  
		      </div>
		      <!-- /.row -->
		      </section>
		     <!-- /.content -->
	      </div>
	  	  <!-- /.content-wrapper -->
	      </c:when>	  
		  <c:otherwise>		
				<tiles:insertAttribute name="body" />		 
		  </c:otherwise>
		</c:choose>		
	  
	  <footer class="main-footer">
	    <div class="pull-right d-none d-sm-inline-block">
	        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
			  <li class="nav-item">
				<a class="nav-link" href="javascript:void(0)">FAQ</a>
			  </li>
			  <li class="nav-item">
				<a class="nav-link" href="#">Purchase Now</a>
			  </li>
			</ul>
	    </div>
		  &copy; 2019 <a href="https://www.multipurposethemes.com/">SIADE - Sistema de Administracion de Edificios.</a>. All Rights Reserved.
	  </footer>
	
	  <!-- Control Sidebar -->
	  <aside class="control-sidebar control-sidebar-light">
	    <!-- Create the tabs -->
	    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
	      <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
	      <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-cog fa-spin"></i></a></li>
	    </ul>
	    <!-- Tab panes -->
	    <div class="tab-content">
	      <!-- Home tab content -->
	      <div class="tab-pane" id="control-sidebar-home-tab">
	        <h3 class="control-sidebar-heading">Recent Activity</h3>
	        <ul class="control-sidebar-menu">
	          <li>
	            <a href="javascript:void(0)">
	              <i class="menu-icon fa fa-birthday-cake bg-red"></i>
	
	              <div class="menu-info">
	                <h4 class="control-sidebar-subheading">Admin Birthday</h4>
	
	                <p>Will be July 24th</p>
	              </div>
	            </a>
	          </li>
	          <li>
	            <a href="javascript:void(0)">
	              <i class="menu-icon fa fa-user bg-yellow"></i>
	
	              <div class="menu-info">
	                <h4 class="control-sidebar-subheading">Jhone Updated His Profile</h4>
	
	                <p>New Email : jhone_doe@demo.com</p>
	              </div>
	            </a>
	          </li>
	          <li>
	            <a href="javascript:void(0)">
	              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
	
	              <div class="menu-info">
	                <h4 class="control-sidebar-subheading">Disha Joined Mailing List</h4>
	
	                <p>disha@demo.com</p>
	              </div>
	            </a>
	          </li>
	          <li>
	            <a href="javascript:void(0)">
	              <i class="menu-icon fa fa-file-code-o bg-green"></i>
	
	              <div class="menu-info">
	                <h4 class="control-sidebar-subheading">Code Change</h4>
	
	                <p>Execution time 15 Days</p>
	              </div>
	            </a>
	          </li>
	        </ul>
	        <!-- /.control-sidebar-menu -->
	
	        <h3 class="control-sidebar-heading">Tasks Progress</h3>
	        <ul class="control-sidebar-menu">
	          <li>
	            <a href="javascript:void(0)">
	              <h4 class="control-sidebar-subheading">
	                Web Design
	                <span class="label label-danger pull-right">40%</span>
	              </h4>
	
	              <div class="progress progress-xxs">
	                <div class="progress-bar progress-bar-danger" style="width: 40%"></div>
	              </div>
	            </a>
	          </li>
	          <li>
	            <a href="javascript:void(0)">
	              <h4 class="control-sidebar-subheading">
	                Update Data
	                <span class="label label-success pull-right">75%</span>
	              </h4>
	
	              <div class="progress progress-xxs">
	                <div class="progress-bar progress-bar-success" style="width: 75%"></div>
	              </div>
	            </a>
	          </li>
	          <li>
	            <a href="javascript:void(0)">
	              <h4 class="control-sidebar-subheading">
	                Order Process
	                <span class="label label-warning pull-right">89%</span>
	              </h4>
	
	              <div class="progress progress-xxs">
	                <div class="progress-bar progress-bar-warning" style="width: 89%"></div>
	              </div>
	            </a>
	          </li>
	          <li>
	            <a href="javascript:void(0)">
	              <h4 class="control-sidebar-subheading">
	                Development 
	                <span class="label label-primary pull-right">72%</span>
	              </h4>
	
	              <div class="progress progress-xxs">
	                <div class="progress-bar progress-bar-primary" style="width: 72%"></div>
	              </div>
	            </a>
	          </li>
	        </ul>
	        <!-- /.control-sidebar-menu -->
	
	      </div>
	      <!-- /.tab-pane -->
	      <!-- Stats tab content -->
	      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
	      <!-- /.tab-pane -->
	      <!-- Settings tab content -->
	      <div class="tab-pane" id="control-sidebar-settings-tab">
	        <form method="post">
	          <h3 class="control-sidebar-heading">General Settings</h3>
	
	          <div class="form-group">
	            <input type="checkbox" id="report_panel" class="chk-col-grey" >
				<label for="report_panel" class="control-sidebar-subheading ">Report panel usage</label>
	
	            <p>
	              general settings information
	            </p>
	          </div>
	          <!-- /.form-group -->
	
	          <div class="form-group">
	            <input type="checkbox" id="allow_mail" class="chk-col-grey" >
				<label for="allow_mail" class="control-sidebar-subheading ">Mail redirect</label>
	
	            <p>
	              Other sets of options are available
	            </p>
	          </div>
	          <!-- /.form-group -->
	
	          <div class="form-group">
	            <input type="checkbox" id="expose_author" class="chk-col-grey" >
				<label for="expose_author" class="control-sidebar-subheading ">Expose author name</label>
	
	            <p>
	              Allow the user to show his name in blog posts
	            </p>
	          </div>
	          <!-- /.form-group -->
	
	          <h3 class="control-sidebar-heading">Chat Settings</h3>
	
	          <div class="form-group">
	            <input type="checkbox" id="show_me_online" class="chk-col-grey" >
				<label for="show_me_online" class="control-sidebar-subheading ">Show me as online</label>
	          </div>
	          <!-- /.form-group -->
	
	          <div class="form-group">
	            <input type="checkbox" id="off_notifications" class="chk-col-grey" >
				<label for="off_notifications" class="control-sidebar-subheading ">Turn off notifications</label>
	          </div>
	          <!-- /.form-group -->
	
	          <div class="form-group">
	            <label class="control-sidebar-subheading">              
	              <a href="javascript:void(0)" class="text-red margin-r-5"><i class="fa fa-trash-o"></i></a>
	              Delete chat history
	            </label>
	          </div>
	          <!-- /.form-group -->
	        </form>
	      </div>
	      <!-- /.tab-pane -->
	    </div>
	  </aside>
	  <!-- /.control-sidebar -->
	  
	  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
	  <div class="control-sidebar-bg"></div>
	  
	</div>
	<!-- FIN MENU -->
	
    <!--[if lt IE 10]>
    <script>
		// To test the @id toggling on password inputs in browsers that don’t support changing an input’s @type dynamically (e.g. Firefox 3.6 or IE), uncomment this:
		// $.fn.hide = function() { return this; }
		// Then uncomment the last rule in the <style> element (in the <head>).
		$(function() {
			// Invoke the plugin
			$('input, textarea').placeholder({customClass:'my-placeholder'});
			// That’s it, really.
			
			var html;

			if ($.fn.placeholder.input && $.fn.placeholder.textarea) {
				html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> and <code>textarea</code> elements.</strong> The plugin won’t run in this case, since it’s not needed. If you want to test the plugin, use an older browser.';
			} else if ($.fn.placeholder.input) {
				html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> elements, but not for <code>textarea</code> elements.</strong> The plugin will only do its thang on the <code>textarea</code>s.';
			}

			if (html) {
				$('<p class="note">' + html + '</p>').insertBefore('form');
			}
		});
	</script>
	<![endif]-->
    <tiles:insertAttribute name="scripts" ignore="true" />  
    <tiles:insertAttribute name="js" ignore="true" />
</body>
</html>