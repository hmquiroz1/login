<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1" />
<meta name="description" content="" />
<meta name="author" content="" />
<meta name="format-detection" content="telephone=no">

<c:set var="ambiente" scope="application">
	<spring:eval expression="@propiedades.getProperty('ambiente')" />
</c:set>
<c:set var="contextoRecursos" scope="application">
	<spring:eval expression="@propiedades.getProperty('contextoRecursos')" />
</c:set>

<c:set var="contextoSiade" scope="application">
	<spring:eval expression="@propiedades.getProperty('contextoSiade')" />
</c:set>

	<!-- INI - LIBRERIAS PARA MENU BOOTSTRAP 4-->
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="<%= request.getContextPath()%>ST/assets/vendor_components/bootstrap/dist/css/bootstrap.css">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="<%= request.getContextPath()%>ST/css/loginSiade/bootstrap-extend.css">
	
	<!-- theme style -->
	<link rel="stylesheet" href="<%= request.getContextPath()%>ST/css/loginSiade/master_style.css">
	
	<!-- Minimalelite Admin skins -->
	<link rel="stylesheet" href="<%= request.getContextPath()%>ST/css/loginSiade/skins/_all-skins.css">
	  
	<!-- Morris charts -->
	<link rel="stylesheet" href="<%= request.getContextPath()%>ST/assets/vendor_components/morris.js/morris.css">
   
    <!-- Vector CSS -->
    <link href="<%= request.getContextPath()%>ST/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
	
	<!-- date picker -->
	<link rel="stylesheet" href="<%= request.getContextPath()%>ST/assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">
	
	<!-- daterange picker -->
	<link rel="stylesheet" href="<%= request.getContextPath()%>ST/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css">
	
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<%= request.getContextPath()%>ST/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css">
	<!-- FIN - LIBRERIAS PARA MENU BOOTSTRAP 4-->