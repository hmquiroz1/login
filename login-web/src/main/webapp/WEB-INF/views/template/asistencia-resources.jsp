<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1" />
<meta name="description" content="" />
<meta name="author" content="" />
<meta name="format-detection" content="telephone=no">

<c:set var="ambiente" scope="application">
	<spring:eval expression="@propiedades.getProperty('ambiente')" />
</c:set>
<c:set var="contextoRecursos" scope="application">
	<spring:eval expression="@propiedades.getProperty('contextoRecursos')" />
</c:set>

<c:set var="contextoSiade" scope="application">
	<spring:eval expression="@propiedades.getProperty('contextoSiade')" />
</c:set>

<%-- <link rel="stylesheet" href="${contextoRecursos}/css/global.css" /> --%>

<!-- Bootstrap 4.2.1 -->
<link href="<%= request.getContextPath()%>ST/css/bootstrap-4.2.1/bootstrap.min.css" rel="stylesheet">
<%-- <script src="<%= request.getContextPath()%>ST/js/jquery-3.3.1/jquery-3.3.1.min.js"></script> --%>
