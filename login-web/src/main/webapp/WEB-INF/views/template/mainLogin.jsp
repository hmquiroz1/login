<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<title><tiles:insertAttribute name="title" /></title>

	<tiles:insertAttribute name="css" />	
    
    <!-- Para insertar Codigos de Conversion Facebook -->
    <tiles:insertAttribute name="conversioncodefacebok" />
</head>
<body id="bodyLogin">
	<!-- Para insertar Codigos de Conversion Google -->
    <tiles:insertAttribute name="conversioncodegoogle" />
	
	<tiles:insertAttribute name="body" />
	
	
    <!--[if lt IE 10]>
    <script>
		// To test the @id toggling on password inputs in browsers that don’t support changing an input’s @type dynamically (e.g. Firefox 3.6 or IE), uncomment this:
		// $.fn.hide = function() { return this; }
		// Then uncomment the last rule in the <style> element (in the <head>).
		$(function() {
			// Invoke the plugin
			$('input, textarea').placeholder({customClass:'my-placeholder'});
			// That’s it, really.
			
			var html;

			if ($.fn.placeholder.input && $.fn.placeholder.textarea) {
				html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> and <code>textarea</code> elements.</strong> The plugin won’t run in this case, since it’s not needed. If you want to test the plugin, use an older browser.';
			} else if ($.fn.placeholder.input) {
				html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> elements, but not for <code>textarea</code> elements.</strong> The plugin will only do its thang on the <code>textarea</code>s.';
			}

			if (html) {
				$('<p class="note">' + html + '</p>').insertBefore('form');
			}
		});
	</script>
	<![endif]-->
    <script type="text/javascript" src="<%= request.getContextPath()%>ST/js/iframeResizer.contentWindow.min.js" ></script>
    <tiles:insertAttribute name="script" />
    <tiles:insertAttribute name="js" />     
</body>
</html>