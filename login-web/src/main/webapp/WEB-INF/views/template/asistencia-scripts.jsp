<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<c:set var="versionJavascript" scope="application"><spring:eval expression="@propiedades.getProperty('version.javascript')" /></c:set>
<c:set var="codigoErrorHeader" scope="application"><spring:eval expression="@propiedades.getProperty('header.codigo.error')" /></c:set>
<c:set var="codigoHeaderLigthBox" scope="application"><spring:eval expression="@propiedades.getProperty('header.codigo.ligthbox')" /></c:set>

<c:set var="inicioRelojEnMinutos" scope="application"><spring:eval expression="@propiedades.getProperty('sesion.limite.tiempo.minutos')" /></c:set>
<c:set var="alertaSesionEnMinutos" scope="application"><spring:eval expression="@propiedades.getProperty('sesion.alerta.tiempo.minutos')" /></c:set>

<script>var inicioRelojEnMinutos = ${inicioRelojEnMinutos};</script>
<script>var alertaSesionEnMinutos = ${alertaSesionEnMinutos};</script>
<script>var codigoErrorHeader = "${codigoErrorHeader}";</script>
<script>var codigoHeaderLigthBox = "${codigoHeaderLigthBox}";</script>
<script>var codigoHeaderNoValidarClaveOperacion = "${codigoHeaderNoValidarClaveOperacion}";</script>

<script>var contextPath = "${pageContext.servletContext.contextPath}";</script>
<script>var contextoRecursos = "${contextoRecursos}";</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<%-- <script src="<%= request.getContextPath()%>ST/js/jquery-3.3.1/jquery-3.3.1.min.js"></script> --%>
<script src="<%= request.getContextPath()%>ST/js/jquery-3.3.1/popper.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<%= request.getContextPath()%>ST/js/bootstrap-4.2.1/bootstrap.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath()%>ST/js/iframeResizer.contentWindow.min.js" ></script>