<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 
<c:set var="versionJavascript" scope="application"><spring:eval expression="@propiedades.getProperty('version.javascript')" /></c:set>
<c:set var="codigoErrorHeader" scope="application"><spring:eval expression="@propiedades.getProperty('header.codigo.error')" /></c:set>
<c:set var="codigoHeaderLigthBox" scope="application"><spring:eval expression="@propiedades.getProperty('header.codigo.ligthbox')" /></c:set>

<c:set var="inicioRelojEnMinutos" scope="application"><spring:eval expression="@propiedades.getProperty('sesion.limite.tiempo.minutos')" /></c:set>
<c:set var="alertaSesionEnMinutos" scope="application"><spring:eval expression="@propiedades.getProperty('sesion.alerta.tiempo.minutos')" /></c:set>

<script>var inicioRelojEnMinutos = ${inicioRelojEnMinutos};</script>
<script>var alertaSesionEnMinutos = ${alertaSesionEnMinutos};</script>
<script>var codigoErrorHeader = "${codigoErrorHeader}";</script>
<script>var codigoHeaderLigthBox = "${codigoHeaderLigthBox}";</script>
<script>var codigoHeaderNoValidarClaveOperacion = "${codigoHeaderNoValidarClaveOperacion}";</script>

<script>var contextPath = "${pageContext.servletContext.contextPath}";</script>
<script>var contextoRecursos = "${contextoRecursos}";</script>


	<!-- INI - LIBRERIAS PARA MENU BOOSTRAP 4 -->
	<!-- jQuery 3 -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/jquery/dist/jquery.js"></script>
	
	<!-- jQuery UI 1.11.4 -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/jquery-ui/jquery-ui.js"></script>
	
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button);
	</script>
	
	<!-- popper -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/popper/dist/popper.min.js"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/bootstrap/dist/js/bootstrap.js"></script>	
	
	<!-- Morris.js charts -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/raphael/raphael.min.js"></script>
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/morris.js/morris.min.js"></script>
	
	<!-- Sparkline -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/jquery-sparkline/dist/jquery.sparkline.js"></script>
	
	<!-- daterangepicker -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/moment/min/moment.min.js"></script>
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/bootstrap-daterangepicker/daterangepicker.js"></script>
		
	<!-- datepicker -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
	
	<!-- Bootstrap WYSIHTML5 -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"></script>
	
	<!-- Slimscroll -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/jquery-slimscroll/jquery.slimscroll.js"></script>
	
	<!-- FastClick -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/fastclick/lib/fastclick.js"></script>
	
	<!-- peity -->
	<script src="<%= request.getContextPath()%>ST/assets/vendor_components/jquery.peity/jquery.peity.js"></script>
	
	<!-- Minimalelite Admin App -->
	<script src="<%= request.getContextPath()%>ST/js/menuSiade/template.js"></script>
	
	<!-- Minimalelite Admin dashboard demo (This is only for demo purposes) -->
	<script src="<%= request.getContextPath()%>ST/js/menuSiade/pages/dashboard.js"></script>
	
	<!-- Minimalelite Admin for demo purposes -->
	<script src="<%= request.getContextPath()%>ST/js/menuSiade/demo.js"></script>	
	
    <!-- Vector map JavaScript -->
    <script src="<%= request.getContextPath()%>ST/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<%= request.getContextPath()%>ST/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<%= request.getContextPath()%>ST/assets/vendor_components/jvectormap/lib2/jquery-jvectormap-us-aea-en.js"></script>
    
    <!-- FIN - LIBRERIAS PARA MENU BOOSTRAP 4 -->
	
	<script type="text/javascript" src="<%= request.getContextPath()%>ST/js/iframeResizer.contentWindow.min.js" ></script>