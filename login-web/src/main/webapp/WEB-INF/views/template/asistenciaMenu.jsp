<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
	<title><tiles:insertAttribute name="title" /></title>
	<tiles:insertAttribute name="resources" ignore="true" />
	
	<tiles:insertAttribute name="css" />	
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>
		
	<tiles:insertAttribute name="body" />
	
	<!--[if lt IE 10]>
    <script>
		// To test the @id toggling on password inputs in browsers that don’t support changing an input’s @type dynamically (e.g. Firefox 3.6 or IE), uncomment this:
		// $.fn.hide = function() { return this; }
		// Then uncomment the last rule in the <style> element (in the <head>).
		$(function() {
			// Invoke the plugin
			$('input, textarea').placeholder({customClass:'my-placeholder'});
			// That’s it, really.
			
			var html;

			if ($.fn.placeholder.input && $.fn.placeholder.textarea) {
				html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> and <code>textarea</code> elements.</strong> The plugin won’t run in this case, since it’s not needed. If you want to test the plugin, use an older browser.';
			} else if ($.fn.placeholder.input) {
				html = '<strong>Your current browser natively supports <code>placeholder</code> for <code>input</code> elements, but not for <code>textarea</code> elements.</strong> The plugin will only do its thang on the <code>textarea</code>s.';
			}

			if (html) {
				$('<p class="note">' + html + '</p>').insertBefore('form');
			}
		});
	</script>
	<![endif]-->
    <tiles:insertAttribute name="scripts" ignore="true" />  
    <tiles:insertAttribute name="js" ignore="true" />
</body>
</html>