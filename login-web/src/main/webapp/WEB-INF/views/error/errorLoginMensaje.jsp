<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Error</tiles:putAttribute>
<tiles:putAttribute name="css">
	<link href="${pageContext.request.contextPath}ST/css/error-login.css" rel="stylesheet">
</tiles:putAttribute>

<tiles:putAttribute name="body">

<div class="well well-lg well-bbva">
	<div class="row">
		<div class="col-sm-12" align="center">
			<h3><b>¡La Clave y/o Usuario no son correctas!</b></h3>
		</div>
		<div class="col-sm-12" align="center">
			<img src="${pageContext.request.contextPath}ST/img/alert.ico" class="img-responsive" style="width:80px"><br>
		</div>
		
		<div class="col-sm-12" align="center">
			<p>Por favor intente nuevamente.</p><br>
		</div>
		<div class="col-sm-12" align="center">
			<a href="<c:url value="/siade/v2"/>" class="btn btn-primary">Volver</a>
		</div>
	</div>
</div>

</tiles:putAttribute>
</tiles:insertDefinition>