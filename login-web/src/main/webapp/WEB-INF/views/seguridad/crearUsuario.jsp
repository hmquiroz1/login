<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">SIADE :: Sistema Integrado de Administracion de Edificios</tiles:putAttribute>

<tiles:putAttribute name="css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}ST/css/bootstrap-4.0.0/bootstrap.min.css">

	<!-- Bootstrap extend-->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}ST/css/loginSiade/bootstrap-extend.css">

	<!-- Theme style -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}ST/css/loginSiade/master_style.css">

	<!-- Minimalelite Admin skins -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}ST/css/loginSiade/skins/_all-skins.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</tiles:putAttribute>		

<tiles:putAttribute name="body">
	<div class="register-box">
	  <div class="register-logo">
	    <a href="../../index.html"><b>SIADE </b>Admin</a>
	  </div>
	
	  <div class="register-box-body">
	    <p class="login-box-msg">Registrar un nuevo usuario</p>
	
	    <form id="formUsuario" name="formUsuario" action="<c:url value="registrar-usuario"/>" method="post" class="form-element">
	      <div class="form-group has-feedback">
	        <input name="nombre" type="text" class="form-control" placeholder="Nombres Completos">
	        <span class="ion ion-person form-control-feedback "></span>
	      </div>
	      <div class="form-group has-feedback">
	        <input name="paterno" type="text" class="form-control" placeholder="Apellido Paterno">
	        <span class="ion ion-person form-control-feedback "></span>
	      </div>
	      <div class="form-group has-feedback">
	        <input name="materno" type="text" class="form-control" placeholder="Apellido Materno">
	        <span class="ion ion-person form-control-feedback "></span>
	      </div>
	      <div class="form-group has-feedback">
	        <input name="email" type="email" class="form-control" placeholder="Email">
	        <span class="ion ion-email form-control-feedback "></span>
	      </div>
	      <div class="form-group has-feedback">
	        <input name="clave" type="password" class="form-control" placeholder="Password">
	        <span class="ion ion-locked form-control-feedback "></span>
	      </div>
	      <div class="form-group has-feedback">
	        <input type="password" class="form-control" placeholder="Repetir password">
	        <span class="ion ion-log-in form-control-feedback "></span>
	      </div>
	      <div class="row">
	        <div class="col-12">
	          <div class="checkbox">
	            <input type="checkbox" id="basic_checkbox_1" >
				<label for="basic_checkbox_1">Acepto los <a href="#"><b>Terminos</b></a></label>
	          </div>
	        </div>
	        <!-- /.col -->
	        <div class="col-12 text-center">
	          <button id="btnRegistrar" type="button" class="btn btn-info btn-block margin-top-10">REGISTRAR</button>
	        </div>
	        <!-- /.col -->
	      </div>
	    </form>
		
		<div class="social-auth-links text-center">
	      <p>- O -</p>
	      <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
	      <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
	    </div>
		<!-- /.social-auth-links -->
	    
	     <div class="margin-top-20 text-center">
	    	<p>Ya tienes una cuenta?<a href="<c:url value="v2"/>" class="text-info m-l-5"> Ingresar</a></p>
	     </div>
	    
	  </div>
	  <!-- /.form-box -->
	</div>
	<!-- /.register-box -->
</tiles:putAttribute>

<tiles:putAttribute name="script">	
	<!-- jQuery 3 -->
	<script src="${pageContext.request.contextPath}ST/js/jquery-3.2.1/jquery.min.js"></script>

	<!-- popper -->
	<script src="${pageContext.request.contextPath}ST/js/popper-1.12.0/popper.min.js"></script>

	<!-- Bootstrap 4.0-->
	<script src="${pageContext.request.contextPath}ST/js/bootstrap-4.0.0/bootstrap.min.js"></script>
</tiles:putAttribute>

<tiles:putAttribute name="js">
		<script type="text/javascript">
			$(document).ready(function() {
				console.log('Carga de Registrar Usuario');
				$("#bodyLogin").addClass('hold-transition');
				$("#bodyLogin").addClass('register-page');
				
				$("#btnRegistrar").click(function( event ) {	  		
			  		document.getElementById("btnRegistrar").disabled = true;
			   		var enviar=1;
			   		//enviar=enviar*validarEmail(true);
			   		//enviar=enviar*validarClave(true);	   		
			   		
					if(enviar==0){
						document.getElementById("submit1").disabled = false;
						event.preventDefault();
					}else{
						document.formUsuario.submit();
						
						/*$.ajax({
						    url : "/login-web/validarLogin",
						    data : {registro: $("input[name='registro']").val(),
				   					clave:$("input[name='clave']").val()},
						    type : 'POST',
//		 		   			beforeSend : function() {		   				
//				   		  		//modalText('Danos un momento mientras buscamos los datos de tu vehículo...');
//				   			},
						    success : function(data, status) {				    	
						    	console.info('Sucess');
						    },
						    error : function(jqXHR, exception) {					    
							    	alert('ocurrió un error');					   					    
						    }
//		 		   			complete : function(e) {
//				   				console.info('Complete');
//				   			}

						});*/
						
					}
			   	});
			});
		</script>
</tiles:putAttribute>
</tiles:insertDefinition>