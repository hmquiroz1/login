<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">SIADE :: Sistema Integrado de Administracion de Edificios</tiles:putAttribute>

<tiles:putAttribute name="css">
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}ST/css/bootstrap-4.0.0/bootstrap.min.css">

	<!-- Bootstrap extend-->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}ST/css/loginSiade/bootstrap-extend.css">

	<!-- Theme style -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}ST/css/loginSiade/master_style.css">

	<!-- Minimalelite Admin skins -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}ST/css/loginSiade/skins/_all-skins.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</tiles:putAttribute>		

<tiles:putAttribute name="body">
	<div class="register-box">
	  <div class="register-logo">
	    <a href="../../index.html"><b>SIADE </b>Admin</a>
	  </div>
	
	  <div class="register-box-body">
	    <p class="login-box-msg">Cuenta creada correctamente</p>
	
	    
		
		<div class="social-auth-links text-center">
	      <p>- O -</p>
	      <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
	      <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
	    </div>
		<!-- /.social-auth-links -->
	    
	     <div class="margin-top-20 text-center">
	    	<p>Deseas Loguearte?<a href="<c:url value="v2"/>" class="text-info m-l-5"> Ingresar</a></p>
	     </div>
	    
	  </div>
	  <!-- /.form-box -->
	</div>
	<!-- /.register-box -->
</tiles:putAttribute>

<tiles:putAttribute name="script">	
	<!-- jQuery 3 -->
	<script src="${pageContext.request.contextPath}ST/js/jquery-3.2.1/jquery.min.js"></script>

	<!-- popper -->
	<script src="${pageContext.request.contextPath}ST/js/popper-1.12.0/popper.min.js"></script>

	<!-- Bootstrap 4.0-->
	<script src="${pageContext.request.contextPath}ST/js/bootstrap-4.0.0/bootstrap.min.js"></script>
</tiles:putAttribute>

<tiles:putAttribute name="js">
		<script type="text/javascript">
			$(document).ready(function() {
				console.log('Carga de Registrar Usuario');
				$("#bodyLogin").addClass('hold-transition');
				$("#bodyLogin").addClass('register-page');
			});
		</script>
</tiles:putAttribute>
</tiles:insertDefinition>