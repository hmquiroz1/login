<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>
<tiles:insertDefinition name="mainMenuV2">
<tiles:putAttribute name="title">SIADE :: Sistema Integrado de Administracion de Edificios :: Intranet</tiles:putAttribute>

<tiles:putAttribute name="body">
	<div class="row">
		<div class="col-sm-6 col-xs-12">
			<p>Empresa:</p>
		</div>
		<div class="col-sm-6 col-xs-12">
			<input type="text" name="txtEmpresa" id="txtEmpresa"/>
		</div>
	</div>
</tiles:putAttribute>

<tiles:putAttribute name="js">
		<script type="text/javascript">
			$(document).ready(function() {
				console.log('Carga de Index');
			});
		</script>
</tiles:putAttribute>
	
</tiles:insertDefinition>