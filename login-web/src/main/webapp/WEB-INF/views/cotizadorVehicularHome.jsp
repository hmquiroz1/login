<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/template/include.jsp"%>

<tiles:insertDefinition name="main">
<tiles:putAttribute name="title">Cotizador Vehicular</tiles:putAttribute>
<tiles:putAttribute name="css">

<link href="${pageContext.request.contextPath}ST/css/cotizador-vehicular.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}ST/css/library_progress_bar.css" rel="stylesheet">

</tiles:putAttribute>

<tiles:putAttribute name="body">

<h3>Cotiza tu Seguro Vehicular y solicítalo en segundos aquí</b></h3>
<div class="row">
	<div class="col-sm-12">
		<div class="init-step1" id="barraProgreso">
			<form novalidate="novalidate" class="form-horizontal tooltip-validation transfer-filter">
				<div class="wizard">
					<div data-title="Cotiza" data-multistep="true">
					</div>
					<div data-title="Solicita" data-multistep="true">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
	
<div class="box_principal">
	<form id="formSeguro" name="formSeguro" action="<c:url value="cotizador-vehicular-mensaje"/>" method="post">
		<input type="hidden" id="anho" name="anho" value="${anho}">
		<input type="hidden" id="valorComercialMin" name="valorComercialMin">
		<input type="hidden" id="valorComercialMax" name="valorComercialMax">
		<div class="row">
			<div class="col-sm-6">
				<div class="seccion">
					<div class="numberCircle hidden-xs">1</div>
					<div class="tabla_input">
						<div class="row">
							<div class="col-sm-12 pad_tb_15">
								<b class="t_azul t_19 bbva_web_book">Ingresa tu número de placa para cargar los datos de tu auto</b><br>
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-sm-12">
								<div class="row">			
									<div class="col-xs-10 col-md-10 pd_right_0">
										<div class="form-group">
											<input type="text" class="errorTooltip form-control" id="numeroPlaca" name="numeroPlaca" placeholder="Número de Placa (ej.: FAU045)" autocomplete="off" maxlength="6" length="30">
											<span class="pd_condiciones" id="legalPlaca"><a id="lnkCondiciones" data-toggle="popover" data-placement="bottom" class="pointer not-underline-link"><u>Ver condiciones aquí</u></a></span>
											<span class="pd_condiciones hidden" id="placaNotFound" style="font-size:14px;font-weight: bold;font-family:BBVA Web Light;color: #C4136C;">No tenemos resultados para este número de placa</span>
										</div>
									</div>
									<div class="col-xs-2 col-md-2 div_btn_placa">
										<button id="btnPlaca" name="btnPlaca" type="button" class="btn btn-default btn_placa"></button>
									</div>
								</div>
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-xs-10 col-md-10 pd_right_0">
								<span class="t_18" id="lblDatos">O ingrésalos directamente aquí</span>
							</div>
							<div class="col-xs-2 col-md-2">
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-xs-10 col-md-10 pd_right_0">
								<div class="form-group" id="form-group-marca">
									<select class="select-picker form-control" data-live-search="true" data-style="form-control form-select" id="marca" name="marca" autocomplete="off">
									<option value="">Marca</option>
									<c:forEach var="marca" items="${marcas}">
									<option value="${marca.codigo}">${marca.nombre}</option>
									</c:forEach>
									</select>
								</div>
							</div>
							<div class="col-xs-2 col-md-2 pd_right_0">
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-xs-10 col-md-10 pd_right_0">
								<div class="form-group" id="form-group-modelo">
									<select class="select-picker form-control" data-live-search="true" data-style="form-control form-select" id="modelo" name="modelo" disabled="disabled">
									<option value="">Modelo</option>
									</select>				 
								</div>
							</div>
							<div class="col-xs-2 col-md-2"></div>
						</div>
						<div class="row pad_tb_row hidden" id="divTipo">
							<div class="col-xs-10 col-md-10 pd_right_0">
								<div class="form-group" id="form-group-tipo">
									<select class="select-picker form-control" data-live-search="true" data-style="form-control form-select" id="tipo" name="tipo">
									<option value="">Tipo</option>
									</select>				 
								</div>
							</div>
							<div class="col-xs-2 col-md-2"></div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-xs-10 col-md-10 pd_right_0">
								<div class="form-group" id="divAnhoFabricacion">
									<input type="text" class="errorTooltip form-control" id="anhoFabricacion" name="anhoFabricacion" placeholder="Año Fabricación" autocomplete="off" maxlength="4">
								</div>
							</div>
							<div class="col-xs-2 col-md-2"></div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-xs-10 col-md-10 pd_right_0">				
								<div class="form-group">
									<input type="text" class="errorTooltip form-control" id="valorComercial" name="valorComercial" placeholder="Valor Comercial (en dólares)" autocomplete="off" data-thousands="," data-decimal="." data-precision=0 data-prefix="$ ">
								</div>
							</div>
							<div class="col-xs-2 col-md-2 pd_left_0">
								<div class="text-center infoTooltip"  id="tooltipValorComercial" data-placement="top" data-trigger="hover"  
								title="Establecemos este valor referencial en base a la marca, modelo y año de fabricación de tu vehículo. Puedes aumentarlo o reducirlo en un 10 % como máximo.">
									<span class="icon-help-16"></span>
								</div>
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-xs-10 col-md-10 pd_right_0">
								<div class="form-group" id="form-group-tipoGNV">
									<select class="select-picker form-control" data-style="form-control form-select" id="tipoGNV" name="tipoGNV" autocomplete="off">
										<option value="">Cambio a GNV/GLP</option>
										<option value="1">Si</option>
										<option value="0">No</option>
									</select>
								</div>
							</div>
							<div class="col-xs-2 col-md-2"></div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-xs-10 col-md-10 pd_right_0">
								<div class="form-group" id="form-group-lugarRegistro">
									<select class="select-picker form-control" data-style="form-control form-select" id="lugarRegistro" name="lugarRegistro" autocomplete="off">
										<option value="">Lugar de uso del vehículo</option>
										<c:forEach var="lugarRegistro" items="${lugarRegistros}">
										<option value="${lugarRegistro.codigo}">${lugarRegistro.nombre}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="col-xs-2 col-md-2"></div>
						</div>
					</div>
				</div>
				<div class="seccion">
					<div class="numberCircle hidden-xs">2</div>
					<div class="tabla_input">
						<div class="row pad_tb_row">
							<div class="col-sm-12">
								<b class="t_azul t_19 bbva_web_book">Ingresa tus datos personales</b>
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="errorTooltip form-control" id="nombre" name="nombre" placeholder="Nombre y apellidos" autocomplete="off" maxlength="50">
								</div>
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-sm-12">
								<div class="form-group" id="form-group-tipoDocumento">
									<select class="select-picker form-control" data-style="form-control form-select" id="tipoDocumento" name="tipoDocumento" autocomplete="off" >
									<c:forEach var="tipoDocumento" items="${tipoDocumentos}">
									<option value="${tipoDocumento.codigo}">${tipoDocumento.nombre}</option>
									</c:forEach>
									</select>
								</div>
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="errorTooltip form-control" id="numeroDocumento" name="numeroDocumento" placeholder="Número de Documento" autocomplete="off" maxlength="8" >
								</div>
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="errorTooltip form-control" id="correo" name="correo" placeholder="Correo electrónico" autocomplete="off" maxlength="80" >
								</div>
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="errorTooltip form-control" id="telefono" name="telefono" placeholder="Celular" autocomplete="off" maxlength="9" >
								</div>
							</div>
						</div>
						<div class="row pad_tb_row">
							<div class="col-sm-12">
								<div class="form-group" id="form-group-horarioSeguro">
									<select class="select-picker form-control" data-style="form-control form-select" id="horarioSeguro" name="horarioSeguro" autocomplete="off" >
									<option value="">Horario de contacto</option>
									<c:forEach var="horario" items="${horarios}">
									<option value="${horario.codigo}">${horario.nombre}</option>
									</c:forEach>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 hidden-xs mtop_20 pad_bonus">
				<div class="row">
					<div class="col-sm-12">
						<img src="${pageContext.request.contextPath}ST/img/seguros/img-seguro-vehicular.png" class="img-responsive hidden-xs">		
					</div>
				</div>
				<div class="row mtop_20">
					<div class="col-sm-12">
						<p class="t_resaltado"><strong>Disfruta de nuestros beneficios:</strong></p>	
					</div>
				</div>
				<div id="divBeneficio" >
					<div class="row">
						<div class="col-sm-3 col-xs-4">
							<img src="${pageContext.request.contextPath}ST/img/seguros/icono-19-dolares.jpg" class="img-responsive i_right">		
						</div>
						<div class="col-sm-9 col-xs-8">
							<p class="t_16">Tenemos hasta 3 planes con primas desde $ 19 mensuales</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 col-xs-4">
							<img src="${pageContext.request.contextPath}ST/img/seguros/icono-descuento.png" class="img-responsive i_right">		
						</div>
						<div class="col-sm-9 col-xs-8">
							<p class="t_16">Hasta 15% de descuento en la prima de tu seguro por buen comportamiento financiero</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 col-xs-4">
							<img src="${pageContext.request.contextPath}ST/img/seguros/icono-cuotas.png" class="img-responsive i_right">		
						</div>
						<div class="col-sm-9 col-xs-8">
							<p class="t_16">Paga tu seguro en <span class="t_azul">12 cuotas</span> sin intereses y con débito automático</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<br>
							<p class="t_resaltado"><strong>Y más:</strong></p>	
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 col-xs-4">
							<img src="${pageContext.request.contextPath}ST/img/seguros/icono-repsol.jpg" class="img-responsive i_right">		
						</div>
						<div class="col-sm-9 col-xs-8">
							<p class="t_16">Hasta <span class="t_azul">8% de descuento</span> en Repsol</p>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-3 col-xs-4">
							<img src="${pageContext.request.contextPath}ST/img/seguros/icono-asistencia-grua.png" class="img-responsive i_right">		
						</div>
						<div class="col-sm-9 col-xs-8">
							<p class="t_16">Asistencia de grúa, auxilio mecánico y mucho más</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<div class="checkbox">
		          	<label style="font-size: 1.2em; padding-left: 0px;">
		            	<input type="checkbox" id="autorizacion" name="autorizacion" value="1">
		            	<span id="spanAutorizacion" class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
		            	<a id="lnkDatosPersonales" href="#" style="padding: 0px;text-align: left;white-space: inherit;" class="btn btn-link" data-toggle="modal" data-target="#myModal">He leído y autorizo el tratamiento de datos personales</a>
		          	</label>
		        	</div>
				</div>
			</div>
		</div>
	</form>
	<div class="row">
		<div class="col-sm-12 text-center">
			<button id="btnEnviar" name="btnEnviar" type="button" class="btn btn-primary">Cotizar</button>
		</div>
	</div>
	<div class="row visible-xs">
		<div class="col-xs-12 disfruta_beneficios">
			<div class="row" id="lnkBeneficio">
				<div class="col-xs-11 pd_right_0">
					<b class="t_azul t_14">Disfruta de nuestros beneficios </b>
				</div>
				<div class="col-xs-1 pd_left_0 pd_right_0">
					<span id="lnkArrow" class="t_resaltado glyphicon glyphicon-triangle-bottom"></span>
				</div>
			</div>
			<div id="detalleBeneficios"></div>
		</div>
	</div>
	<div class="row" style="display: none">
		<div class="col-sm-12">
			<div class="form-group">
				<label>
					<input type="text" class="errorTooltip form-control" id="opcional" name="opcional" placeholder="Opcional" autocomplete="off" maxlength="40">
				</label>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="tabla_disclaimers" id="tabla_disclaimers">
			<ul>
				
				<li>El seguro solo aplica para autos particulares. NO aplica para taxis,vehículos de uso comercial.</li>
				<li class="t_resaltado">Al ingresar el número de Placa autorizo al BBVA a completar la información de mi vehículo con los datos de mi tarjeta de propiedad según información de SUNARP.</li>
				<li>Vigencia: Desde 22/08 al 30/09 <br>El monto de la prima de US$19 mensuales (o su equivalente de S/ 62 nuevos soles; calculado al tipo de cambio referencial de S/3.24) es referencial calculada para un auto Nissan March del año 2017, categoría BR2 cuyo valor comercial es de US$ 13690 (Suma Asegurada) y asegurado bajo el Plan Básico. 
				La prima varía en función de la categoría del vehículo, suma asegurada y plan elegido. Prevalecen los términos y condiciones de la póliza del Seguro No. 2001-716888 código SBS RG0502120265 contratada con Rímac Seguros y Reaseguros. En caso de consultas, reclamos y/o siniestros podrás llamar a la Central de Aló Rimac al 411-1111 (Lima) y 0800-41111 (Provincias) o a la Central de Seguro Vehicular BBVA Continental (01) 595-9000.  
				Para mayor información sobre coberturas, exclusiones y condiciones del seguro en
				<a href="https://www.bbvacontinental.pe/" target="_blank" class="t_resaltado">www.bbvacontinental.pe.</a></li>
			</ul>
			<br>
			<p class="disc_importante"><b class="t_resaltado">IMPORTANTE:</b> Prevalecen los términos y condiciones de la póliza del Seguro No. 2001-716888 código SBS RG0502120265 contratada por Rímac Seguros y Reaseguros. En caso de consultas, reclamos y/o siniestros podrás llamar a la Central de Aló Rímac al 411-1111 (Lima) y 0800-4111 (Provincias) o a la Central del Seguro Vehicular BBVA Continental (01)595-9000. Información sobre coberturas, exclusiones y condiciones del Seguro Vehicular en bbvacontinental.pe y en nuestra Central del Seguro Vehicular BBVA Continental (01) 595-9000.
			</p>
		</div>
	</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">AUTORIZACIÓN DE RECOPILACIÓN Y TRATAMIENTO DE DATOS</h4>
			</div>
			<div class="modal-body">
	        &#8226; La información que nos proporciona al solicitar un producto y/o servicio como su nombre, apellido, nacionalidad, estado civil, DNI, ocupación, domicilio, estado de salud, entre otros, así como la referida a los rasgos físicos y/o conducta que lo identifican o lo hacen identificable como es su huella dactilar, su voz, etc. (datos biométricos), conforme a la Ley es considerada como Datos Personales.<br>
			&#8226; Usted nos da su consentimiento libre, previo o expreso e informado para que sus Datos Personales sean tratados por el Banco conforme establecen las normas sobre Protección de Datos Personales. Esta autorización es indefinida.<br>
			&#8226; Sus Datos Personales serán almacenados (guardados) en el Bando de Datos de Clientes del cual el Banco es titular o en cualquier otro que en el futuro podamos establecer. Hemos adoptado las medidas necesarias para mantener segura su información.<br>
			&#8226; Con esta autorización el Banco podrá (i) evaluar si otorga el (los) producto(s) y/o servicio(s) que solicita, (ii) ofrecerle los productos y/o servicios del Banco y/o de terceros vinculados o no (por ejemplo cuentas, préstamos, entre otros), así como enviarle ofertas comerciales o publicidad sobre éstos, (iii)usar y/o transferir (dentro o fuera del país) a terceros vinculados o no al Banco.<br>
			&#8226; IMPORTANTE: Si no da su autorización, no podremos tratar sus Datos Personales, en la forma explicada en esta autorización.<br>
			&#8226; Asimismo, Usted puede revocar el consentimiento para tratar sus Datos Personales. Para ejercer este derecho o cualquier otro que la ley establece con relación a sus Datos Personales deberá presentar una solicitud escrita en nuestras oficinas. Se podrán establecer otros canales para tramitar estas solicitudes, lo que será informado oportunamente por el Banco a través de su página web<br>
			</div>
		</div>
	</div>
</div>
<!-- Modal Error-->
<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="divAlert" class="modal-dialog hidden" style="margin-top: 180px;">
        <div class="modal-content">
			<div class="modal-header" style="border-bottom: 0px;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><br>
            </div>
            <div class="modal-body" align="center" id="errorDetalle">
            </div>
            <div class="modal-footer" style="text-align: center; border-top: 0px;">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
    <div id="textLoading" class="loading-text text-center hidden">
	</div>
	<div id="imgLoading" class="well loading hidden">
		<img src="${pageContext.request.contextPath}ST/img/loader.gif">
	</div>
</div>

</tiles:putAttribute>

<tiles:putAttribute name="script">
<script src="${pageContext.request.contextPath}ST/js/cotizador_seguro_vehicular_datalayer.js"></script>
<%-- <script src="${pageContext.request.contextPath}ST/js/library_progress_bar.js"></script> --%>
<script src="${pageContext.request.contextPath}ST/js/library_progress_bar_plugin.js"></script>
<script type="text/javascript">
	$(window).on("load", function() {
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA, "");
	});
	var flagBuscarPlaca=false;
	$(document).ready(function(){
		$("#numeroPlaca").focus();
		
		setTimeout(function() {
			$('html, body',window.parent.document).animate({
				scrollTop: 0
			}, 'fast');				 
		}, 100);
		
	});
	$("#numeroPlaca").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_NUMERO_PLACA);
	});	
	
	$("#lnkCondiciones").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_LINK_CONDICIONES);
		$("#lnkCondiciones").popover("show");
	});	
	
	$("#form-group-marca").click(function() {
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_COMBO_MARCA);
	});
	
	$("#form-group-modelo").click(function() {
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_COMBO_MODELO);
	});
	
	$("#form-group-tipo").click(function() {
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_COMBO_TIPO);
	});
	
	$("#form-group-tipoGNV").click(function() {
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_COMBO_TIPO_GNV);
	});
	
	$("#form-group-lugarRegistro").click(function() {
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_COMBO_LUGAR_REGISTRO);
	});
	
	$("#form-group-tipoDocumento").click(function() {
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_COMBO_TIPO_DOCUMENTO);
	});
	
	$("#form-group-horarioSeguro").click(function() {
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_COMBO_HORARIO_SEGURO);
	});
	
	$("#spanAutorizacion").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_CHECK_AUTORIZACION);
	});	
	
	$("#lnkDatosPersonales").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_LINK_DATOS_PERSONALES);
	});	
	
	$("#anhoFabricacion").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_ANIO_FABRICACION);
	});
	
	$("#valorComercial").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_VALOR_COMERCIAL);
	});
	
	$("#nombre").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_NOMBRE);
	});
	
	$("#numeroDocumento").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_NUMERO_DOCUMENTO);
	});
	
	$("#correo").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_CORREO);
	});
	
	$("#telefono").click(function(){
		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_TELEFONO);
	});
	
	$('#numeroPlaca').keypress(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$("#btnPlaca").click();
		}
		event.stopPropagation();
	});
	$(".select-picker").selectpicker();
	$(".select-picker").prev().prev().hover(
	  function(e) {
	    $(this).removeAttr('title');
	  },
	  function() {}
	);
	$('#tooltipValorComercial').tooltip();
	$("#modelo").parent().addClass('infoTooltip');
  	$("#modelo").parent().tooltip({trigger : 'hover', title : 'Para seleccionar un modelo debes indicar previamente la marca de tu vehículo'});
  	var elem = '<div class=" text-right"><a class="ct_fff" tabindex="-1" align="right" onclick="$(&quot;#lnkCondiciones&quot;).popover(&quot;hide&quot;);"><b>X</b></a></div>'+
    '<div style="margin-top:-12px;">Al ingresar el número de Placa autorizo al BBVA a completar la información de mi vehículo con los datos de mi tarjeta de propiedad según información de SUNARP.<br>'+
    '<div class=" text-right"><a tabindex="0" class="ct_fff lnk_underline" onclick="$(&quot;#lnkCondiciones&quot;).popover(&quot;hide&quot;);"><b>OK, entendido</b></a></div></div>';
	$('#lnkCondiciones').popover({animation:true, content:elem, html:true, trigger: 'manual'});
  	function modalText(texto){
   		$('#textLoading').html(texto);
   		$('#textLoading').removeClass('hidden');
   		$('#imgLoading').removeClass('hidden');
   		$('#divAlert').addClass('hidden');
   		$('#errorModal').modal('show');
    }
  	$("#lnkBeneficio").click(function(event){
  		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_LINK_BENEFICIO);
    	$('#lnkArrow').toggleClass('glyphicon-triangle-bottom glyphicon-triangle-top');
    	if ($("#lnkArrow").hasClass( "glyphicon-triangle-bottom")){
    		$('#detalleBeneficios').html("");
    	}else{
    		$('#detalleBeneficios').html($('#divBeneficio').html());	
    	}
    	event.preventDefault();
    });
  	$("#btnPlaca").click(function(eventBtnPlaca) {
  		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_BOTON_PLACA);
   		if($("#numeroPlaca").val().length<6){
			$("#numeroPlaca").tooltip({trigger : 'hover', title : 'Escribe el número de placa'});
			$("#numeroPlaca").addClass('form-control-error');
		}else{
			$('#numeroPlaca').tooltip('destroy');
			$("#numeroPlaca").removeClass('form-control-error');
			$('#legalPlaca').removeClass('hidden');
	   		$('#placaNotFound').addClass('hidden');
	   		$('#lblDatos').html("O ingrésalos directamente aquí");
	   		$('#marca').val("");
	   		$('#marca').selectpicker('refresh');
	   		$("#marca").parent().removeClass('errorTooltip');
			$("#marca").parent().tooltip('destroy');
			$("#marca").prev().prev().removeClass('form-control-error');
	   		$("#modelo").empty();
	   		$('#modelo').append('<option value="">Modelo</option>');
			$('#modelo').selectpicker('refresh');
			$("#modelo").parent().removeClass('errorTooltip');
			$("#modelo").parent().tooltip('destroy');
			$("#modelo").prev().prev().removeClass('form-control-error');
			$("#tipo").empty();
			$('#tipo').append('<option value="">Tipo</option>');
			$('#tipo').selectpicker('refresh');
			$("#tipo").parent().removeClass('errorTooltip');
			$("#tipo").parent().tooltip('destroy');
			$("#tipo").prev().prev().removeClass('form-control-error');
	   		$('#anhoFabricacion').val("");
			$('#anhoFabricacion').tooltip('destroy');
			$("#anhoFabricacion").removeClass('form-control-error');
			$('#valorComercial').val("");
			$('#valorComercial').tooltip('destroy');
	   		$("#valorComercial").removeClass('form-control-error');
	   		$("#tipoGNV").val("");
			$('#tipoGNV').selectpicker('refresh');
			$("#tipoGNV").parent().removeClass('errorTooltip');
			$("#tipoGNV").parent().tooltip('destroy');
			$("#tipoGNV").prev().prev().removeClass('form-control-error');
			$("#lugarRegistro").val("");
			$('#lugarRegistro').selectpicker('refresh');
			$("#lugarRegistro").parent().removeClass('errorTooltip');
			$("#lugarRegistro").parent().tooltip('destroy');
			$("#lugarRegistro").prev().prev().removeClass('form-control-error');
			$.ajax({
	   			contentType : "application/json",
	   			url : "cotizador-placa",
	   			data : {numeroPlaca: $('#numeroPlaca').val()},
	   			beforeSend : function() {
	   				flagBuscarPlaca=true;
	   		  		modalText('Danos un momento mientras buscamos los datos de tu vehículo...');
	   			},
	   			success : function(vehSunarp) {
	   				if(vehSunarp.length==0){
	   					$('#placaNotFound').removeClass('hidden');
   	   					$('#lblDatos').html("Por favor, ingresa los datos aquí");	
   	   					$('#legalPlaca').addClass('hidden');
   	   					$("#numeroPlaca").addClass('form-control-error');
		   				$('#errorModal').modal('hide');
	   				}else if(vehSunarp.anhoFabricacion < $("#anho").val()-19){
	   					$('#textLoading').html('');
	   			   		$('#textLoading').addClass('hidden');
	   			   		$('#imgLoading').addClass('hidden');
	   			   		$('#divAlert').removeClass('hidden');
	   			   		$('#errorDetalle').html('Tu vehículo no es asegurable por tener una antigüedad mayor a 20 años');
	   				}else{
	   					$('#marca').val(vehSunarp.marca);
		   				$('#marca').selectpicker('refresh');
		   				buscarModelos();
		   				$('#modelo').val(vehSunarp.modelo);
		   				$('#modelo').selectpicker('refresh');
		   				buscarTipos();
		   				$('#tipo').val(vehSunarp.tipo);
		   				$('#tipo').selectpicker('refresh');
		   				$('#anhoFabricacion').val(vehSunarp.anhoFabricacion);
		   				buscarValorComercial();
		   				$('#errorModal').modal('hide');
	   				}
	   			},
	    		error : function(e) {
	    				
	   			},
	   			complete : function(e) {
	   		   		flagBuscarPlaca=false;
	   			}
	   		});
		}
   	});
  	$("#btnEnviar").click(function( eventBtnEnviar ) {
  		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_BOTON_COTIZAR);
  		document.getElementById("btnEnviar").disabled = true;
   		var enviar=1;
   		enviar=enviar*horarioSeguro(true);
   		enviar=enviar*telefono(true);
   		enviar=enviar*correo(true);
   		enviar=enviar*numeroDocumento(true);
   		enviar=enviar*nombre(true);
   		enviar=enviar*lugarRegistro(true);
   		enviar=enviar*tipoGNV(true);
   		enviar=enviar*valorComercial(true);
   		enviar=enviar*anhoFabricacion(true);
   		enviar=enviar*tipo(true);
   		enviar=enviar*modelo(true);
   		enviar=enviar*marca(true);
   		
		if(enviar==0){
			document.getElementById("btnEnviar").disabled = false;
			eventBtnEnviar.preventDefault();
		}else{
			_satellite.track("cotizadorVehicularEvent57");
			setTimeout(function() {document.formSeguro.submit();
			}, 650);
		}
   	});
  	$("#numeroPlaca").mask('AAAAAA', {
        onKeyPress: function (value, event) {
            event.currentTarget.value = value.toUpperCase();
        }
	});
  	$("#nombre").alpha({disallow :'¿¡'});
  	$("#tipoDocumento").change(function(){
    	$("#numeroDocumento").unbind();
    	$("#numeroDocumento").click(function(){
    		llenarDataLayerCotizadorSeguroVehicular(TAG_COTIZA_EVENTO_CLICK, DESCRIPCION_NUMERO_DOCUMENTO);
    	});
    	$("#numeroDocumento").blur(function(){numeroDocumento();});
   		if($("#tipoDocumento").val()=='L'){
   			$("#numeroDocumento").attr("maxlength", "8");
   	    	$("#numeroDocumento").numeric({allowMinus:false});
   		}else if($("#tipoDocumento").val()=='E' || $("#tipoDocumento").val()=='D'){
   			$("#numeroDocumento").attr("maxlength", "12");
   	    	$("#numeroDocumento").alphanum({allow :'/-', allowSpace :false});
   		}else if($("#tipoDocumento").val()=='P'){
   			$("#numeroDocumento").attr("maxlength", "12");
   	    	$("#numeroDocumento").alphanum({allowSpace :false});
   		}
   	});
   	$("#numeroDocumento").numeric({allowMinus:false});
   	$("#telefono").numeric({allowMinus:false});
   	$("#anhoFabricacion").numeric({allowMinus:false});
  	//$("#valorComercial").mask('000,000,000,000,000', {reverse: true});
  	$("#valorComercial").maskMoney();
  	$("#valorComercial").attr("maxlength", "9");
  	/*$("#valorComercial").maskMoney({  		
  		prefix:'US$ ', // The symbol to be displayed before the value entered by the user
  		allowZero:false, // Prevent users from inputing zero
  		allowNegative:true, // Prevent users from inputing negative values
  		defaultZero:false, // when the user enters the field, it sets a default mask using zero
  		thousands: ',', // The thousands separator
  		decimal: '.' , // The decimal separator
  		precision: 0, // How many decimal places are allowed
  		affixesStay : false, // set if the symbol will stay in the field after the user exits the field.
  		symbolPosition : 'left' // use this setting to position the symbol at the left or right side of the value. default 'left'
  		});*/

  	$("#marca").change(function(){marca(false);buscarModelos();});
  	$("#modelo").change(function(){modelo(false);buscarTipos();});
  	$("#tipo").change(function(){tipo(false);});
  	$("#anhoFabricacion").blur(function(){anhoFabricacion(false);buscarValorComercial();});
  	$("#valorComercial").blur(function(){valorComercial(false);});
   	$("#tipoGNV").change(function(){tipoGNV(false);});
   	$("#lugarRegistro").change(function(){lugarRegistro(false);});
   	$("#nombre").blur(function(){nombre(false);});
   	$("#numeroDocumento").blur(function(){numeroDocumento(false);});
   	$("#correo").blur(function(){correo(false);});
   	$("#telefono").blur(function(){telefono(false);});
   	$("#horarioSeguro").change(function(){horarioSeguro(false);});
  	function buscarModelos() {
   		$("#modelo").empty();
   		$("#divTipo").addClass('hidden');
   		if($("#marca").val()==''){
   			$("#modelo").empty();
	   		$('#modelo').append('<option value="">Modelo</option>');
   			$('#modelo').attr("disabled","disabled");
			$("#modelo").parent().addClass('infoTooltip');
			$("#modelo").parent().removeClass('errorTooltip');
	      	$("#modelo").parent().tooltip({trigger : 'hover', title : 'Para seleccionar un modelo debes indicar previamente la marca de tu vehículo'});
			$("#modelo").parent().attr('data-original-title', 'Para seleccionar un modelo debes indicar previamente la marca de tu vehículo').tooltip('fixTitle');
			$("#modelo").prev().prev().removeClass('form-control-error');
			$('#modelo').selectpicker('refresh');
   			return;
   		}
   		$.ajax({
   			contentType : "application/json",
   			url : "cotizador-marca",
   			async : false,
   			data : {marca: $("#marca").val()},
   			success : function(vehMarca) {
   				var opcs = '<option value="">Modelo</option>' ;
   				$(vehMarca.vehModelos).each(function(index){
   				    opcs += '<option value="'+this.codigo+'">'+this.nombre+'</option>' ;
   				});
   				$('#modelo').append(opcs);
   				if(vehMarca.vehModelos!=null){
   					$('#modelo').removeAttr("disabled");
   					$("#modelo").parent().tooltip('destroy');
   					$("#modelo").parent().removeClass('infoTooltip');
   					$("#modelo").parent().addClass('errorTooltip');
   					$("#modelo").parent().removeAttr('data-original-title');
   				}else{
   					$('#modelo').attr("disabled","disabled");
   					$("#modelo").parent().addClass('infoTooltip');
   					$("#modelo").parent().removeClass('errorTooltip');
   			      	$("#modelo").parent().tooltip({trigger : 'hover', title : 'Para seleccionar un modelo debes indicar previamente la marca de tu vehículo'});
					$("#modelo").parent().attr('data-original-title', 'Para seleccionar un modelo debes indicar previamente la marca de tu vehículo').tooltip('fixTitle');
					$("#modelo").prev().prev().removeClass('form-control-error');
   				}
				$('#modelo').selectpicker('refresh');
   			},
    		error : function(e) {
   			},
   			complete : function(e) {
   			}
   		});
   	}
  	function buscarTipos() {
   		$("#tipo").empty();
   		$("#divTipo").addClass('hidden');
   		if($("#modelo").val()==''){
   			return;
   		}
   		$.ajax({
   			contentType : "application/json",
   			url : "cotizador-modelo",
   			async : false,
   			data : {marca: $("#marca").val(), modelo: $("#modelo").val()},
   			success : function(vehModelo) {
   				if(vehModelo.vehTipos!=null){
   					if(vehModelo.vehTipos.length>1){
   						var opcs = '<option value="">Tipo</option>' ;
   						$("#divTipo").removeClass('hidden');
   					}
   					$(vehModelo.vehTipos).each(function(index){
   	   				    opcs += '<option value="'+this.codigo+'">'+this.nombre+'</option>' ;
   	   				});
   	   				$('#tipo').append(opcs);
   				}
				$('#tipo').selectpicker('refresh');
   			},
    		error : function(e) {
   			},
   			complete : function(e) {
   			}
   		});
   	}
  	function buscarValorComercial() {
  		if($("#marca").val()=='' || $("#modelo").val()=='' || 
  				anhoFabricacion(false)==0){
  			return;
  		}
   		$('#valorComercialMin').val(0);
   		$('#valorComercialMax').val(0);
		$('#valorComercial').val('');
		$('#valorComercial').tooltip('destroy');
		$("#valorComercial").removeClass('form-control-error');
   		$.ajax({
   			contentType : "application/json",
   			url : "cotizador-valor",
   			async : false,
   			data : {marca: $("#marca").val(),modelo: $("#modelo").val(),anhoFabricacion: $("#anhoFabricacion").val()},
   			timeout : 5000,
   			beforeSend : function() {
   				if(!flagBuscarPlaca){
   		  			modalText('Danos un momento mientras calculamos el valor de tu vehículo...');
   		  		}
   			},
   			success : function(vehAutoMas) {
   				if(vehAutoMas.valorComercial!=null){
   					$('#valorComercialMin').val(vehAutoMas.valorComercialMin);
   	   				$('#valorComercialMax').val(vehAutoMas.valorComercialMax);
   					$('#valorComercial').val('$ ' + $.number(vehAutoMas.valorComercial,0, '.', ','));
   				}
   			},
    		error : function(e) {
   			},
   			complete : function(e) {
   				$('#errorModal').modal('hide');
   			}
   		});
   	}
  	function marca(focus){
   		$("#marca").val($("#marca").val());
		$("#marca").parent().removeClass('errorTooltip');
		$("#marca").parent().tooltip('destroy');
		$("#marca").prev().prev().removeClass('form-control-error');
   		if($("#marca").val()==''){
   			$("#marca").parent().addClass('errorTooltip');
	      	$("#marca").parent().tooltip({trigger : 'hover', title : 'Selecciona la marca de tu auto'});
	      	$("#marca").prev().prev().addClass('form-control-error');
	      	if(focus){$("#marca").focus();}
			return 0;
		}else{
			return 1;
		}
   	}
  	function modelo(focus){
   		$("#modelo").val($.trim($("#modelo").val()));
   		if($("#modelo").val()==''){
   			$("#modelo").parent().removeClass('infoTooltip');
   			$("#modelo").parent().addClass('errorTooltip');
	      	$("#modelo").parent().tooltip({trigger : 'hover', title : 'Selecciona el modelo de tu auto'});
	      	$("#modelo").parent().attr('data-original-title', 'Selecciona el modelo de tu auto').tooltip('fixTitle');
	      	$("#modelo").prev().prev().addClass('form-control-error');
   			if(focus){$("#modelo").focus();}
			return 0;
		}else{
	   		$("#modelo").parent().removeClass('errorTooltip');
			$("#modelo").parent().tooltip('destroy');
			$("#modelo").prev().prev().removeClass('form-control-error');
			return 1;
		}
   	}
  	function tipo(focus){
   		$("#tipo").val($.trim($("#tipo").val()));
   		$("#tipo").parent().removeClass('errorTooltip');
		$("#tipo").parent().tooltip('destroy');
		$("#tipo").prev().prev().removeClass('form-control-error');
   		if($("#tipo").val()==''){
   			$("#tipo").parent().addClass('errorTooltip');
	      	$("#tipo").parent().tooltip({trigger : 'hover', title : 'Selecciona el tipo de tu vehículo'});
	      	$("#tipo").prev().prev().addClass('form-control-error');
   			if(focus){$("#tipo").focus();}
			return 0;
		}else{
			return 1;
		}
   	}
  	function anhoFabricacion(focus){
   		if($("#anhoFabricacion").val().length!=4){
   			$("#anhoFabricacion").tooltip({trigger : 'hover', title : 'Escribe el año de fabricación de tu auto'});
   			$("#anhoFabricacion").attr('data-original-title', 'Escribe el año de fabricación de tu auto').tooltip('fixTitle');
			$("#anhoFabricacion").addClass('form-control-error');
			if(focus){$("#anhoFabricacion").focus();}
			return 0;
   		}else if( $("#anhoFabricacion").val() < $("#anho").val()-19){
			$("#anhoFabricacion").tooltip({trigger : 'hover', title : 'Tu vehículo no es asegurable por tener una antiguedad mayor a 20 años'});
			$("#anhoFabricacion").attr('data-original-title', 'Tu vehículo no es asegurable por tener una antigüedad mayor a 20 años').tooltip('fixTitle');
			$("#anhoFabricacion").addClass('form-control-error');
			if(focus){$("#anhoFabricacion").focus();}
			return 0;
   		}else if ($("#anhoFabricacion").val() > $("#anho").val()){
			$("#anhoFabricacion").tooltip({trigger : 'hover', title : 'Ingresa un año de fabricación que no exceda el año actual'});
			$("#anhoFabricacion").attr('data-original-title', 'Ingresa un año de fabricación que no exceda el año actual').tooltip('fixTitle');
			$("#anhoFabricacion").addClass('form-control-error');
			if(focus){$("#anhoFabricacion").focus();}
			return 0;
		}else{
			$('#anhoFabricacion').tooltip('destroy');
			$("#anhoFabricacion").removeClass('form-control-error');
			return 1;
		}
   	}
  	function valorComercial(focus){
       	var tmonto=$('#valorComercial').val().replace(/,/g,"").replace("$","")*1.0;
       	var tMin=$('#valorComercialMin').val().toString();
       	var tMax=$('#valorComercialMax').val().toString();
   		if(tmonto==0 || isNaN(tmonto)){
   			$("#valorComercial").tooltip({trigger : 'hover', title : 'Escribe el valor comercial de tu auto'});
   			$("#valorComercial").attr('data-original-title', 'Escribe el valor comercial de tu auto').tooltip('fixTitle');
   			$("#valorComercial").addClass('form-control-error');
   			if(focus){$("#valorComercial").focus();}
   			return 0;
   		}else if(tMin>0 && tMax>0 && (tmonto<tMin || tmonto>tMax)){
   			var tAdicional='Ingresa un valor comercial entre $' + $.number(tMin,0, '.', ',') + ' y $' + $.number(tMax,0, '.', ',');
   			$("#valorComercial").tooltip({trigger : 'hover', title : tAdicional});
   			$("#valorComercial").attr('data-original-title', tAdicional).tooltip('fixTitle');
   			$("#valorComercial").addClass('form-control-error');
   			if(focus){$("#valorComercial").focus();}
   			return 0;
   		}else{
   			$('#valorComercial').tooltip('destroy');
   			$("#valorComercial").removeClass('form-control-error');
   			return 1;
   		}
	}
  	function tipoGNV(focus){
   		$("#tipoGNV").val($.trim($("#tipoGNV").val()));
		$("#tipoGNV").parent().removeClass('errorTooltip');
		$("#tipoGNV").parent().tooltip('destroy');
		$("#tipoGNV").prev().prev().removeClass('form-control-error');
   		if($("#tipoGNV").val()=='' || ($("#tipoGNV").val()!='1' && $("#tipoGNV").val()!='0')){
   			$("#tipoGNV").parent().addClass('errorTooltip');
   			$("#tipoGNV").parent().tooltip({trigger : 'hover', title : 'Selecciona si tu auto ha sido cambiado a GNV/GLP'});
   			$("#tipoGNV").prev().prev().addClass('form-control-error');
   			if(focus){$("#tipoGNV").focus();}
			return 0;
		}else{
			return 1;
		}
	}	
	function lugarRegistro(focus){
   		$("#lugarRegistro").val($.trim($("#lugarRegistro").val()));
		$("#lugarRegistro").parent().removeClass('errorTooltip');
		$("#lugarRegistro").parent().tooltip('destroy');
		$("#lugarRegistro").prev().prev().removeClass('form-control-error');
   		if($("#lugarRegistro").val()==''){
   			$("#lugarRegistro").parent().addClass('errorTooltip');
   			$("#lugarRegistro").parent().tooltip({trigger : 'hover', title : 'Selecciona el lugar donde usas tu vehículo.'});
   			$("#lugarRegistro").prev().prev().addClass('form-control-error');
   			if(focus){$("#lugarRegistro").focus();}
			return 0;
		}else{
			return 1;
		}
	}
	function nombre(focus){
   		$("#nombre").val($.trim($("#nombre").val()));
   		if($("#nombre").val()==''){
   			$("#nombre").tooltip({trigger : 'hover', title : 'Escribe tu nombre y apellidos'});
			$("#nombre").addClass('form-control-error');
			if(focus){$("#nombre").focus();}
			return 0;
		}else{
			$('#nombre').tooltip('destroy');
			$("#nombre").removeClass('form-control-error');
			return 1;
		}
   	}
	function numeroDocumento(focus){
   		if($("#numeroDocumento").val()=='' || 
				($("#numeroDocumento").val().length!=8 && $("#tipoDocumento").val()=='L')){
			$("#numeroDocumento").tooltip({trigger : 'hover', title : 'Escribe un número de documento válido'});
			$("#numeroDocumento").addClass('form-control-error');
			if(focus){$("#numeroDocumento").focus();}
			return 0;
		}else{
			$('#numeroDocumento').tooltip('destroy');
			$("#numeroDocumento").removeClass('form-control-error');
			return 1;
		}
   	}
	function correo(focus){
   		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   		if($("#correo").val()=='' || !regex.test($("#correo").val())){
			$("#correo").tooltip({trigger : 'hover', title : 'Escribe un correo válido'});
			$("#correo").addClass('form-control-error');
			if(focus){$("#correo").focus();}
			return 0;
		}else{
			$('#correo').tooltip('destroy');
			$("#correo").removeClass('form-control-error');
			return 1;
		}
   	}
	function telefono(focus){
   		if($("#telefono").val().length<9 || 
   				$("#telefono").val().length>9){
			$("#telefono").tooltip({trigger : 'hover', title : 'Escribe un número de teléfono válido'});
			$("#telefono").addClass('form-control-error');
			if(focus){$("#telefono").focus();}
			return 0;
		}else{
			$('#telefono').tooltip('destroy');
			$("#telefono").removeClass('form-control-error');
			return 1;
		}
   	}
	function horarioSeguro(focus){
   		$("#horarioSeguro").val($.trim($("#horarioSeguro").val()));
		$("#horarioSeguro").parent().removeClass('errorTooltip');
		$("#horarioSeguro").parent().tooltip('destroy');
		$("#horarioSeguro").prev().prev().removeClass('form-control-error');
   		if($("#horarioSeguro").val()==''){
   			$("#horarioSeguro").parent().addClass('errorTooltip');
   			$("#horarioSeguro").parent().tooltip({trigger : 'hover', title : 'Selecciona un horario válido'});
   			$("#horarioSeguro").prev().prev().addClass('form-control-error');
   			if(focus){$("#horarioSeguro").focus();}
			return 0;
		}else{
			return 1;
		}
	}
	
	(function($) {
		$.widget("marmots.wizard", {
			
			// plugin vars
			current: 0,
			currentMultiStep: 0,
			animating: false,
			animatingHeight: false,
			scrolling: false,
			steps: [],
			stepContainer: undefined,
			visualization: undefined,
			buttonBar: undefined,
			form: undefined,
			checkAlert: undefined,	

			// default options
			options : {
				labels: {
					prev: 'Previous',
					next: 'Next',
					confirm: 'Confirm',
					done: 'Done',
					cancel: 'Cancelar',
					aceptar: 'Aceptar'
				}
			},

			// the constructor
			_create : function() {
				var self = this;
				
				self.steps = self.element.children();
				self.form = self.element.closest('form').length == 0?undefined:self.element.closest('form');
				
				$(window).on('resize', function(){
					self._resized();
				});
				
				$(document).on('capgemini.wizard.resize', function(){
					// SOLO aplica a desktop. Setea overflow hidden para evitar que se vean partes crecientes.
					// Se cambiará a visible al finalizar la animación del _animateHeight
					var $stepContainer = self.element.find('.step-container');
					if($stepContainer.length > 0) {
						$stepContainer.css('overflow', 'hidden');
					}
	        		
					setTimeout(function() {
						self._animateHeight(false);
					}, 100);
				});
				
				$(document).on('capgemini.wizard.resizeNoScroll', function(){
					// SOLO aplica a desktop. Setea overflow hidden para evitar que se vean partes crecientes.
					// Se cambiará a visible al finalizar la animación del _animateHeight
					var $stepContainer = self.element.find('.step-container');
					if($stepContainer.length > 0) {
						$stepContainer.css('overflow', 'hidden');
					}
	        		
					setTimeout(function() {
						self._animateHeight(false);
					}, 100);
				});
				
				// inicializa botones next internos
				self.element.find('.next').each(function() {
					$(this).on('vclick', function(){
						self.next();
						return false;
					});
				});
				
				self._refresh();
				
				processNoPrev(self);
				processNoNext(self);
				processCancel(self);
				//processClaveDigitalWarning(self);
			},
					
			// return current step
			_current: function(){
				return $(this.steps[this.current]);
			},

			_nextStep: function(){
				return $(this.steps[this.current+1]);
			},

			_prevStep: function(){
				return $(this.steps[this.current-1]);
			},
			
			// Animate container height to step height
	    	_animateHeight: function(doScroll){

	    		var self = this;
	    		if(self.animatingHeight == false) {
		    		self.animatingHeight = true;
		    		
		    		var height = self._current().outerHeight(true) + 5; // TODO (this pixel)
		    		$(this).find('.step').css('height', height);
		    		
		    		if(isIE8()) {
		    			self.element.find('.step-container').css('height', height);
		    			self.animatingHeight = false;
		    		}
		    		else {
		    			self.element.find('.step-container').animate({
			    			height: height
			    		}, function(){
			    			if(doScroll == true) {		    				
			    				self._scrollToXX();
			    			}
			    			var $stepContainer = self.element.find('.step-container');
							if($stepContainer.length > 0) {
								$stepContainer.css('overflow', 'visible');
							}					
							self.animatingHeight = false;
			    		});
		    		}
	    		}
			},
			
			// scrollTo
			_scrollToXX: function(){
				var self = this;
				
				var $target = self._current().prev('a.step:visible').length == 0 ? self.element : self._current().prev('a.step');
				var scrollPosition = $target.offset().top;
				
				if((scrollPosition > 0) && !isIE8()) {	
					// en Desktop
					/*if(!Modernizr.mq('(max-width:768px)')){
						scrollPosition = scrollPosition - 65;
						
						// no mueve scroll si esta por encima de 460
						var $position = $(document).scrollTop();
						if($position < 460) {
							return false;
						}
					}*/
					if(self.scrolling == false) {
						setTimeout(function() {
							self.scrolling = true;
							$('html, body').animate({
						        scrollTop: scrollPosition
						    }, 
						    { duration: 'slow',
			    			  complete: function(){
			    				self.scrolling = false;
					    	}});				 
						}, 50);	
					}		
				}
			},
			
			// scrollTo
			_scrollTo: function(){
				/*var self = this;
				
					console.log('_scrollTo ejecutado');
				if(Modernizr.mq('(max-width:768px)')){
					var $target = self._current().prev('a.step:visible').length == 0 ? self.element : self._current().prev('a.step');
					var scrollPosition = $target.offset().top;
					
					setTimeout(function() {
						$('html, body').animate({
					        scrollTop: scrollPosition
					    }, 'slow');				 
					}, 100);	
				}*/
			},
			
			_constructButtons: function() {
				var self = this;
				
				if(self.buttonBar === undefined){
					// -----------------
					// Create button bar
					self.buttonBar = $('<div class="button-bar clearfix"></div>');
					
					if($(this.element).data('otherbuttons') == null) {
						// Previous
						var $prev = $('<button type="button" class="btn arrow-left prev pull-left wizardBack hidden" id="wizardBack">' + self.options.labels.prev + '</button>');
						$prev.on('vclick', function(){
							self.prev();
							return false;						
						});
						self.buttonBar.append($prev);
						
						// Next
						var $next = $('<button class="btn arrow-right next pull-right wizardNext" name="wizardNext" id="wizardNext">' + (self.steps.length > 2 ? self.options.labels.next : self.options.labels.confirm) + '</button>');
						$next.on('vclick', function(){
							self.next();
							return false;
						});
						self.buttonBar.append($next);
					}
					else {
						// Previous
						var $prev = $('<button type="button" class="btn prev pull-left wizardBack" data-dismiss="modal" id="wizardBack">' + self.options.labels.cancel + '</button>');
						self.buttonBar.append($prev);
						
						// Cancel
						var $cancel = $('<button type="button" class="btn cancel hidden" data-dismiss="modal" id="wizardCancel">' + self.options.labels.cancel + '</button>');
						self.buttonBar.append($cancel);
						
						// Next
						var $next = $('<button class="btn next pull-right wizardNext" name="wizardNext" id="wizardNext">' + self.options.labels.aceptar + '</button>');
						$next.on('vclick', function(){
							self.next();
							return false;
						});
						self.buttonBar.append($next);
					}
				}
				return self.buttonBar;
			},
			
			_constructTablet: function() {
				var self = this;
				
				if(self.visualization === undefined || self.visualization !== 'tablet'){
					var step = 0;
					self.steps.each(function(){
						var $step = $('<a href="#step-' + (step++) + '" class="step ' + (self.current > step - 1?'completed':'') + '"><span class="step_img_'+ (step) + ' "></span>' + (step) + '. ' + $(this).data('title') + '</a>');
						self.element.append($step);
						self.element.append($(this).hide());
					});
					self.element.find('> a.step.active').removeClass('active');
					/*if(self._isRemoteStepLoaded()){
						self._loadStepContent(function(){
							self._current().show().append(self._constructButtons()).prev().addClass('active');
						});
					} else {
						self._current().show().append(self._constructButtons()).prev().addClass('active');
					}*/
					
					// Clean elements from desktop display
					if(self.stepContainer !== undefined){
						self.element.find('.wizard-progress-bar').remove();
						self.stepContainer.remove();
						self.stepContainer = undefined;
					}
					
					self.visualization = 'tablet';
				}
			},
			
			_constructDesktop: function() {
				var self = this;
				
				if(self.visualization === undefined || self.visualization !== 'desktop'){
					self._loadStepContent();
					
					if(self.element.find('.step-container').length == 0) {
						// Create step container
						self.stepContainer = $('<div class="step-container" style="width:' + self.element.width() + 'px"></div>');			
						self.element.append(self.stepContainer);
						
						// Create slide viewer
						var $slideViewer = $('<div id="wizard-slide-viewer" class="slide-viewer"></div>');
						self.stepContainer.append($slideViewer);
						
						// Get steps
						self.steps.each(function(){
							var $step = $('<div class="step" style="width:' + self.element.width() + 'px; height:0px"></div>');
							$step.append($(this).hide());
							$slideViewer.append($step);
						});
						//self._current().show().parent().addClass('active');
						
						// Width calculation functions
						self._desktopResized();
						
						// Append button bar to wizard element
						//self.element.append(self._constructButtons());
						
						self.element.find('.step-container').css({
			    			height: self._current().outerHeight(true) + 5 // TODO (this pixel)
			    		});
						
						// Clean elements from tablet display
						self.element.find('> a.step').remove();
						
						self.visualization = 'desktop';
					}
				}
			},
			
			_desktopResized: function() {
				var self = this;
				// -------------------
				// Create progress bar
				var $progressBar = self.element.find('.wizard-progress-bar').empty();
				if($progressBar.length == 0){
					$progressBar = $('<div class="wizard-progress-bar"></div>');
					//$errorMessage = $('<p class="block-error iconed-24 hide" id="checkAlert"><span class="icon-24 m01-alerta red"></span>Debes aceptar los términos y condiciones para continuar el proceso<br/></p>')

					// Append progress bar to wizard element
					//self.element.prepend($errorMessage);
					self.element.prepend($progressBar);

					self.checkAlert = self.element.children()[1];
				}
				
				// Calculate width and append progess bar line
				var width = self.element.width() - 55;
				var $progressLine = $('<div class="progress-line" style="width:' + width + 'px"><div class="progress-display"></div></div>');
				
				// Append progess steps
				var step = 0;
				self.steps.each(function(){
					var title = '<span class="step_img_'+ (step+1) +'"></span> ' + $(this).data('title');
					var left = (((($progressLine.width()) / (self.steps.length - 1)) - 1) * step) - 1;
					var $progressPoint = $('<div class="point-background" style="left:' + left + 'px;"><div class="text"> ' + title + '</div><div class="point"></div></div>');
					$progressPoint.removeClass('active').removeClass('now');				
					if(step < self.current){
						$progressPoint.addClass('active');
					}
					else if(step == self.current){
						$progressPoint.addClass('active').addClass('now');					
					}
					step++;				
					
					$progressLine.append($progressPoint);
				});
				
				// Set progress display width
				$progressLine.find('> .progress-display').css({
	        		width: ((($progressLine.width()-6) / (self.steps.length - 1)) * self.current)
				});
				
				// append progress bar line
				$progressBar.append($progressLine);
				
				// text positioning (needed after displaying on page)
				var step = 0;
				self.element.find('.wizard-progress-bar .progress-line .point-background .text').each(function(){
					var left = step == 0 ? 0 : step == self.steps.length - 1 ? - ($(this).width() - 18) : - (($(this).width()  - 18) / 2);
					step++;
					$(this).css('left', left + 'px');
				});
				
				// position current step layer
				self.element.find('.slide-viewer').css({
	    			left: -(self.element.find('.step-container').outerWidth() * (self.current))
	    		});
				
			},
			
			// called when created, and later when changing options
			_refresh : function() {
				var self = this;
				
				/*if($(this.element).data('alwaysdesktop') == true) {*/
					self._constructDesktop();
					self._desktopResized();
				/*}
				else {
					if(Modernizr.mq('(max-width:768px)')){
						self._constructTablet();
					} else {
						self._constructDesktop();
						self._desktopResized();
					}
				}*/
				self._current().show();
			},
			
			// _resized is called when document is resized
	        _resized: function() {
	        	/* var self = this;
	        	
	            self.element.find('.step-container').css('width', self.element.width());
	            self.element.find('.step').css('width', self.element.width());
	            self._refresh(); */
	        },
	        
	        _isRemoteStepLoaded: function(){
	        	var self = this;
	        	var $step = self._current();
	        	var href = $step.data('href');
	        	
	        	return href !== undefined && $step.is(':empty');
	        },
	        
	        _loadStepContent: function(handler){
	        	var self = this;
	        	var $step = self._current();
	        	
	        	var href = $step.data('href');
	        	if(href !== undefined){
	        		$.ajax({
	        			url: href,
	        			method: 'post',
	        			data: self.form === undefined ? undefined : self.form.serialize(),
	        			beforeSend: function(data){
							$(document).trigger('capgemini.ajax.start', $step);
						},
	        			success: function(data){
	        				var $noRemove = $step.find('.button-bar');
	        				$step.html($noRemove).prepend(data);
	        				
	        				// inicializa botones next internos
	        				$step.find('.next').each(function() {
	        					$(this).on('vclick', function(){
	            					self.next();
	            					return false;
	            				});
	        				});
	        				
	            			if(handler !== undefined){
	            				handler();
	            			}
	            			
	            			self.element.find('.step-container').css({
	        	    			height: $step.outerHeight(true) + 5 // TODO (this pixel)
	        	    		});
	            			$(document).trigger('capgemini.ajax.load', $step);
	            		}
	        		});
	        		
	        	}
	        },
			
			// _setOptions is called with a hash of all options that are changing
	        // always refresh when changing options
	        _setOptions: function() {
	            // _super and _superApply handle keeping the right this-context
	            this._superApply( arguments );
	            this._refresh();
	        },

	        // _setOption is called for each individual option that is changing
	        _setOption: function( key, value ) {
	            // prevent invalid color values
	            this._super( key, value );
	        },
	        
			// Public functions
			public_function : function() {
				return true;
			},
			
			step2: function(){
				var self = this;
				self.current=0;
				self.nextQuick(true);
				
				processNoPrev(self);
				processCancel(self);
				
				setTimeout(function(){
					var $wizard = $('.wizard');
					if($wizard.length > 0) {
						$(document).trigger('capgemini.wizard.resize', $wizard);
					}
				}, 300);
			},
	        
			step3: function(){
				var self = this;
				self.current = 0;
				self.nextQuick(false);
				self.nextQuick(true);
				
				setTimeout(function(){
					var $wizard = $('.wizard');
					if($wizard.length > 0) {
						$(document).trigger('capgemini.wizard.resize', $wizard);
					}
				}, 300);
			},
			
			 nextQuick: function(showStep){
		        var self = this;
		        
		        self.current++;
	        	self._loadStepContent();
	    	
	        	if(self.visualization === 'desktop'){
		        	
	        		// Mark active
		    		self.element.find('div.step.active').removeClass('active');
		    		self._current().parent().addClass('active');
		    		self._current().show();
		    		self._resized();
					
					//scroll hacia arriba
					self._scrollTo();
		    		
		    		// Animate progress display
		        	var $progressLine = self.element.find('.wizard-progress-bar .progress-line');
		        	$progressLine.find('> .progress-display').animate({
		        		width: ((($progressLine.width()-6) / (self.steps.length - 1)) * self.current)
		        	}, 'slow', function(){
		        		// Actions to run once animation finishes
		        		$progressLine.find('.point-background:nth-of-type(' + (self.current + 2) + ')').addClass('active').addClass('now');
		        	});
		        	
	        	} else {
	        		self.element.find('> a.step.active').removeClass('active').addClass('completed');
	        		var $prevFrame = $(self.steps[self.current - 1]);
	        		var $nextFrame = $(self.steps[self.current]);
	        		$prevFrame.animate({ height: "hide" }, { duration: 0});
	        		//self._current().append(self._constructButtons()).prev().addClass('active');
	        		if(showStep) {
	        			self._current().show();
	        			self._scrollTo();
	        		}
	        		if(self.current < self.steps.length-1) {
	        			self.element.find('.button-bar .prev').show();
	        		}	        	
	        	}	
			 },
			
	        next: function(){
	        	var self = this;
	        	var $next = self._nextStep();
	        	var $step = self._current();
	        	var isCheckLabel = false;
	        	var errorMessage;
	        	
	        	var isCheckStep = $step.data('requiredcheck');
	        	if (isCheckStep == true){        	
	        		
	        		/*if (typeof $step.find('#checkAlertPhone')[0] == "undefined"){
	        			errorMessage = $('<p class="block-error iconed-24 hide" id="checkAlertPhone"><span class="icon-24 m01-alerta red"></span>Debes aceptar los términos y condiciones para continuar el proceso<br/></p>')
					// Append progress bar to wizard element
						$step.prepend(errorMessage);
	        		}else{
	        			errorMessage = $step.find('#checkAlertPhone');
	        		}*/			

	        	//	var prueba = $step.find('#condiciones').attr('class').index(" on");
	    			var labelCheckClasses = $step.find('#condiciones')[0].className.toString().split(" ");
	    			
	    			for (var a = 0; a < labelCheckClasses.length; a++){
	    				if (labelCheckClasses[a] == "on") {
	    					isCheckLabel = true;
	    				}
					}
				}

	    		var visualization = self.visualization;

	    		var alertBox = $(self.checkAlert);
	    		if (!isCheckLabel && isCheckStep==true){
					if (visualization!="desktop"){
						errorMessage.removeClass('hide');
						if (typeof alertBox.attr("class")!="undefined"  && alertBox.attr("class").indexOf('hide')== -1){
							alertBox.addClass('hide');
						}
					} else {
						alertBox.removeClass('hide');
						if (typeof errorMessage.attr("class")!="undefined" && errorMessage.attr("class").indexOf('hide')== -1){
							errorMessage.addClass('hide');
						}
					}
				} else {
					//$self.find('#checkAlert').addClass('hide');	
					if (visualization!="desktop"){
						if (typeof errorMessage!="undefined"){
							errorMessage.removeClass('hide');
						}
					} else {
						alertBox.addClass('hide');	
					}
					if(/*self.form === undefined || self.form.valid()*/true){
			        	if(self.animating){
	        				return;
	        			}
			        	/* multistep */
			        	var $currentMultiStep = $step.find('.sub-step:visible');
						if($currentMultiStep.hasClass('hasCheck') && !hasCheckedCheck($currentMultiStep)){
							return;
						}
			        	
			        	var multistep = $step.data('multistep');
			        	if (multistep!=true){
			        		multistep=false;
			        	}
			        	
			        	if(multistep == true) {
			        		self.currentMultiStep++;
			        		var numSubSteps = $step.find('.sub-step').length;
			        		
			        		// init multistep - openModal
			        		var counter = 0;
			        		var openModal = false;
		        			$step.find('.sub-step').each(function() {
		        				if(self.currentMultiStep == counter) {
		        					if($(this).hasClass('open-modal')) {
		        						$(this).find('a').click();
		        						openModal = true;
		        					}
		        				}
		        				counter++;	        				
		        			});
		        			if(openModal) {
		        				return;
		        			}	  
		        			// end multistep - openModal
		        			if(self.currentMultiStep < numSubSteps) {
			        			// oculta boton confirmar
			        			$('.wizardNext', this.element).hide();
			        			self.element.find('.button-bar .prev').show();		        			
			        			
			        			var counter = 0;
			        			$step.find('.sub-step').each(function() {
			        				if(self.currentMultiStep == counter) {
			        					$(this).removeClass('hidden');
			        				} else {
			        					$(this).addClass('hidden');				
			        				}
			        				counter++;
			        			});
			        			self._animateHeight();
			        			self._scrollTo();
			        			return;		        			
			        		}else{		        			
		        				var submitevent = $step.data('submitevent');	        				
		        				if(submitevent!= null){
		        					$(document).trigger(submitevent);	        				
		        				}
		        			}
			        	}
			        	self.currentMultiStep = 0;
			        	/* fin multistep */
			        		        	
			        	self.animating = true;
			        	
			        	self.current++;
			        	self._loadStepContent();
		        	
			        	if(self.visualization === 'desktop'){
				        	// move next step
			        		self._current().show();
			        		var $stepContainer = self.element.find('.step-container');
			        		if(isIE8()) {
				        		// IE8 dont animate
			        			var newPositionLeft = -1 * self.current * $stepContainer.outerWidth();
			        			self.element.find('#wizard-slide-viewer').css('left', newPositionLeft);
			        			self._animateHeight(false);
				    			if(self.current < self.steps.length-1) {
		        					self.element.find('.button-bar .prev').show();
		        				}
				    			self.animating = false;
				    			$stepContainer.css('overflow', 'none');
			        		}else{
			        			$stepContainer.css('overflow', 'hidden');
			        			// show boton prev
				    			if(self.current < self.steps.length-1) {
		        					self.element.find('.button-bar .prev').show();
		        				}
				        		self.element.find('#wizard-slide-viewer').animate({
					    				left: '-=' + $stepContainer.outerWidth()
					    			},
					    			{ 	easing: 'swing',
					    				duration: 'slow',
					    				complete: function(){
							    			$stepContainer.css('overflow', 'none');
							        		// Animate container height to step height
							    			self._animateHeight(false);						    			
							    			self.animating = false;
							    		}
					    			}
					    		);
			        		}
			        		
				    		// Mark active
				    		self.element.find('div.step.active').removeClass('active');			    		
				    		self._current().parent().addClass('active');			    		
				    		
				    		// Animate progress display
				        	var $progressLine = self.element.find('.wizard-progress-bar .progress-line');
				        	$progressLine.find('> .progress-display').animate({
				        		width: ((($progressLine.width()-6) / (self.steps.length - 1)) * self.current)
				        	}, 'slow', function(){
				        		// Actions to run once animation finishes			        		
				        		$('.point-background').removeClass('now');
				        		$progressLine.find('.point-background:nth-of-type(' + (self.current + 2) + ')').addClass('active');
				        		$progressLine.find('.point-background:nth-of-type(' + (self.current + 2) + ')').addClass('now');
				        									
				        	});
				        	
			        	} else {
			        		self.element.find('> a.step.active').removeClass('active').addClass('completed');
			        		var $prevFrame = $(self.steps[self.current - 1]);
			        		var $nextFrame = $(self.steps[self.current]);
			        		// show boton prev	   
	        				if(self.current < self.steps.length-1) {
	        					self.element.find('.button-bar .prev').show();
	        				}
			        		$prevFrame.animate({ height: "hide" }, { duration: 'slow', complete: function () {
			        			//self._current().append(self._constructButtons()).prev().addClass('active');
			        			$nextFrame.animate({ height: "show" }, { duration: 'slow', complete: function () {
			        				self._scrollTo();
					    			
				        			self.animating = false;
			        			}});
			        	    }});
			        	}
		        	
			        	// handle button display
			    		if(self.current == self.steps.length - 2){
			    			var $btnNext = self.element.find('.button-bar .next');
			    			// Si es un multistep no cambia el literal del boton next en el penultimo paso.
			    			if (self._current().data('multistep')!=true){
				        		$btnNext.html(self.options.labels.confirm);
				        	}
			    			$btnNext.off('vclick').on('vclick', function(){
			    				self.submit();
			    				return false;
			    			});
			    		} else if(self.current == self.steps.length - 1){
			    			self.element.find('.button-bar .prev').hide();
			    					    			
			    			var opContainer = $(document).find('.wizard-container').attr('class');	    			
			    			var isModal = true;
			    			if (typeof opContainer != "undefined"){
			    				isModal = false;
			    			}

			    			self.element.find('.button-bar .cancel').html(self.options.labels.done);
			    			
			    			if (!isModal){
			    				self.element.find('.button-bar .next').html(self.options.labels.done).removeClass('arrow-right').off('vclick').on('vclick', function(){
			    				//clearErrors();
								$('#operaciones-list-container').slideDown();
								if (self.element.data('page')!=true){
									$('.wizard-container').slideUp(function(){
										if($(this).find('.conformar-cheque').length > 0){
											var url = '?src=posicion-global.php';
											window.location = url;
										}
									});
								}else{
									var url = '?src=posicion-global.php';
									window.location = url;
								}

								return false;
								});
			    			}else{
			    				self.element.find('.button-bar .next').html(self.options.labels.done).removeClass('arrow-right').off('vclick').on('vclick', function(){
			    					// wizard is in a modal
			    					if(self.element.closest('.modal').length > 0) {
			    						self.element.closest('.modal').modal('hide').remove();
			    					}
			    					else if(self.element.closest('.close-redirect').length > 0) {
			    						var redirect = self.element.closest('.close-redirect').data('redirect');
			    						window.location.href = redirect;
			    					}
			    					else {
			    						// close operation substep
			    						if($('#operaciones-list-container').hasClass('in-sub-step')){
			    							var listContainer = $('#operaciones-list-container.in-sub-step');
			    							var $slider = $(listContainer).find('.operaciones-slider');
			    							var elements = $('ul', $slider);
			    							var elementWidth = 100/elements.length;
			    							var activeUl = $slider.find('ul.active');
			    							var targetUl = $slider.find('ul.' + $(activeUl).data('parent'));
			    							$(activeUl).removeClass('active');
			    							$(targetUl).addClass('active');

			    							$slider.css('margin-left', '-' + (elementWidth * $(elements).index($(targetUl)))*2 + '%');
			    							$slider.css('height', 'auto');
			    							
			    							if($(targetUl).hasClass('main')){
			    								// We're back home again!
			    								/*if(Modernizr.mq('(min-width:769px)')){
			    									// Mobile needs te class a little longer.
			    									$slider.closest('#operaciones-list-container').removeClass('in-sub-step');
			    								}*/
			    								$(this).off('.slideBack');
			    							}
			    							$('#quiero-btn').addClass('hidden');
			    						}
			    						
			    						// wizard isn't in a modal
										/*if(Modernizr.mq('(max-width:768px)')){
											// Phone
											$(this).closest('ul').find('> li').removeClass('inactive active');
											$('#tab-operaciones').removeClass('active');
											$('.tabmain-operaciones').removeClass('hidden').addClass('active');
											$('#operaciones-list-container').addClass('active');
											$('#operacion-container').empty();
											$('#option-content').empty();

											var $backButton = $('.return2');
											$backButton.removeClass('return return2').text('Quiero').addClass('want');
											$backButton.click();
											self._scrollTo();
										}
										else {*/
											// Desktop
											$(this).closest('ul').find('> li').removeClass('inactive active');
											$('#operacion-container').removeClass('active');
											$('.tabmain-operaciones').removeClass('hidden').addClass('active');
											$('#operacion-container').empty();
											$('#option-content').empty();
											
											$('#operaciones-list-container').addClass('active');
											$('#tab-operaciones-link').click();
										/*}*/
			    					}
			    					
				    				return false;
				    			});
			    			}
			    		}
		        	}
	        	}
	    		
	    		processNoPrev(self);
	    		processNoNext(self);
	    		processCancel(self);
	    		//processClaveDigitalWarning(self);
	    		
	    		$(document).trigger('capgemini.scroll.init', $next);
	    		// close keyboard tablet
	    		//closeKeyBoardTablet(1000);    		
	        },
	        
	        // TODO submit form via ajax
	        submit: function(){
	        	var self = this;
	        	if(self.form !== undefined){ 
	        		var $form = self.form;
	        		//clearErrors($form);
	        		if(/*self.form.valid()*/true){
		        		var action = $form.attr('action');
		        		if(action !== undefined && action !== '#'){
		        			$.ajax({
		            			url: action,
		            			data: $form.serialize(),
		            			method: $form.attr('method') === undefined ? 'post' : $form.attr('method'),
		            			beforeSend: function(data){
									$(document).trigger('capgemini.ajax.start', $form);
								},
		            			success: function(data, status, request){
		            				self.next();
		            			},
		            			error : function(request, status, error){
		            				self._current().prepend(request.responseText);
		            				self._animateHeight(false);
		            			}
		            		});
		        		} else {
		        			self.next();
		        		}
	        		}
	        	} else {
	        		self.next();
	        	}
	        },
	        
	        prev: function(){
	        	var self = this;
	        	
	        	//check blocked
	        	var $previousStep = $(self.steps[self.current-1]);
	        	var blocked = $previousStep.data('blocked');
	        	
	        	if(blocked){
	        		return;
	        	}
	        	
	        	if(self.animating){
	        		return;
	        	}
	        	self.animating = true;
	        	
	        	if(self.form !== undefined){ 
	        		//clearErrors(self.form);
	        	}
	        	
	        	/* reset multistep if was the current step */
	    		self.currentMultiStep = 0;
	    		var $prevStep = $(self.steps[self.current]);
	    		var multistep = $prevStep.data('multistep');
	        	if (multistep==true){
	        		var counter = 0;
	        		$prevStep.find('.sub-step').each(function() {
	    				if(counter == 0) {
	    					$(this).removeClass('hidden');
	    				}
	    				else {
	    					$(this).addClass('hidden');	        					
	    				}
	    				// clear inputs
	    				$(this).find("input").each(function () {
	    					$(this).val('');
	    				});
	    				counter++;
	    			});
	        		// Si hay un multistep en el paso1, se resetea el multistep pero no
	        		// navega hacia atras
	        		if(self.current == 0) {
	        			self.element.find('.button-bar .next').show();
	        			self.element.find('.button-bar .prev').hide();
	        			self._animateHeight(false);
	        			self.animating = false;
	        			return;
	        		}
	        	}
	        	/* end reset multistep */
	        	
	        	// decrease current
	    		self.current--;
	    		
	        	if(self.visualization === 'desktop'){
	        		var $progressLine = self.element.find('.wizard-progress-bar .progress-line');
		        	$progressLine.find('.point-background:nth-of-type(' + (self.current + 3) + ')').removeClass('active').removeClass('now');
		        	$progressLine.find('.point-background:nth-of-type(' + (self.current + 2) + ')').addClass('now');
		        	
		    		// move previous step
		        	$(self.steps[self.current+1]).hide();
		    		
		    		var $stepContainer = self.element.find('.step-container');
	        		if(isIE8()) {
	        			// IE8 dont animate
	        			var newPositionLeft = -1 * self.current * $stepContainer.outerWidth();
	        			self.element.find('#wizard-slide-viewer').css('left', newPositionLeft);
		        		self._animateHeight(false);
		    			if(self.current < self.steps.length-1) {
	    					self.element.find('.button-bar .prev').show();
	    				}
		    			self.animating = false;		
		    			$stepContainer.css('overflow', 'none');
	        		}
	        		else {		
	        			$stepContainer.css('overflow', 'hidden');
			    		self.element.find('#wizard-slide-viewer').animate({
			    			left: '+=' + $stepContainer.outerWidth()
			    		}, 'slow', function(){
			    			$stepContainer.css('overflow', 'none');
			    		});
	        		}
		    		
		    		// Mark active
		    		self.element.find('div.step.active').removeClass('active');
		    		self._current().parent().addClass('active');
		    		
		    		// Animate progress display
		        	$progressLine.find('> .progress-display').animate({
		        		width: ((($progressLine.width()-10) / (self.steps.length - 1)) * self.current)
		        	}, 'slow', function(){
		        		// Actions to run once animation finishes
		        		self.animating = false;
		        		
		        		// Animate container height to step height
			        	self._animateHeight(false);
		        	});
		        	
		        	self._current().show();
		        	
	        	} else {
	        		self.element.find('> a.step.active').removeClass('active').removeClass('completed');
	        		
	        		var $currentFrame = $(self.steps[self.current + 1]);
	        		var $prevFrame = $(self.steps[self.current]);
	        		$currentFrame.animate({ height: "hide" }, { duration: 'slow', complete: function () {
	        			//self._current().append(self._constructButtons()).prev().addClass('active').removeClass('completed');
	        			$prevFrame.animate({ height: "show" }, { duration: 'slow', complete: function () {
	        				self._scrollTo();
		        			self.animating = false;
	        			}});
	        	    }});
	        	}
	        	
	        	// handle button display
	    		self.element.find('.button-bar .next').show();
	    		if(self.current == 0){
	    			self.element.find('.button-bar .prev').hide();    			
	    		}    		
	    		if(self.current == self.steps.length - 3){
	    			self.element.find('.button-bar .next').html(self.options.labels.next).off('vclick').on('vclick', function(){
						self.next();
						return false;
					});
	    		} else if(self.current == self.steps.length - 2){
	    			self.element.find('.button-bar .next').html(self.options.labels.confirm).off('vclick').on('vclick', function(){
	    				self.submit();
	    				return false;
	    			});
	    		} 
	    		
	    		processNoPrev(self);
	    		processNoNext(self);
	    		processCancel(self);
	    		//processClaveDigitalWarning(self);
			    
	    		$(document).trigger('capgemini.scroll.init', $previousStep);
	    		// close keyboard tablet
	    		//closeKeyBoardTablet(1000);
	    		
	        },
	        
	        scrollTop: function(){
	        	this._scrollTo();
	        },

	        resize: function(){
	        	this._resized();
	        },
	        
	        resetMultiStep: function(){
	        	var self = this;
	        	self.currentMultiStep = 0;
	        },
	        
	        getStep: function(){
	        	var self = this;
	        	return self.current;
	        }
	        
		});
	})(jQuery);















	function isIE8() {
	    var BrowserDetect = 
	    {
	        init: function () 
	        {
	            this.browser = this.searchString(this.dataBrowser) || "Other";
	            this.version = this.searchVersion(navigator.userAgent) ||       this.searchVersion(navigator.appVersion) || "Unknown";
	        },

	        searchString: function (data) 
	        {
	            for (var i=0 ; i < data.length ; i++)   
	            {
	                var dataString = data[i].string;
	                this.versionSearchString = data[i].subString;

	                if (dataString.indexOf(data[i].subString) != -1)
	                {
	                    return data[i].identity;
	                }
	            }
	        },

	        searchVersion: function (dataString) 
	        {
	            var index = dataString.indexOf(this.versionSearchString);
	            if (index == -1) return;
	            return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	        },

	        dataBrowser: 
	        [
	            { string: navigator.userAgent, subString: "Chrome",  identity: "Chrome" },
	            { string: navigator.userAgent, subString: "MSIE",    identity: "Explorer" },
	            { string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
	            { string: navigator.userAgent, subString: "Safari",  identity: "Safari" },
	            { string: navigator.userAgent, subString: "Opera",   identity: "Opera" }
	        ]

	    };
	    BrowserDetect.init();
	    
	    if((BrowserDetect.browser == 'Explorer') && (BrowserDetect.version == 8)) {
	        return true;
	    }
	    return false;
	}



	var processCancel = function(self){
		var $showcancel = self._current().data('showcancel');
		if ($showcancel==true){
			$('#wizardCancel').removeClass('hidden');
		}
		else {
			$('#wizardCancel').addClass('hidden');
		}
	}

	var processNoNext = function(self){
		var $nonext = self._current().data('nonext');
		var $multistep = self._current().data('multistep') && (self.currentMultiStep > 0);
		var $nextBtn = $('.wizardNext', self.element);
	    if (($nonext==true) || ($multistep==true)){
		   	$.each($nextBtn,function() {
		       	$(this).hide();
		    });
	    } else {
	      	$.each($nextBtn,function() {
	          	$(this).show();
	        });
	    }
	}

	var processNoPrev = function(self){
		if(self.current == 0) {
			var $noprev = self._current().data('noprev');
			var $prevBtn = $('.wizardBack', self.element);
			if ($noprev==true){
			   	$.each($prevBtn,function() {
			       	$(this).hide();
			    });
		    }
		}
		if((self.current > 0) && (self.current < self.steps.length-1)) {
			var $noprev = self._current().data('noprev');
			var $prevBtn = $('.wizardBack', self.element);
			if ($noprev==true){
			   	$.each($prevBtn,function() {
			       	$(this).hide();
			    });
		    } else {
		      	$.each($prevBtn,function() {
		          	$(this).show();
		        });
		    }
		}
	}

	$(document).ready(function(){
		var createWizard = function($element) {
			$element.wizard({
				labels: {
					prev: 'Anterior',
					next: 'Siguiente',
					confirm: 'Confirmar',
					done: 'Cerrar'
				}
			});
		};
		
		$(document).on('capgemini.ajax.load', function(event, element){
			createWizard($(element).find('.wizard'));
		});
		
		createWizard($('.wizard'));
	});

	function initStep2Transfers($context){
		$('.init-step2', $context).each(function(){
			setTimeout(function() {
				$('.wizard').wizard('step2');
			}, 100);
		});		
	}

	function initStep3Transfers($context){
		$('.init-step3', $context).each(function(){
			setTimeout(function() {
				$('.wizard').wizard('step3');
			}, 100);
		});		
	}

	function transicion_barra_progreso(pasoNuevo,retraso){
		if(pasoNuevo==2 || pasoNuevo==3){
			$('#barraProgreso').removeClass('init-step'+(pasoNuevo-1));
			$('#barraProgreso').addClass('init-step'+pasoNuevo);
			$('.init-step'+pasoNuevo).each(function(){
				setTimeout(function() {
					$('.wizard').wizard('next');
				}, retraso);
			});
		}
	}

	$(function(){
		
		
		
		initStep2Transfers($(document));
		initStep3Transfers($(document));
		
		
	});
	
	
</script>
</tiles:putAttribute>
</tiles:insertDefinition>