package pe.com.hypersoft.login.model.global;

import java.io.Serializable;

public class UsuarioModel implements Serializable{

	/**
	 * Clase para Administrar los Items de Menu seleccionado por usuario
	 */
	private static final long serialVersionUID = 1L;
	
	private String email;
	private String clave;
	private String nombre;
	private String paterno;	
	private String materno;
	private String registro;
	private String tipodoc;
	private String nrodoc;
	private String fecnacimiento;
	private String estadocivil;
	private String telefono;
	private String estado;
	private Integer perfil;
	private String tipoperfil;
	private String nomEdificio;
	private String token;
	private String idcc;
	private String iduo;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public String getTipodoc() {
		return tipodoc;
	}
	public void setTipodoc(String tipodoc) {
		this.tipodoc = tipodoc;
	}
	public String getNrodoc() {
		return nrodoc;
	}
	public void setNrodoc(String nrodoc) {
		this.nrodoc = nrodoc;
	}
	public String getFecnacimiento() {
		return fecnacimiento;
	}
	public void setFecnacimiento(String fecnacimiento) {
		this.fecnacimiento = fecnacimiento;
	}
	public String getEstadocivil() {
		return estadocivil;
	}
	public void setEstadocivil(String estadocivil) {
		this.estadocivil = estadocivil;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getPerfil() {
		return perfil;
	}
	public void setPerfil(Integer perfil) {
		this.perfil = perfil;
	}
	public String getIdcc() {
		return idcc;
	}
	public void setIdcc(String idcc) {
		this.idcc = idcc;
	}
	public String getIduo() {
		return iduo;
	}
	public void setIduo(String iduo) {
		this.iduo = iduo;
	}
	public String getTipoperfil() {
		return tipoperfil;
	}
	public void setTipoperfil(String tipoperfil) {
		this.tipoperfil = tipoperfil;
	}
	public String getNomEdificio() {
		return nomEdificio;
	}
	public void setNomEdificio(String nomEdificio) {
		this.nomEdificio = nomEdificio;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
		
}
