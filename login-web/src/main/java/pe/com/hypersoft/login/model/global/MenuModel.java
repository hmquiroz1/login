package pe.com.hypersoft.login.model.global;

import java.io.Serializable;

public class MenuModel implements Serializable{

	/**
	 * Clase para Administrar los Items de Menu seleccionado por usuario
	 */
	private static final long serialVersionUID = 1L;
	
	private String idopcionpadre;
	private String idopcionhijo;
	private String descopcionpadre;
	private String descopcionhijo;
	private String iconopcionpadre;
	private String titulocontentheader;
	
	public String getIdopcionpadre() {
		return idopcionpadre;
	}
	public void setIdopcionpadre(String idopcionpadre) {
		this.idopcionpadre = idopcionpadre;
	}
	public String getIdopcionhijo() {
		return idopcionhijo;
	}
	public void setIdopcionhijo(String idopcionhijo) {
		this.idopcionhijo = idopcionhijo;
	}
	public String getDescopcionpadre() {
		return descopcionpadre;
	}
	public void setDescopcionpadre(String descopcionpadre) {
		this.descopcionpadre = descopcionpadre;
	}
	public String getDescopcionhijo() {
		return descopcionhijo;
	}
	public void setDescopcionhijo(String descopcionhijo) {
		this.descopcionhijo = descopcionhijo;
	}
	public String getIconopcionpadre() {
		return iconopcionpadre;
	}
	public void setIconopcionpadre(String iconopcionpadre) {
		this.iconopcionpadre = iconopcionpadre;
	}
	public String getTitulocontentheader() {
		return titulocontentheader;
	}
	public void setTitulocontentheader(String titulocontentheader) {
		this.titulocontentheader = titulocontentheader;
	}
		
	
}
