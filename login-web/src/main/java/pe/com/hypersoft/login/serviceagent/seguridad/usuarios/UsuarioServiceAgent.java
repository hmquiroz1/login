package pe.com.hypersoft.login.serviceagent.seguridad.usuarios;

import pe.com.hypersoft.login.model.seguridad.usuarios.DatosEntradaUsuarioModel;
import pe.com.hypersoft.login.model.seguridad.usuarios.DatosSalidaUsuarioModel;

public interface UsuarioServiceAgent {
  public DatosSalidaUsuarioModel agregarUsuario(DatosEntradaUsuarioModel datosUsuario);
}
