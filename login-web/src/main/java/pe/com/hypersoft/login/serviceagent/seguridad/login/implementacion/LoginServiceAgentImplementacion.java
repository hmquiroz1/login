package pe.com.hypersoft.login.serviceagent.seguridad.login.implementacion;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import ma.glasnost.orika.MapperFacade;
import pe.com.hypersoft.login.dto.seguridad.login.DatosEntradaLoginDTO;
import pe.com.hypersoft.login.dto.seguridad.login.DatosSalidaLoginDTO;
import pe.com.hypersoft.login.dto.seguridad.usuarios.DatosEntradaCrearUsuarioDTO;
import pe.com.hypersoft.login.dto.seguridad.usuarios.DatosSalidaCrearUsuarioDTO;
import pe.com.hypersoft.login.dto.seguridad.usuarios.SeguridadRestDTO;
import pe.com.hypersoft.login.dto.seguridad.usuarios.UsuarioDTO;
import pe.com.hypersoft.login.enumeration.ParametrosRestClientEnum;
import pe.com.hypersoft.login.model.seguridad.login.DatosEntradaLoginModel;
import pe.com.hypersoft.login.model.seguridad.login.DatosSalidaLoginModel;
import pe.com.hypersoft.login.model.seguridad.usuarios.DatosEntradaUsuarioModel;
import pe.com.hypersoft.login.model.seguridad.usuarios.DatosSalidaUsuarioModel;
import pe.com.hypersoft.login.restclient.seguridad.login.LoginRestClient;
import pe.com.hypersoft.login.restclient.seguridad.usuarios.UsuarioRestClient;
import pe.com.hypersoft.login.serviceagent.seguridad.login.LoginServiceAgent;
import pe.com.hypersoft.login.serviceagent.seguridad.usuarios.UsuarioServiceAgent;

@Service
public class LoginServiceAgentImplementacion implements LoginServiceAgent {
	
  @Autowired
  LoginRestClient loginRestClient;
	
  @Autowired
  private MapperFacade mapper;
  
  /**
   * Restful para  validar acceso al Sistema	
   */
  public DatosSalidaLoginModel validarAcceso(DatosEntradaLoginModel datosEntradaLoginModel){
	  	  
	  DatosEntradaLoginDTO datosEntradaLoginDTO = this.mapper.map(datosEntradaLoginModel,DatosEntradaLoginDTO.class);
	  Map<Object, Object> objetoSolicitud = new HashMap<Object, Object>();
	  objetoSolicitud.put(ParametrosRestClientEnum.OBJETO_PEDIDO, datosEntradaLoginDTO);
		
	  DatosSalidaLoginDTO datosSalidaLoginDTO = this.loginRestClient.validarAcceso(objetoSolicitud);
	  DatosSalidaLoginModel datosSalidaLoginModel = this.mapper.map(datosSalidaLoginDTO,DatosSalidaLoginModel.class);
	  return datosSalidaLoginModel;
  }
  
  /**
   * Restful para  obtener listado de menu para Perfil	
   */
  public DatosSalidaLoginModel lstMenuOpciones(DatosEntradaLoginModel datosEntradaLoginModel,String token){
	  	  
	  DatosEntradaLoginDTO datosEntradaLoginDTO = this.mapper.map(datosEntradaLoginModel,DatosEntradaLoginDTO.class);
	  Map<Object, Object> objetoSolicitud = new HashMap<Object, Object>();
	  objetoSolicitud.put(ParametrosRestClientEnum.OBJETO_PEDIDO, datosEntradaLoginDTO);
		
	  DatosSalidaLoginDTO datosSalidaLoginDTO = this.loginRestClient.lstMenuOpciones(objetoSolicitud,token);
	  DatosSalidaLoginModel datosSalidaLoginModel = this.mapper.map(datosSalidaLoginDTO,DatosSalidaLoginModel.class);
	  return datosSalidaLoginModel;
  }
}
