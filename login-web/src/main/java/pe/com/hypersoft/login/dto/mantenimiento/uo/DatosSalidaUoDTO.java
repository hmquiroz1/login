package pe.com.hypersoft.login.dto.mantenimiento.uo;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatosSalidaUoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("codigo")	
	private String codigo;
	
	@JsonProperty("valor")	
	private String valor;
		

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
}
