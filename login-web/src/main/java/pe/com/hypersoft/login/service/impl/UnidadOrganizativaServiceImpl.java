package pe.com.hypersoft.login.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.hypersoft.login.domain.UO;
import pe.com.hypersoft.login.repository.UnidadOrganizativaDao;
import pe.com.hypersoft.login.service.UnidadOrganizativaService;

@Service
public class UnidadOrganizativaServiceImpl implements UnidadOrganizativaService {
	
	@Autowired
	private UnidadOrganizativaDao uoDao;
		
	public List<UO> lstUnidadOrganizativa() {		
		return uoDao.lstUnidadOrganizativa();
	}
}	
