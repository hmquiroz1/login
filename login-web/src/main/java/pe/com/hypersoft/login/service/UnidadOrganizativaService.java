package pe.com.hypersoft.login.service;

import java.util.List;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.UO;
import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.model.uo.UOEntradaModel;
import pe.com.hypersoft.login.model.uo.UOSalidaModel;

public interface UnidadOrganizativaService {
		
	List<UO> lstUnidadOrganizativa();	
}
