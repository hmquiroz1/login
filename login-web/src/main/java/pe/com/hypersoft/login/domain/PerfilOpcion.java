package pe.com.hypersoft.login.domain;

import java.io.Serializable;
import java.util.List;

public class PerfilOpcion implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long idperfilopcion;
	
	private String descperfil;
	
	private String descopcion;

	private String link;

	private String icono;
	
	private String indnew;
	
	private String padre;
	
	private String orden;
	
	private String tituloheader;
	
	private List<PerfilOpcion> hijos;

	public Long getIdperfilopcion() {
		return idperfilopcion;
	}

	public void setIdperfilopcion(Long idperfilopcion) {
		this.idperfilopcion = idperfilopcion;
	}

	public String getDescperfil() {
		return descperfil;
	}

	public void setDescperfil(String descperfil) {
		this.descperfil = descperfil;
	}

	public String getDescopcion() {
		return descopcion;
	}

	public void setDescopcion(String descopcion) {
		this.descopcion = descopcion;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getIndnew() {
		return indnew;
	}

	public void setIndnew(String indnew) {
		this.indnew = indnew;
	}

	public String getPadre() {
		return padre;
	}

	public void setPadre(String padre) {
		this.padre = padre;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}

	public List<PerfilOpcion> getHijos() {
		return hijos;
	}

	public void setHijos(List<PerfilOpcion> hijos) {
		this.hijos = hijos;
	}

	public String getTituloheader() {
		return tituloheader;
	}

	public void setTituloheader(String tituloheader) {
		this.tituloheader = tituloheader;
	}
	
	
}
