package pe.com.hypersoft.login.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.hypersoft.login.common.excepcion.ExcepcionSIADE;

public class UtilJson {

	public static Object convertirCadenaADTO(String json, Class<?> claseDTO) {
		ObjectMapper jackson = new ObjectMapper();
		Object objetoDTO = null;
		try {
			objetoDTO = jackson.readValue(json, claseDTO);
		}
		catch (Exception e) {
			throw new ExcepcionSIADE("mensaje.error.conversion");
		}
		return objetoDTO;
	}

	public static String convertirObjectoACadena(Object objeto) {
		ObjectMapper jackson = new ObjectMapper();
		String json = null;
		try {
			json = jackson.writeValueAsString(objeto);
		}
		catch (Exception e) {
			throw new ExcepcionSIADE("mensaje.error.conversion");
		}
		return json;
	}

}
