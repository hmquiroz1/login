package pe.com.hypersoft.login.enumeration;

public enum ParametrosRestClientEnum {
	OBJETO_PEDIDO_TSEC("OBJETO_PEDIDO_TSEC"),
	OBJETO_PEDIDO("OBJETO_PEDIDO"),
	OBJETO_PARAMETROS("OBJETO_PARAMETROS"),
	CABECERA_CON_CLAVE_TSEC("CABECERA_CON_CLAVE_TSEC"),
	CODIGO_TSEC("tsec"),
	CODIGO_CLAVE_ACCESO_PAGINACION("paginationKey"),
	CODIGO_CANTIDAD_ITEMS_POR_PAGINACION("pageSize"),
	CODIGO_CLIENTE("vnd.bbva.target-user-id"),
	TIPO_SERVICIO("tipos");

	private final String codigo;

	private ParametrosRestClientEnum(final String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

}
