package pe.com.hypersoft.login.restclient.mantenimiento.uo;

import java.util.Map;

import pe.com.hypersoft.login.dto.mantenimiento.uo.DatosSalidaUoDTO;

public interface UoRestClient {
	public DatosSalidaUoDTO addUnidadOrganizativa(Map<Object, Object> datos,String token);
}
