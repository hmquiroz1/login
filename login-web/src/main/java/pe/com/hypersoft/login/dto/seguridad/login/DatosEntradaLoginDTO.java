package pe.com.hypersoft.login.dto.seguridad.login;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatosEntradaLoginDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("autenticacion")
	private LoginDTO autenticacion;

	public LoginDTO getAutenticacion() {
		return autenticacion;
	}

	public void setAutenticacion(LoginDTO autenticacion) {
		this.autenticacion = autenticacion;
	} 
		
	
	
}
