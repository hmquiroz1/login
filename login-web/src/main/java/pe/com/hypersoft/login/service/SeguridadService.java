package pe.com.hypersoft.login.service;

import java.util.List;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.model.seguridad.login.DatosEntradaLoginModel;

public interface SeguridadService {
	
	List<Usuario> login(DatosEntradaLoginModel usuarioInput);
	List<PerfilOpcion> lstMenuOpciones(Integer idPerfil);
}
