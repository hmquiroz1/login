package pe.com.hypersoft.login.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

//import pe.com.bbva.gifole.domain.SisPerfil;
import pe.com.hypersoft.login.domain.Usuario;

@Component
public class UsuarioMapper implements RowMapper<Usuario> {
	
	public Usuario mapRow(ResultSet rs, int i)
			throws SQLException {
		Usuario usuario = new Usuario();
		//SisPerfil sisPerfil = new SisPerfil();
		//sisPerfil.setId(rs.getLong("ID_PERFIL"));s
		//sisPerfil.setDescripcion(rs.getString("DESCRIPCION"));
		//sisPerfil.setEstado(rs.getString("ESTADO"));		
		usuario.setId(rs.getLong("ID_USUARIO"));
		usuario.setRegistro(rs.getString("REGISTRO"));
		usuario.setNombre(rs.getString("NOMBRE"));
		usuario.setPaterno(rs.getString("PATERNO"));
		usuario.setMaterno(rs.getString("MATERNO"));
		usuario.setTipodoc(rs.getString("TIPODOC"));
		usuario.setNrodoc(rs.getString("NRODOC"));
		usuario.setEmail(rs.getString("EMAIL"));
		usuario.setTelefono(rs.getString("TELEFONO"));
		usuario.setEstado(rs.getString("ESTADO"));
		usuario.setToken(rs.getString("API_TOKEN"));
		usuario.setNomEdificio(rs.getString("NOMEDIFICIO"));
		usuario.setPerfil(rs.getInt("PERFIL"));
		usuario.setTipoperfil(rs.getString("TIPOPERFIL"));
		usuario.setIdcc(rs.getInt("CENTRO_COSTO"));
		usuario.setIduo(rs.getInt("UO"));
		return usuario;
	}

}
