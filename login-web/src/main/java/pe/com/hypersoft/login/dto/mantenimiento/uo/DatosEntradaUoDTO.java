package pe.com.hypersoft.login.dto.mantenimiento.uo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatosEntradaUoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("data")
	private UoDTO data;

	public UoDTO getData() {
		return data;
	}

	public void setData(UoDTO data) {
		this.data = data;
	}

		
	
	
}
