package pe.com.hypersoft.login.dto.seguridad.login;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.hypersoft.login.dto.seguridad.usuarios.UsuarioDTO;
import pe.com.hypersoft.login.model.seguridad.login.PerfilOpcionModel;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatosSalidaLoginDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("codigo")	
	private String codigo;
	
	@JsonProperty("valor")	
	private String valor;
	
	@JsonProperty("listaUsuarios")
	private List<UsuarioDTO> listaUsuarios;
	
	@JsonProperty("listaMenuOpciones")
	private List<PerfilOpcionModel> listaMenuOpciones;
	
	@JsonProperty("token")
	private String token;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public List<UsuarioDTO> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuarioDTO> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<PerfilOpcionModel> getListaMenuOpciones() {
		return listaMenuOpciones;
	}

	public void setListaMenuOpciones(List<PerfilOpcionModel> listaMenuOpciones) {
		this.listaMenuOpciones = listaMenuOpciones;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
