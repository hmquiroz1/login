package pe.com.hypersoft.login.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.UO;
import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.repository.UnidadOrganizativaDao;
import pe.com.hypersoft.login.repository.UsuarioDao;
import pe.com.hypersoft.login.repository.mapper.PerfilOpcionMapper;
import pe.com.hypersoft.login.repository.mapper.UnidadOrganizativaMapper;
import pe.com.hypersoft.login.repository.mapper.UsuarioMapper;
import pe.com.hypersoft.login.util.GifoleUtil;

@Repository
public class UnidadOrganizativaDaoImpl implements UnidadOrganizativaDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
		
	@Autowired
	UnidadOrganizativaMapper uoMapper;
		
		
	public List<UO> lstUnidadOrganizativa() {
		StringBuilder sql=new StringBuilder();
		sql.append("select trim(a.ID_UO) as ID_UO,trim(a.DESCRIPCION) as DESC_UO,trim(a.NOMBRE) as NOM_UO,trim(a.ESTADO) as ESTADO_UO,a.FECINIVIG as FEC_INI_VIG,a.FECFINVIG as FEC_FIN_VIG  ");		
		sql.append("from APP_SEGURIDAD.UO a ");
		sql.append("order by a.ID_UO ASC ");
				
		return jdbcTemplate.query(sql.toString(), uoMapper);
	}
	
}
