package pe.com.hypersoft.login.restclient.seguridad.login.implementacion;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pe.com.hypersoft.login.dto.seguridad.login.DatosEntradaLoginDTO;
import pe.com.hypersoft.login.dto.seguridad.login.DatosSalidaLoginDTO;
import pe.com.hypersoft.login.enumeration.AtributosSesionEnum;
import pe.com.hypersoft.login.enumeration.ParametrosRestClientEnum;
import pe.com.hypersoft.login.restclient.seguridad.login.LoginRestClient;

@Service
@Scope("prototype")
public class LoginRestClientImpl implements LoginRestClient{
	protected final Logger logger = LogManager.getLogger(getClass());
	
	@Autowired
	RestTemplate restTemplateLigero;
	
	@Value("${servicio.rest.login.autenticar}")
	private String servicioAutenticacion;
	
	@Value("${servicio.rest.login.listar.menu}")
	private String servicioListarMenu;
	
	
	public DatosSalidaLoginDTO validarAcceso(Map<Object, Object> datos){
		logger.info("validarAcceso : inicio");
		DatosEntradaLoginDTO datosEntradaLoginDTO = (DatosEntradaLoginDTO)datos.get(ParametrosRestClientEnum.OBJETO_PEDIDO);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Object> requestHeader = new HttpEntity<Object>(datosEntradaLoginDTO, headers);
		
		ResponseEntity<DatosSalidaLoginDTO> respuesta =this.restTemplateLigero.exchange(servicioAutenticacion, HttpMethod.POST,
				requestHeader,DatosSalidaLoginDTO.class,datosEntradaLoginDTO);
		HttpHeaders tokenSalidaLoginDTO = respuesta.getHeaders();
		//tokenSalidaLoginDTO.get("token").get(0)
		DatosSalidaLoginDTO datosSalidaLoginDTO = respuesta.getBody();
		logger.info("validarAcceso : fin");
		return datosSalidaLoginDTO;
	}
	
	public DatosSalidaLoginDTO lstMenuOpciones(Map<Object, Object> datos,String token){
		logger.info("lstMenuOpciones : inicio");
		DatosEntradaLoginDTO datosEntradaLoginDTO = (DatosEntradaLoginDTO)datos.get(ParametrosRestClientEnum.OBJETO_PEDIDO);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set(AtributosSesionEnum.AUTHORIZATION.getCodigo(), token);
		HttpEntity<Object> requestHeader = new HttpEntity<Object>(datosEntradaLoginDTO, headers);
		
		ResponseEntity<DatosSalidaLoginDTO> respuesta =this.restTemplateLigero.exchange(servicioListarMenu, HttpMethod.POST,
				requestHeader,DatosSalidaLoginDTO.class,datosEntradaLoginDTO);
		DatosSalidaLoginDTO datosSalidaLoginDTO = respuesta.getBody();
		logger.info("lstMenuOpciones : fin");
		return datosSalidaLoginDTO;
	}
}
