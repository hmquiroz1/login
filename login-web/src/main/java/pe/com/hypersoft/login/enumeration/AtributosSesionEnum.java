package pe.com.hypersoft.login.enumeration;

public enum AtributosSesionEnum {		
	ACCESO_USUARIO("accesoUsuarioModel"),
	TOKEN("token"),
	AUTHORIZATION("Authorization"),
	ACTIVE_HOME("activeHome"),
	ACTIVE_SUB_HOME("activeSubHome"),
	MENU_ITEM("menuItem");
	
	private String codigo;
	
	private AtributosSesionEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

}