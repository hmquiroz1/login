package pe.com.hypersoft.login.web.mantenimiento.uo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.com.hypersoft.login.domain.UO;
import pe.com.hypersoft.login.enumeration.AtributosSesionEnum;
import pe.com.hypersoft.login.model.global.MenuModel;
import pe.com.hypersoft.login.model.uo.UOEntradaModel;
import pe.com.hypersoft.login.model.uo.UOSalidaModel;
import pe.com.hypersoft.login.service.SeguridadService;
import pe.com.hypersoft.login.service.UnidadOrganizativaService;
import pe.com.hypersoft.login.serviceagent.mantenimiento.uo.UOServiceAgent;


@Controller
@Scope("request")
@RequestMapping(value = "/mantenimiento/uo")
public class UOController {

	@Autowired
	SeguridadService seguridadService;
	
	@Autowired 
	UnidadOrganizativaService uoService;
	
	@Autowired 
	UOServiceAgent uoServiceAgent;
	
	@RequestMapping(value="/",method = {RequestMethod.GET, RequestMethod.POST})
    public String ingresoLogin(Model model,HttpServletRequest request, HttpServletResponse response,HttpSession sesion,MenuModel mnuItem)
            throws ServletException, IOException {
		Map<String, Object> map= new HashMap<String, Object>();
		request.getSession().setAttribute("parametros", map);
		List<UO> lstUO = new ArrayList<UO>();
		
		Calendar calendar=Calendar.getInstance();
		map.put("anho", calendar.get(Calendar.YEAR));
		
		//Invocar a Listado de UOs
		lstUO = uoService.lstUnidadOrganizativa();
		
		model.addAttribute("usuarioModel",sesion.getAttribute("usuarioModel"));
		model.addAttribute("listaMenuOpcionesModel",sesion.getAttribute("listaMenuOpcionesModel"));
		model.addAttribute("listaUO", lstUO);
		model.addAttribute("fechaHoy",new Date());
		//Setear Item de Menu Activo
		model.addAttribute(AtributosSesionEnum.ACTIVE_HOME.getCodigo(), mnuItem.getIdopcionpadre());
		model.addAttribute(AtributosSesionEnum.ACTIVE_SUB_HOME.getCodigo(), mnuItem.getIdopcionhijo());
		model.addAttribute(AtributosSesionEnum.MENU_ITEM.getCodigo(), mnuItem);
		return "/mantenimiento/uo/mntUO";
    }	
	
	@SuppressWarnings("unused")
	@RequestMapping(value = "/registrar",method = RequestMethod.POST)
	public @ResponseBody UO crearUo(HttpSession sesion,  UOEntradaModel uo) {
		//DireccionClienteModel direccionClienteModel = this.direccionClienteServiceAgent.obtenerDireccionCliente();
		String token = (String) sesion.getAttribute(AtributosSesionEnum.TOKEN.getCodigo());
		UO unidadOrg = null;
		UOSalidaModel uoSalida = new UOSalidaModel();
		
			uoSalida = new UOSalidaModel();
			uoSalida = uoServiceAgent.addUnidadOrganizativa(uo,token);
		
		return unidadOrg;
	}	
        
	@RequestMapping(value = "/editar", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public Object editarUO(HttpSession sesion, Model model,String iduo) {	
		UOSalidaModel uoSalida = new UOSalidaModel();
		uoSalida.setCodigo("001");
		uoSalida.setValor("Correcto");
		return uoSalida;
	}
	
	
	
}
