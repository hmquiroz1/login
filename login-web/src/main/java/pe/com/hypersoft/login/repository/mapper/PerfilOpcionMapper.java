package pe.com.hypersoft.login.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.hypersoft.login.domain.PerfilOpcion;
//import pe.com.bbva.gifole.domain.SisPerfil;
import pe.com.hypersoft.login.domain.Usuario;

@Component
public class PerfilOpcionMapper implements RowMapper<PerfilOpcion> {
	
	public PerfilOpcion mapRow(ResultSet rs, int i)
			throws SQLException {
		PerfilOpcion po = new PerfilOpcion();
		po.setIdperfilopcion(rs.getLong("ID_OPCION"));
		po.setDescperfil(rs.getString("DESCPERFIL"));
		po.setDescopcion(rs.getString("DESCOPCION"));
		po.setLink(rs.getString("LINK"));
		po.setIcono(rs.getString("ICONO"));
		po.setIndnew(rs.getString("NEW"));
		po.setPadre(rs.getString("PADRE"));
		po.setOrden(rs.getString("ORDEN"));		
		po.setTituloheader(rs.getString("TITULO_HEADER"));	
		return po;
	}

}
