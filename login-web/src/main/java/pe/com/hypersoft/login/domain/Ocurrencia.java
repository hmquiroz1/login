package pe.com.hypersoft.login.domain;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class Ocurrencia implements Serializable{

	private Long idOcurr;
	private String desccorta;
	private String desclarga;
	private String fecIniSuceso;
	private String fecFinSuceso;
	private String color;
	private String tipoOcurr;
	private String estado;
	private String usuarioCreacion;
	private String fecCreacion;
	private String usuarioModif;
	private String fecModif;
	
	/**
	 * ID del Centro de Costo
	 */
	private String idcc;
	/**
	 * ID de la UO
	 */
	private String iduo;
	
	
	public Ocurrencia(){
		
	}	
	
	public String getIdcc() {
		return idcc;
	}
	public void setIdcc(String idcc) {
		this.idcc = idcc;
	}
	public String getIduo() {
		return iduo;
	}
	public void setIduo(String iduo) {
		this.iduo = iduo;
	}

	public Long getIdOcurr() {
		return idOcurr;
	}

	public void setIdOcurr(Long idOcurr) {
		this.idOcurr = idOcurr;
	}

	public String getDesccorta() {
		return desccorta;
	}

	public void setDesccorta(String desccorta) {
		this.desccorta = desccorta;
	}

	public String getDesclarga() {
		return desclarga;
	}

	public void setDesclarga(String desclarga) {
		this.desclarga = desclarga;
	}

	public String getFecIniSuceso() {
		return fecIniSuceso;
	}

	public void setFecIniSuceso(String fecIniSuceso) {
		this.fecIniSuceso = fecIniSuceso;
	}

	public String getFecFinSuceso() {
		return fecFinSuceso;
	}

	public void setFecFinSuceso(String fecFinSuceso) {
		this.fecFinSuceso = fecFinSuceso;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getTipoOcurr() {
		return tipoOcurr;
	}

	public void setTipoOcurr(String tipoOcurr) {
		this.tipoOcurr = tipoOcurr;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getFecCreacion() {
		return fecCreacion;
	}

	public void setFecCreacion(String fecCreacion) {
		this.fecCreacion = fecCreacion;
	}

	public String getUsuarioModif() {
		return usuarioModif;
	}

	public void setUsuarioModif(String usuarioModif) {
		this.usuarioModif = usuarioModif;
	}

	public String getFecModif() {
		return fecModif;
	}

	public void setFecModif(String fecModif) {
		this.fecModif = fecModif;
	}

}
