package pe.com.hypersoft.login.model.seguridad.login;


import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PerfilOpcionModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("idperfilopcion")
	private Long idperfilopcion;
	
	@JsonProperty("descperfil")
	private String descperfil;
	
	@JsonProperty("descopcion")
	private String descopcion;

	@JsonProperty("link")
	private String link;

	@JsonProperty("icono")
	private String icono;
	
	@JsonProperty("indnew")
	private String indnew;
	
	@JsonProperty("padre")
	private String padre;
	
	@JsonProperty("orden")
	private String orden;
	
	@JsonProperty("hijos")
	private List<PerfilOpcionModel> hijos;

	public Long getIdperfilopcion() {
		return idperfilopcion;
	}

	public void setIdperfilopcion(Long idperfilopcion) {
		this.idperfilopcion = idperfilopcion;
	}

	public String getDescperfil() {
		return descperfil;
	}

	public void setDescperfil(String descperfil) {
		this.descperfil = descperfil;
	}

	public String getDescopcion() {
		return descopcion;
	}

	public void setDescopcion(String descopcion) {
		this.descopcion = descopcion;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getIcono() {
		return icono;
	}

	public void setIcono(String icono) {
		this.icono = icono;
	}

	public String getIndnew() {
		return indnew;
	}

	public void setIndnew(String indnew) {
		this.indnew = indnew;
	}

	public String getPadre() {
		return padre;
	}

	public void setPadre(String padre) {
		this.padre = padre;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}

	public List<PerfilOpcionModel> getHijos() {
		return hijos;
	}

	public void setHijos(List<PerfilOpcionModel> hijos) {
		this.hijos = hijos;
	}
	
	
}
