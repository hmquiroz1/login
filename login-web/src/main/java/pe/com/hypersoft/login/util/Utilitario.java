package pe.com.hypersoft.login.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.util.Constante;

/**
 * Class Utilitario
 * Descripcion: Contiene metodos genericos utiles para toda la aplicacion.
 * @author HUMBERTO
 * Fecha Creacion: 14/05/2018
 *
 */
public class Utilitario {

	private static Logger logger = LogManager.getLogger(Utilitario.class);
	
	public static List<PerfilOpcion> reordenarPadreHijoMenuOpciones(List<PerfilOpcion> menuOpciones)
    {
		List<PerfilOpcion> resultado = new ArrayList<PerfilOpcion>();
		List<PerfilOpcion> copy = menuOpciones;
		List<PerfilOpcion> hijos = new ArrayList<PerfilOpcion>();
		
		for(int a=0;a<menuOpciones.size();a++){
			PerfilOpcion tmpPadre = menuOpciones.get(a);
			if(tmpPadre.getPadre().equalsIgnoreCase("0"))
			{
				for(int e=0;e<copy.size();e++){
					if(copy.get(e).getPadre().equals(String.valueOf(tmpPadre.getIdperfilopcion()))){
						hijos.add(copy.get(e));
					}
				}
				tmpPadre.setHijos(hijos);
				resultado.add(tmpPadre);
				hijos = new ArrayList<PerfilOpcion>();
			}
			
		}
		
		return resultado;
    }
}
