package pe.com.hypersoft.login.restclient.seguridad.usuarios.implementacion;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pe.com.hypersoft.login.dto.seguridad.usuarios.DatosEntradaCrearUsuarioDTO;
import pe.com.hypersoft.login.dto.seguridad.usuarios.DatosSalidaCrearUsuarioDTO;
import pe.com.hypersoft.login.enumeration.ParametrosRestClientEnum;
import pe.com.hypersoft.login.restclient.seguridad.usuarios.UsuarioRestClient;

@Service
@Scope("prototype")
public class UsuarioRestClientImpl implements UsuarioRestClient{
	protected final Logger logger = LogManager.getLogger(getClass());
	
	@Autowired
	RestTemplate restTemplateLigero;
	
	@Value("${servicio.rest.usuarios.crear}")
	private String servicioCrearUsuario;
	
	public DatosSalidaCrearUsuarioDTO agregarUsuario(Map<Object, Object> datos){
		logger.info("agregarUsuario : inicio");
		DatosEntradaCrearUsuarioDTO datosEntradaUsuarioDTO = (DatosEntradaCrearUsuarioDTO)datos.get(ParametrosRestClientEnum.OBJETO_PEDIDO);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<Object> requestHeader = new HttpEntity<Object>(datosEntradaUsuarioDTO, headers);
		
		ResponseEntity<DatosSalidaCrearUsuarioDTO> respuesta =this.restTemplateLigero.exchange(servicioCrearUsuario, HttpMethod.POST,
				requestHeader,DatosSalidaCrearUsuarioDTO.class,datosEntradaUsuarioDTO);
		DatosSalidaCrearUsuarioDTO datosSalidaCrearUsuarioDTO = respuesta.getBody();
		logger.info("agregarUsuario : fin");
		return datosSalidaCrearUsuarioDTO;
	}
}
