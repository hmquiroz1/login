package pe.com.hypersoft.login.domain;

import java.io.Serializable;
import java.util.Date;


@SuppressWarnings("serial")
public class OcurrenciaWrapper implements Serializable{

	private String title;
	private String start;
	private String end;
	private String className;
	
	public OcurrenciaWrapper(){
		
	}

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getStart() {
		return start;
	}


	public void setStart(String start) {
		this.start = start;
	}


	public String getEnd() {
		return end;
	}


	public void setEnd(String end) {
		this.end = end;
	}


	public String getClassName() {
		return className;
	}


	public void setClassName(String className) {
		this.className = className;
	}	

}
