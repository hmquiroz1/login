package pe.com.hypersoft.login.restclient.seguridad.usuarios;

import java.util.Map;

import pe.com.hypersoft.login.dto.seguridad.usuarios.DatosSalidaCrearUsuarioDTO;

public interface UsuarioRestClient {
	public DatosSalidaCrearUsuarioDTO agregarUsuario(Map<Object, Object> datos);
}
