package pe.com.hypersoft.login.serviceagent.seguridad.login;

import pe.com.hypersoft.login.model.seguridad.login.DatosEntradaLoginModel;
import pe.com.hypersoft.login.model.seguridad.login.DatosSalidaLoginModel;

public interface LoginServiceAgent {
  public DatosSalidaLoginModel validarAcceso(DatosEntradaLoginModel datosUsuario);
  public DatosSalidaLoginModel lstMenuOpciones(DatosEntradaLoginModel datosUsuario,String token);
}
