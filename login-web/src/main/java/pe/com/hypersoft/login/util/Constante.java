package pe.com.hypersoft.login.util;

public abstract class Constante 
{
	
	public static abstract class PARAMETRO
	{		
		public static abstract class TIPO
		{
			public static final String TIPO_DOC = "TIPO_DOC";
			public static final String HORARIO_SEGURO = "HORARIO_SEGURO";
			public static final String TIPO_SEG = "TIPO_SEG";
			public static final String DEPARTAMENTO = "DEPARTAMENTO";
			public static final String PLACA_SEGURO = "PLACA_SEGURO";
			public static final String TASA_CALC_MIO ="TASA_CALC_MIO";
			public static final String FREC_CALC_MIO ="FREC_CALC_MIO";
			public static final String OPC_CALC_MIO ="OPC_CALC_MIO";
			public static final String SITUACION_LABORAL = "SITUACION_LABORAL";
			public static final String TIPO_HIPOTECARIO = "TIPO_HIPOTECARIO";
			public static final String SERV_CALC_MIO ="SERV_CALC_MIO";
			public static final String VEH_MONTO_GAS ="VEH_MONTO_GAS";
			public static final String VEH_MONTO_GAS_MENS ="VEH_MONTO_GAS_MENS";
			public static final String LUGAR_REGISTRO ="LUGAR_REGISTRO";
			public static final String SERVER_TAG= "TMS";
			public static final String ENTORNO_TAG = "ambTag";
			 
		}
		public static abstract class COTIZADOR
		{
			public static final String COTIZADOR_DIVISA_DOL ="USD";
			public static final String COTIZADOR_USO_PARTICULAR ="0001";
			public static final String COTIZADOR_UBICACION_LIMA ="L";
			public static final String COTIZADOR_UBICACION_PROVINCIA ="P";
		}
	}
	
	public static abstract class SESION
    {
        public final static String USUARIO = "SESION_USUARIO";
        public final static String ITEM = "SESION_ITEM";
    }
	
	public static abstract class DESARROLLO
    {
        public static final String MODO = "0";
    }
	
	//TEST
	//public static final String URL_REST = "http://118.180.14.22:8080";
	//PRODUCCION
	public static final String URL_REST = "http://118.180.13.221:8080";
	//DESA
	//public static final String URL_REST = "http://118.220.21.114:8080";
		
	public static final String URL_REST_INTAVE = "http://118.180.13.221:8080/intave";
	//public static final String URL_REST_INTAVE = "http://118.180.14.22:8080/intave";
	//public static final String URL_REST_INTAVE = "http://localhost:8080/intave";
	public static final String URL_LANDIN = "https://www.bbvacontinental.pe/";
	public static final String URL_AFILIACION_BXI = "https://bancaporinternet.bbvacontinental.pe/bdpnux_pe_web/bdpnux_pe_web/inscripcion/index";
		
}

