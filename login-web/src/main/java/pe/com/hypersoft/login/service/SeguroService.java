package pe.com.hypersoft.login.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


public interface SeguroService extends Serializable {
	
	void cargarParametros();
	Map<String, Object> consultarParametrosSeguro();
	Map<String, Object> consultarParametrosSeguroVehicular();
//	List<VehModelo> consultarModelosMarca(String marca);
//	List<VehTipo> consultarTipos(String marca, String modelo);
//	void registrarSeguro(Seguro seguro);
//	void registrarSeguroVehicular(SeguroVehicular seguroVehicular);
//	VehAutoMas buscarValorComercial(Map<String,String> map);
//	VehSunarp buscarPlaca(String placa);
//	VehCotizacion cotizarVehiculo(VehCotizacion vehCotizacion);
//	void registrarCotizacionVehicular(VehCotizacion vehCotizacion);
}
