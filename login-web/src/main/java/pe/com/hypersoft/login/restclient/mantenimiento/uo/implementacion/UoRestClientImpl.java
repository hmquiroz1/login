package pe.com.hypersoft.login.restclient.mantenimiento.uo.implementacion;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import pe.com.hypersoft.login.dto.mantenimiento.uo.DatosEntradaUoDTO;
import pe.com.hypersoft.login.dto.mantenimiento.uo.DatosSalidaUoDTO;
import pe.com.hypersoft.login.dto.seguridad.login.DatosEntradaLoginDTO;
import pe.com.hypersoft.login.dto.seguridad.login.DatosSalidaLoginDTO;
import pe.com.hypersoft.login.enumeration.AtributosSesionEnum;
import pe.com.hypersoft.login.enumeration.ParametrosRestClientEnum;
import pe.com.hypersoft.login.restclient.mantenimiento.uo.UoRestClient;
import pe.com.hypersoft.login.restclient.seguridad.login.LoginRestClient;

@Service
@Scope("prototype")
public class UoRestClientImpl implements UoRestClient{
	protected final Logger logger = LogManager.getLogger(getClass());
	
	@Autowired
	RestTemplate restTemplateLigero;
	
	@Value("${servicio.rest.uo.crear}")
	private String servicioUO;
	
	public DatosSalidaUoDTO addUnidadOrganizativa(Map<Object, Object> datos,String token){
		logger.info("addUnidadOrganizativa : inicio");
		DatosEntradaUoDTO datosEntradaUoDTO = (DatosEntradaUoDTO)datos.get(ParametrosRestClientEnum.OBJETO_PEDIDO);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set(AtributosSesionEnum.AUTHORIZATION.getCodigo(), token);
		HttpEntity<Object> requestHeader = new HttpEntity<Object>(datosEntradaUoDTO, headers);
		
		ResponseEntity<DatosSalidaUoDTO> respuesta =this.restTemplateLigero.exchange(servicioUO, HttpMethod.POST,
				requestHeader,DatosSalidaUoDTO.class,datosEntradaUoDTO);
		HttpHeaders tokenSalidaLoginDTO = respuesta.getHeaders();
		//tokenSalidaLoginDTO.get("token").get(0)
		DatosSalidaUoDTO datosSalidaUoDTO = respuesta.getBody();
		logger.info("addUnidadOrganizativa : fin");
		return datosSalidaUoDTO;
	}
}
