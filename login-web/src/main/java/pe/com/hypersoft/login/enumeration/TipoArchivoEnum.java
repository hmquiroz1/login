package pe.com.hypersoft.login.enumeration;

public enum TipoArchivoEnum {

	AUTOMAS("VEH_COT_AUTOMAS","Automás", "automas.txt", "automas_ant.txt", "CARGAR_AUTOMAS", "EXT_VEH_AUTOMAS","automas_pivot.txt"),
	CATEGORIA("VEH_COT_CATEGORIA","Categoría", "categoria.txt", "categoria_ant.txt", "CARGAR_CATEGORIA", "EXT_VEH_CATEGORIA","categoria_pivot.txt"),
	BENEFICIO("VEH_COT_BENEFICIO","Beneficio", "beneficio.txt", "beneficio_ant.txt", "CARGAR_BENEFICIO", "EXT_VEH_BENEFICIO","beneficio_pivot.txt"),
	DESCUENTO_CAPA("VEH_COT_DESCUENTO_CAPA","Descuento Capa", "descuento_capa.txt", "descuento_capa_ant.txt", "CARGAR_DESCUENTO_CAPA", "EXT_VEH_DESCUENTO_CAPA", "descuento_capa_pivot.txt"),
	PLAN("VEH_COT_PLAN","Plan", "plan.txt", "plan_ant.txt", "CARGAR_PLAN", "EXT_VEH_PLAN","plan_pivot.txt"),
	PLAN_BENEFICIO("VEH_COT_PLAN_BENEFICIO","Plan Beneficio", "plan_beneficio.txt", "plan_beneficio_ant.txt", "CARGAR_PLAN_BENEFICIO", "EXT_VEH_PLAN_BENEFICIO","plan_beneficio_pivot.txt"),
	PRIMA_MINIMA("VEH_COT_PRIMA_MINIMA","Prima Mínima", "prima_minima.txt", "prima_minima_ant.txt", "CARGAR_PRIMA_MINIMA", "EXT_VEH_PRIMA_MINIMA","prima_minima_pivot.txt"),
	SUNARP("VEH_COT_SUNARP","Sunarp", "sunarp.txt", "sunarp_ant.txt", "CARGAR_SUNARP", "EXT_VEH_SUNARP","sunarp_pivot.txt"),
	TARIFA("VEH_COT_TARIFA","Tarifa", "tarifa.txt", "tarifa_ant.txt", "CARGAR_TARIFA", "EXT_VEH_TARIFA","tarifa_pivot.txt"),
	CLIENTES("VEH_COT_CLIENTE","Clientes", "cliente.txt", "cliente_ant.txt", "CARGAR_CLIENTE", "EXT_VEH_CLIENTE","cliente_pivot.txt"),
	COBERTURAS_SEGUROS("VEH_COT_COBERTURAS","Detalle de los Planes PDF", "Coberturas_Seguro_Vehicular_BBVA.pdf", "", "", "",""),
	DEDUCIBLES_SEGUROS("VEH_COT_DEDUCIBLES","Detalle de Deducibles PDF", "Deducibles_Seguro_Vehicular_BBVA.pdf", "", "", "","");

	private String codigo;
	private String nombre;
	private String archivoTxt;
	private String archivoTxtAnt;
	private String nombreProcedimiento;
	private String nombreTablaExterna;
	private String archivoTxtPivot;
	
	private TipoArchivoEnum(String codigo, String nombre, String archivoTxt, String archivoTxtAnt, String nombreProcedimiento, String nombreTablaExterna,String archivoTxtPivot) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.archivoTxt = archivoTxt;
		this.archivoTxtAnt = archivoTxtAnt;
		this.nombreProcedimiento = nombreProcedimiento;
		this.nombreTablaExterna = nombreTablaExterna;
		this.archivoTxtPivot =  archivoTxtPivot;
	}

	public static TipoArchivoEnum getCodigo(String codigo) {
		if (codigo == null) {
			return null;
		}
		for (TipoArchivoEnum archivo : TipoArchivoEnum.values()) {
			if (codigo.compareTo(archivo.getCodigo()) == 0) {
				return archivo;
			}
		}
		return null;
	}
	
	public static TipoArchivoEnum getNombre(String nombre) {
		if (nombre == null) {
			return null;
		}
		for (TipoArchivoEnum archivo : TipoArchivoEnum.values()) {
			if (nombre.compareTo(archivo.getNombre()) == 0) {
				return archivo;
			}
		}
		return null;
	}
	
	public static TipoArchivoEnum getArchivoTxt(String archivoTxt) {
		if (archivoTxt == null) {
			return null;
		}
		for (TipoArchivoEnum archivo : TipoArchivoEnum.values()) {
			if (archivoTxt.compareTo(archivo.getArchivoTxt()) == 0) {
				return archivo;
			}
		}
		return null;
	}
	
	public static TipoArchivoEnum getArchivoTxtAnt(String archivoTxtAnt) {
		if (archivoTxtAnt == null) {
			return null;
		}
		for (TipoArchivoEnum archivo : TipoArchivoEnum.values()) {
			if (archivoTxtAnt.compareTo(archivo.getArchivoTxtAnt()) == 0) {
				return archivo;
			}
		}
		return null;
	}
	
	public static TipoArchivoEnum getNombreProcedimiento(String nombreProcedimiento) {
		if (nombreProcedimiento == null) {
			return null;
		}
		for (TipoArchivoEnum archivo : TipoArchivoEnum.values()) {
			if (nombreProcedimiento.compareTo(archivo.getNombreProcedimiento()) == 0) {
				return archivo;
			}
		}
		return null;
	}
	
	public static TipoArchivoEnum getNombreTablaExterna(String nombreTablaExterna) {
		if (nombreTablaExterna == null) {
			return null;
		}
		for (TipoArchivoEnum archivo : TipoArchivoEnum.values()) {
			if (nombreTablaExterna.compareTo(archivo.getNombreTablaExterna()) == 0) {
				return archivo;
			}
		}
		return null;
	}
	
	public static TipoArchivoEnum getArchivoTxtPivot(String archivoTxtPivot) {
		if (archivoTxtPivot == null) {
			return null;
		}
		for (TipoArchivoEnum archivo : TipoArchivoEnum.values()) {
			if (archivoTxtPivot.compareTo(archivo.getArchivoTxtPivot()) == 0) {
				return archivo;
			}
		}
		return null;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getArchivoTxt() {
		return archivoTxt;
	}

	public void setArchivoTxt(String archivoTxt) {
		this.archivoTxt = archivoTxt;
	}

	public String getArchivoTxtAnt() {
		return archivoTxtAnt;
	}

	public void setArchivoTxtAnt(String archivoTxtAnt) {
		this.archivoTxtAnt = archivoTxtAnt;
	}

	public String getNombreProcedimiento() {
		return nombreProcedimiento;
	}

	public void setNombreProcedimiento(String nombreProcedimiento) {
		this.nombreProcedimiento = nombreProcedimiento;
	}

	public String getNombreTablaExterna() {
		return nombreTablaExterna;
	}

	public void setNombreTablaExterna(String nombreTablaExterna) {
		this.nombreTablaExterna = nombreTablaExterna;
	}
	
	public String getArchivoTxtPivot() {
		return archivoTxtPivot;
	}

	public void setArchivoTxtPivot(String archivoTxtPivot) {
		this.archivoTxtPivot = archivoTxtPivot;
	}

	
}
