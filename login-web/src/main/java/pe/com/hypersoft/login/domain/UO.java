package pe.com.hypersoft.login.domain;

import java.io.Serializable;
import java.util.List;

public class UO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long iduo;
	private String descripcion;
	private String nombre;
	private String estado;
	private String fecinivig;
	private String fecfinvig;
	
	public Long getIduo() {
		return iduo;
	}
	public void setIduo(Long iduo) {
		this.iduo = iduo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getFecinivig() {
		return fecinivig;
	}
	public void setFecinivig(String fecinivig) {
		this.fecinivig = fecinivig;
	}
	public String getFecfinvig() {
		return fecfinvig;
	}
	public void setFecfinvig(String fecfinvig) {
		this.fecfinvig = fecfinvig;
	}
	
	
	
	
}
