package pe.com.hypersoft.login.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.model.seguridad.login.DatosEntradaLoginModel;
import pe.com.hypersoft.login.model.global.UsuarioModel;
import pe.com.hypersoft.login.repository.UsuarioDao;
import pe.com.hypersoft.login.repository.mapper.PerfilOpcionMapper;
import pe.com.hypersoft.login.repository.mapper.UsuarioMapper;
import pe.com.hypersoft.login.util.GifoleUtil;

@Repository
public class UsuarioDaoImpl implements UsuarioDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private UsuarioMapper usuarioMapper;
	
	@Autowired
	PerfilOpcionMapper perfilOpcionMapper;
		
	public List<Usuario> buscarUsuarioConsultaSQL(Usuario usuario) {
		
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT A.ID AS ID_USUARIO , A.REGISTRO, A.NOMBRE, A.PATERNO, A.MATERNO, A.ESTADO FROM APP_SEGURIDAD.USUARIO A ");		
		sql.append("WHERE  ");
		sql.append(GifoleUtil.manejoCondicionalConsultaSQL(usuario.getRegistro(), "A.REGISTRO") + " AND ");
		sql.append(GifoleUtil.manejoCondicionalConsultaSQL(usuario.getNombre(), "A.NOMBRE") + " AND ");
		sql.append(GifoleUtil.manejoCondicionalConsultaSQL(usuario.getPaterno(), "A.PATERNO") + " AND ");
		sql.append(GifoleUtil.manejoCondicionalConsultaSQL(usuario.getMaterno(), "A.MATERNO") + " AND ");
		sql.append("ORDER BY A.NOMBRE ASC ");
		
		return jdbcTemplate.query(sql.toString(), usuarioMapper);
	}
	
	public void eliminarUsuario(Long id) {
		jdbcTemplate.update("DELETE FROM APP_SEGURIDAD.USUARIO WHERE id = ?", new Object[] { id });	
	}
	
	public void crearUsuario(Usuario usuario) {
		
		String sqlId = "select APP_SEGURIDAD.SQ_USUARIO.nextval from dual";
		Integer id = (Integer)jdbcTemplate.queryForObject(sqlId,  Integer.class);
		String sql="INSERT INTO APP_SEGURIDAD.USUARIO (ID, REGISTRO, NOMBRE, PATERNO, MATERNO, ESTADO) "				
				+ "values (?,?,?,?,?,?)";
		jdbcTemplate.update(sql, new Object[] { 
				id, 
				usuario.getRegistro(),
				usuario.getNombre(),
				usuario.getPaterno(),
				usuario.getMaterno(),
				"A"
		});
			
	}

	public void actualizarUsuario(Usuario usuario) {
		jdbcTemplate.update("UPDATE APP_SEGURIDAD.USUARIO SET NOMBRE = ?,PATERNO = ?,MATERNO = ?,EMAIL = ?,TELEFONO = ?  WHERE ID = ?", 
				new Object[] { usuario.getNombre(),usuario.getPaterno(),usuario.getMaterno(),usuario.getEmail(),usuario.getTelefono(), usuario.getId() });
	}


	public List<Usuario> obtenerUsuario(DatosEntradaLoginModel usuarioInput) {
		StringBuilder sql=new StringBuilder();
		sql.append("SELECT A.ID_USUARIO , A.REGISTRO, A.NOMBRE, A.PATERNO, A.MATERNO, A.TIPODOC,A.NRODOC,A.EMAIL,A.TELEFONO,A.ESTADO,A.API_TOKEN,U.DESCRIPCION AS NOMEDIFICIO,A.ID_PERFIL AS PERFIL,CASE A.ID_PERFIL WHEN 1 THEN 'ADMIN' ELSE 'USUARIO' END AS TIPOPERFIL,A.IDCC AS CENTRO_COSTO,A.ID_UO AS UO ");		
		sql.append("FROM APP_SEGURIDAD.USUARIO A,APP_SEGURIDAD.CENTRO_COSTO C,APP_SEGURIDAD.UO U ");
		sql.append("WHERE  A.IDCC = C.IDCC and A.ID_UO = C.ID_UO and C.ID_UO = U.ID_UO AND ");
		sql.append(GifoleUtil.manejoCondicionalConsultaSQL(usuarioInput.getAutenticacion().getEmail(), "A.EMAIL") + " AND ");
		sql.append(GifoleUtil.manejoCondicionalConsultaSQL(usuarioInput.getAutenticacion().getClave(), "A.CLAVE") + " AND ");
		sql.append(GifoleUtil.manejoCondicionalConsultaSQL("A", "A.ESTADO"));
				
		return jdbcTemplate.query(sql.toString(), usuarioMapper);
	}
	
	public List<PerfilOpcion> lstMenuOpciones(Integer idPerfil) {
		StringBuilder sql=new StringBuilder();
		sql.append("select PO.ID_OPCION,P.DESCRIPCION AS DESCPERFIL,TRIM(O.DESCRIPCION) AS DESCOPCION,TRIM(O.LINK) AS LINK,TRIM(O.TITULO_CONTENT_HEADER) AS TITULO_HEADER,TRIM(O.ICONO) AS ICONO,O.IND_NEW AS NEW,IFNULL(O.PADRE, 0) AS PADRE , O.ORDEN AS ORDEN ");		
		sql.append("from APP_SEGURIDAD.PERFIL_OPCION PO,APP_SEGURIDAD.OPCION O,APP_SEGURIDAD.PERFIL P ");
		sql.append("where PO.ID_OPCION = O.ID_OPCION AND PO.ID_PERFIL = P.ID_PERFIL AND O.ESTADO = 'A' AND P.ESTADO = 'A' AND PO.ESTADO = 'A' AND ");
		sql.append(GifoleUtil.manejoCondicionalConsultaSQL(Integer.toString(idPerfil), "PO.ID_PERFIL") + "  ");
		sql.append("order by ORDEN ASC ");
				
		return jdbcTemplate.query(sql.toString(), perfilOpcionMapper);
	}
}
