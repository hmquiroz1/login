package pe.com.hypersoft.login.enumeration;

public final class VehCotizadorConstant {
	 
	public static final String DIRECTORIO_FILE = "/mnt/compartido/gifole/seguro/cotizador_vehicular/in/";
	public static final String DIRECTORIO_FILE_PDF = "/mnt/compartido/gifole/seguro/cotizador_vehicular/";
	public static final String CODIGO_ESTADO_SIN_PROCESO = "S";
	public static final String DESCRIPCION_ESTADO_SIN_PROCESO = "Sin Procesar";	
	public static final String CODIGO_ESTADO_PROCESADO = "P";
	public static final String DESCRIPCION_ESTADO_PROCESADO = "Procesado";
	public static final String CODIGO_ESTADO_ERROR = "E";
	public static final String DESCRIPCION_ESTADO_ERROR = "No Terminado";
	public static final String CODIGO_ESTADO_CATEGORIA_ACTIVADO = "A";
}
