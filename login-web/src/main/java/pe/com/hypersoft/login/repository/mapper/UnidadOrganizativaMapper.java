package pe.com.hypersoft.login.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.UO;
//import pe.com.bbva.gifole.domain.SisPerfil;
import pe.com.hypersoft.login.domain.Usuario;

@Component
public class UnidadOrganizativaMapper implements RowMapper<UO> {
	
	public UO mapRow(ResultSet rs, int i)
			throws SQLException {
		UO unidadOrg = new UO();
		unidadOrg.setIduo(rs.getLong("ID_UO"));		
		unidadOrg.setDescripcion(rs.getString("DESC_UO"));
		unidadOrg.setNombre(rs.getString("NOM_UO"));
		unidadOrg.setEstado(rs.getString("ESTADO_UO"));
		unidadOrg.setFecinivig(rs.getString("FEC_INI_VIG"));
		unidadOrg.setFecfinvig(rs.getString("FEC_FIN_VIG"));	
		return unidadOrg;
	}

}
