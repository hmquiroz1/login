package pe.com.hypersoft.login.util;

public class Funcion {
	public static String SimboloMoneda(String moneda){
		String simbolo="";
		if(moneda.toUpperCase().indexOf("SOLES")>-1 || moneda.toUpperCase().indexOf("PEN")>-1){
			simbolo="S/";
		}else if(moneda.toUpperCase().indexOf("DOLARES")>-1 || moneda.toUpperCase().indexOf("USD")>-1){
			simbolo="$";
		}
		return simbolo;
	}
	
	/**
	 * Formatear el importe tipo string
	 * 
	 *
	 * @param monto
	 *            Importe a formatear
	 * @param tipo
	 *            <code>1</code> con punto decimal con separador de miles / 
	 *            <code>2</code> sin decimales con separador de miles /
	 *            <code>3</code> sin decimales ni separador de miles
	 * @return fmonto
	 * 			Monto formateado
	 */
	public static String FormatoMonto(String monto, int tipo){
		Number numero;
		String numeroformateado="";
		try{
			java.text.DecimalFormatSymbols dfs = new java.text.DecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			dfs.setGroupingSeparator(',');
			java.text.DecimalFormat df;
			if (tipo==1){
				df = new java.text.DecimalFormat("#,###,###.00",dfs);}
			else if (tipo==2){
				df = new java.text.DecimalFormat("#,###,###",dfs);
			} else {
				monto=monto.replace(",","");
				df = new java.text.DecimalFormat("#######",dfs);
			}
			numero = df.parse(monto);
			numeroformateado= df.format(numero.doubleValue());
		}catch(Exception e){
			numeroformateado="0.00";
		}
		return numeroformateado;
	}
}
