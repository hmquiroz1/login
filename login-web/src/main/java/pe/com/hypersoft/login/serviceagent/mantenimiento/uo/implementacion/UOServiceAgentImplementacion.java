package pe.com.hypersoft.login.serviceagent.mantenimiento.uo.implementacion;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ma.glasnost.orika.MapperFacade;
import pe.com.hypersoft.login.dto.mantenimiento.uo.DatosEntradaUoDTO;
import pe.com.hypersoft.login.dto.mantenimiento.uo.DatosSalidaUoDTO;
import pe.com.hypersoft.login.enumeration.ParametrosRestClientEnum;
import pe.com.hypersoft.login.model.uo.UOEntradaModel;
import pe.com.hypersoft.login.model.uo.UOSalidaModel;
import pe.com.hypersoft.login.restclient.mantenimiento.uo.UoRestClient;
import pe.com.hypersoft.login.serviceagent.mantenimiento.uo.UOServiceAgent;

@Service
public class UOServiceAgentImplementacion implements UOServiceAgent {
	
	@Autowired
	UoRestClient uoRestClient;
	@Autowired
	private MapperFacade mapper;
	
	/**
	   * Restful para  validar acceso al Sistema	
	   */
	  public UOSalidaModel addUnidadOrganizativa(UOEntradaModel entradaUoModel,String token){
		  	  
		  DatosEntradaUoDTO datosEntradaUoDTO = this.mapper.map(entradaUoModel,DatosEntradaUoDTO.class);
		  Map<Object, Object> objetoSolicitud = new HashMap<Object, Object>();
		  objetoSolicitud.put(ParametrosRestClientEnum.OBJETO_PEDIDO, datosEntradaUoDTO);
			
		  DatosSalidaUoDTO datosSalidaUoDTO = this.uoRestClient.addUnidadOrganizativa(objetoSolicitud,token);
		  UOSalidaModel uoSalidaModel = this.mapper.map(datosSalidaUoDTO,UOSalidaModel.class);
		  return uoSalidaModel;
	  }
}
