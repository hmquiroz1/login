package pe.com.hypersoft.login.repository;

import java.util.List;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.model.seguridad.login.DatosEntradaLoginModel;

public interface UsuarioDao {
	
	
	void eliminarUsuario(Long id);
	void crearUsuario(Usuario usuario);
	void actualizarUsuario(Usuario usuario);
	List<Usuario> buscarUsuarioConsultaSQL(Usuario usuario);
	List<Usuario> obtenerUsuario(DatosEntradaLoginModel usuarioInput);
	List<PerfilOpcion> lstMenuOpciones(Integer idPerfil);
}
