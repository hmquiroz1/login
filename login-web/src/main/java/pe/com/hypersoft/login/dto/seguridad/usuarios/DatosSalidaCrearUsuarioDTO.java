package pe.com.hypersoft.login.dto.seguridad.usuarios;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatosSalidaCrearUsuarioDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("data")	
	private UsuarioDTO data;
	
	public UsuarioDTO getData() {
		return data;
	}

	public void setData(UsuarioDTO data) {
		this.data = data;
	}
}
