package pe.com.hypersoft.login.model.seguridad.usuarios;

import java.io.Serializable;

public class DatosEntradaUsuarioModel implements Serializable{

	/**
	 * Clase para Administrar los Items de Menu seleccionado por usuario
	 */
	private static final long serialVersionUID = 1L;
	
	private String email;
	private String clave;
	private String nombre;
	private String paterno;	
	private String materno;	
	private String tipodoc;
	private String nrodoc;
	private String fecnacimiento;
	private String estadocivil;
	private String telefono;
	private String estado;
	private String perfil;
	private String idcc;
	private String iduo;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getTipodoc() {
		return tipodoc;
	}
	public void setTipodoc(String tipodoc) {
		this.tipodoc = tipodoc;
	}
	public String getNrodoc() {
		return nrodoc;
	}
	public void setNrodoc(String nrodoc) {
		this.nrodoc = nrodoc;
	}
	public String getFecnacimiento() {
		return fecnacimiento;
	}
	public void setFecnacimiento(String fecnacimiento) {
		this.fecnacimiento = fecnacimiento;
	}
	public String getEstadocivil() {
		return estadocivil;
	}
	public void setEstadocivil(String estadocivil) {
		this.estadocivil = estadocivil;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getIdcc() {
		return idcc;
	}
	public void setIdcc(String idcc) {
		this.idcc = idcc;
	}
	public String getIduo() {
		return iduo;
	}
	public void setIduo(String iduo) {
		this.iduo = iduo;
	}
	
		
}
