package pe.com.hypersoft.login.serviceagent.seguridad.usuarios.implementacion;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import pe.com.hypersoft.login.dto.seguridad.usuarios.DatosEntradaCrearUsuarioDTO;
import pe.com.hypersoft.login.dto.seguridad.usuarios.DatosSalidaCrearUsuarioDTO;
import pe.com.hypersoft.login.dto.seguridad.usuarios.SeguridadRestDTO;
import pe.com.hypersoft.login.dto.seguridad.usuarios.UsuarioDTO;
import pe.com.hypersoft.login.enumeration.ParametrosRestClientEnum;
import pe.com.hypersoft.login.model.seguridad.usuarios.DatosEntradaUsuarioModel;
import pe.com.hypersoft.login.model.seguridad.usuarios.DatosSalidaUsuarioModel;
import pe.com.hypersoft.login.restclient.seguridad.usuarios.UsuarioRestClient;
import pe.com.hypersoft.login.serviceagent.seguridad.usuarios.UsuarioServiceAgent;

@Service
public class UsuarioServiceAgentImplementacion implements UsuarioServiceAgent {
	
  @Autowired
  UsuarioRestClient usuarioRestClient;
	
  /**
   * Restful para registrar usuario	
   */
  public DatosSalidaUsuarioModel agregarUsuario(DatosEntradaUsuarioModel datosUsuario){
	  DatosSalidaUsuarioModel datoSalidaUsuarioModel = new DatosSalidaUsuarioModel();
	  
	  DatosEntradaCrearUsuarioDTO datosEntradaUsuarioDTO = new DatosEntradaCrearUsuarioDTO();
	  SeguridadRestDTO security = new SeguridadRestDTO();
	  security.setUsuario("VVNSWFJFU1Q=");
	  security.setPassword("U3RvbmUxMjM0NTY=");
	  datosEntradaUsuarioDTO.setSecurity(security);
	  UsuarioDTO data = new UsuarioDTO();
	  data.setRegistro("YCASTRO");
	  data.setClave("coneja");
	  data.setNombre("YANINA KATHERINE");
	  data.setPaterno("CASTRO");
	  data.setMaterno("VENTURA");
	  data.setTipodoc("DNI");
	  data.setNrodoc("41414141");
	  data.setEmail("peticienta@yahoo.es");
	  data.setFecnacimiento("10/04/1983");
	  data.setEstadocivil("CASADA");
	  data.setTelefono("949250562");
	  data.setEstado("I");
	  data.setPerfil("1");
	  data.setIdcc("1");
	  data.setIduo("1");
	  datosEntradaUsuarioDTO.setData(data);
	  
	  Map<Object, Object> objetoSolicitud = new HashMap<Object, Object>();
	  objetoSolicitud.put(ParametrosRestClientEnum.OBJETO_PEDIDO, datosEntradaUsuarioDTO);
		
	  DatosSalidaCrearUsuarioDTO datosSalidaCrearUsuarioDTO = this.usuarioRestClient.agregarUsuario(objetoSolicitud);
	  
	  return datoSalidaUsuarioModel;
  }
}
