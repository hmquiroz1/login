package pe.com.hypersoft.login.model.seguridad.login;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.model.global.UsuarioModel;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatosSalidaLoginModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	
	private String valor;
	
	private List<UsuarioModel> listaUsuarios;
	
	private List<PerfilOpcion> listaMenuOpciones;
	
	private String token;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public List<UsuarioModel> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(List<UsuarioModel> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public List<PerfilOpcion> getListaMenuOpciones() {
		return listaMenuOpciones;
	}

	public void setListaMenuOpciones(List<PerfilOpcion> listaMenuOpciones) {
		this.listaMenuOpciones = listaMenuOpciones;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	
	
}
