package pe.com.hypersoft.login.web.mantenimiento.asistencia;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import pe.com.hypersoft.login.common.excepcion.ExcepcionSIADE;
import pe.com.hypersoft.login.domain.Ocurrencia;
import pe.com.hypersoft.login.domain.OcurrenciaWrapper;
import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.UO;
import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.enumeration.AtributosSesionEnum;
import pe.com.hypersoft.login.model.global.MenuModel;
import pe.com.hypersoft.login.model.global.UsuarioModel;
import pe.com.hypersoft.login.model.uo.UOEntradaModel;
import pe.com.hypersoft.login.service.SeguridadService;
import pe.com.hypersoft.login.service.UnidadOrganizativaService;
import pe.com.hypersoft.login.util.UtilJson;
import pe.com.hypersoft.login.util.Utilitario;


@Controller
@Scope("request")
@RequestMapping(value = "/mantenimiento/asistencia")
public class AsistenciaController {

	@Autowired
	SeguridadService seguridadService;
	
	@Autowired 
	UnidadOrganizativaService uoService;
	
	@RequestMapping(value="/registro",method = {RequestMethod.GET, RequestMethod.POST})
    public String registrarAsistencia(Model model,HttpServletRequest request, HttpServletResponse response,HttpSession sesion,MenuModel mnuItem)
            throws ServletException, IOException {
		Map<String, Object> map= new HashMap<String, Object>();
		request.getSession().setAttribute("parametros", map);		
		Calendar calendar=Calendar.getInstance();
		map.put("anho", calendar.get(Calendar.YEAR));		
		
		model.addAttribute("fechaHoy",new Date());
		//Setear Item de Menu Activo
		model.addAttribute("usuarioModel",sesion.getAttribute("usuarioModel"));
		model.addAttribute("listaMenuOpcionesModel",sesion.getAttribute("listaMenuOpcionesModel"));
		model.addAttribute(AtributosSesionEnum.ACTIVE_HOME.getCodigo(), mnuItem.getIdopcionpadre());
		model.addAttribute(AtributosSesionEnum.ACTIVE_SUB_HOME.getCodigo(), mnuItem.getIdopcionhijo());
		model.addAttribute(AtributosSesionEnum.MENU_ITEM.getCodigo(), mnuItem);
		return "/mantenimiento/asistencia/registro";
    }	
	
	@RequestMapping(value = "/edicion",method = RequestMethod.POST)
	public String actualizarAsistencia(Model model,HttpServletRequest request, HttpServletResponse response,HttpSession sesion,MenuModel mnuItem) {
		UO unidadOrg = null;
		return "/mantenimiento/asistencia/edicion";
	}	
        
	@RequestMapping(value = "/{uo}/{cc}",method = RequestMethod.GET)
	public @ResponseBody String obtenerListaOcurrencias(HttpSession sesion, @PathVariable("uo") String iduo,@PathVariable("cc") String idcc) {
		String json = "[]";
		List<OcurrenciaWrapper> rpta = null;
		rpta = new ArrayList<OcurrenciaWrapper>();
		
		OcurrenciaWrapper oc1 =  new OcurrenciaWrapper();
		oc1.setTitle("Permiso Sr Luis- Odontologo");
		oc1.setStart("2019-03-01");
		oc1.setEnd("2019-03-01");
		oc1.setClassName("bg-info");
		rpta.add(oc1);
		
		OcurrenciaWrapper oc2 =  new OcurrenciaWrapper();
		oc2.setTitle("Asistencia Adicional Sr Luis");
		oc2.setStart("2019-03-08");
		oc2.setEnd("2019-03-08");
		oc2.setClassName("bg-success");
		rpta.add(oc2);
		
		OcurrenciaWrapper oc3 =  new OcurrenciaWrapper();		
		oc3.setTitle("Falta Injustificada Sr Luis");
		oc3.setStart("2019-03-13");
		oc3.setEnd("2019-03-13");
		oc3.setClassName("bg-danger");
		rpta.add(oc3);
		
		json = UtilJson.convertirObjectoACadena(rpta);
		
		return json;
	}
	
	@RequestMapping(value = "/agregar",method = RequestMethod.POST)
	public @ResponseBody String agregarOcurrencia(HttpSession sesion,Ocurrencia data) {
	
		String mensajeError = "";//MensajesErrorEnum.MENSAJE_CARGA_ERROR_TOC.getCodigo();
		UsuarioModel usuario = (UsuarioModel) sesion.getAttribute("usuarioModel");
		try {
						
			//sesion.setAttribute(AtributosSesionEnum.TARJETA_ADICIONAL_ONE_CLICK.getCodigo(), tarjAdicional);
			return "data:OK";			
		} 
		catch (Exception ex) {			
			throw new ExcepcionSIADE(mensajeError);
		}
		
	}
	
	
}
