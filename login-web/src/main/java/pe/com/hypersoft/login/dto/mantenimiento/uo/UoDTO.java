package pe.com.hypersoft.login.dto.mantenimiento.uo;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UoDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("iduo")
	private Long iduo;
	@JsonProperty("descripcion")
	private String descripcion;
	@JsonProperty("nombre")
	private String nombre;
	@JsonProperty("estado")
	private String estado;
	@JsonProperty("fecinivig")
	private String fecinivig;
	@JsonProperty("fecfinvig")
	private String fecfinvig;
	
	public Long getIduo() {
		return iduo;
	}
	public void setIduo(Long iduo) {
		this.iduo = iduo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getFecinivig() {
		return fecinivig;
	}
	public void setFecinivig(String fecinivig) {
		this.fecinivig = fecinivig;
	}
	public String getFecfinvig() {
		return fecfinvig;
	}
	public void setFecfinvig(String fecfinvig) {
		this.fecfinvig = fecfinvig;
	}
}
