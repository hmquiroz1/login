package pe.com.hypersoft.login.serviceagent.mantenimiento.uo;

import pe.com.hypersoft.login.model.uo.UOEntradaModel;
import pe.com.hypersoft.login.model.uo.UOSalidaModel;

public interface UOServiceAgent {
	UOSalidaModel addUnidadOrganizativa(UOEntradaModel uo,String token);
}
