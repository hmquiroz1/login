package pe.com.hypersoft.login.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.util.Constante;

public class GifoleUtil {

	private static Logger logger = LogManager.getLogger(GifoleUtil.class);
	
	public static Usuario obtenerUsuarioSesion()
    {
        Usuario usuario = null;
        if(RequestContextHolder.getRequestAttributes()!=null){
            ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
                            .currentRequestAttributes();
            HttpServletRequest request = requestAttributes.getRequest();
            HttpSession session = request.getSession(false);
            if(session != null)
            {
            	usuario = (Usuario) session.getAttribute(Constante.SESION.USUARIO);
            }
        }
        return usuario;
    }
	
	
	public static int ejecutarSCP(String desde, String hacia) {
		StringBuilder strComando = new StringBuilder();
		strComando.append("/usr/bin/scp ");
		strComando.append(desde).append(" ");
		strComando.append(hacia);
		
		logger.info("Comando a ejecutar:" + strComando.toString());
		
		Process pros;
		try {
			pros = Runtime.getRuntime().exec(strComando.toString());
			pros.waitFor();
			int valorSalida = pros.exitValue();
			if(valorSalida != 0) {
				logger.error("Error scp: " + IOUtils.toString(pros.getErrorStream()));
				valorSalida=-1;
			}
			return valorSalida;
		} catch (Throwable e) {
			logger.error("Error al ejecutar el comando scp", e);
		}
		
		return -1;
	}
		
	
	public static String toString(Object obj) {
		return ToStringBuilder.reflectionToString(obj);
	}
	
	/** Metodo usado para formatear un importe con separador de miles o decimales
	 * @param monto Es el importe a formatear
	 * @param formato Es el formato a usar. Ejm: <code>###,###.##</code>
	 * @return importe formateado. Si hubiera algun error retorna el monto sin formato */
	public static String formatearImporte(String monto, String formato){
		DecimalFormatSymbols simbolo=new DecimalFormatSymbols();
	    simbolo.setDecimalSeparator('.');
	    simbolo.setGroupingSeparator(',');
	    DecimalFormat formateador = new DecimalFormat(formato,simbolo);
		try {
			double d = Double.parseDouble(monto);
		    monto = formateador.format (d);
		} catch (Exception e) {
		}
		return monto;
	}
	
	public static Map<String, String> obtenerUsuarioConsultaLego(){
		Map<String, String> mapSct = new HashMap<String, String>();		
		mapSct.put("usuario", "TEVHTw==");
		mapSct.put("password", "R0lGT0xFX1dT");		
		return mapSct;
	}
	
	public static String manejoCondicionalConsultaSQL(String valorConsulta, String columnaTabla){
		String retorno = "1 = 1";
		try {
			
			if(StringUtils.isBlank(valorConsulta)){
				return retorno;
			}
			retorno = "UPPER("+columnaTabla+") LIKE UPPER('%"+valorConsulta+"%')";
		} catch (Exception e) {
		}
		return retorno;		
	}
	
}
