package pe.com.hypersoft.login.web;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.enumeration.AtributosSesionEnum;
import pe.com.hypersoft.login.model.seguridad.login.DatosEntradaLoginModel;
import pe.com.hypersoft.login.model.global.UsuarioModel;
import pe.com.hypersoft.login.model.seguridad.login.DatosSalidaLoginModel;
import pe.com.hypersoft.login.model.seguridad.usuarios.DatosEntradaUsuarioModel;
import pe.com.hypersoft.login.model.seguridad.usuarios.DatosSalidaUsuarioModel;
import pe.com.hypersoft.login.service.SeguridadService;
import pe.com.hypersoft.login.serviceagent.seguridad.login.LoginServiceAgent;
import pe.com.hypersoft.login.serviceagent.seguridad.usuarios.UsuarioServiceAgent;
import pe.com.hypersoft.login.util.Utilitario;


@Controller
@Scope("request")
@RequestMapping(value = "/siade")
public class LoginController {

	@Autowired
	SeguridadService seguridadService;
	
	@Autowired
	LoginServiceAgent loginServiceAgent;
	
	@Autowired
	UsuarioServiceAgent usuarioServiceAgent;
		
	@Value("${descripcion.error.login.E001}")
	private String e001;
	
	@Value("${mensaje.error.general}")
	private String msjErrorGeneral;
	
	/**
	 * Ingreso a Login V2 usando Boostrap 4
	 * @param request
	 * @param response
	 * @param sesion
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@RequestMapping(value="/v2",method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView ingresoLoginSiadeV2(HttpServletRequest request, HttpServletResponse response,HttpSession sesion)
            throws ServletException, IOException {
		Map<String, Object> map= new HashMap<String, Object>();//seguroService.consultarParametrosSeguroVehicular();
		limpiarDatos(sesion);
		
		Calendar calendar=Calendar.getInstance();
		map.put("anho", calendar.get(Calendar.YEAR));
		
		System.out.println(e001);
		System.out.println(msjErrorGeneral);
        return new ModelAndView("loginSiadeV2", map);
    }	
	
	@RequestMapping(value="/asistencia",method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView ingresoLoginAsistencia(HttpServletRequest request, HttpServletResponse response,HttpSession sesion)
            throws ServletException, IOException {
		Map<String, Object> map= new HashMap<String, Object>();//seguroService.consultarParametrosSeguroVehicular();
		limpiarDatos(sesion);
		
		Calendar calendar=Calendar.getInstance();
		map.put("anho", calendar.get(Calendar.YEAR));
		
		System.out.println(e001);
		System.out.println(msjErrorGeneral);
        return new ModelAndView("loginAsistencia", map);
    }	
	
		
	/**
	 * Validar Login para Menu v2 usando Bootstrap 4
	 * @param registro
	 * @param model
	 * @param request
	 * @param response
	 * @param sesion
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/validarLoginV2")
    public String validarLoginV2(DatosEntradaLoginModel usuarioInput,Model model,HttpServletRequest request, HttpServletResponse response,HttpSession sesion)
            throws Exception {
		Map<String, Object> map= new HashMap<String, Object>();
		map.put("anho", "2018");
		request.getSession().setAttribute("parametros", map);
		String token = "";
		
		DatosSalidaLoginModel datosSalidaLoginModel = new DatosSalidaLoginModel();
		DatosSalidaLoginModel datosSalidaMenuModel = new DatosSalidaLoginModel();
		List<PerfilOpcion> menuOpciones = null;
		try {
			datosSalidaLoginModel = loginServiceAgent.validarAcceso(usuarioInput);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sesion.setAttribute(AtributosSesionEnum.TOKEN.getCodigo(), datosSalidaLoginModel.getToken()!=null?datosSalidaLoginModel.getToken():"");
		
		if(datosSalidaLoginModel.getListaUsuarios() != null){
			if(datosSalidaLoginModel.getListaUsuarios().size()>0){
				//OK: Extraer Menu de Opciones x Perfil
				usuarioInput.getAutenticacion().setPerfil(datosSalidaLoginModel.getListaUsuarios().get(0).getPerfil());
				token = (String) sesion.getAttribute(AtributosSesionEnum.TOKEN.getCodigo());
				datosSalidaMenuModel = loginServiceAgent.lstMenuOpciones(usuarioInput,token);
				//List<PerfilOpcion> menuOpciones = seguridadService.lstMenuOpciones(datosSalidaLoginModel.getListaUsuarios().get(0).getPerfil());
				menuOpciones = Utilitario.reordenarPadreHijoMenuOpciones(datosSalidaMenuModel.getListaMenuOpciones());
				model.addAttribute("paginaprincipal","S");
				model.addAttribute("usuarioModel",	datosSalidaLoginModel.getListaUsuarios().get(0));
				model.addAttribute("listaMenuOpcionesModel",	menuOpciones);
				sesion.setAttribute("usuarioModel", datosSalidaLoginModel.getListaUsuarios().get(0));
				sesion.setAttribute("listaMenuOpcionesModel", menuOpciones);
				return "menuPrincipalV2";
			}else{
				//Error
				return "error/errorLoginMensaje";
			}
		}else{
			//Error
			return "error/errorLoginMensaje";
		}
		/*List<Usuario> usuario = seguridadService.login(usuarioInput);
		if(usuario.size()>0){			
			//OK: Extraer Menu de Opciones x Perfil
			List<PerfilOpcion> menuOpciones = seguridadService.lstMenuOpciones(usuario.get(0).getPerfil());
			menuOpciones = Utilitario.reordenarPadreHijoMenuOpciones(menuOpciones);
			model.addAttribute("paginaprincipal","S");
			model.addAttribute("usuarioModel",	usuario.get(0));
			model.addAttribute("listaMenuOpcionesModel",	menuOpciones);
			sesion.setAttribute("usuarioModel", usuario.get(0));
			sesion.setAttribute("listaMenuOpcionesModel", menuOpciones);
			return "menuPrincipalV2";
		}else{
			//Error
			return "error/errorLoginMensaje";
		}	*/	
        
    }
	
	@RequestMapping(value="/crear-usuario")
    public ModelAndView crearUsuario(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		Map<String, Object> map= new HashMap<String, Object>();
		request.getSession().setAttribute("parametros", map);
		
		Calendar calendar=Calendar.getInstance();
		map.put("anho", calendar.get(Calendar.YEAR));

        return new ModelAndView("seguridad/crearUsuario", map);
    }	
	
	@RequestMapping(value="/registrar-usuario")
    public ModelAndView registrarUsuario(UsuarioModel usuarioInput,HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		Map<String, Object> map= new HashMap<String, Object>();
		request.getSession().setAttribute("parametros", map);
		
		Calendar calendar=Calendar.getInstance();
		map.put("anho", calendar.get(Calendar.YEAR));
		DatosEntradaUsuarioModel datosUsuario =  new DatosEntradaUsuarioModel();
		
		DatosSalidaUsuarioModel datosSalidaUsuarioModel = usuarioServiceAgent.agregarUsuario(datosUsuario);
        return new ModelAndView("seguridad/sucessUsuario", map);
    }
		
	
	@RequestMapping(value="/logoutV2",method = {RequestMethod.GET})
    public ModelAndView logoutV2(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		Map<String, Object> map= new HashMap<String, Object>();
		request.getSession().setAttribute("parametros", map);
		
		Calendar calendar=Calendar.getInstance();
		
		limpiarDatos(request.getSession());
		
        return new ModelAndView("loginSiadeV2", map);
    }
	
	private void limpiarDatos(HttpSession sesion){		
		sesion.setAttribute("usuarioModel", null);
		sesion.setAttribute("listaMenuOpcionesModel", null);		
	}
}
