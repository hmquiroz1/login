package pe.com.hypersoft.login.dto.seguridad.login;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("registro")
	private String registro;
		
	@JsonProperty("clave")
	private String clave;	
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("perfil")
	private Integer perfil;
		
	public Integer getPerfil() {
		return perfil;
	}
	public void setPerfil(Integer perfil) {
		this.perfil = perfil;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}		
}
