package pe.com.hypersoft.login.model.seguridad.login;

import java.io.Serializable;

import pe.com.hypersoft.login.model.global.UsuarioModel;

public class DatosEntradaLoginModel implements Serializable{

	/**
	 * Clase para Administrar los datos de Acceso Login al Sistema
	 */
	private static final long serialVersionUID = 1L;
	
	private UsuarioModel autenticacion;

	public UsuarioModel getAutenticacion() {
		return autenticacion;
	}

	public void setAutenticacion(UsuarioModel autenticacion) {
		this.autenticacion = autenticacion;
	}
		
		
		
}
