package pe.com.hypersoft.login.restclient.seguridad.login;

import java.util.Map;

import pe.com.hypersoft.login.dto.seguridad.login.DatosSalidaLoginDTO;

public interface LoginRestClient {
	public DatosSalidaLoginDTO validarAcceso(Map<Object, Object> datos);
	public DatosSalidaLoginDTO lstMenuOpciones(Map<Object, Object> datos,String token);
}
