package pe.com.hypersoft.login.dto.seguridad.usuarios;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UsuarioDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("registro")
	private String registro;
		
	@JsonProperty("clave")
	private String clave;
	
	@JsonProperty("nombre")
	private String nombre;
	
	@JsonProperty("paterno")
	private String paterno;
	
	@JsonProperty("materno")
	private String materno;
	
	@JsonProperty("tipoDoc")
	private String tipodoc;
	
	@JsonProperty("nroDoc")
	private String nrodoc;
	
	@JsonProperty("email")
	private String email;
	
	@JsonProperty("fecNacimiento")
	private String fecnacimiento;
	
	@JsonProperty("estadoCivil")
	private String estadocivil;
	
	@JsonProperty("telefono")
	private String telefono;
	
	@JsonProperty("estado")
	private String estado;
	
	@JsonProperty("perfil")
	private String perfil;
	
	@JsonProperty("idcc")
	private String idcc;
	
	@JsonProperty("iduo")
	private String iduo;
	
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPaterno() {
		return paterno;
	}
	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}
	public String getMaterno() {
		return materno;
	}
	public void setMaterno(String materno) {
		this.materno = materno;
	}
	public String getTipodoc() {
		return tipodoc;
	}
	public void setTipodoc(String tipodoc) {
		this.tipodoc = tipodoc;
	}
	public String getNrodoc() {
		return nrodoc;
	}
	public void setNrodoc(String nrodoc) {
		this.nrodoc = nrodoc;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFecnacimiento() {
		return fecnacimiento;
	}
	public void setFecnacimiento(String fecnacimiento) {
		this.fecnacimiento = fecnacimiento;
	}
	public String getEstadocivil() {
		return estadocivil;
	}
	public void setEstadocivil(String estadocivil) {
		this.estadocivil = estadocivil;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
	public String getIdcc() {
		return idcc;
	}
	public void setIdcc(String idcc) {
		this.idcc = idcc;
	}
	public String getIduo() {
		return iduo;
	}
	public void setIduo(String iduo) {
		this.iduo = iduo;
	}
	
}
