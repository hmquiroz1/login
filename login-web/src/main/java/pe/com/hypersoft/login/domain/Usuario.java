package pe.com.hypersoft.login.domain;

import java.io.Serializable;

public class Usuario implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private String registro;
	
	private String clave;

	private String nombre;

	private String paterno;
	
	private String materno;
	
	private String tipodoc;
	
	private String nrodoc;
	
	private String fecnacimiento;
	
	private String estadoCivil;
	
	private String email;
	
	private String telefono;
	
	private String estado;
	
	private String token;
	
	private String nomEdificio;
	
	private Integer perfil;
	
	private String tipoperfil;
	
	private Integer iduo;
	
	private Integer idcc;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegistro() {
		return registro;
	}

	public void setRegistro(String registro) {
		this.registro = registro;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getFecnacimiento() {
		return fecnacimiento;
	}

	public void setFecnacimiento(String fecnacimiento) {
		this.fecnacimiento = fecnacimiento;
	}

	public String getNrodoc() {
		return nrodoc;
	}

	public void setNrodoc(String nrodoc) {
		this.nrodoc = nrodoc;
	}

	public String getTipodoc() {
		return tipodoc;
	}

	public void setTipodoc(String tipodoc) {
		this.tipodoc = tipodoc;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNomEdificio() {
		return nomEdificio;
	}

	public void setNomEdificio(String nomEdificio) {
		this.nomEdificio = nomEdificio;
	}

	public Integer getPerfil() {
		return perfil;
	}

	public void setPerfil(Integer perfil) {
		this.perfil = perfil;
	}

	public String getTipoperfil() {
		return tipoperfil;
	}

	public void setTipoperfil(String tipoperfil) {
		this.tipoperfil = tipoperfil;
	}

	public Integer getIduo() {
		return iduo;
	}

	public void setIduo(Integer iduo) {
		this.iduo = iduo;
	}

	public Integer getIdcc() {
		return idcc;
	}

	public void setIdcc(Integer idcc) {
		this.idcc = idcc;
	}

	public static abstract class PERFIL
    {
		public final static String ADMIN = "ADMIN";
		public final static String CONSULTA = "CONSULTA";
		public final static String OPERACION_TC = "OPERACION_TC";
		public final static String CARGA_COTIZADOR = "CARGA_COTIZADOR";
    }
	
}
