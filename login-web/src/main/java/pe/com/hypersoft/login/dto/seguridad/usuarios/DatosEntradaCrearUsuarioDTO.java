package pe.com.hypersoft.login.dto.seguridad.usuarios;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DatosEntradaCrearUsuarioDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@JsonProperty("security")
	private SeguridadRestDTO security; 
	
	@JsonProperty("objeto")	
	private UsuarioDTO data;
	
	public SeguridadRestDTO getSecurity() {
		return security;
	}

	public void setSecurity(SeguridadRestDTO security) {
		this.security = security;
	}

	public UsuarioDTO getData() {
		return data;
	}

	public void setData(UsuarioDTO data) {
		this.data = data;
	}
}
