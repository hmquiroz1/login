package pe.com.hypersoft.login.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.Usuario;
import pe.com.hypersoft.login.model.seguridad.login.DatosEntradaLoginModel;
import pe.com.hypersoft.login.repository.UsuarioDao;
import pe.com.hypersoft.login.service.SeguridadService;

@Service
public class SeguridadServiceImpl implements SeguridadService {
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	public List<Usuario> login(DatosEntradaLoginModel usuarioInput) {		
		return usuarioDao.obtenerUsuario(usuarioInput);
	}	
	
	public List<PerfilOpcion> lstMenuOpciones(Integer idPerfil) {		
		return usuarioDao.lstMenuOpciones(idPerfil);
	}
}	
