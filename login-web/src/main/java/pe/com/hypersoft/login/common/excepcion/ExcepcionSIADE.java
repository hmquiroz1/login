package pe.com.hypersoft.login.common.excepcion;

import java.io.Serializable;

import org.springframework.validation.Errors;

// TODO: Auto-generated Javadoc
/**
 * The Class ExcepcionBBVA.
 */
public class ExcepcionSIADE extends RuntimeException implements Serializable {

	private static final long serialVersionUID = 1L;
	private Errors errores;
	private String codigoError;
	private String mensajeError;
	private Boolean esProperty;

	public Boolean getEsProperty() {
		return esProperty;
	}

	public void setEsProperty(Boolean esProperty) {
		this.esProperty = esProperty;
	}

	public ExcepcionSIADE() {
	}

	public ExcepcionSIADE(String codigoError) {
		super(codigoError);
	}

	public ExcepcionSIADE(Errors errores) {
		this.errores = errores;
	}

	/**
	 * 
	 * @param errores
	 * @param codigoError
	 * @param mensajeError
	 */
	public ExcepcionSIADE(Errors errores, String codigoError, String mensajeError) {
		super(mensajeError);
		this.errores = errores;
		this.codigoError = codigoError;
		this.mensajeError = mensajeError;
	}

	/**
	 * 
	 * @param errores
	 * @param codigoError
	 * @param mensajeError
	 * @param esProperty Indica si el mensaje enviado debe traducirse como un recurso (property)
	 */
	public ExcepcionSIADE(Errors errores, String codigoError, String mensajeError, Boolean esProperty) {
		super(mensajeError);
		this.errores = errores;
		this.codigoError = codigoError;
		this.mensajeError = mensajeError;
		this.esProperty = esProperty;
	}
	
	public Errors getErrores() {
		return errores;
	}

	public void setErrores(Errors errores) {
		this.errores = errores;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	@Override
	public String toString() {
		return "ExcepcionBBVA [errores=" + errores + ", mensajeError=" + mensajeError + "]";
	}
}
