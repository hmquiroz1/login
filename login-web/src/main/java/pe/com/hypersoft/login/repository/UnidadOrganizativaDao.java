package pe.com.hypersoft.login.repository;

import java.util.List;

import pe.com.hypersoft.login.domain.PerfilOpcion;
import pe.com.hypersoft.login.domain.UO;
import pe.com.hypersoft.login.domain.Usuario;

public interface UnidadOrganizativaDao {
	
	List<UO> lstUnidadOrganizativa();
}
