$(document).ready(function() {
		
	$('[data-toggle="datepicker"]').datepicker({
        autoHide: true,
        format: "dd/mm/yyyy",
        zIndex: 2048,
      });
	
//	//DatePicker 
//	$( "#fechaDesde" ).datepicker({
//		format: "dd/mm/yyyy"	   
//	    });
//	
//	$( "#fechaHasta" ).datepicker({
//		format: "dd/mm/yyyy"
//	    });
	
	//DataTable
	//aLengthMenu, configurar maximos de longitud de busqueda del menu
	//language, usado para especificar textos de grilla y formato numerico
    $('#tblUO').DataTable(
		{
			dom: 'Bfrtip',
	        buttons: [	 
                {
                  text: 'JSON',
                  action: function ( e, dt, button, config ) {
                      var data = dt.buttons.exportData();
   
                      $.fn.dataTable.fileSave(
                          new Blob( [ JSON.stringify( data ) ] ),
                          'ExportUO.json'
                      );
                  }
                },
	            {
	                extend: 'copy',
	                text: 'Copy',
	                title: 'Lista Unidades Organizativas'
	            },
	            {
	                extend: 'csv',
	                text: 'CSV',
	                title: 'Lista Unidades Organizativas'
	            },
	            {
	                extend: 'excel',
	                text: 'Excel',
	                title: 'Lista Unidades Organizativas',
	                customize: function(xlsx) { //Personalizar Export en Excel 
	                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
	     
	                    // Loop over the cells in column `D`(Estado)
	                    $('row c[r^="D"]', sheet).each( function () {
	                        // Get the value
	                        if ( $('is t', this).text() == 'ACTIVO' ) {
	                            $(this).attr( 's', '15' );//Normal text, green background
	                        }
	                        if ( $('is t', this).text() == 'INACTIVO' ) {
	                            $(this).attr( 's', '10' );//Normal text, red background
	                        }
	                    });
	                }
	            },
	            {
	                extend: 'pdf',
	                text: 'PDF',
	                title: 'Lista Unidades Organizativas',
	                messageTop: 'PDF creado por SIADE.',
	                orientation: 'portrait',
	                pageSize: 'A4',
	                download: 'open'
	            },
	            {
	                extend: 'print',
	                text: 'Print all',
	                exportOptions: {
	                    modifier: {
	                        selected: null
	                    }
	                }
	            },
	            {
	                extend: 'print',
	                text: 'Print selected'
	            }
	        ],
	        select: true,
			"aLengthMenu" : [ [ 5, 10, 25, -1 ],
					[ 5, 10, 25, "All" ] ],
			"iDisplayLength" : 5,
			"language": {
				"search": "Busqueda",				
				"lengthMenu": "Mostrar _MENU_ registros",
	            "zeroRecords": "Nada encontrado - sorry",
	            "info": "Mostrando página _PAGE_ de _PAGES_",
	            "infoEmpty": "No se encontraron registros",
	            "infoFiltered": "(filtrado de _MAX_ registros totales)",
	            "decimal": ",",
	            "thousands": ".",
	            buttons: {
	                copyTitle: 'Copiado al portapapeles',
	                copyKeys: 'Presione <i> ctrl </i> + <i> C </i> para copiar los datos de la tabla a su portapapeles. <br> Para cancelar, haga clic en este mensaje o presione Esc.',
	                copySuccess: {
	                    _: '%d registros copiados',
	                    1: '1 registro copiado'
	                }
	            }
	        }
		});
    //Para marcado de todos los checks
    $("#tblUO #checkall").click(function () {
        if ($("#tblUO #checkall").is(':checked')) {
            $("#tblUO input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("#tblUO input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    
    jQuery('body').on('click', '[data-target="#editarModal"]', function() {   
    	var uoID = jQuery(this).data("uoid");
    	console.info(uoID);
        if(uoID != '' ) {        
        	$("#editarModal #txtNombreEdit").val("Edificio Jose Angel")
        	$("#editarModal #txtDescripcionEdit").val("Edificio en San Borja de 11 Pisos");
        	$("#editarModal #fechaDesdeEdit").val("23/03/2020");
        	$("#editarModal #fechaHastaEdit").val("24/03/2020");
        	$("#editarModal #chkEstadoEdit").prop("checked",true);
        	$("#editarModal #lblEstadoEdit").text("ACTIVO");
        }
     
    });
} );

$('#test').click(function() {
	console.info('Hizo Click');
	$.toast({
        heading: 'Welcome to my admin',
        text: 'Use the predefined ones, or specify a custom position object.',
        position: 'top-right',
        loaderBg: '#ff6849',
        icon: 'error',
        hideAfter: 3500

    });
});

$('#grabarUO').click(function() {
	console.info('Hizo Click');
	$.ajax({
		url : contextPath + "/mantenimiento/uo/registrar",
		//url : "/login-web" + "/mantenimiento/uo/registrar",		
		data : {"data.descripcion":$("#txtDescripcion").val().trim(),
				"data.nombre":$("#txtNombre").val().trim(),
				"data.estado":$('input:checkbox[id=chkEstadoNew]:checked').val()=='on'?'A':'I',
				"data.fecinivig":$("#fechaDesde").val().trim(),
				"data.fecfinvig":$("#fechaHasta").val().trim()},
		method : "POST",
		success : function(data) {			
			console.info('Exito');			
			notificacionToastr('Guardado Correcto','Se ha grabado correctamente el registro.','top-right','success');
		},
	    error: function (xhr, ajaxOptions, thrownError) {	    	
	    	notificacionToastr('Guardado Incorrecto','No se ha grabado el registro.','top-right','error');
	    	console.info(xhr.status);
	    	console.info(xhr.responseText);
	    	console.info(thrownError);
	    }
	});
});

//Editar UO
function editarUO(idUO){
	console.info('UO:' + idUO);
	$.ajax({
		type : 'post',
		data: { iduo:idUO},
		url : contextPath + "/mantenimiento/uo/editar",
		beforeSend : function() {
		},
		error : function() {
		},
		success : function(data,status,error) {
			 
			if (error.getResponseHeader(codigoErrorHeader) != null) {
				 //mostrarErrorDetalle();
				 //$('body, html').animate({scrollTop: 180}, 'slow');				
			 }else{		
				 console.info('OK');
				 if(null != data && data != ""){
					 console.info('data:'+data.valor);
					 $('#editarModal').modal('toggle');
					 //mostrarDetalleImposicion(data);
			 	}else{
			 		 //mostrarErrorDetalle();
			 	}
			 }
		}		
	});	
}
