	var TAG_COTIZA = "COTIZA";
	var TAG_SOLICITA = "SOLICITA";
	
	var TAG_COTIZA_EVENTO_CLICK = "cotiza-click";
	var DESCRIPCION_NUMERO_PLACA = "NUMERO_PLACA";
	var DESCRIPCION_BOTON_PLACA = "BOTON_PLACA";
	var DESCRIPCION_LINK_CONDICIONES = "LINK_CONDICIONES";
	var DESCRIPCION_COMBO_MARCA = "COMBO_MARCA";
	var DESCRIPCION_ANIO_FABRICACION = "ANIO_FABRICACION";
	var DESCRIPCION_VALOR_COMERCIAL = "VALOR_COMERCIAL";
	var DESCRIPCION_NOMBRE = "NOMBRE";
	var DESCRIPCION_NUMERO_DOCUMENTO = "NUMERO_DOCUMENTO";
	var DESCRIPCION_CORREO = "CORREO";
	var DESCRIPCION_TELEFONO = "TELEFONO";
	var DESCRIPCION_BOTON_COTIZAR = "BOTON_COTIZAR";
	var DESCRIPCION_COMBO_MARCA = "COMBO_MARCA";
	var DESCRIPCION_COMBO_MODELO = "COMBO_MODELO";
	var DESCRIPCION_COMBO_TIPO = "COMBO_TIPO";
	var DESCRIPCION_COMBO_TIPO_GNV = "COMBO_TIPO_GNV";
	var DESCRIPCION_COMBO_LUGAR_REGISTRO = "COMBO_LUGAR_REGISTRO";
	var DESCRIPCION_COMBO_TIPO_DOCUMENTO = "COMBO_TIPO_DOCUMENTO";
	var DESCRIPCION_COMBO_HORARIO_SEGURO = "COMBO_HORARIO_SEGURO";
	var DESCRIPCION_CHECK_AUTORIZACION = "CHECK_AUTORIZACION";
	var DESCRIPCION_LINK_DATOS_PERSONALES = "LINK_DATOS_PERSONALES";
	var DESCRIPCION_LINK_BENEFICIO = "LINK_BENEFICIO";
	
	function llenarDataLayerCotizadorSeguroVehicular(page,elemento) {
		if(TAG_COTIZA==page) {
			reiniciarEstructura();
			setDataLayerCotiza();
		}else if(TAG_SOLICITA==page){
			reiniciarEstructura();
			setDataLayerSolicita();
		}else if(TAG_COTIZA_EVENTO_CLICK==page) {
			setDataLayerOnClick(elemento);
		}
	}

	function setDataLayerOnClick(elemento) {
		setApplicationStep("app started:" + elemento);
		tms_funnel("App Started", digitalData);
	}
	
	function setDataLayerCotiza() {
		digitalData.versionDL = "1.10";
	    setPageInstanceID(obtenerEntorno());
	    setPageIntent("transaccion");
	    setPageSegment("personas");
	    setSysEnv("escritorio");
	    setVersion("1.0");
	    setChannel("online");
	    setLanguage("ES");
	    setGeoRegion("");
	    setLevel(1, "seguros");
	    setLevel(2, "cotizador seguro vehicular");
	    setLevel(3, "1 cotiza");
		setArea("publica");	
		setServer();
		setBussinessUnit("BBVA Continental");
		setPageName();
		setUserAgent();
		setMobile();
		setUserState("no logado");
		setApplicationType("simulador");
		setApplicationName("cotizador seguro vehicular");
		setFulfillmentModel("offline");
		setApplicationStep("1 cotiza");
		setApplicationState("inicio");
		setPrimaryCategory("seguros");
		setProductName("seguro vehicular bbva");
		tms_funnel("App Page Visit", digitalData, true);
	}
	
	function setDataLayerSolicita() {
		digitalData.versionDL = "1.10";
	    setPageInstanceID(obtenerEntorno());
	    setPageIntent("transaccion");
	    setPageSegment("personas");
	    setSysEnv("escritorio");
	    setVersion("1.0");
	    setChannel("online");
	    setLanguage("ES");
	    setGeoRegion("");
	    setLevel(1, "seguros");
	    setLevel(2, "cotizador seguro vehicular");
	    setLevel(3, "pagina exitosa");
		setArea("publica");	
		setServer();
		setBussinessUnit("BBVA Continental");
		setPageName();
		setUserAgent();
		setMobile();
		setUserState("no logado");
		setApplicationType("simulador");
		setApplicationName("cotizador seguro vehicular");
		setFulfillmentModel("offline");
		setApplicationStep("pagina exitosa");
		setApplicationState("finalizado");
		setPrimaryCategory("seguros");
		setProductName("seguro vehicular bbva");
		tms_funnel("App Completed", digitalData, true);
	}

	function setTipoDivisa(a){
		if(a=='SOLES'){
			return "PEN";
		}else {
			return "USD";
		}
	}
	
	function setMonto(tipo,monto,montoFuturo){
		if(tipo == '1'){
			return monto;
		}else{
			return montoFuturo;
		}
	}

	function setPeriodicidad(tipo,montoCuota){
		if(tipo == '1'){
			return "";
		}else{
			return montoCuota;
		}
	}
	
	function setCuota(tipo,periodicidad){
		if(tipo == '1'){
			return "";
		}else{
			return periodicidad;
		}
	}
	
	function setFondos(descFondo1, descFondo2){
		var fsol = "";
		var fdol = "";
		if(descFondo1 !=null && descFondo1 == "BBVA CASH SOLES"){	
			fsol = "bbva soles fmv";
		}
		
		if(descFondo1 !=null && descFondo1 == "BBVA CASH DOLARES"){	
			fdol = "bbva dolares fmv";
		}
		
		if(descFondo2 !=null && descFondo2 == "BBVA CASH SOLES"){	
			fsol = "bbva soles fmv";
		}
		
		if(descFondo2 !=null && descFondo2 == "BBVA CASH DOLARES"){	
			fdol = "bbva dolares fmv";
		}
		
		if(descFondo2 !=null && descFondo2 !=""){
			return fsol + " y " +  fdol;
		}else{
			return fsol + fdol;
		}
		
	}
	
	function indCliente(indClienteBBVACash,indAfiliacionBXI){
		if(indClienteBBVACash == null || indClienteBBVACash == "" ){
			return "no cliente ffmm";
		} else if(indAfiliacionBXI == null || indAfiliacionBXI == "") {
			return "cliente ffmm no digitalizado";
		}else {
			return "cliente ffmm digitalizado";
		}
	}
	
	function isObjetivo(objetivo){
		if(objetivo == "Mi departamento" ){
			return "depa";
		} else if(objetivo == "Mi carro" ) {
			return "carro";
		}else if(objetivo == "Mis estudios" ) {
			return "estudios";
		}else if(objetivo == "Mis vacaciones" ) {
			return "vacaciones";
		}else if(objetivo == "Mi matrimonio" ) {
			return "matrimonio";
		}else {
			return "otros";
		}
	}
	
	
	function reiniciarEstructura(){
		digitalData = JSON.parse(JSON.stringify(digitalData_blank));
	}

	function obtenerAniosDias(anios) {
		var resultado = "";
		return resultado + (parseInt(anios, 10)*365);
	}

	function obtenerCodigoMoneda() {
		var codigoMoneda = $("#moneda").prop("checked")?$("#hdnCodigoSoles").val():$("#hdnCodigoDolares").val();
		return codigoMoneda;
	}
	
	function obtenerTipoPrestamoDescPorCodigo(codigo) {
		var descripcion = "";
		if($("#hdnTipoPrestamoComprar").val()===codigo) {
			descripcion = CTE_DESC_TIPO_PRESTAMO_COMPRAR;
		} else if($("#hdnTipoPrestamoConstruir").val()===codigo) {
			descripcion = CTE_DESC_TIPO_PRESTAMO_CONSTRUIR;
		} else if($("#hdnTipoPrestamoRemodelar").val()===codigo) {
			descripcion = CTE_DESC_TIPO_PRESTAMO_REMODELAR;
		}
		return descripcion;
	}
	

	function obtenerEntorno() {
		return ("bbvaperuprod" == amb_tag) ? "pro" : "ei";
	}
	
	function obtenerMontoFinanciar() {
		var precioMilesNum = $('#txtPrecioMiles').maskMoney('unmasked')[0];
		var cuotaMilesNum = $('#txtCuotaMiles').maskMoney('unmasked')[0];
		var resultado = precioMilesNum - cuotaMilesNum;
		
		return ""+resultado;
	}
		
	