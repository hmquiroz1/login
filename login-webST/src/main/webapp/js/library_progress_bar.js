$(document).ready(function(){
	setTimeout(function() {
		$('html, body',window.parent.document).animate({
			scrollTop: 0
		}, 'fast');				 
	}, 100);
});

(function($) {
	$.widget("marmots.wizard", {
		
		// plugin vars
		current: 0,
		currentMultiStep: 0,
		animating: false,
		animatingHeight: false,
		scrolling: false,
		steps: [],
		stepContainer: undefined,
		visualization: undefined,
		buttonBar: undefined,
		form: undefined,
		checkAlert: undefined,	

		// default options
		options : {
			labels: {
				prev: 'Previous',
				next: 'Next',
				confirm: 'Confirm',
				done: 'Done',
				cancel: 'Cancelar',
				aceptar: 'Aceptar'
			}
		},

		// the constructor
		_create : function() {
			var self = this;
			
			self.steps = self.element.children();
			self.form = self.element.closest('form').length == 0?undefined:self.element.closest('form');
			
			$(window).on('resize', function(){
				self._resized();
			});
			
			$(document).on('capgemini.wizard.resize', function(){
				// SOLO aplica a desktop. Setea overflow hidden para evitar que se vean partes crecientes.
				// Se cambiará a visible al finalizar la animación del _animateHeight
				var $stepContainer = self.element.find('.step-container');
				if($stepContainer.length > 0) {
					$stepContainer.css('overflow', 'hidden');
				}
        		
				setTimeout(function() {
					self._animateHeight(false);
				}, 100);
			});
			
			$(document).on('capgemini.wizard.resizeNoScroll', function(){
				// SOLO aplica a desktop. Setea overflow hidden para evitar que se vean partes crecientes.
				// Se cambiará a visible al finalizar la animación del _animateHeight
				var $stepContainer = self.element.find('.step-container');
				if($stepContainer.length > 0) {
					$stepContainer.css('overflow', 'hidden');
				}
        		
				setTimeout(function() {
					self._animateHeight(false);
				}, 100);
			});
			
			// inicializa botones next internos
			self.element.find('.next').each(function() {
				$(this).on('vclick', function(){
					self.next();
					return false;
				});
			});
			
			self._refresh();
			
			processNoPrev(self);
			processNoNext(self);
			processCancel(self);
			//processClaveDigitalWarning(self);
		},
				
		// return current step
		_current: function(){
			return $(this.steps[this.current]);
		},

		_nextStep: function(){
			return $(this.steps[this.current+1]);
		},

		_prevStep: function(){
			return $(this.steps[this.current-1]);
		},
		
		// Animate container height to step height
    	_animateHeight: function(doScroll){

    		var self = this;
    		if(self.animatingHeight == false) {
	    		self.animatingHeight = true;
	    		
	    		var height = self._current().outerHeight(true) + 5; // TODO (this pixel)
	    		$(this).find('.step').css('height', height);
	    		
	    		if(isIE8()) {
	    			self.element.find('.step-container').css('height', height);
	    			self.animatingHeight = false;
	    		}
	    		else {
	    			self.element.find('.step-container').animate({
		    			height: height
		    		}, function(){
		    			if(doScroll == true) {		    				
		    				self._scrollToXX();
		    			}
		    			var $stepContainer = self.element.find('.step-container');
						if($stepContainer.length > 0) {
							$stepContainer.css('overflow', 'visible');
						}					
						self.animatingHeight = false;
		    		});
	    		}
    		}
		},
		
		// scrollTo
		_scrollToXX: function(){
			var self = this;
			
			var $target = self._current().prev('a.step:visible').length == 0 ? self.element : self._current().prev('a.step');
			var scrollPosition = $target.offset().top;
			
			if((scrollPosition > 0) && !isIE8()) {	
				// en Desktop
				/*if(!Modernizr.mq('(max-width:768px)')){
					scrollPosition = scrollPosition - 65;
					
					// no mueve scroll si esta por encima de 460
					var $position = $(document).scrollTop();
					if($position < 460) {
						return false;
					}
				}*/
				if(self.scrolling == false) {
					setTimeout(function() {
						self.scrolling = true;
						$('html, body').animate({
					        scrollTop: scrollPosition
					    }, 
					    { duration: 'slow',
		    			  complete: function(){
		    				self.scrolling = false;
				    	}});				 
					}, 50);	
				}		
			}
		},
		
		// scrollTo
		_scrollTo: function(){
			/*var self = this;
			
				console.log('_scrollTo ejecutado');
			if(Modernizr.mq('(max-width:768px)')){
				var $target = self._current().prev('a.step:visible').length == 0 ? self.element : self._current().prev('a.step');
				var scrollPosition = $target.offset().top;
				
				setTimeout(function() {
					$('html, body').animate({
				        scrollTop: scrollPosition
				    }, 'slow');				 
				}, 100);	
			}*/
		},
		
		_constructButtons: function() {
			var self = this;
			
			if(self.buttonBar === undefined){
				// -----------------
				// Create button bar
				self.buttonBar = $('<div class="button-bar clearfix"></div>');
				
				if($(this.element).data('otherbuttons') == null) {
					// Previous
					var $prev = $('<button type="button" class="btn arrow-left prev pull-left wizardBack hidden" id="wizardBack">' + self.options.labels.prev + '</button>');
					$prev.on('vclick', function(){
						self.prev();
						return false;						
					});
					self.buttonBar.append($prev);
					
					// Next
					var $next = $('<button class="btn arrow-right next pull-right wizardNext" name="wizardNext" id="wizardNext">' + (self.steps.length > 2 ? self.options.labels.next : self.options.labels.confirm) + '</button>');
					$next.on('vclick', function(){
						self.next();
						return false;
					});
					self.buttonBar.append($next);
				}
				else {
					// Previous
					var $prev = $('<button type="button" class="btn prev pull-left wizardBack" data-dismiss="modal" id="wizardBack">' + self.options.labels.cancel + '</button>');
					self.buttonBar.append($prev);
					
					// Cancel
					var $cancel = $('<button type="button" class="btn cancel hidden" data-dismiss="modal" id="wizardCancel">' + self.options.labels.cancel + '</button>');
					self.buttonBar.append($cancel);
					
					// Next
					var $next = $('<button class="btn next pull-right wizardNext" name="wizardNext" id="wizardNext">' + self.options.labels.aceptar + '</button>');
					$next.on('vclick', function(){
						self.next();
						return false;
					});
					self.buttonBar.append($next);
				}
			}
			return self.buttonBar;
		},
		
		_constructTablet: function() {
			var self = this;
			
			if(self.visualization === undefined || self.visualization !== 'tablet'){
				var step = 0;
				self.steps.each(function(){
					var $step = $('<a href="#step-' + (step++) + '" class="step ' + (self.current > step - 1?'completed':'') + '"><span class="step_img_'+ (step) + ' "></span>' + (step) + '. ' + $(this).data('title') + '</a>');
					self.element.append($step);
					self.element.append($(this).hide());
				});
				self.element.find('> a.step.active').removeClass('active');
				/*if(self._isRemoteStepLoaded()){
					self._loadStepContent(function(){
						self._current().show().append(self._constructButtons()).prev().addClass('active');
					});
				} else {
					self._current().show().append(self._constructButtons()).prev().addClass('active');
				}*/
				
				// Clean elements from desktop display
				if(self.stepContainer !== undefined){
					self.element.find('.wizard-progress-bar').remove();
					self.stepContainer.remove();
					self.stepContainer = undefined;
				}
				
				self.visualization = 'tablet';
			}
		},
		
		_constructDesktop: function() {
			var self = this;
			
			if(self.visualization === undefined || self.visualization !== 'desktop'){
				self._loadStepContent();
				
				if(self.element.find('.step-container').length == 0) {
					// Create step container
					self.stepContainer = $('<div class="step-container" style="width:' + self.element.width() + 'px"></div>');			
					self.element.append(self.stepContainer);
					
					// Create slide viewer
					var $slideViewer = $('<div id="wizard-slide-viewer" class="slide-viewer"></div>');
					self.stepContainer.append($slideViewer);
					
					// Get steps
					self.steps.each(function(){
						var $step = $('<div class="step" style="width:' + self.element.width() + 'px; height:0px"></div>');
						$step.append($(this).hide());
						$slideViewer.append($step);
					});
					//self._current().show().parent().addClass('active');
					
					// Width calculation functions
					self._desktopResized();
					
					// Append button bar to wizard element
					//self.element.append(self._constructButtons());
					
					self.element.find('.step-container').css({
		    			height: self._current().outerHeight(true) + 5 // TODO (this pixel)
		    		});
					
					// Clean elements from tablet display
					self.element.find('> a.step').remove();
					
					self.visualization = 'desktop';
				}
			}
		},
		
		_desktopResized: function() {
			var self = this;
			// -------------------
			// Create progress bar
			var $progressBar = self.element.find('.wizard-progress-bar').empty();
			if($progressBar.length == 0){
				$progressBar = $('<div class="wizard-progress-bar"></div>');
				//$errorMessage = $('<p class="block-error iconed-24 hide" id="checkAlert"><span class="icon-24 m01-alerta red"></span>Debes aceptar los términos y condiciones para continuar el proceso<br/></p>')

				// Append progress bar to wizard element
				//self.element.prepend($errorMessage);
				self.element.prepend($progressBar);

				self.checkAlert = self.element.children()[1];
			}
			
			// Calculate width and append progess bar line
			var width = self.element.width() - 55;
			var $progressLine = $('<div class="progress-line" style="width:' + width + 'px"><div class="progress-display"></div></div>');
			
			// Append progess steps
			var step = 0;
			self.steps.each(function(){
				var title = '<span class="step_img_'+ (step+1) +'"></span> ' + $(this).data('title');
				var left = (((($progressLine.width()) / (self.steps.length - 1)) - 1) * step) - 1;
				var $progressPoint = $('<div class="point-background" style="left:' + left + 'px;"><div class="text"> ' + title + '</div><div class="point"></div></div>');
				$progressPoint.removeClass('active').removeClass('now');				
				if(step < self.current){
					$progressPoint.addClass('active');
				}
				else if(step == self.current){
					$progressPoint.addClass('active').addClass('now');					
				}
				step++;				
				
				$progressLine.append($progressPoint);
			});
			
			// Set progress display width
			$progressLine.find('> .progress-display').css({
        		width: ((($progressLine.width()-6) / (self.steps.length - 1)) * self.current)
			});
			
			// append progress bar line
			$progressBar.append($progressLine);
			
			// text positioning (needed after displaying on page)
			var step = 0;
			self.element.find('.wizard-progress-bar .progress-line .point-background .text').each(function(){
				var left = step == 0 ? 0 : step == self.steps.length - 1 ? - ($(this).width() - 18) : - (($(this).width()  - 18) / 2);
				step++;
				$(this).css('left', left + 'px');
			});
			
			// position current step layer
			self.element.find('.slide-viewer').css({
    			left: -(self.element.find('.step-container').outerWidth() * (self.current))
    		});
			
		},
		
		// called when created, and later when changing options
		_refresh : function() {
			var self = this;
			
			/*if($(this.element).data('alwaysdesktop') == true) {*/
				self._constructDesktop();
				self._desktopResized();
			/*}
			else {
				if(Modernizr.mq('(max-width:768px)')){
					self._constructTablet();
				} else {
					self._constructDesktop();
					self._desktopResized();
				}
			}*/
			self._current().show();
		},
		
		// _resized is called when document is resized
        _resized: function() {
        	var self = this;
        	
            self.element.find('.step-container').css('width', self.element.width());
            self.element.find('.step').css('width', self.element.width());
            self._refresh();
        },
        
        _isRemoteStepLoaded: function(){
        	var self = this;
        	var $step = self._current();
        	var href = $step.data('href');
        	
        	return href !== undefined && $step.is(':empty');
        },
        
        _loadStepContent: function(handler){
        	var self = this;
        	var $step = self._current();
        	
        	var href = $step.data('href');
        	if(href !== undefined){
        		$.ajax({
        			url: href,
        			method: 'post',
        			data: self.form === undefined ? undefined : self.form.serialize(),
        			beforeSend: function(data){
						$(document).trigger('capgemini.ajax.start', $step);
					},
        			success: function(data){
        				var $noRemove = $step.find('.button-bar');
        				$step.html($noRemove).prepend(data);
        				
        				// inicializa botones next internos
        				$step.find('.next').each(function() {
        					$(this).on('vclick', function(){
            					self.next();
            					return false;
            				});
        				});
        				
            			if(handler !== undefined){
            				handler();
            			}
            			
            			self.element.find('.step-container').css({
        	    			height: $step.outerHeight(true) + 5 // TODO (this pixel)
        	    		});
            			$(document).trigger('capgemini.ajax.load', $step);
            		}
        		});
        		
        	}
        },
		
		// _setOptions is called with a hash of all options that are changing
        // always refresh when changing options
        _setOptions: function() {
            // _super and _superApply handle keeping the right this-context
            this._superApply( arguments );
            this._refresh();
        },

        // _setOption is called for each individual option that is changing
        _setOption: function( key, value ) {
            // prevent invalid color values
            this._super( key, value );
        },
        
		// Public functions
		public_function : function() {
			return true;
		},
		
		step2: function(){
			var self = this;
			self.current=0;
			self.nextQuick(true);
			
			processNoPrev(self);
			processCancel(self);
			
			setTimeout(function(){
				var $wizard = $('.wizard');
				if($wizard.length > 0) {
					$(document).trigger('capgemini.wizard.resize', $wizard);
				}
			}, 300);
		},
        
		step3: function(){
			var self = this;
			self.current = 0;
			self.nextQuick(false);
			self.nextQuick(true);
			
			setTimeout(function(){
				var $wizard = $('.wizard');
				if($wizard.length > 0) {
					$(document).trigger('capgemini.wizard.resize', $wizard);
				}
			}, 300);
		},
		
		 nextQuick: function(showStep){
	        var self = this;
	        
	        self.current++;
        	self._loadStepContent();
    	
        	if(self.visualization === 'desktop'){
	        	
        		// Mark active
	    		self.element.find('div.step.active').removeClass('active');
	    		self._current().parent().addClass('active');
	    		self._current().show();
	    		self._resized();
				
				//scroll hacia arriba
				self._scrollTo();
	    		
	    		// Animate progress display
	        	var $progressLine = self.element.find('.wizard-progress-bar .progress-line');
	        	$progressLine.find('> .progress-display').animate({
	        		width: ((($progressLine.width()-6) / (self.steps.length - 1)) * self.current)
	        	}, 'slow', function(){
	        		// Actions to run once animation finishes
	        		$progressLine.find('.point-background:nth-of-type(' + (self.current + 2) + ')').addClass('active').addClass('now');
	        	});
	        	
        	} else {
        		self.element.find('> a.step.active').removeClass('active').addClass('completed');
        		var $prevFrame = $(self.steps[self.current - 1]);
        		var $nextFrame = $(self.steps[self.current]);
        		$prevFrame.animate({ height: "hide" }, { duration: 0});
        		//self._current().append(self._constructButtons()).prev().addClass('active');
        		if(showStep) {
        			self._current().show();
        			self._scrollTo();
        		}
        		if(self.current < self.steps.length-1) {
        			self.element.find('.button-bar .prev').show();
        		}	        	
        	}	
		 },
		
        next: function(){
        	var self = this;
        	var $next = self._nextStep();
        	var $step = self._current();
        	var isCheckLabel = false;
        	var errorMessage;
        	
        	var isCheckStep = $step.data('requiredcheck');
        	if (isCheckStep == true){        	
        		
        		/*if (typeof $step.find('#checkAlertPhone')[0] == "undefined"){
        			errorMessage = $('<p class="block-error iconed-24 hide" id="checkAlertPhone"><span class="icon-24 m01-alerta red"></span>Debes aceptar los términos y condiciones para continuar el proceso<br/></p>')
				// Append progress bar to wizard element
					$step.prepend(errorMessage);
        		}else{
        			errorMessage = $step.find('#checkAlertPhone');
        		}*/			

        	//	var prueba = $step.find('#condiciones').attr('class').index(" on");
    			var labelCheckClasses = $step.find('#condiciones')[0].className.toString().split(" ");
    			
    			for (var a = 0; a < labelCheckClasses.length; a++){
    				if (labelCheckClasses[a] == "on") {
    					isCheckLabel = true;
    				}
				}
			}

    		var visualization = self.visualization;

    		var alertBox = $(self.checkAlert);
    		if (!isCheckLabel && isCheckStep==true){
				if (visualization!="desktop"){
					errorMessage.removeClass('hide');
					if (typeof alertBox.attr("class")!="undefined"  && alertBox.attr("class").indexOf('hide')== -1){
						alertBox.addClass('hide');
					}
				} else {
					alertBox.removeClass('hide');
					if (typeof errorMessage.attr("class")!="undefined" && errorMessage.attr("class").indexOf('hide')== -1){
						errorMessage.addClass('hide');
					}
				}
			} else {
				//$self.find('#checkAlert').addClass('hide');	
				if (visualization!="desktop"){
					if (typeof errorMessage!="undefined"){
						errorMessage.removeClass('hide');
					}
				} else {
					alertBox.addClass('hide');	
				}
				if(/*self.form === undefined || self.form.valid()*/true){
		        	if(self.animating){
        				return;
        			}
		        	/* multistep */
		        	var $currentMultiStep = $step.find('.sub-step:visible');
					if($currentMultiStep.hasClass('hasCheck') && !hasCheckedCheck($currentMultiStep)){
						return;
					}
		        	
		        	var multistep = $step.data('multistep');
		        	if (multistep!=true){
		        		multistep=false;
		        	}
		        	
		        	if(multistep == true) {
		        		self.currentMultiStep++;
		        		var numSubSteps = $step.find('.sub-step').length;
		        		
		        		// init multistep - openModal
		        		var counter = 0;
		        		var openModal = false;
	        			$step.find('.sub-step').each(function() {
	        				if(self.currentMultiStep == counter) {
	        					if($(this).hasClass('open-modal')) {
	        						$(this).find('a').click();
	        						openModal = true;
	        					}
	        				}
	        				counter++;	        				
	        			});
	        			if(openModal) {
	        				return;
	        			}	  
	        			// end multistep - openModal
	        			if(self.currentMultiStep < numSubSteps) {
		        			// oculta boton confirmar
		        			$('.wizardNext', this.element).hide();
		        			self.element.find('.button-bar .prev').show();		        			
		        			
		        			var counter = 0;
		        			$step.find('.sub-step').each(function() {
		        				if(self.currentMultiStep == counter) {
		        					$(this).removeClass('hidden');
		        				} else {
		        					$(this).addClass('hidden');				
		        				}
		        				counter++;
		        			});
		        			self._animateHeight();
		        			self._scrollTo();
		        			return;		        			
		        		}else{		        			
	        				var submitevent = $step.data('submitevent');	        				
	        				if(submitevent!= null){
	        					$(document).trigger(submitevent);	        				
	        				}
	        			}
		        	}
		        	self.currentMultiStep = 0;
		        	/* fin multistep */
		        		        	
		        	self.animating = true;
		        	
		        	self.current++;
		        	self._loadStepContent();
	        	
		        	if(self.visualization === 'desktop'){
			        	// move next step
		        		self._current().show();
		        		var $stepContainer = self.element.find('.step-container');
		        		if(isIE8()) {
			        		// IE8 dont animate
		        			var newPositionLeft = -1 * self.current * $stepContainer.outerWidth();
		        			self.element.find('#wizard-slide-viewer').css('left', newPositionLeft);
		        			self._animateHeight(false);
			    			if(self.current < self.steps.length-1) {
	        					self.element.find('.button-bar .prev').show();
	        				}
			    			self.animating = false;
			    			$stepContainer.css('overflow', 'none');
		        		}else{
		        			$stepContainer.css('overflow', 'hidden');
		        			// show boton prev
			    			if(self.current < self.steps.length-1) {
	        					self.element.find('.button-bar .prev').show();
	        				}
			        		self.element.find('#wizard-slide-viewer').animate({
				    				left: '-=' + $stepContainer.outerWidth()
				    			},
				    			{ 	easing: 'swing',
				    				duration: 'slow',
				    				complete: function(){
						    			$stepContainer.css('overflow', 'none');
						        		// Animate container height to step height
						    			self._animateHeight(false);						    			
						    			self.animating = false;
						    		}
				    			}
				    		);
		        		}
		        		
			    		// Mark active
			    		self.element.find('div.step.active').removeClass('active');			    		
			    		self._current().parent().addClass('active');			    		
			    		
			    		// Animate progress display
			        	var $progressLine = self.element.find('.wizard-progress-bar .progress-line');
			        	$progressLine.find('> .progress-display').animate({
			        		width: ((($progressLine.width()-6) / (self.steps.length - 1)) * self.current)
			        	}, 'slow', function(){
			        		// Actions to run once animation finishes			        		
			        		$('.point-background').removeClass('now');
			        		$progressLine.find('.point-background:nth-of-type(' + (self.current + 2) + ')').addClass('active');
			        		$progressLine.find('.point-background:nth-of-type(' + (self.current + 2) + ')').addClass('now');
			        									
			        	});
			        	
		        	} else {
		        		self.element.find('> a.step.active').removeClass('active').addClass('completed');
		        		var $prevFrame = $(self.steps[self.current - 1]);
		        		var $nextFrame = $(self.steps[self.current]);
		        		// show boton prev	   
        				if(self.current < self.steps.length-1) {
        					self.element.find('.button-bar .prev').show();
        				}
		        		$prevFrame.animate({ height: "hide" }, { duration: 'slow', complete: function () {
		        			//self._current().append(self._constructButtons()).prev().addClass('active');
		        			$nextFrame.animate({ height: "show" }, { duration: 'slow', complete: function () {
		        				self._scrollTo();
				    			
			        			self.animating = false;
		        			}});
		        	    }});
		        	}
	        	
		        	// handle button display
		    		if(self.current == self.steps.length - 2){
		    			var $btnNext = self.element.find('.button-bar .next');
		    			// Si es un multistep no cambia el literal del boton next en el penultimo paso.
		    			if (self._current().data('multistep')!=true){
			        		$btnNext.html(self.options.labels.confirm);
			        	}
		    			$btnNext.off('vclick').on('vclick', function(){
		    				self.submit();
		    				return false;
		    			});
		    		} else if(self.current == self.steps.length - 1){
		    			self.element.find('.button-bar .prev').hide();
		    					    			
		    			var opContainer = $(document).find('.wizard-container').attr('class');	    			
		    			var isModal = true;
		    			if (typeof opContainer != "undefined"){
		    				isModal = false;
		    			}

		    			self.element.find('.button-bar .cancel').html(self.options.labels.done);
		    			
		    			if (!isModal){
		    				self.element.find('.button-bar .next').html(self.options.labels.done).removeClass('arrow-right').off('vclick').on('vclick', function(){
		    				//clearErrors();
							$('#operaciones-list-container').slideDown();
							if (self.element.data('page')!=true){
								$('.wizard-container').slideUp(function(){
									if($(this).find('.conformar-cheque').length > 0){
										var url = '?src=posicion-global.php';
										window.location = url;
									}
								});
							}else{
								var url = '?src=posicion-global.php';
								window.location = url;
							}

							return false;
							});
		    			}else{
		    				self.element.find('.button-bar .next').html(self.options.labels.done).removeClass('arrow-right').off('vclick').on('vclick', function(){
		    					// wizard is in a modal
		    					if(self.element.closest('.modal').length > 0) {
		    						self.element.closest('.modal').modal('hide').remove();
		    					}
		    					else if(self.element.closest('.close-redirect').length > 0) {
		    						var redirect = self.element.closest('.close-redirect').data('redirect');
		    						window.location.href = redirect;
		    					}
		    					else {
		    						// close operation substep
		    						if($('#operaciones-list-container').hasClass('in-sub-step')){
		    							var listContainer = $('#operaciones-list-container.in-sub-step');
		    							var $slider = $(listContainer).find('.operaciones-slider');
		    							var elements = $('ul', $slider);
		    							var elementWidth = 100/elements.length;
		    							var activeUl = $slider.find('ul.active');
		    							var targetUl = $slider.find('ul.' + $(activeUl).data('parent'));
		    							$(activeUl).removeClass('active');
		    							$(targetUl).addClass('active');

		    							$slider.css('margin-left', '-' + (elementWidth * $(elements).index($(targetUl)))*2 + '%');
		    							$slider.css('height', 'auto');
		    							
		    							if($(targetUl).hasClass('main')){
		    								// We're back home again!
		    								/*if(Modernizr.mq('(min-width:769px)')){
		    									// Mobile needs te class a little longer.
		    									$slider.closest('#operaciones-list-container').removeClass('in-sub-step');
		    								}*/
		    								$(this).off('.slideBack');
		    							}
		    							$('#quiero-btn').addClass('hidden');
		    						}
		    						
		    						// wizard isn't in a modal
									/*if(Modernizr.mq('(max-width:768px)')){
										// Phone
										$(this).closest('ul').find('> li').removeClass('inactive active');
										$('#tab-operaciones').removeClass('active');
										$('.tabmain-operaciones').removeClass('hidden').addClass('active');
										$('#operaciones-list-container').addClass('active');
										$('#operacion-container').empty();
										$('#option-content').empty();

										var $backButton = $('.return2');
										$backButton.removeClass('return return2').text('Quiero').addClass('want');
										$backButton.click();
										self._scrollTo();
									}
									else {*/
										// Desktop
										$(this).closest('ul').find('> li').removeClass('inactive active');
										$('#operacion-container').removeClass('active');
										$('.tabmain-operaciones').removeClass('hidden').addClass('active');
										$('#operacion-container').empty();
										$('#option-content').empty();
										
										$('#operaciones-list-container').addClass('active');
										$('#tab-operaciones-link').click();
									/*}*/
		    					}
		    					
			    				return false;
			    			});
		    			}
		    		}
	        	}
        	}
    		
    		processNoPrev(self);
    		processNoNext(self);
    		processCancel(self);
    		//processClaveDigitalWarning(self);
    		
    		$(document).trigger('capgemini.scroll.init', $next);
    		// close keyboard tablet
    		//closeKeyBoardTablet(1000);    		
        },
        
        // TODO submit form via ajax
        submit: function(){
        	var self = this;
        	if(self.form !== undefined){ 
        		var $form = self.form;
        		//clearErrors($form);
        		if(/*self.form.valid()*/true){
	        		var action = $form.attr('action');
	        		if(action !== undefined && action !== '#'){
	        			$.ajax({
	            			url: action,
	            			data: $form.serialize(),
	            			method: $form.attr('method') === undefined ? 'post' : $form.attr('method'),
	            			beforeSend: function(data){
								$(document).trigger('capgemini.ajax.start', $form);
							},
	            			success: function(data, status, request){
	            				self.next();
	            			},
	            			error : function(request, status, error){
	            				self._current().prepend(request.responseText);
	            				self._animateHeight(false);
	            			}
	            		});
	        		} else {
	        			self.next();
	        		}
        		}
        	} else {
        		self.next();
        	}
        },
        
        prev: function(){
        	var self = this;
        	
        	//check blocked
        	var $previousStep = $(self.steps[self.current-1]);
        	var blocked = $previousStep.data('blocked');
        	
        	if(blocked){
        		return;
        	}
        	
        	if(self.animating){
        		return;
        	}
        	self.animating = true;
        	
        	if(self.form !== undefined){ 
        		//clearErrors(self.form);
        	}
        	
        	/* reset multistep if was the current step */
    		self.currentMultiStep = 0;
    		var $prevStep = $(self.steps[self.current]);
    		var multistep = $prevStep.data('multistep');
        	if (multistep==true){
        		var counter = 0;
        		$prevStep.find('.sub-step').each(function() {
    				if(counter == 0) {
    					$(this).removeClass('hidden');
    				}
    				else {
    					$(this).addClass('hidden');	        					
    				}
    				// clear inputs
    				$(this).find("input").each(function () {
    					$(this).val('');
    				});
    				counter++;
    			});
        		// Si hay un multistep en el paso1, se resetea el multistep pero no
        		// navega hacia atras
        		if(self.current == 0) {
        			self.element.find('.button-bar .next').show();
        			self.element.find('.button-bar .prev').hide();
        			self._animateHeight(false);
        			self.animating = false;
        			return;
        		}
        	}
        	/* end reset multistep */
        	
        	// decrease current
    		self.current--;
    		
        	if(self.visualization === 'desktop'){
        		var $progressLine = self.element.find('.wizard-progress-bar .progress-line');
	        	$progressLine.find('.point-background:nth-of-type(' + (self.current + 3) + ')').removeClass('active').removeClass('now');
	        	$progressLine.find('.point-background:nth-of-type(' + (self.current + 2) + ')').addClass('now');
	        	
	    		// move previous step
	        	$(self.steps[self.current+1]).hide();
	    		
	    		var $stepContainer = self.element.find('.step-container');
        		if(isIE8()) {
        			// IE8 dont animate
        			var newPositionLeft = -1 * self.current * $stepContainer.outerWidth();
        			self.element.find('#wizard-slide-viewer').css('left', newPositionLeft);
	        		self._animateHeight(false);
	    			if(self.current < self.steps.length-1) {
    					self.element.find('.button-bar .prev').show();
    				}
	    			self.animating = false;		
	    			$stepContainer.css('overflow', 'none');
        		}
        		else {		
        			$stepContainer.css('overflow', 'hidden');
		    		self.element.find('#wizard-slide-viewer').animate({
		    			left: '+=' + $stepContainer.outerWidth()
		    		}, 'slow', function(){
		    			$stepContainer.css('overflow', 'none');
		    		});
        		}
	    		
	    		// Mark active
	    		self.element.find('div.step.active').removeClass('active');
	    		self._current().parent().addClass('active');
	    		
	    		// Animate progress display
	        	$progressLine.find('> .progress-display').animate({
	        		width: ((($progressLine.width()-10) / (self.steps.length - 1)) * self.current)
	        	}, 'slow', function(){
	        		// Actions to run once animation finishes
	        		self.animating = false;
	        		
	        		// Animate container height to step height
		        	self._animateHeight(false);
	        	});
	        	
	        	self._current().show();
	        	
        	} else {
        		self.element.find('> a.step.active').removeClass('active').removeClass('completed');
        		
        		var $currentFrame = $(self.steps[self.current + 1]);
        		var $prevFrame = $(self.steps[self.current]);
        		$currentFrame.animate({ height: "hide" }, { duration: 'slow', complete: function () {
        			//self._current().append(self._constructButtons()).prev().addClass('active').removeClass('completed');
        			$prevFrame.animate({ height: "show" }, { duration: 'slow', complete: function () {
        				self._scrollTo();
	        			self.animating = false;
        			}});
        	    }});
        	}
        	
        	// handle button display
    		self.element.find('.button-bar .next').show();
    		if(self.current == 0){
    			self.element.find('.button-bar .prev').hide();    			
    		}    		
    		if(self.current == self.steps.length - 3){
    			self.element.find('.button-bar .next').html(self.options.labels.next).off('vclick').on('vclick', function(){
					self.next();
					return false;
				});
    		} else if(self.current == self.steps.length - 2){
    			self.element.find('.button-bar .next').html(self.options.labels.confirm).off('vclick').on('vclick', function(){
    				self.submit();
    				return false;
    			});
    		} 
    		
    		processNoPrev(self);
    		processNoNext(self);
    		processCancel(self);
    		//processClaveDigitalWarning(self);
		    
    		$(document).trigger('capgemini.scroll.init', $previousStep);
    		// close keyboard tablet
    		//closeKeyBoardTablet(1000);
    		
        },
        
        scrollTop: function(){
        	this._scrollTo();
        },

        resize: function(){
        	this._resized();
        },
        
        resetMultiStep: function(){
        	var self = this;
        	self.currentMultiStep = 0;
        },
        
        getStep: function(){
        	var self = this;
        	return self.current;
        }
        
	});
})(jQuery);















function isIE8() {
    var BrowserDetect = 
    {
        init: function () 
        {
            this.browser = this.searchString(this.dataBrowser) || "Other";
            this.version = this.searchVersion(navigator.userAgent) ||       this.searchVersion(navigator.appVersion) || "Unknown";
        },

        searchString: function (data) 
        {
            for (var i=0 ; i < data.length ; i++)   
            {
                var dataString = data[i].string;
                this.versionSearchString = data[i].subString;

                if (dataString.indexOf(data[i].subString) != -1)
                {
                    return data[i].identity;
                }
            }
        },

        searchVersion: function (dataString) 
        {
            var index = dataString.indexOf(this.versionSearchString);
            if (index == -1) return;
            return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
        },

        dataBrowser: 
        [
            { string: navigator.userAgent, subString: "Chrome",  identity: "Chrome" },
            { string: navigator.userAgent, subString: "MSIE",    identity: "Explorer" },
            { string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
            { string: navigator.userAgent, subString: "Safari",  identity: "Safari" },
            { string: navigator.userAgent, subString: "Opera",   identity: "Opera" }
        ]

    };
    BrowserDetect.init();
    
    if((BrowserDetect.browser == 'Explorer') && (BrowserDetect.version == 8)) {
        return true;
    }
    return false;
}



var processCancel = function(self){
	var $showcancel = self._current().data('showcancel');
	if ($showcancel==true){
		$('#wizardCancel').removeClass('hidden');
	}
	else {
		$('#wizardCancel').addClass('hidden');
	}
}

var processNoNext = function(self){
	var $nonext = self._current().data('nonext');
	var $multistep = self._current().data('multistep') && (self.currentMultiStep > 0);
	var $nextBtn = $('.wizardNext', self.element);
    if (($nonext==true) || ($multistep==true)){
	   	$.each($nextBtn,function() {
	       	$(this).hide();
	    });
    } else {
      	$.each($nextBtn,function() {
          	$(this).show();
        });
    }
}

var processNoPrev = function(self){
	if(self.current == 0) {
		var $noprev = self._current().data('noprev');
		var $prevBtn = $('.wizardBack', self.element);
		if ($noprev==true){
		   	$.each($prevBtn,function() {
		       	$(this).hide();
		    });
	    }
	}
	if((self.current > 0) && (self.current < self.steps.length-1)) {
		var $noprev = self._current().data('noprev');
		var $prevBtn = $('.wizardBack', self.element);
		if ($noprev==true){
		   	$.each($prevBtn,function() {
		       	$(this).hide();
		    });
	    } else {
	      	$.each($prevBtn,function() {
	          	$(this).show();
	        });
	    }
	}
}

$(document).ready(function(){
	var createWizard = function($element) {
		$element.wizard({
			labels: {
				prev: 'Anterior',
				next: 'Siguiente',
				confirm: 'Confirmar',
				done: 'Cerrar'
			}
		});
	};
	
	$(document).on('capgemini.ajax.load', function(event, element){
		createWizard($(element).find('.wizard'));
	});
	
	createWizard($('.wizard'));
});

function initStep2Transfers($context){
	$('.init-step2', $context).each(function(){
		setTimeout(function() {
			$('.wizard').wizard('step2');
		}, 100);
	});		
}

function initStep3Transfers($context){
	$('.init-step3', $context).each(function(){
		setTimeout(function() {
			$('.wizard').wizard('step3');
		}, 100);
	});		
}

function transicion_barra_progreso(pasoNuevo,retraso){
	if(pasoNuevo==2 || pasoNuevo==3){
		$('#barraProgreso').removeClass('init-step'+(pasoNuevo-1));
		$('#barraProgreso').addClass('init-step'+pasoNuevo);
		$('.init-step'+pasoNuevo).each(function(){
			setTimeout(function() {
				$('.wizard').wizard('next');
			}, retraso);
		});
	}
}

$(function(){
	
	
	
	initStep2Transfers($(document));
	initStep3Transfers($(document));
	
	
});

